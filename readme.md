# Web app

## Installation and usage

The interface has been developed with the Angular framework, so you need to have Node.js installed on your machine in order to run the web application.
Once you have Node.js installed you need to install the Angular CLI. Open a command line and run:

```bash
npm install -g @angular/cli
```

### Configuration

Open the /src/assets/config/config.json file and update the configuration in order to match yours.

### Run the web application

Using the Angular CLI:

```bash
ng serve
```

Using the npm script (which use the Angular CLI):

```bash
npm run start
```

### Using with rendertron

Add 127.0.0.1 data.grandlyon.docker rendertron.data.grandlyon.docker to your host
Run the container with 

```bash
docker-compose -f docker-compose-developement.rendertron.yml up -d 
```

Start the web application with npm. You can no acces to both app and SSR version through the 2 domains aforesaid.

### Build the application

We defined in the package.json two scripts that can build the application. One that generates an optimized build and the other an unoptimized build. The optimized build takes longer than the normal one.
Those two scripts contain an option --max_old_space_size=\<nb of max RAM octets the node process can take\>. Make sure the number does not exceed your RAM capacity (you can simply remove the option, it will use the default value: 512mb on 32-bit systems and 1gb on 64-bit systems). By incrising the node process memory limit we decrease the build time.

For development environment (not optimized)

```bash
npm run build-i18n:dev
```

For production environment (optimized)

```bash
npm run build-i18n:prod
```

### Build and deploy with Docker

The related files are:

* docker-compose.yml which indicates what Dockerfile should be used, what image is going to be build, volumes, exposed port...
* Dockerfile which describe the steps to build the docker image.

Some environment variables need to be set before building or running the image. Use the appropriate command to set those variables: SET on Windows, export on Linux.

```bash
export TAG=<version>
export WEB_APP_BIND_PORT=<port the application should be running on>
export MATOMO_SITE_ID=<the id of your site in matomo>
export MATOMO_SERVER_URL=<the of your matomo running instance>
```

Then to build the image run:

```bash
docker-compose build --build-arg conf=prod web-app #you can change prod to dev if you don't want an optimized build
```

Once the image is built on your machine, run the following command to start a container:

```bash
docker-compose --project-name web-app up
```
