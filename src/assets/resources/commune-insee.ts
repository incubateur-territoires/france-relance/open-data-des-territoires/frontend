export const communeInsee = [{
  commune: 'Charly',
  insee: '69046'
  ,
}, {
  commune: 'Vaulx-en-Velin',
  insee: '69256'
  ,
}, {
  commune: 'Saint-Priest',
  insee: '69290'
  ,
}, {
  commune: 'Solaize',
  insee: '69296'
  ,
}, {
  commune: 'Saint-Fons',
  insee: '69199'
  ,
}, {
  commune: 'Mions',
  insee: '69283'
  ,
}, {
  commune: 'Sathonay-Camp',
  insee: '69292'
  ,
}, {
  commune: 'Feyzin',
  insee: '69276'
  ,
}, {
  commune: 'Oullins',
  insee: '69149'
  ,
}, {
  commune: 'Meyzieu',
  insee: '69282'
  ,
}, {
  commune: 'Champagne-au-Mont-d\'Or',
  insee: '69040'
  ,
}, {
  commune: '\u00c9cully',
  insee: '69081'
  ,
}, {
  commune: 'Saint-Romain-au-Mont-d\'Or',
  insee: '69233'
  ,
}, {
  commune: 'Saint-Cyr-au-Mont-d\'Or',
  insee: '69191'
  ,
}, {
  commune: 'Collonges-au-Mont-d\'Or',
  insee: '69063'
  ,
}, {
  commune: 'Craponne',
  insee: '69069'
  ,
}, {
  commune: 'Poleymieux-au-Mont-d\'Or',
  insee: '69153'
  ,
}, {
  commune: 'Limonest',
  insee: '69116'
  ,
}, {
  commune: 'Saint-Genis-les-Olli\u00e8res',
  insee: '69205'
  ,
}, {
  commune: 'Saint-Didier-au-Mont-d\'Or',
  insee: '69194'
  ,
}, {
  commune: 'Dardilly',
  insee: '69072'
  ,
}, {
  commune: 'Francheville',
  insee: '69089'
  ,
}, {
  commune: 'Sainte-Foy-l\u00e8s-Lyon',
  insee: '69202'
  ,
}, {
  commune: 'V\u00e9nissieux',
  insee: '69259'
  ,
}, {
  commune: 'Fontaines-sur-Sa\u00f4ne',
  insee: '69088'
  ,
}, {
  commune: 'Irigny',
  insee: '69100'
  ,
}, {
  commune: 'Saint-Genis-Laval',
  insee: '69204'
  ,
}, {
  commune: 'Lyon',
  insee: '69123'
  ,
}, {
  commune: 'Sathonay-Village',
  insee: '69293'
  ,
}, {
  commune: 'Givors',
  insee: '69091'
  ,
}, {
  commune: 'Grigny',
  insee: '69096'
  ,
}, {
  commune: 'Lissieu',
  insee: '69117'
  ,
}, {
  commune: 'Chassieu',
  insee: '69271'
  ,
}, {
  commune: 'Marcy-l\'\u00c9toile',
  insee: '69127'
  ,
}, {
  commune: 'D\u00e9cines-Charpieu',
  insee: '69275'
  ,
}, {
  commune: 'Genay',
  insee: '69278'
  ,
}, {
  commune: 'Villeurbanne',
  insee: '69266'
  ,
}, {
  commune: 'Marennes',
  insee: '69281'
  ,
}, {
  commune: 'Brignais',
  insee: '69027'
  ,
}, {
  commune: 'Bron',
  insee: '69029'
  ,
}, {
  commune: 'Quincieux',
  insee: '69163'
  ,
}, {
  commune: 'Rillieux-la-Pape',
  insee: '69286'
  ,
}, {
  commune: 'Chaponnay',
  insee: '69270'
  ,
}, {
  commune: 'Genas',
  insee: '69277'
  ,
}, {
  commune: 'Chaponost',
  insee: '69043'
  ,
}, {
  commune: 'Corbas',
  insee: '69273'
  ,
}, {
  commune: 'Pierre-B\u00e9nite',
  insee: '69152'
  ,
}, {
  commune: 'Saint-Bonnet-de-Mure',
  insee: '69287'
  ,
}, {
  commune: 'Saint-Symphorien-d\'Ozon',
  insee: '69291'
  ,
}, {
  commune: 'Saint-Laurent-de-Mure',
  insee: '69288'
  ,
}, {
  commune: 'Vourles',
  insee: '69268'
  ,
}, {
  commune: 'Millery',
  insee: '69133'
  ,
}, {
  commune: 'Dommartin',
  insee: '69076'
  ,
}, {
  commune: 'Couzon-au-Mont-d\'Or',
  insee: '69068'
  ,
}, {
  commune: 'Communay',
  insee: '69272'
  ,
}, {
  commune: 'Lentilly',
  insee: '69112'
  ,
}, {
  commune: 'Ternay',
  insee: '69297'
  ,
}, {
  commune: 'Marcilly-d\'Azergues',
  insee: '69125'
  ,
}, {
  commune: 'Pollionnay',
  insee: '69154'
  ,
}, {
  commune: 'Soucieu-en-Jarrest',
  insee: '69176'
  ,
}, {
  commune: 'Orli\u00e9nas',
  insee: '69148'
  ,
}, {
  commune: 'Vaugneray',
  insee: '69255'
  ,
}, {
  commune: 'Montagny',
  insee: '69136'
  ,
}, {
  commune: 'Albigny-sur-Sa\u00f4ne',
  insee: '69003'
  ,
}, {
  commune: 'La Tour-de-Salvagny',
  insee: '69250'
  ,
}, {
  commune: 'Tassin-la-Demi-Lune',
  insee: '69244'
  ,
}, {
  commune: 'Charbonni\u00e8res-les-Bains',
  insee: '69044'
  ,
}, {
  commune: 'Fleurieu-sur-Sa\u00f4ne',
  insee: '69085'
  ,
}, {
  commune: 'Bourg-Saint-Christophe',
  insee: '01054'
  ,
}, {
  commune: 'La Mulati\u00e8re',
  insee: '69142'
  ,
}, {
  commune: 'Neuville-sur-Sa\u00f4ne',
  insee: '69143'
  ,
}, {
  commune: 'Curis-au-Mont-d\'Or',
  insee: '69071'
  ,
}, {
  commune: 'Rochetaill\u00e9e-sur-Sa\u00f4ne',
  insee: '69168'
  ,
}, {
  commune: 'Montanay',
  insee: '69284'
  ,
}, {
  commune: 'Ars-sur-Formans',
  insee: '01021'
  ,
}, {
  commune: 'Frans',
  insee: '01166'
  ,
}, {
  commune: 'Jassans-Riottier',
  insee: '01194'
  ,
}, {
  commune: 'Caluire-et-Cuire',
  insee: '69034'
  ,
}, {
  commune: 'Fontaines-Saint-Martin',
  insee: '69087'
  ,
}, {
  commune: 'Jonage',
  insee: '69279'
  ,
}, {
  commune: 'Cailloux-sur-Fontaines',
  insee: '69033'
  ,
}, {
  commune: 'Vernaison',
  insee: '69260'
  ,
}, {
  commune: 'Saint-Germain-au-Mont-d\'Or',
  insee: '69207'
  ,
}, {
  commune: 'Pusignan',
  insee: '69285'
  ,
}, {
  commune: 'Saint-Pierre-de-Chandieu',
  insee: '69289'
  ,
}, {
  commune: 'Colombier-Saugnieu',
  insee: '69299'
  ,
}, {
  commune: 'S\u00e9r\u00e9zin-du-Rh\u00f4ne',
  insee: '69294'
  ,
}, {
  commune: 'Sainte-Consorce',
  insee: '69190'
  ,
}, {
  commune: 'Simandres',
  insee: '69295'
  ,
}, {
  commune: 'Chasselay',
  insee: '69049'
  ,
}, {
  commune: 'Gr\u00e9zieu-la-Varenne',
  insee: '69094'
  ,
}, {
  commune: 'Ranc\u00e9',
  insee: '01318'
  ,
}, {
  commune: 'Reyrieux',
  insee: '01322'
  ,
}, {
  commune: 'Dagneux',
  insee: '01142'
  ,
}, {
  commune: 'Tramoyes',
  insee: '01424'
  ,
}, {
  commune: 'Mionnay',
  insee: '01248'
  ,
}, {
  commune: 'Bressolles',
  insee: '01062'
  ,
}, {
  commune: 'Parcieux',
  insee: '01285'
  ,
}, {
  commune: 'Lozanne',
  insee: '69121'
  ,
}, {
  commune: 'Montluel',
  insee: '01262'
  ,
}, {
  commune: 'Amb\u00e9rieux',
  insee: '69005'
  ,
}, {
  commune: 'Chasse-sur-Rh\u00f4ne',
  insee: '38087'
  ,
}, {
  commune: 'Messimy',
  insee: '69131'
  ,
}, {
  commune: 'Rontalon',
  insee: '69170'
  ,
}, {
  commune: 'Taluyers',
  insee: '69241'
  ,
}, {
  commune: 'Saint-Laurent-d\'Agny',
  insee: '69219'
  ,
}, {
  commune: 'Chaussan',
  insee: '69051'
  ,
}, {
  commune: 'Pizay',
  insee: '01297'
  ,
}, {
  commune: 'Sainte-Croix',
  insee: '01342'
  ,
}, {
  commune: 'Saint-Sorlin',
  insee: '69237'
  ,
}, {
  commune: 'Le Montellier',
  insee: '01260'
  ,
}, {
  commune: 'Faramans',
  insee: '01156'
  ,
}, {
  commune: 'Saint-Pierre-la-Palud',
  insee: '69231'
  ,
}, {
  commune: 'Beauvallon',
  insee: '69179'
  ,
}, {
  commune: 'Mornant',
  insee: '69141'
  ,
}, {
  commune: 'Joyeux',
  insee: '01198'
  ,
}, {
  commune: '\u00c9veux',
  insee: '69083'
  ,
}, {
  commune: 'Birieux',
  insee: '01045'
  ,
}, {
  commune: 'Saint-Marcel',
  insee: '01371'
  ,
}, {
  commune: 'Fleurieux-sur-l\'Arbresle',
  insee: '69086'
  ,
}, {
  commune: 'Monthieux',
  insee: '01261'
  ,
}, {
  commune: 'Saint-Andr\u00e9-de-Corcy',
  insee: '01333'
  ,
}, {
  commune: 'L\'Arbresle',
  insee: '69010'
  ,
}, {
  commune: 'Saint-Germain-Nuelles',
  insee: '69208'
  ,
}, {
  commune: 'Saint-Jean-de-Thurigneux',
  insee: '01362'
  ,
}, {
  commune: 'Lachassagne',
  insee: '69106'
  ,
}, {
  commune: 'Massieux',
  insee: '01238'
  ,
}, {
  commune: 'Jons',
  insee: '69280'
  ,
}, {
  commune: 'Ni\u00e9vroz',
  insee: '01276'
  ,
}, {
  commune: 'Belmont',
  insee: '69020'
  ,
}, {
  commune: 'Toussieux',
  insee: '01423'
  ,
}, {
  commune: 'Tr\u00e9voux',
  insee: '01427'
  ,
}, {
  commune: 'Saint-Didier-de-Formans',
  insee: '01347'
  ,
}, {
  commune: 'Saint-Bernard',
  insee: '01339'
  ,
}, {
  commune: 'Saint-Jean-des-Vignes',
  insee: '69212'
  ,
}, {
  commune: 'Les Ch\u00e8res',
  insee: '69055'
  ,
}, {
  commune: 'Sainte-Euph\u00e9mie',
  insee: '01353'
  ,
}, {
  commune: 'Mis\u00e9rieux',
  insee: '01250'
  ,
}, {
  commune: 'Beauregard',
  insee: '01030'
  ,
}, {
  commune: 'Ch\u00e2tillon',
  insee: '69050'
  ,
}, {
  commune: 'Chessy',
  insee: '69056'
  ,
}, {
  commune: 'Civrieux-d\'Azergues',
  insee: '69059'
  ,
}, {
  commune: 'Brindas',
  insee: '69028'
  ,
}, {
  commune: 'Heyrieux',
  insee: '38189'
  ,
}, {
  commune: 'Villette-d\'Anthon',
  insee: '38557'
  ,
}, {
  commune: 'Luzinay',
  insee: '38215'
  ,
}, {
  commune: 'Saint-Just-Chaleyssin',
  insee: '38408'
  ,
}, {
  commune: 'Grenay',
  insee: '38184'
  ,
}, {
  commune: 'Villette-de-Vienne',
  insee: '38558'
  ,
}, {
  commune: 'Saint-Quentin-Fallavier',
  insee: '38449'
  ,
}, {
  commune: 'Valencin',
  insee: '38519'
  ,
}, {
  commune: 'Beynost',
  insee: '01043'
  ,
}, {
  commune: 'Di\u00e9moz',
  insee: '38144'
  ,
}, {
  commune: 'Saint-Maurice-de-Beynost',
  insee: '01376'
  ,
}, {
  commune: 'Bonnefamille',
  insee: '38048'
  ,
}, {
  commune: 'Neyron',
  insee: '01275'
  ,
}, {
  commune: 'Satolas-et-Bonce',
  insee: '38475'
  ,
}, {
  commune: 'Miribel',
  insee: '01249'
  ,
}, {
  commune: 'La Boisse',
  insee: '01049'
  ,
}, {
  commune: 'Civrieux',
  insee: '01105'
  ,
}, {
  commune: 'Balan',
  insee: '01027'
  ,
}, {
  commune: 'Toussieu',
  insee: '69298'
  ,
}, {
  commune: 'Limas',
  insee: '69115'
  ,
}, {
  commune: 'Lucenay',
  insee: '69122'
  ,
}, {
  commune: 'Saint-Romain-en-Gier',
  insee: '69236'
  ,
}, {
  commune: '\u00c9chalas',
  insee: '69080'
  ,
}, {
  commune: 'Loire-sur-Rh\u00f4ne',
  insee: '69118'
  ,
}, {
  commune: 'Sourcieux-les-Mines',
  insee: '69177'
  ,
}, {
  commune: 'Thil',
  insee: '01418'
  ,
}, {
  commune: 'Janneyrias',
  insee: '38197'
  ,
}, {
  commune: 'Chazay-d\'Azergues',
  insee: '69052'
  ,
}, {
  commune: 'Bagnols',
  insee: '69017'
  ,
}, {
  commune: 'Charnay',
  insee: '69047'
  ,
}, {
  commune: 'Moranc\u00e9',
  insee: '69140'
  ,
}, {
  commune: 'Frontenas',
  insee: '69090'
  ,
}, {
  commune: 'Alix',
  insee: '69004'
  ,
}, {
  commune: 'Marcy',
  insee: '69126'
  ,
}, {
  commune: 'Anse',
  insee: '69009'
  ,
}, {
  commune: 'Pommiers',
  insee: '69156'
  ,
}, {
  commune: 'Villefranche-sur-Sa\u00f4ne',
  insee: '69264'
  ,
}, {
  commune: 'Lyon 1er Arrondissement',
  insee: '69381'
  ,
}, {
  commune: 'Gleiz\u00e9',
  insee: '69092'
  ,
}, {
  commune: 'Lacenas',
  insee: '69105'
  ,
}, {
  commune: 'Jarnioux',
  insee: '69101'
  ,
}, {
  commune: 'Porte des Pierres Dor\u00e9es',
  insee: '69159'
  ,
}, {
  commune: 'Lyon 3e Arrondissement',
  insee: '69383'
  ,
}, {
  commune: 'Lyon 4e Arrondissement',
  insee: '69384'
  ,
}, {
  commune: 'Lyon 9e Arrondissement',
  insee: '69389'
  ,
}, {
  commune: 'Lyon 5e Arrondissement',
  insee: '69385'
  ,
}, {
  commune: 'Lyon 2e Arrondissement',
  insee: '69382'
  ,
}, {
  commune: 'Lyon 7e Arrondissement',
  insee: '69387'
  ,
}, {
  commune: 'Lyon 8e Arrondissement',
  insee: '69388'
  ,
}, {
  commune: 'Lyon 6e Arrondissement',
  insee: '69386'
  ,
}];
