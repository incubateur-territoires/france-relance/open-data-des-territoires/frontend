import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

// Read the dynamic configuration of the application from the json file stored in the assets folder
fetch('./assets/config/config.json')
  .then(response => response.json())
  .then((config) => {
    if (!config) {
      return;
    }

    // Store the response so that your ConfigService can read it.
    window['portailDataEnvConfig'] = config;

    platformBrowserDynamic().bootstrapModule(AppModule)
      .catch(err => console.log(err));
  });
