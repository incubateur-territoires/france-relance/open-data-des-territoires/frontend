import { TestBed, waitForAsync } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { MockComponent } from 'ng2-mock-component';
import { RouterTestingModule } from '@angular/router/testing';
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2Piwik } from 'angulartics2/piwik';
import { NavigationHistoryService } from './core/services';

describe('AppComponent', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockComponent({ selector: 'app-header' }),
        MockComponent({ selector: 'app-main' }),
        MockComponent({ selector: 'app-footer' }),
        MockComponent({ selector: 'app-notifications' }),
      ],
      imports: [
        RouterTestingModule,
        Angulartics2Module.forRoot([Angulartics2Piwik]),
      ],
      providers: [
        NavigationHistoryService,
      ],
    }).compileComponents();
  }));
  it('should create the app', waitForAsync(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
