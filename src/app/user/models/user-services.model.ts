export interface IRestrictedAccessDataset {
  datasetId: number;
  datasetName: string;
  geonetUuid: string;
  abstract: string;
  urlPattern: string;
  services: IService[];
}

export interface IService {
  id: number;
  name: string;
  abstract: string;
}

export class Service {
  id: number;
  name: string;
  abstract: string;
  geonetUuid: string;
  selected: boolean;

  constructor(data: IService) {
    this.id = data.id ? data.id : null;
    this.name = data.name ? data.name : null;
    this.abstract = data.abstract ? data.abstract : null;
    this.selected = false;
  }
}

export interface IResource {
  datasetId: number;
  datasetName: string;
  status: string;
  serviceName: string;
  geonetUuid: string;
  urlPattern: string;
  validUntil: string;
}

// DATASET = SERVICE
export class Resource {

  datasetId: number;
  datasetName: string;
  datasetTitle: string;
  datasetSlug: string;
  geonetUuid: string;
  urlPattern: string;
  services: (ServiceAccess | Service)[];

  constructor(data: IResource | IRestrictedAccessDataset | Resource) {
    this.datasetId = data.datasetId ? data.datasetId : null;
    this.datasetName = data.datasetName ? data.datasetName : null;
    this.geonetUuid = data.geonetUuid ? data.geonetUuid : null;
    this.urlPattern = data.urlPattern ? data.urlPattern : null;
    this.services = [];

    if (data['services']) {
      data['services'].forEach((service) => {
        this.services.push(new Service(service));
      });
    }
  }
}

export class ServiceAccess {
  id: number;
  status?: string;
  name: string;
  validUntil?: Date;
  selected: boolean; // Used in the array to select the access to be deleted

  constructor(data: IResource, id: number) {
    this.id = id ? id : null;
    this.name = data.serviceName ? data.serviceName : null;
    this.validUntil = data.validUntil ? new Date(data.validUntil) : null;
    this.status = data.status ? data.status : null;

    if (this.validUntil && this.validUntil.getTime() < Date.now()) {
      this.status = 'expired';
    }
    this.selected = false;
  }
}

export class RenewAccessToResourceResponse {
  successfullyRenewalRequested: string[];
  unsuccessfullyRenewalRequested: string[];
}

export class AccessRequestToResourceResponse {
  successfullyRequested: string[];
  unsuccessfullyRequested: string[];
}

export class DeleteAccessToRessourceResponse {
  successfullyDeleted: string[];
  unsuccessfullyDeleted: string[];
}
