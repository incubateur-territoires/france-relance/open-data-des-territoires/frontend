export class User {
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  is_superuser:boolean;

  //  payload is the decrypted payload of the JWT token
  constructor(payload) {
    if (payload) {
      this.firstName = payload.firstName;
      this.lastName = payload.lastName;
      this.email = payload.email;
      this.username = payload.username;
      this.is_superuser = payload.is_superuser;
    }
  }
}

export interface ILoginResponse {
  token: string;
}

export interface ICreateAccountForm {
  firstName: string;
  lastName: string;
  email: string;
  emailConfirmation: string;
  password: string;
  passwordConfirmation: string;
  acceptMessages: boolean;
  entreprise: string;
  address: string;
  zipcode: string;
  city: string;
  country: string;
  // cgu: boolean;
  consent: boolean;
}

export class LegacyAccount {
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  password: string;
  acceptMessages: boolean;
  entreprise: string;
  address: string;
  zipcode: string;
  city: string;
  country: string;

  constructor(form: ICreateAccountForm) {
    this.firstName = form.firstName;
    this.lastName = form.lastName;
    this.username = form.email;
    this.email = form.email;
    this.password = form.password;
    this.acceptMessages = form.acceptMessages;
    this.entreprise = form.entreprise;
    this.address = form.address;
    this.zipcode = form.zipcode;
    this.city = form.city;
    this.country = form.country;
  }
}

export interface IUserInfo {
  username: string;
  lastName: string;
  firstName: string;
  geosourceId: number;
  entreprise: string;
  address: string;
  city: string;
  country: string;
  zipcode: string;
  acceptMessages: boolean;
  email: string;
  consent: boolean;
}

export class UserInfo {
  username: string;
  lastName: string;
  firstName: string;
  entreprise: string;
  address: string;
  city: string;
  country: string;
  zipcode: string;
  acceptMessages: boolean;
  email: string;

  constructor(form: IUserInfo) {
    this.firstName = form.firstName;
    this.lastName = form.lastName;
    this.username = form.email;
    this.email = form.email;
    this.acceptMessages = form.acceptMessages;
    this.entreprise = form.entreprise;
    this.address = form.address;
    this.zipcode = form.zipcode;
    this.city = form.city;
    this.country = form.country;
  }
}

export class PasswordUpdateForm {
  newPassword: string;
  oldPassword: string;

  constructor(newPassword: string, oldPassword: string) {
    this.newPassword = newPassword;
    this.oldPassword = oldPassword;
  }
}

export interface IPasswordForgottenForm {
  email: string;
}
