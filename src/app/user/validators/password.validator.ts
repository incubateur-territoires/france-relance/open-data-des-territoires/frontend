import { AbstractControl } from '@angular/forms';

const passwordBlackList = ['grandlyon', 'smartdata', 'lyon'];

// tslint:disable-next-line:function-name
export function ValidatePassword(control: AbstractControl) {
  const errors = {};
  let res = null;
  if (!new RegExp(/^[\u0020-\u007e]*$/).test(control.value)) {
    errors['invalidCharacters'] = true;
  }
  if (!new RegExp(/[~`!#$%\$\(@\)\^&*\.+=\-\[\]\\';,/{}|\\":<>\?]/).test(control.value)) {
    errors['missingSpecialCharacters'] = true;
  }
  if (!new RegExp(/[A-Z]/).test(control.value)) {
    errors['missingUppercasedLetter'] = true;
  }
  if (!new RegExp(/[a-z]/).test(control.value)) {
    errors['missingLowercasedLetter'] = true;
  }
  if (!new RegExp(/[0-9]/).test(control.value)) {
    errors['missingNumber'] = true;
  }
  if (new RegExp(passwordBlackList.join('|')).test(control.value)) {
    errors['forbiddenWord'] = true;
  }

  if (errors !== {}) {
    res = errors;
  }

  return res;

}
