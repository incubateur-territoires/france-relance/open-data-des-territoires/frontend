import { Component, OnInit } from '@angular/core';
import { pageTitles } from '../../../../i18n/traductions';
import { NavigationHistoryService } from '../../../core/services';
import { Router } from '@angular/router';
import { AppRoutes } from '../../../routes';
import { IPageHeaderInfo, IBasicTab } from '../../../shared/models';

@Component({
  selector: 'app-user-profil',
  templateUrl: './user-profil.component.html',
  styleUrls: ['./user-profil.component.scss'],
})
export class UserProfilComponent implements OnInit {

  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.userProfil,
  };
  tabs: IBasicTab[] = [
    {
      name: pageTitles.userInfo,
      fullRouterLinkPath: `${AppRoutes.userInfo.uri}`,
    },
    {
      name: pageTitles.userPasswordUpdate,
      fullRouterLinkPath: `${AppRoutes.userPasswordUpdate.uri}`,
    },
  ];
  

  constructor(
    private _navigationHistoryService: NavigationHistoryService,
    private _router: Router,
  ) {
  }

  ngOnInit() {}

  goToPreviousPage() {
    let index = 1; // Start to retrieve the previous element
    let url = this._navigationHistoryService.getFromLast(index);
    // While the previous element still has /datasets or is not null (no previous element)
    // get the previous element of the previous element...
    while (url !== null && url.search(`${AppRoutes.userProfil.uri}/`) !== -1) {
      index += 1;
      url = this._navigationHistoryService.getFromLast(index);
    }

    // If url is null then redirect to research page
    if (url == null) {
      url = `/${AppRoutes.home.uri}`;
    }

    this._router.navigateByUrl(url);
  }
}
