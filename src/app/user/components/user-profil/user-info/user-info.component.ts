import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { notificationMessages } from '../../../../../i18n/traductions';
import { NotificationService } from '../../../../core/services';
import { AppRoutes } from '../../../../routes';
import { LegacyAccount } from '../../../models';
import { UserService } from '../../../services';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss'],
})
export class UserInfoComponent implements OnInit {

  AppRoutes = AppRoutes;
  userInfoUpdateForm: FormGroup;
  userInfoUpdateRequestIsPending = false;
  userAccountDeletionRequestIsPending = false;
  // Modal state
  deleteAccountModalIsOpened = false;

  constructor(
    private _fb: FormBuilder,
    private _userService: UserService,
    private _notificationService: NotificationService,
    private _router: Router,
  ) {
    this.userInfoUpdateForm = this._fb.group({
      email: [],
      firstName: [, [
        Validators.required,
        Validators.pattern('([A-ZÀ-ÖØ-Ý][a-zà-öø-ýÿA-ZÀ-ÖØ-Ý]*)([ \'-][a-zA-ZÀ-ÖØ-öø-ýÿ]*)*'),
      ]],
      lastName: [, [
        Validators.required,
        Validators.pattern('([A-ZÀ-ÖØ-Ý][a-zà-öø-ýÿA-ZÀ-ÖØ-Ý]*)([ \'-][a-zA-ZÀ-ÖØ-öø-ýÿ]*)*'),
      ]],
      acceptMessages: [],
      entreprise: [],
      address: [],
      zipcode: [],
      city: [],
      country: [],
      consent: [false, Validators.requiredTrue],
    });
    this.userInfoUpdateForm.disable();
  }

  ngOnInit() {
    this.initializeUserInfoForm();
  }

  // Getters for form inputs
  get email() { return this.userInfoUpdateForm.get('email'); }
  get firstName() { return this.userInfoUpdateForm.get('firstName'); }
  get lastName() { return this.userInfoUpdateForm.get('lastName'); }
  get acceptMessages() { return this.userInfoUpdateForm.get('acceptMessages'); }
  get entreprise() { return this.userInfoUpdateForm.get('entreprise'); }
  get address() { return this.userInfoUpdateForm.get('address'); }
  get zipcode() { return this.userInfoUpdateForm.get('zipcode'); }
  get city() { return this.userInfoUpdateForm.get('city'); }
  get contry() { return this.userInfoUpdateForm.get('contry'); }
  get consent() { return this.userInfoUpdateForm.get('consent'); }

  get userInfoUpdateFormDisabled(): boolean {
    return this.userInfoUpdateForm.disabled;
  }

  get userInfoUpdateFormIsInvalid() {
    return this.userInfoUpdateForm.invalid;
  }

  // Functions concerning the profil information update
  initializeUserInfoForm() {
    this._userService.getUserInfo().subscribe(
      (info) => {
        this.userInfoUpdateForm.reset({
          email: info.email,
          firstName: info.firstName,
          lastName: info.lastName,
          acceptMessages: info.acceptMessages,
          entreprise: info.entreprise,
          address: info.address,
          zipcode: info.zipcode,
          city: info.city,
          country: info.country,
        });
        this.userInfoUpdateForm.enable();
      },
      (err) => {
        this._notificationService.notify(
          {
            type: 'error',
            message: notificationMessages.userInfo.failedToLoadUserInfo,
          },
        );
        this.handle401(err);
      },
    );
  }

  // Validity for the form field depends on dirty because the fields prefilled so it makes more sense
  // to display errors or validity only when the field has been changed
  fieldIsValid(form: FormGroup, field: string) {
    return (form.get(field).dirty && form.get(field).touched) && form.get(field).valid;
  }

  fieldIsInvalid(form: FormGroup, field: string) {
    return (form.get(field).dirty && form.get(field).touched) && form.get(field).invalid;
  }

  toUppercase(form: FormGroup, controlName: string) {
    const input = form.get(controlName).value;
    const uppercased = input.substring(0, 1).toUpperCase() + input.substring(1);
    form.get(controlName).patchValue(uppercased);
  }

  updateUserInfo() {
    this.userInfoUpdateForm.disable();
    this.userInfoUpdateRequestIsPending = true; // Display loader
    const account = new LegacyAccount(this.userInfoUpdateForm.value);
    this._userService.updateUserInfo(this.userInfoUpdateForm.getRawValue()).pipe(
      // To do both on success and error when the observable
      finalize(() => {
        this.userInfoUpdateRequestIsPending = false;
        this.userInfoUpdateForm.enable();
      }),
    ).subscribe(
      (res) => {
        this._notificationService.notify(
          {
            type: 'success',
            message: notificationMessages.userInfo.userInfoUpdated,
          },
        );
        // Set the session before any other call in order to update the cookie and the xsrfToken header
        this._userService.setSession(res);
        this.initializeUserInfoForm();
      },
      (err) => {
        this._notificationService.notify(
          {
            type: 'error',
            message: notificationMessages.userInfo.failedToUpdateUserInfo,
          },
        );
        this.handle401(err);
      },
    );
  }

  // Functions concerning the account deletion

  openDeleteAccountModal() {
    this.deleteAccountModalIsOpened = true;
  }

  closeDeleteAccountModal() {
    this.deleteAccountModalIsOpened = false;
  }

  deleteAccount() {
    this.userAccountDeletionRequestIsPending = true;
    this.closeDeleteAccountModal();
    this._userService.deleteAccount().subscribe(
      () => {
        this.userAccountDeletionRequestIsPending = false;
        this._userService.resetAuth();
        this._router.navigate(['/', AppRoutes.signin.uri]);
      },
      (err) => {
        this.userAccountDeletionRequestIsPending = false;
        this.closeDeleteAccountModal();
        this._notificationService.notify(
          {
            type: 'error',
            message: notificationMessages.userInfo.errorDeletingAccount,
          },
        );
        this.handle401(err);
      },
    );
  }

  // If the error received is an authentication error then redirect to the login page
  // because only authenticated users are supposed to access this component
  handle401(err) {
    if (err && err.status === 401) {
      this._router.navigate(['/', AppRoutes.signin.uri]);
    }
  }

}
