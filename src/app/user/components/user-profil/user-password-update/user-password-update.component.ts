import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { ValidatePassword } from '../../../validators/password.validator';
import { notificationMessages } from '../../../../../i18n/traductions';
import { UserService } from '../../../services';
import { NotificationService } from '../../../../core/services';
import { Router } from '@angular/router';
import { PasswordUpdateForm } from '../../../models';
import { finalize } from 'rxjs/operators';
import { AppRoutes } from '../../../../routes';

@Component({
  selector: 'app-user-password-update',
  templateUrl: './user-password-update.component.html',
  styleUrls: ['./user-password-update.component.scss'],
})
export class UserPasswordUpdateComponent implements OnInit {

  passwordUpdateForm: FormGroup;
  passwordUpdateRequestIsPending = false;

  constructor(
    private _fb: FormBuilder,
    private _userService: UserService,
    private _notificationService: NotificationService,
    private _router: Router,
  ) {
    this.passwordUpdateForm = this._fb.group({
      newPassword: ['', [
        Validators.required,
        Validators.minLength(environment.passwordMinLength),
        ValidatePassword,
      ]],
      newPasswordConfirmation: ['', [Validators.required]],
      oldPassword: ['', [Validators.required]],
    });
  }

  ngOnInit() {
  }

  // Getters for form inputs
  get newPassword(): AbstractControl { return this.passwordUpdateForm.get('newPassword'); }
  get newPasswordConfirmation(): AbstractControl { return this.passwordUpdateForm.get('newPasswordConfirmation'); }
  get oldPassword(): AbstractControl { return this.passwordUpdateForm.get('oldPassword'); }
  get passwordMinLength(): number {
    return environment.passwordMinLength;
  }
  get newPasswordConfirmationIsCorrect(): boolean {
    const value = this.newPassword.value === this.newPasswordConfirmation.value ? true : false;
    return value;
  }
  get newPasswordConfirmationError() {
    // Display the message if
    // Input values have been modified
    // Input values have been modified but the email values are not similar
    return this.newPassword.touched && this.newPasswordConfirmation.touched && !this.newPasswordConfirmationIsCorrect;
  }
  get passwordUpdateFormDisabled(): boolean {
    return this.passwordUpdateForm.disabled;
  }

  // Return true if one of the fields at least doesn't respect its validators
  // or if the confirmation email is different from the email
  // or if the confirmation password is different from the password
  get passwordUpdateFormIsInvalid() {
    const value = this.passwordUpdateForm.invalid || this.newPasswordConfirmationError ||
                  !this.newPassword.touched || !this.newPasswordConfirmation.touched;
    return value;
  }

  // Validity for the password field depends on touched because the fields are not prefilled
  passwordFormFieldIsValid(field: string) {
    return (this.passwordUpdateForm.get(field).touched) && this.passwordUpdateForm.get(field).valid;
  }

  passwordFormFieldIsInvalid(field: string) {
    return (this.passwordUpdateForm.get(field).touched) && this.passwordUpdateForm.get(field).invalid;
  }

  updatePassword() {
    if (!this.passwordUpdateFormIsInvalid && !this.passwordUpdateFormDisabled) {
      // Formatting the object to be send to the server
      const form = new PasswordUpdateForm(this.newPassword.value, this.oldPassword.value);

      this.passwordUpdateForm.disable();
      this.passwordUpdateRequestIsPending = true; // Display loader

      this._userService.updateUserPassword(form).pipe(
        // To do both on success and error when the observable
        finalize(() => {
          this.passwordUpdateRequestIsPending = false;
          this.passwordUpdateForm.enable();
        }),
      ).subscribe(
        (res) => {
          this._notificationService.notify(
            {
              type: 'success',
              message: notificationMessages.userInfo.userPasswordUpdated,
            },
          );
          // If password updated with success
          // Relog the user with the new credentials
          const loginForm = { username: this._userService.user.username, password: this.newPassword.value };
          this._userService.login(loginForm).subscribe(
            // If loggedin with success reset the form
            // Else redirect the user to the login page
            (res) => {
              this.passwordUpdateForm.reset();
            },
            (err) => {
              this._userService.resetAuth();
              this._router.navigate(['/', AppRoutes.signin.uri]);
            },
          );
        },
        (err) => {
          let message = notificationMessages.userInfo.failedToUpdateUserPassword;
          if (err.error.message = 'unidentifiedUser') {
            message = notificationMessages.userInfo.wrongOldPassword;
            this.oldPassword.setErrors({ wrongPassword: true });
          }
          this._notificationService.notify(
            {
              message,
              type: 'error',
            },
          );
          this.handle401(err);
        },
      );
    }
  }

  // If the error received is an authentication error then redirect to the login page
  // because only authenticated users are supposed to access this component
  handle401(err) {
    if (err && err.status === 401) {
      this._router.navigate(['/', AppRoutes.signin.uri]); 
    }
  }
}
