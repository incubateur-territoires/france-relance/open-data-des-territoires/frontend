import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { notificationMessages, userServicesStatuses } from '../../../../../i18n/traductions';
import { Notification } from '../../../../core/models';
import { NotificationService } from '../../../../core/services';
import { AppRoutes } from '../../../../routes';
import { IResource, IService, Resource, ServiceAccess } from '../../../models';
import { UserServicesService } from '../../../services/user-services.service';

@Component({
  selector: 'app-user-services',
  templateUrl: './user-services.component.html',
  styleUrls: ['./user-services.component.scss'],
})
export class UserServicesComponent implements OnInit {

  private _pendingRequests: number = 0;
  isRemovingAccess = false;
  isRenewingAccess = false;
  AppRoutes = AppRoutes;
  userResources: Resource[] = [];
  services: IService[] = [];
  statusesTrad = userServicesStatuses;
  slugsUuids = {};

  // For the user services
  userResourcesOpenedRow: number[] = [];

  constructor(
    private _userAccessService: UserServicesService,
    private _notificationService: NotificationService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this._pendingRequests += 1;
    forkJoin([
      // Get the list of the requested services by the user
      this._userAccessService.getUserResources(),
      // Get the list of the different modes (wms, ws...)
      this._userAccessService.getServices(),
    ]).pipe(
      mergeMap((results) => {
        this.services = results[1];

        const resources = results[0].filter((e) => {
          return (e.geonetUuid && e.geonetUuid !== '');
        });

        const uuids = resources.map((e) => { return e.geonetUuid; });

        return this._userAccessService.getSlugsFromUuid(uuids).pipe(
          map((response) => {
            this.slugsUuids = response;
            this.initUserResources(resources);
          }),
        );
      }),
    ).subscribe(
      () => { },
      (err) => {
        this._notificationService.notify(new Notification({
          type: 'error',
          message: `${notificationMessages.userServices.initializationError}`,
        }));
        this.handle401(err);
      },
      () => {
        this._pendingRequests -= 1;
      },
    );
  }

  initUserResources(services: IResource[]) {
    const grouped = this.groupBy(services, 'datasetId');
    const formated = [];

    this.userResourcesOpenedRow = [];

    for (const key in grouped) {
      if (grouped.hasOwnProperty(key)) {
        const resource = new Resource(grouped[key][0]);
        if (this.slugsUuids[resource.geonetUuid]) {
          resource.datasetSlug = this.slugsUuids[resource.geonetUuid]['slug'];
          resource.datasetTitle = this.slugsUuids[resource.geonetUuid]['title'];
        }
        grouped[key].forEach((element: IResource) => {
          const service = this.services.find(m => m.name === element.serviceName);
          const id = service ? service.id : null;
          resource.services.push(new ServiceAccess(element, id));
        });

        formated.push(resource);
      }
    }

    this.userResources = formated;
  }

  // If al the services status of a ressource are the same, the function returns the status, else return 'other'
  getResourceStatus(resource: Resource) {
    const servicesStatus: ServiceAccess[] = resource.services.map(mapObj => mapObj['status']);
    const filtered = servicesStatus.filter((v, i) => servicesStatus.indexOf(v) === i);
    return filtered && filtered.length === 1 ? filtered[0] : 'other';
  }

  // If the all the services have the same validity date then returns it, else return null
  getResourceValidityDate(resource) {
    // tslint:disable-next-line:max-line-length
    const servicesValidityDate: ServiceAccess[] = resource.services.map(
      mapObj => mapObj['validUntil'] ? mapObj['validUntil'].toString() : null,
    );
    const filtered = servicesValidityDate.filter((v, i) => servicesValidityDate.indexOf(v) === i);
    return filtered && filtered.length === 1 ? filtered[0] : null;
  }

  /** Helpers function for the state of the ressources table **/
  // Returns 1 if number is in array, -1 else
  isRowOpened(id: number, array: number[]): boolean {
    return array.includes(id);
  }

  toggleRow(id: number, array: number[]) {
    if (this.isRowOpened(id, array)) {
      this.removeRow(id, array);
    } else {
      this.addRow(id, array);
    }
  }

  addRow(id: number, array: number[]) {
    array.push(id);
  }

  removeRow(id, array: number[]) {
    const index = array.findIndex(row => row === id);
    if (index !== -1) {
      array.splice(index, 1);
    }
  }

  toggleResource(elementId, array: Resource[]) {
    const resource = array.find(e => e.datasetId === elementId);
    if (resource) {
      let bool = false;
      const unselectedMode = resource.services.find(e => !e.selected);

      if (unselectedMode) {
        bool = true;
      }

      resource.services.forEach((mode) => {
        mode.selected = bool;
      });
    }
  }

  allServicesAreSelected(datasetId: number, array: Resource[]) {
    let res = true;

    const resource = array.find(e => e.datasetId === datasetId);
    if (resource) {
      const unselectedMode = resource.services.find(e => !e.selected);

      if (unselectedMode) {
        res = false;
      }
    }

    return res;
  }

  isUndeterminated(datasetId: number, array: Resource[]) {
    let res = false;

    const resource = array.find(e => e.datasetId === datasetId);
    if (resource) {
      const unselectedService = resource.services.find(e => !e.selected);
      const selectedService = resource.services.find(e => e.selected);

      if (unselectedService && selectedService) {
        res = true;
      }
    }

    return res;
  }

  selectedResources(array: Resource[]) {
    return array.filter((e) => {
      return e.services.findIndex(m => m.selected) !== -1 ? true : false;
    }).map((e) => {
      const servicesArray = e.services.filter(m => m.selected);
      const r = new Resource(e);
      r.services = servicesArray;
      return r;
    });
  }

  resetUserRessourcesState() {
    this.userResources.forEach(userResource =>
      userResource.services.forEach(service =>
        service.selected = false,
      ),
    );
  }

  private groupBy(array: any, property: string) {
    return array.reduce(
      (groups: any, item: any) => {
        const val = item[property];
        groups[val] = groups[val] || [];
        groups[val].push(item);
        return groups;
      },
      {},
    );
  }

  get isLoading() {
    return this._pendingRequests > 0 ? true : false;
  }

  // Functions perfoming actions on user access
  removeAccess() {
    const accessRequests = [];
    // Building an array of request (one object per dataset containing the id of the dataset
    // and the list of services ID)
    this.selectedResources(this.userResources).forEach((e) => {
      accessRequests.push({ id: e.datasetId, servicesId: e.services.map(e => e.id) });
    });

    this._pendingRequests += 1;
    this.isRemovingAccess = true;

    this._userAccessService.deleteAccessToResource(accessRequests).subscribe(
      (res) => {
        // Displaying notification for each sucessful and unsuccessful request
        res.successfullyDeleted.forEach((message: string) => {
          this._notificationService.notify(new Notification({
            type: 'success',
            message: `${notificationMessages.userServices.removeSuccess} ${message}`,
          }));
        });

        res.unsuccessfullyDeleted.forEach((message: string) => {
          this._notificationService.notify(new Notification({
            type: 'success',
            message: `${notificationMessages.userServices.removeFailedDetailed} ${message}`,
          }));
        });

        // Update the list of the user services
        this._userAccessService.getUserResources().subscribe(
          (res) => {
            this.initUserResources(res);
          },
          (err) => {
            this._notificationService.notify(new Notification({
              type: 'error',
              message: `${notificationMessages.userServices.failedToLoadUserResources}`,
            }));
            this.handle401(err);
          },
          () => {
            this._pendingRequests -= 1;
            this.isRemovingAccess = false;
          },
        );
      },
      (err) => {
        this._pendingRequests -= 1;
        this._notificationService.notify(new Notification({
          type: 'error',
          message: `${notificationMessages.userServices.removeFailed}`,
        }));
        this.handle401(err);
      },
    );
  }

  renewAccesses() {
    const accessRequests = this.selectedResources(this.userResources).map(
      (selectedResource) => {
        return {
          id: selectedResource.datasetId,
          servicesId: selectedResource.services.map(s => s.id),
        };
      },
    );

    this._pendingRequests += 1;
    this.isRenewingAccess = true;

    this._userAccessService.renewAccessToResource(accessRequests).subscribe(
      (res) => {
        this.resetUserRessourcesState();
        // Displaying notification for each sucessful and unsuccessful request
        res.successfullyRenewalRequested.forEach((message: string) => {
          this._notificationService.notify(new Notification({
            type: 'success',
            message: `${notificationMessages.userServices.renewSuccess} ${message}`,
          }));
        });

        res.unsuccessfullyRenewalRequested.forEach((message: string) => {
          this._notificationService.notify(new Notification({
            type: 'error',
            message: `${notificationMessages.userServices.renewFailedDetailed.partOne}
                      ${message} ${notificationMessages.userServices.renewFailedDetailed.partTwo}`,
          }));
        });
      },
      (err) => {
        this._notificationService.notify(new Notification({
          type: 'error',
          message: `${notificationMessages.userServices.renewFailed}`,
        }));
        this.handle401(err);
      },
      () => {
        this._pendingRequests -= 1;
        this.isRenewingAccess = false;
      },
    );
  }

  // If the error received is an authentication error then redirect to the login page
  // because only authenticated users are supposed to access this component
  handle401(err) {
    if (err && err.status === 401) {
      this._router.navigate(['/', AppRoutes.signin.uri]);
    }
  }
}
