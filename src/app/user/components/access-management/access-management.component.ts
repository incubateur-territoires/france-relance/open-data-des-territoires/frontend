import { Component, OnInit } from '@angular/core';
import { IPageHeaderInfo, IBasicTab } from '../../../shared/models';
import { pageTitles } from '../../../../i18n/traductions';
import { AppRoutes } from '../../../routes';
import { NavigationHistoryService } from '../../../core/services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-access-management',
  templateUrl: './access-management.component.html',
  styleUrls: ['./access-management.component.scss'],
})
export class AccessManagementComponent implements OnInit {

  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.accessManagement,
  };
  tabs: IBasicTab[] = [
    {
      name: pageTitles.userServices,
      fullRouterLinkPath: `${AppRoutes.userServices.uri}`,
    },
    {
      name: pageTitles.availableData,
      fullRouterLinkPath: `${AppRoutes.availableData.uri}`,
    },
  ];

  constructor(
    private _navigationHistoryService: NavigationHistoryService,
    private _router: Router,
  ) { }

  ngOnInit() {
  }

  goToPreviousPage() {
    let index = 1; // Start to retrieve the previous element
    let url = this._navigationHistoryService.getFromLast(index);
    // While the previous element still has /datasets or is not null (no previous element)
    // get the previous element of the previous element...
    while (url !== null && url.search(`${AppRoutes.accessManagement.uri}/`) !== -1) {
      index += 1;
      url = this._navigationHistoryService.getFromLast(index);
    }

    // If url is null then redirect to research page
    if (url == null) {
      url = `/${AppRoutes.home.uri}`;
    }

    this._router.navigateByUrl(url);
  }

}
