import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { notificationMessages } from '../../../../../i18n/traductions';
import { Notification } from '../../../../core/models';
import { NotificationService } from '../../../../core/services';
import { ElasticsearchService } from '../../../../elasticsearch/services/elasticsearch.service';
import { AppRoutes } from '../../../../routes';
import { IResource, IRestrictedAccessDataset, IService, Resource, ServiceAccess } from '../../../models';
import { UserServicesService } from '../../../services';

@Component({
  selector: 'app-available-data',
  templateUrl: './available-data.component.html',
  styleUrls: ['./available-data.component.scss'],
})
export class AvailableDataComponent implements OnInit {

  private _pendingRequests: number = 0;
  AppRoutes = AppRoutes;
  requestableResources: Resource[] = [];
  openedRow: number[] = [];
  userResources: Resource[] = [];
  services: IService[] = [];
  restrictedAccessDatasets: IRestrictedAccessDataset[] = [];
  slugsUuids = {};

  constructor(
    private _userAccessService: UserServicesService,
    private _notificationService: NotificationService,
    private _elasticSearchService: ElasticsearchService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this._pendingRequests += 1;
    forkJoin([
      // Get the list of the different modes (wms, ws...)
      this._userAccessService.getServices(),
      this._userAccessService.getRestrictedAccessDatasets(),
      // Get the list of the requested services by the user
      this._userAccessService.getUserResources(),
    ]).subscribe(
      (results) => {
        this.services = results[0];
        this.restrictedAccessDatasets = results[1];
        this.initUserResources(results[2]);
        this.restrictedAccessDatasets = this.restrictedAccessDatasets.filter((e) => {
          return (e.geonetUuid && e.geonetUuid !== '');
        });
        const uuids = this.restrictedAccessDatasets.map((e) => { return e.geonetUuid; });
        this._userAccessService.getSlugsFromUuid(uuids).subscribe(
          (response) => {
            this.slugsUuids = response;
            this.initRequestableResources();
          },
          (err) => { },
          () => {
            this._pendingRequests -= 1;
          },
        );
      },
      (err) => {
        this._pendingRequests -= 1;
        this._notificationService.notify(new Notification({
          type: 'error',
          message: `${notificationMessages.userServices.initializationError}`,
        }));
        this.handle401(err);
      },
    );
  }

  // Associating services to datasets and removing service/datasets that alreay have been requested
  initRequestableResources() {
    this.requestableResources = [];
    this.openedRow = [];
    // Iterate over the access restricted services
    this.restrictedAccessDatasets.forEach((restrictedDataset) => {
      const resource = new Resource(restrictedDataset);
      if (this.slugsUuids[restrictedDataset.geonetUuid]) {
        resource.datasetSlug = this.slugsUuids[restrictedDataset.geonetUuid]['slug'];
        resource.datasetTitle = this.slugsUuids[restrictedDataset.geonetUuid]['title'];
      }
      const services = [];
      resource.services.forEach((service, index) => {
        // Add service if not already in the user available dataset
        // meaning if the user didn't already have requested the access for that dataset and that service)
        if (
          this.userResources.findIndex(
            us => (us.datasetId === restrictedDataset.datasetId) &&
              (us.services.findIndex(s => s.name === service.name) !== -1),
          ) === -1) {
          services.push(service);
        }
      });

      resource.services = services;
      // If already all the mode have been requested then do not display the service in the list
      if (resource.services.length > 0) {
        this.requestableResources.push(resource);
      }

    });
  }

  initUserResources(services: IResource[]) {
    const grouped = this.groupBy(services, 'datasetId');
    const formated = [];

    for (const key in grouped) {
      if (grouped.hasOwnProperty(key)) {
        const resource = new Resource(grouped[key][0]);
        grouped[key].forEach((element: IResource) => {
          const service = this.services.find(m => m.name === element.serviceName);
          const id = service ? service.id : null;
          resource.services.push(new ServiceAccess(element, id));
        });

        formated.push(resource);
      }
    }

    this.userResources = formated;
  }

  private groupBy(array: any, property: string) {
    return array.reduce(
      (groups: any, item: any) => {
        const val = item[property];
        groups[val] = groups[val] || [];
        groups[val].push(item);
        return groups;
      },
      {},
    );
  }

  selectedResources(array: Resource[]) {
    return array.filter((e) => {
      return e.services.findIndex(m => m.selected) !== -1 ? true : false;
    }).map((e) => {
      const servicesArray = e.services.filter(m => m.selected);
      const r = new Resource(e);
      r.services = servicesArray;
      return r;
    });
  }

  requestAccess() {
    const accessRequests = [];
    // Building an array of request (one object per dataset containing the id of the dataset
    // and the list of services ID)
    this.selectedResources(this.requestableResources).forEach((e) => {
      accessRequests.push({ id: e.datasetId, servicesId: e.services.map(e => e.id) });
    });
    this._pendingRequests += 1;
    this._userAccessService.requestAccessToResource(accessRequests).subscribe(
      (res) => {
        // Displaying notification for each sucessful and unsuccessful request
        res.successfullyRequested.forEach((message: string) => {
          this._notificationService.notify(new Notification({
            type: 'success',
            message: `${notificationMessages.userServices.addSuccess} ${message}`,
          }));
        });

        res.unsuccessfullyRequested.forEach((message: string) => {
          this._notificationService.notify(new Notification({
            type: 'success',
            message: `${notificationMessages.userServices.addFailedDetailed} ${message}`,
          }));
        });

        // Update the list of the user services
        this._userAccessService.getUserResources().subscribe(
          (res) => {
            this.initUserResources(res);
            this.initRequestableResources();
          },
          (err) => {
            this._notificationService.notify(new Notification({
              type: 'error',
              message: `${notificationMessages.userServices.failedToLoadUserResources}`,
            }));
            this.handle401(err);
          },
          () => {
            this._pendingRequests -= 1;
          },
        );
      },
      (err) => {
        this._pendingRequests -= 1;
        this._notificationService.notify(new Notification({
          type: 'error',
          message: `${notificationMessages.userServices.addFailed}`,
        }));
        this.handle401(err);
      },
    );
  }

  getResourceServices(resource: Resource): string {
    let str = '';
    const maxElementDisplayed = 3;
    const modes = resource.services.map(service => service.name);

    if (modes.length > maxElementDisplayed) {
      str = `${modes.slice(0, 3).join(', ')} ...`;
    } else {
      str = modes.slice(0, 3).join(', ');
    }

    return str;
  }

  allServicesAreSelected(datasetId: number, array: Resource[]) {
    let res = true;

    const resource = array.find(e => e.datasetId === datasetId);
    if (resource) {
      const unselectedMode = resource.services.find(e => !e.selected);

      if (unselectedMode) {
        res = false;
      }
    }

    return res;
  }

  isUndeterminated(datasetId: number, array: Resource[]) {
    let res = false;

    const resource = array.find(e => e.datasetId === datasetId);
    if (resource) {
      const unselectedService = resource.services.find(e => !e.selected);
      const selectedService = resource.services.find(e => e.selected);

      if (unselectedService && selectedService) {
        res = true;
      }
    }

    return res;
  }

  toggleResource(elementId, array: Resource[]) {
    const resource = array.find(e => e.datasetId === elementId);
    if (resource) {
      let bool = false;
      const unselectedMode = resource.services.find(e => !e.selected);

      if (unselectedMode) {
        bool = true;
      }

      resource.services.forEach((mode) => {
        mode.selected = bool;
      });
    }
  }

  /** Helpers for row state (selected or not, toogle state) **/

  // Returns 1 if number is in array, -1 else
  isRowOpened(id: number, array: number[]): boolean {
    return array.includes(id);
  }

  toggleRow(id: number, array: number[]) {
    if (this.isRowOpened(id, array)) {
      this.removeRow(id, array);
    } else {
      this.addRow(id, array);
    }
  }

  addRow(id: number, array: number[]) {
    array.push(id);
  }

  removeRow(id, array: number[]) {
    const index = array.findIndex(row => row === id);
    if (index !== -1) {
      array.splice(index, 1);
    }
  }

  get isLoading() {
    return this._pendingRequests > 0 ? true : false;
  }

  // If the error received is an authentication error then redirect to the login page
  // because only authenticated users are supposed to access this component
  handle401(err) {
    if (err && err.status === 401) {
      this._router.navigate(['/', AppRoutes.signin.uri]);
    }
  }

}
