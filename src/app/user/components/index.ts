import { UserServicesComponent } from './access-management/user-services/user-services.component';
import { UserProfilComponent } from '../components/user-profil/user-profil.component';
import { LoginComponent } from './auth/login/login.component';
import { SignUpComponent } from './auth/sign-up/sign-up.component';
import { PasswordForgottenComponent } from './auth/password-forgotten/password-forgotten.component';
import { PasswordResetComponent } from './auth/password-reset/password-reset.component';
import { AccessManagementComponent } from './access-management/access-management.component';
import { AvailableDataComponent } from './access-management/available-data/available-data.component';
import { UserInfoComponent } from './user-profil/user-info/user-info.component';
import { UserPasswordUpdateComponent } from './user-profil/user-password-update/user-password-update.component';

export { UserServicesComponent, UserProfilComponent, LoginComponent, SignUpComponent, PasswordForgottenComponent,
  PasswordResetComponent, AccessManagementComponent, AvailableDataComponent, UserInfoComponent,
  UserPasswordUpdateComponent };

// tslint:disable-next-line:variable-name
export const UserComponents = [
  UserServicesComponent,
  UserProfilComponent,
  LoginComponent,
  SignUpComponent,
  PasswordForgottenComponent,
  PasswordResetComponent,
  AccessManagementComponent,
  AvailableDataComponent,
  UserInfoComponent,
  UserPasswordUpdateComponent,
];
