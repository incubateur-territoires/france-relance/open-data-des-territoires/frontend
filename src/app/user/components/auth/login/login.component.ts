import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { UserService } from '../../../services';
import { AppRoutes } from '../../../../routes';
import { IPageHeaderInfo } from '../../../../shared/models';
import { pageTitles } from '../../../../../i18n/traductions';
import { NavigationHistoryService } from '../../../../core/services';
import { APP_CONFIG } from '../../../../core/services/app-config.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  APP_CONFIG = APP_CONFIG;
  form: FormGroup;
  errorStatus: number = null;
  AppRoutes = AppRoutes;
  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.login,
  };

  showPassword: boolean;
  returnUrl: string;
  
  showOIDCButton = false;
  // Account validation
  token: string; // uuid4 allowing to identify the account that needs to be validated
  accountValidatationStatus = 'pending'; // pending, validated, failed
  errorType = null;

  constructor(
    private _fb: FormBuilder,
    private _userService: UserService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _navigationHistoryService: NavigationHistoryService,
  ) {

    this.form = this._fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });

    this.returnUrl = this._navigationHistoryService.getFromLast(0);
    if(null  == this.returnUrl || this._router.url == this.returnUrl){
      this.returnUrl = '/' + AppRoutes.home.uri;
    }
  }

  ngOnInit() {
    this.showPassword = false;
    if(APP_CONFIG.backendUrls.oidc){
      this.showOIDCButton = true;
    }
    this._activatedRoute.queryParams.subscribe((params) => {
      this.token = params.token;

      if (this.token) {
        this._userService.validateAccount(this.token).subscribe(
          (res) => {
            this.accountValidatationStatus = 'validated';
          },
          (err) => {
            this.accountValidatationStatus = 'failed';
            if (err.error && err.error.message) {
              this.errorType = err.error.message;
            } else {
              this.errorType = 'default';
            }
          },
        );
      } else {
        this.accountValidatationStatus = 'noTokenProvided';
      }
    });
  }

  login() {
    if (this.form.valid) {
      this.form.disable();
      this._userService.login(this.form.value).subscribe(
        (res) => {
          this.form.enable();
          this.errorStatus = null;
          this._router.navigate([this.returnUrl]);
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            this.errorStatus = err.status;
          } else {
            this.errorStatus = 500;
          }
          this.form.enable();
        },
      );
    } else {
      this.errorStatus = 400;
    }
  }

  fieldIsValid(field: string) {
    return (this.form.get(field).touched) && this.form.get(field).valid;
  }

  fieldIsInvalid(field: string) {
    return (this.form.get(field).touched) && this.form.get(field).invalid;
  }

  get username() { return this.form.get('username'); }
  get password() { return this.form.get('password'); }

  get formDisabled() {
    return this.form.disabled;
  }

}
