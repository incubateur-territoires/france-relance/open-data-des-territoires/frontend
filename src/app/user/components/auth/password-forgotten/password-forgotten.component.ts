import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppRoutes } from '../../../../routes';
import { UserService } from '../../../services';
import { NotificationService } from '../../../../core/services';
import { notificationMessages, pageTitles } from '../../../../../i18n/traductions';
import { IPageHeaderInfo } from '../../../../shared/models';

@Component({
  selector: 'app-password-forgotten',
  templateUrl: './password-forgotten.component.html',
  styleUrls: ['./password-forgotten.component.scss'],
})
export class PasswordForgottenComponent implements OnInit {

  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.passwordForgotten,
  };
  form: FormGroup;
  AppRoutes = AppRoutes;
  passwordForgottenRequestIsPending = false; // Used to display loading

  constructor(
    private _fb: FormBuilder,
    private _userService: UserService,
    private _notificationService: NotificationService,
  ) {
    this.form = this._fb.group({
      email: [, [Validators.required, Validators.email]],
    });
  }

  ngOnInit() {
  }

  get email() { return this.form.get('email'); }

  fieldIsValid(field: string) {
    return (this.form.get(field).touched) && this.form.get(field).valid;
  }

  fieldIsInvalid(field: string) {
    return (this.form.get(field).touched) && this.form.get(field).invalid;
  }

  send() {
    if (!this.form.invalid) {
      this.passwordForgottenRequestIsPending = true;
      this.form.disable();
      this._userService.forgotPassword(this.form.value).subscribe(
        (res) => {
          this._notificationService.notify({
            type: 'success',
            message: notificationMessages.userPasswordForgotten.success,
          });
          this.passwordForgottenRequestIsPending = false;
          this.form.enable();
          this.form.reset();
        },
        () => {
          this.passwordForgottenRequestIsPending = false;
          this.form.enable();
          this._notificationService.notify({
            type: 'error',
            message: notificationMessages.userPasswordForgotten.failed,
          });
        },
      );
    }
  }

}
