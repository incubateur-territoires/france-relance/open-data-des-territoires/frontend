import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PasswordForgottenComponent } from './password-forgotten.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../../../services';
import { NotificationService } from '../../../../core/services';

export class UserServiceMock {

  constructor() { }

  get forgotPassword() {
    return false;
  }

}

export class NotificationServiceMock {

  constructor() { }

}

describe('PasswordForgottenComponent', () => {
  let component: PasswordForgottenComponent;
  let fixture: ComponentFixture<PasswordForgottenComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: UserService,
          useClass: UserServiceMock,
        },
        {
          provide: NotificationService,
          useClass: NotificationServiceMock,
        },
      ],
      declarations: [PasswordForgottenComponent],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordForgottenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
