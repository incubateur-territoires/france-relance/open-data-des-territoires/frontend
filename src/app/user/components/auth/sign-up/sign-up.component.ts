import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from '../../../../../environments/environment';
import { notificationMessages, pageTitles } from '../../../../../i18n/traductions';
import { Notification } from '../../../../core/models';
import { NotificationService } from '../../../../core/services';
import { AppRoutes } from '../../../../routes';
import { IPageHeaderInfo } from '../../../../shared/models';
import { LegacyAccount } from '../../../models';
import { UserService } from '../../../services';
import { ValidatePassword } from '../../../validators/password.validator';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {

  form: FormGroup;
  AppRoutes = AppRoutes;
  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.signup,
  };

  constructor(
    private _fb: FormBuilder,
    private _userService: UserService,
    private _notificationService: NotificationService,
    private _router: Router,
  ) {
    this.form = this._fb.group({
      email: ['', [Validators.required, Validators.email]],
      emailConfirmation: ['', [Validators.required, Validators.email]],
      firstName: ['', [
        Validators.required,
        Validators.pattern('([A-ZÀ-ÖØ-Ý][a-zà-öø-ýÿA-ZÀ-ÖØ-Ý]*)([ \'-][a-zA-ZÀ-ÖØ-öø-ýÿ]*)*'),
      ]],
      lastName: ['', [
        Validators.required,
        Validators.pattern('([A-ZÀ-ÖØ-Ý][a-zà-öø-ýÿA-ZÀ-ÖØ-Ý]*)([ \'-][a-zA-ZÀ-ÖØ-öø-ýÿ]*)*'),
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(environment.passwordMinLength),
        ValidatePassword,
      ]],
      passwordConfirmation: ['', Validators.required],
      acceptMessages: [false],
      entreprise: [''],
      address: [''],
      zipcode: [''],
      city: [''],
      country: [''],
      cgu: [false, Validators.requiredTrue],
      consent: [false, Validators.requiredTrue],
    });
  }

  ngOnInit() {
  }

  // Getters for form inputs
  get email() { return this.form.get('email'); }
  get emailConfirmation() { return this.form.get('emailConfirmation'); }
  get firstName() { return this.form.get('firstName'); }
  get lastName() { return this.form.get('lastName'); }
  get password(): AbstractControl { return this.form.get('password'); }
  get passwordConfirmation() { return this.form.get('passwordConfirmation'); }
  get acceptMessages() { return this.form.get('acceptMessages'); }
  get entreprise() { return this.form.get('entreprise'); }
  get address() { return this.form.get('address'); }
  get zipcode() { return this.form.get('zipcode'); }
  get city() { return this.form.get('city'); }
  get contry() { return this.form.get('contry'); }
  get cgu() { return this.form.get('cgu'); }
  get consent() { return this.form.get('consent'); }

  get formDisabled(): boolean {
    return this.form.disabled;
  }

  // Return true if one of the fields at least doesn't respect its validators
  // or if the confirmation email is different from the email
  // or if the confirmation password is different from the password
  get formIsInvalid() {
    const value = this.form.invalid || this.emailConfirmationError || this.passwordConfirmationError;
    return value;
  }

  fieldIsValid(field: string) {
    return (this.form.get(field).touched) && this.form.get(field).valid;
  }

  fieldIsInvalid(field: string) {
    return (this.form.get(field).touched) && this.form.get(field).invalid;
  }

  get emailConfirmationIsCorrect(): boolean {
    const value = this.email.value === this.emailConfirmation.value ? true : false;
    return value;
  }

  get emailConfirmationError() {
    // Display the message if
    // Input values have been modified
    // Input values have been modified but the email values are not similar
    return this.email.touched && this.emailConfirmation.touched && !this.emailConfirmationIsCorrect;
  }

  get passwordConfirmationIsCorrect(): boolean {
    const value = this.password.value === this.passwordConfirmation.value ? true : false;
    return value;
  }

  get passwordConfirmationError() {
    // Display the message if
    // Input values have been modified
    // Input values have been modified but the email values are not similar
    return this.password.touched && this.passwordConfirmation.touched && !this.passwordConfirmationIsCorrect;
  }

  get passwordMinLength(): number {
    return environment.passwordMinLength;
  }

  send() {
    if (!this.formIsInvalid && !this.formDisabled) {
      const account = new LegacyAccount(this.form.value);
      this.form.disable();
      this._userService.createAccount(account).subscribe(
        (res) => {
          this.form.enable();
          this._notificationService.notify(
            {
              type: 'success',
              message: notificationMessages.signup.accountCreated,
            },
          );
          this.form.reset();
          this._router.navigate(['/', AppRoutes.signin.uri]);
        },
        (err) => {
          this.form.enable();
          const notif = new Notification({ type: 'error', message: '' });
          if (err instanceof HttpErrorResponse && err.error.message && err.error.statusCode === 400) {
            const message = notificationMessages.signup[err.error.message];
            if (message) {
              notif.message = message;
            } else {
              notif.message = notificationMessages.signup.accountCreationFailed;
            }
          } else {
            notif.message = notificationMessages.signup.accountCreationFailed;
          }
          this._notificationService.notify(notif);
        },
      );
    }
  }

  toUppercase(controlName: string) {
    const input = this.form.get(controlName).value;
    const uppercased = input.substring(0, 1).toUpperCase() + input.substring(1);
    this.form.get(controlName).patchValue(uppercased);
  }

}
