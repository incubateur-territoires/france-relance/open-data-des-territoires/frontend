import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SignUpComponent } from './sign-up.component';
import { AbstractControl, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from '../../../../../environments/environment';
import { UserService } from '../../../services';
import { NotificationService } from '../../../../core/services';

export class UserServiceMock {

  constructor() { }

}

export class NotificationServiceMock {

  constructor() { }

}

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;
  let firstName: AbstractControl;
  let lastName: AbstractControl;
  let email: AbstractControl;
  let emailConfirmation: AbstractControl;
  let password: AbstractControl;
  let passwordConfirmation: AbstractControl;
  let acceptMessages: AbstractControl;
  let cgu: AbstractControl;

  describe('Template', () => {
    beforeEach(waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          ReactiveFormsModule,
          RouterTestingModule,
        ],
        providers: [
          {
            provide: UserService,
            useClass: UserServiceMock,
          },
          {
            provide: NotificationService,
            useClass: NotificationServiceMock,
          },
        ],
        declarations: [SignUpComponent],
      })
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(SignUpComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      firstName = component.form.get('firstName');
      lastName = component.form.get('lastName');
      email = component.form.get('email');
      emailConfirmation = component.form.get('emailConfirmation');
      password = component.form.get('password');
      passwordConfirmation = component.form.get('passwordConfirmation');
      acceptMessages = component.form.get('acceptMessages');
      cgu = component.form.get('cgu');
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    describe('Form validation (when testing if control is invalid we are also testing if the form is invalid)', () => {

      it('form invalid when empty', () => {
        expect(component.form.valid).toBeFalsy();
      });

      describe('FirstName control validation', () => {
        beforeEach(() => {
          firstName.reset();
        });

        it('firstName control is valid with lowercase and uppercase letter', () => {
          // Given
          const value = 'John Doe';
          // When
          firstName.setValue(value);
          // Then
          expect(firstName.valid).toBeTruthy();
        });

        it('firstName control is valid with letter and space', () => {
          // Given
          const value = 'John Doe';
          // When
          firstName.setValue(value);
          // Then
          expect(firstName.valid).toBeTruthy();
        });

        it('firstName control is valid with letter and dash', () => {
          // Given
          const value = 'Jean-jacques';
          // When
          firstName.setValue(value);
          // Then
          expect(firstName.valid).toBeTruthy();
        });

        it('firstName control is valid with letter and apostrophe', () => {
          // Given
          const value = 'De\'Andre';
          // When
          firstName.setValue(value);
          // Then
          expect(firstName.valid).toBeTruthy();
        });

        it('firstName control accept any of those characters', () => {
          // Given
          let specialChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
          specialChars += 'abcdefghijklmnopqrstuvwxyz';
          specialChars += 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ';
          specialChars += 'àáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ';
          // When
          firstName.setValue(specialChars);
          // Then
          expect(firstName.valid).toBeTruthy();
        });

        it('firstName control is invalid if first letter is lowercased, has error "pattern"', () => {
          // Given
          const value = 'david';
          // When
          firstName.setValue(value);
          const errors = firstName.errors || {};
          // Then
          expect(firstName.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('firstName control is invalid if contains numbers', () => {
          // Given
          const value = '0123456789';
          // When
          firstName.setValue(value);
          const errors = firstName.errors || {};
          // Then
          expect(firstName.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('firstName control is invalid if contains special characters', () => {
          // Given
          const value = '"*$^!qjsdk+°)';
          // When
          firstName.setValue(value);
          const errors = firstName.errors || {};
          // Then
          expect(firstName.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('firstName control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          firstName.setValue(value);
          const errors = firstName.errors || {};
          // Then
          expect(firstName.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });
      });

      describe('LastName control validation', () => {
        beforeEach(() => {
          lastName.reset();
        });

        it('lastName control is valid with lowercase and uppercase letter', () => {
          // Given
          const value = 'John Doe';
          // When
          lastName.setValue(value);
          // Then
          expect(lastName.valid).toBeTruthy();
        });

        it('lastName control is valid with letter and space', () => {
          // Given
          const value = 'John Doe';
          // When
          lastName.setValue(value);
          // Then
          expect(lastName.valid).toBeTruthy();
        });

        it('lastName control is valid with letter and dash', () => {
          // Given
          const value = 'Jean-jacques';
          // When
          lastName.setValue(value);
          // Then
          expect(lastName.valid).toBeTruthy();
        });

        it('lastName control is valid with letter and apostrophe', () => {
          // Given
          const value = 'De\'Andre';
          // When
          lastName.setValue(value);
          // Then
          expect(lastName.valid).toBeTruthy();
        });

        it('lastName control accept any of those characters', () => {
          // Given
          let specialChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
          specialChars += 'abcdefghijklmnopqrstuvwxyz';
          specialChars += 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ';
          specialChars += 'àáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ';
          // When
          lastName.setValue(specialChars);
          // Then
          expect(lastName.valid).toBeTruthy();
        });

        it('lastName control is invalid if first letter is lowercased, has error "pattern"', () => {
          // Given
          const value = 'david';
          // When
          lastName.setValue(value);
          const errors = lastName.errors || {};
          // Then
          expect(lastName.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('lastName control is invalid if contains numbers', () => {
          // Given
          const value = '0123456789';
          // When
          lastName.setValue(value);
          const errors = lastName.errors || {};
          // Then
          expect(lastName.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('lastName control is invalid if contains special characters', () => {
          // Given
          const value = '"*$^!qjsdk+°)';
          // When
          lastName.setValue(value);
          const errors = lastName.errors || {};
          // Then
          expect(lastName.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('lastName control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          lastName.setValue(value);
          const errors = lastName.errors || {};
          // Then
          expect(lastName.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });
      });

      describe('Email control validation', () => {
        beforeEach(() => {
          email.reset();
        });

        it('Email control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          email.setValue(value);
          const errors = email.errors || {};
          // Then
          expect(email.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });

        it('Email control is invalid if the value is not a valid email', () => {
          // Given
          const value = 'toto.grandlyon.com';
          // When
          email.setValue(value);
          const errors = email.errors || {};
          // Then
          expect(email.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['email']).toBeTruthy();
        });

        it('Email control is valid if the value is a valid email', () => {
          // Given
          const value = 'toto@grandlyon.com';
          // When
          email.setValue(value);
          const errors = email.errors || {};
          // Then
          expect(email.valid).toBeTruthy();
          expect(errors).toEqual({});
        });
      });

      describe('emailConfirmation control validation', () => {
        beforeEach(() => {
          emailConfirmation.reset();
        });

        it('emailConfirmation control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          emailConfirmation.setValue(value);
          const errors = emailConfirmation.errors || {};
          // Then
          expect(emailConfirmation.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });

        it('emailConfirmation control is invalid if the value is not a valid emailConfirmation', () => {
          // Given
          const value = 'toto.grandlyon.com';
          // When
          emailConfirmation.setValue(value);
          const errors = emailConfirmation.errors || {};
          // Then
          expect(emailConfirmation.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['email']).toBeTruthy();
        });

        it('emailConfirmation control is valid if the value is a valid emailConfirmation', () => {
          // Given
          const value = 'toto@grandlyon.com';
          // When
          emailConfirmation.setValue(value);
          const errors = emailConfirmation.errors || {};
          // Then
          expect(emailConfirmation.valid).toBeTruthy();
          expect(errors).toEqual({});
        });
      });

      describe('password control validation', () => {
        beforeEach(() => {
          password.reset();
        });

        it('password control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(password.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });

        it('password control shouldn\'t have required error if the value is set', () => {
          // Given
          const value = 'dsfsfdf';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(errors['required']).toBeFalsy();
        });

        it(`password control is invalid if its length is inferior to ${environment.passwordMinLength}`, () => {
          // Given
          const value = 'abc';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(password.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['minlength']).toBeTruthy();
        });

        // tslint:disable-next-line:max-line-length
        it(`password control has not minLength error if its length is superior to ${environment.passwordMinLength}`, () => {
          // Given
          const value = 'abcqqsdsqdqsdsqdqsdsqsqsdsqddsqsdsqdsq';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(errors['minlength']).toBeFalsy();
        });

        // tslint:disable-next-line:max-line-length
        it('password control has \'missingSpecialCharacters\' error if there is not at least one special character', () => {
          // Given
          const value = 'abcq';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(errors['missingSpecialCharacters']).toBeTruthy();
        });

        it('password control has \'invalidCharacters\' error if there is one of the forbidden caracters', () => {
          // Given
          const value = 'abcqeé€';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(errors['invalidCharacters']).toBeTruthy();
        });

        // tslint:disable-next-line:max-line-length
        it('password control has \'missingUppercasedLetter\' error if there is not at least one uppercased letter', () => {
          // Given
          const value = 'abcq';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(errors['missingUppercasedLetter']).toBeTruthy();
        });

        // tslint:disable-next-line:max-line-length
        it('password control has \'missingLowercasedLetter\' error if there is not at least one lowercassed letter', () => {
          // Given
          const value = '1234TYARKS';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(errors['missingLowercasedLetter']).toBeTruthy();
        });

        // tslint:disable-next-line:max-line-length
        it('password control has \'missingNumber\' error if there is not at least one digit', () => {
          // Given
          const value = 'aaazezfrez';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(errors['missingNumber']).toBeTruthy();
        });

        // tslint:disable-next-line:max-line-length
        it('password control has \'forbiddenWord\' error if the password contains one of the forbidden words', () => {
          // Given
          const value = 'sqddsqgrandlyondsqsqdsq';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(errors['forbiddenWord']).toBeTruthy();
        });

        // tslint:disable-next-line:max-line-length
        it('password control has no errors from validatePassword is respecting all the rules', () => {
          // Given
          const value = 'azeAZE123!';
          // When
          password.setValue(value);
          const errors = password.errors || {};
          // Then
          expect(errors['missingSpecialCharacters']).toBeFalsy();
          expect(errors['missingUppercasedLetter']).toBeFalsy();
          expect(errors['missingLowercasedLetter']).toBeFalsy();
          expect(errors['missingNumber']).toBeFalsy();
          expect(errors['forbiddenWord']).toBeFalsy();
        });

        it('password control should be valid when respecting all the validator', () => {
          // Given
          const value = 'azeAZE123!';
          // When
          password.setValue(value);
          // Then
          expect(password.valid).toBeTruthy();
        });
      });

      describe('passwordConfirmation control validation', () => {
        beforeEach(() => {
          passwordConfirmation.reset();
        });

        it('passwordConfirmation control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          passwordConfirmation.setValue(value);
          const errors = passwordConfirmation.errors || {};
          // Then
          expect(passwordConfirmation.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });
      });

      describe('acceptMessages control validation', () => {

        it('acceptMessages is initialized to false', () => {
          // Given
          // When
          // Then
          expect(acceptMessages.value).toEqual(false);
        });
      });

      describe('cgu control validation', () => {
        it('cgu is initialized to false', () => {
          // Given
          // When
          // Then
          expect(cgu.value).toEqual(false);
        });

        it('cgu control is invalid if it is not set to true', () => {
          // Given
          const value = false;
          // When
          cgu.setValue(value);
          const errors = cgu.errors || {};
          // Then
          expect(cgu.valid).toBeFalsy();
          expect(component.form.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });

        it('cgu control is valid if is set to true', () => {
          // Given
          const value = true;
          // When
          cgu.setValue(value);
          const errors = cgu.errors || {};
          // Then
          expect(cgu.valid).toBeTruthy();
          expect(errors['required']).toBeFalsy();
        });
      });
    });
  });
});
