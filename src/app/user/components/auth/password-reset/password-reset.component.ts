import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services';
import { AppRoutes } from '../../../../routes';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { ValidatePassword } from '../../../validators/password.validator';
import { NotificationService } from '../../../../core/services';
import { notificationMessages, pageTitles } from '../../../../../i18n/traductions';
import { IPageHeaderInfo } from '../../../../shared/models';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss'],
})
export class PasswordResetComponent implements OnInit {

  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.passwordReset,
  };
  passwordResetTokenIsValid: boolean = null;
  error = false;
  passwordMinLength = 8;
  AppRoutes = AppRoutes;
  passwordResetRequestIsPending = false; // Used to display loading
  token: string;

  form: FormGroup;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _userService: UserService,
    private _fb: FormBuilder,
    private _router: Router,
    private _notificationService: NotificationService,
  ) {
    this.form = this._fb.group({
      newPassword: ['', [
        Validators.required,
        Validators.minLength(environment.passwordMinLength),
        ValidatePassword,
      ]],
      newPasswordConfirmation: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.passwordMinLength = environment.passwordMinLength;

    this._activatedRoute.queryParams.subscribe((params) => {
      this.token = params.token;

      this._userService.isPasswordResetTokenValid(this.token).subscribe(
        (res) => {
          this.passwordResetTokenIsValid = res;
        },
        (err) => {
          this.error = true;
        },
      );
    });
  }

  get newPassword(): AbstractControl { return this.form.get('newPassword'); }
  get newPasswordConfirmation(): AbstractControl { return this.form.get('newPasswordConfirmation'); }

  fieldIsValid(field: string) {
    return (this.form.get(field).touched) && this.form.get(field).valid;
  }

  fieldIsInvalid(field: string) {
    return (this.form.get(field).touched) && this.form.get(field).invalid;
  }

  // Return true if one of the fields at least doesn't respect its validators
  // or if the confirmation password is different from the password
  get passwordResetFormIsInvalid() {
    const value = this.form.invalid || !this.newPasswordConfirmationIsCorrect ||
                  !this.newPassword.dirty || !this.newPasswordConfirmation.dirty ;
    return value;
  }

  get newPasswordConfirmationIsCorrect(): boolean {
    const value = this.newPassword.value === this.newPasswordConfirmation.value ? true : false;
    return value;
  }
  get newPasswordConfirmationError() {
    // Display the message if
    // Input values have been modified but the email values are not similar
    return (this.newPassword.touched && this.newPasswordConfirmation.touched && !this.newPasswordConfirmationIsCorrect);
  }

  updatePassword() {
    if (!this.passwordResetFormIsInvalid && !this.form.disabled) {
      this.passwordResetRequestIsPending = true;
      this.form.disable();
      this._userService.resetPassword(this.token, this.newPassword.value).subscribe(
        (res) => {
          this._notificationService.notify({
            type: 'success',
            message: notificationMessages.userPasswordReset.success,
          });
          this.passwordResetRequestIsPending = false;
          this.form.reset();
          this.form.enable();
          this._router.navigate(['/', AppRoutes.signin.uri]);
        },
        (err) => {
          this._notificationService.notify({
            type: 'error',
            message: notificationMessages.userPasswordReset.failed,
          });
          this.form.enable();
          this.passwordResetRequestIsPending = false;
        },
      );
    }
  }

}
