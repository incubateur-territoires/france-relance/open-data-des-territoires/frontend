import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PasswordResetComponent } from './password-reset.component';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../../../services';
import { NotificationService } from '../../../../core/services';
import { environment } from '../../../../../environments/environment';
import { of } from 'rxjs';

export class UserServiceMock {

  constructor() { }

  forgotPassword() {
    return false;
  }

  isPasswordResetTokenValid() {
    return of(false);
  }

}

export class NotificationServiceMock {

  constructor() { }

}

describe('PasswordResetComponent', () => {
  let component: PasswordResetComponent;
  let fixture: ComponentFixture<PasswordResetComponent>;
  let newPassword: AbstractControl;
  let newPasswordConfirmation: AbstractControl;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: UserService,
          useClass: UserServiceMock,
        },
        {
          provide: NotificationService,
          useClass: NotificationServiceMock,
        },
      ],
      declarations: [PasswordResetComponent],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordResetComponent);
    component = fixture.componentInstance;
    newPassword = component.form.get('newPassword');
    newPasswordConfirmation = component.form.get('newPasswordConfirmation');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('newPassword control validation', () => {
    beforeEach(() => {
      newPassword.reset();
    });

    it('newPassword control is invalid if the value is empty', () => {
      // Given
      const value = '';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(newPassword.valid).toBeFalsy();
      expect(component.form.valid).toBeFalsy();
      expect(errors['required']).toBeTruthy();
    });

    it('newPassword control shouldn\'t have required error if the value is set', () => {
      // Given
      const value = 'dsfsfdf';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(errors['required']).toBeFalsy();
    });

    it(`password control is invalid if its length is inferior to ${environment.passwordMinLength}`, () => {
      // Given
      const value = 'abc';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(newPassword.valid).toBeFalsy();
      expect(component.form.valid).toBeFalsy();
      expect(errors['minlength']).toBeTruthy();
    });

    // tslint:disable-next-line:max-line-length
    it(`password control has not minLength error if its length is superior to ${environment.passwordMinLength}`, () => {
      // Given
      const value = 'abcqqsdsqdqsdsqdqsdsqsqsdsqddsqsdsqdsq';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(errors['minlength']).toBeFalsy();
    });

    // tslint:disable-next-line:max-line-length
    it('newPassword control has \'missingSpecialCharacters\' error if there is not at least one special character', () => {
      // Given
      const value = 'abcq';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(errors['missingSpecialCharacters']).toBeTruthy();
    });

    it('newPassword control has \'invalidCharacters\' error if there is one of the forbidden caracters', () => {
      // Given
      const value = 'abcqeé€';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(errors['invalidCharacters']).toBeTruthy();
    });

    // tslint:disable-next-line:max-line-length
    it('newPassword control has \'missingUppercasedLetter\' error if there is not at least one uppercased letter', () => {
      // Given
      const value = 'abcq';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(errors['missingUppercasedLetter']).toBeTruthy();
    });

    // tslint:disable-next-line:max-line-length
    it('newPassword control has \'missingLowercasedLetter\' error if there is not at least one lowercassed letter', () => {
      // Given
      const value = '1234TYARKS';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(errors['missingLowercasedLetter']).toBeTruthy();
    });

    // tslint:disable-next-line:max-line-length
    it('newPassword control has \'missingNumber\' error if there is not at least one digit', () => {
      // Given
      const value = 'aaazezfrez';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(errors['missingNumber']).toBeTruthy();
    });

    // tslint:disable-next-line:max-line-length
    it('newPassword control has \'forbiddenWord\' error if the newPassword contains one of the forbidden words', () => {
      // Given
      const value = 'sqddsqgrandlyondsqsqdsq';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(errors['forbiddenWord']).toBeTruthy();
    });

    // tslint:disable-next-line:max-line-length
    it('newPassword control has no errors from validatePassword is respecting all the rules', () => {
      // Given
      const value = 'azeAZE123!';
      // When
      newPassword.setValue(value);
      const errors = newPassword.errors || {};
      // Then
      expect(errors['missingSpecialCharacters']).toBeFalsy();
      expect(errors['missingUppercasedLetter']).toBeFalsy();
      expect(errors['missingLowercasedLetter']).toBeFalsy();
      expect(errors['missingNumber']).toBeFalsy();
      expect(errors['forbiddenWord']).toBeFalsy();
    });

    it('newPassword control should be valid when respecting all the validator', () => {
      // Given
      const value = 'azeAZE123!';
      // When
      newPassword.setValue(value);
      // Then
      expect(newPassword.valid).toBeTruthy();
    });
  });

  describe('newPasswordConfirmation control validation', () => {
    beforeEach(() => {
      newPasswordConfirmation.reset();
    });

    it('newPasswordConfirmation control is invalid if the value is empty', () => {
      // Given
      const value = '';
      // When
      newPasswordConfirmation.setValue(value);
      const errors = newPasswordConfirmation.errors || {};
      // Then
      expect(newPasswordConfirmation.valid).toBeFalsy();
      expect(component.form.valid).toBeFalsy();
      expect(errors['required']).toBeTruthy();
    });
  });
});
