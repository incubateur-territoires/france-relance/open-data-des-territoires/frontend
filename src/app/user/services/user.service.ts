import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject,of } from 'rxjs';
import { IUserInfo, PasswordUpdateForm, User, ILoginResponse, LegacyAccount, IPasswordForgottenForm } from '../models';
import { map, mergeMap, tap,catchError } from 'rxjs/operators';
import * as NodeRSA from 'node-rsa';
import { APP_CONFIG } from '../../core/services/app-config.service';

@Injectable()
export class UserService {

  private _user: User =null;/*
    new User({
    firstName: "userInfo.user.first_name",
    lastName: "userInfo.user.last_name",
    email: "userInfo.user.email",
    username: "userInfo.user.email",
    is_superuser:true
  });*/
  _userStatusChangedSubject: Subject<boolean>;

  constructor(
    private _http: HttpClient,
  ) {
    this._userStatusChangedSubject = new Subject<boolean>();
  }

  initializeService() {
    console.log("initializeService");
    return this.getUserInfo().pipe(
      catchError(err => {
        console.log(err);
        if(err.status == 403 && err.error.code=='terms-required'){
          console.log("redirect to term validation");
          const redirectUrl = window.location.pathname + window.location.search;
          window.location.href=window.location.origin+'/onegeo-login/terms-of-use?next='+redirectUrl
        }
        console.log('Handling error locally and rethrowing it...', err);
        return of(err);
      }),
      map((userInfo) => {
        console.log(userInfo)
        if(userInfo.authenticated){
          const userInfos= {
            firstName: userInfo.user.first_name,
            lastName: userInfo.user.last_name,
            email: userInfo.user.email,
            username: userInfo.user.email,
            is_superuser: userInfo.user.is_superuser,
          }
          this._user = new User(userInfos);
        }
        
      }),
    );
  }

  hasMapAcces(): boolean {
    if(APP_CONFIG.theFunctionalitiesInterruptor.mapsRestrictedToAdmins){
      if(this.user && this.user.is_superuser){
        return true;
      }
      return false;
    }
    return true;
  }
  // Function and helpers allowing the management of the user session (jwt), info...
  setSession(authResult): boolean {
    let success = false;
    if (authResult && authResult.token) {
      this.initializeService();
    } else {
      this.resetAuth();
    }

    return success;
  }

  resetAuth() {
    this.logout().subscribe(() => {
      this._user = null;
      this._userStatusChangedSubject.next(false);
    });
  }

  get user() {
    return this._user;
  }

  get userIsSignedIn() {
    return this._user != null;
  }

  get userStatusChanged$(): Observable<boolean> {
    return this._userStatusChangedSubject.asObservable();
  }

  // HTTP Calls
  login(loginForm): Observable<boolean> {
    // Make sure not to mofidy the object passed in parameter (object reference) when encrypting the password
    const form = Object.assign({}, loginForm);

    return this._http.post<ILoginResponse>(
          `${APP_CONFIG.backendUrls.auth}signin/`,
          form,
          { withCredentials: true },
        ).pipe(
          map(
            (res) => {
              return this.setSession(res);
            },
          ),
    );
  }

  logout(): Observable<boolean> {
    return this._http.get<boolean>(`${APP_CONFIG.backendUrls.auth}signout/`, { withCredentials: true });
  }

  createAccount(legacyAccount: LegacyAccount): Observable<boolean> {
    // Make sure not to mofidy the object passed in parameter (object reference) when encrypting the password
    const form = Object.assign({}, legacyAccount);

    return this._http.post<any>(`${APP_CONFIG.backendUrls.middlewareLegacyAuth}user/`, form);
    
  }

  validateAccount(token: string) {
    return this._http.post<any>(`${APP_CONFIG.backendUrls.middlewareLegacyAuth}user/validateAccount`, { token });
  }

  getUserInfo(): Observable<any> {
  
    return this._http.get<any>(`${APP_CONFIG.backendUrls.auth}session/`, { withCredentials: true });
  }

  updateUserInfo(info: IUserInfo): Observable<{ token: string }> {
    return this._http.put<{ token: string }>(
      `${APP_CONFIG.backendUrls.auth}user/update`,
      info,
      { withCredentials: true },
    );
  }

  updateUserPassword(passwordUpdateform: PasswordUpdateForm): Observable<void> {
    // Make sure not to mofidy the object passed in parameter (object reference) when encrypting the password
    const form = Object.assign({}, passwordUpdateform);
    return this._http.put<void>(
      `${APP_CONFIG.backendUrls.middlewareLegacyAuth}user/updatePassword`,
          form,
          { withCredentials: true },
    );


  }

  forgotPassword(email: IPasswordForgottenForm): Observable<void> {
    return this._http.post<void>(`${APP_CONFIG.backendUrls.middlewareLegacyAuth}passwordForgotten`, email).pipe(
      map(
        (res) => {
          return;
        },
      ),
    );
  }

  isPasswordResetTokenValid(token: string): Observable<boolean> {
    // tslint:disable-next-line:max-line-length
    return this._http.get<boolean>(`${APP_CONFIG.backendUrls.middlewareLegacyAuth}isPasswordResetTokenValid?token=${token}`);
  }

  resetPassword(token: string, password: string): Observable<void> {
    // Make sure not to mofidy the object passed in parameter (object reference) when encrypting the password

    // tslint:disable-next-line:max-line-length
    return this._http.put<void>(
          `${APP_CONFIG.backendUrls.middlewareLegacyAuth}user/resetPassword`,
          { token, password: password },
          { withCredentials: true },
        );
     
  }

  deleteAccount(): Observable<void> {
    return this._http.delete<any>(`${APP_CONFIG.backendUrls.middlewareLegacyAuth}user`, { withCredentials: true }).pipe(
      map(
        (res) => {
          return;
        },
      ),
    );
  }

  

}
