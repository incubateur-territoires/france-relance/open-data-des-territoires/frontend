import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { ElasticsearchService } from '../../elasticsearch/services/elasticsearch.service';
import { AccessRequestToResourceResponse, DeleteAccessToRessourceResponse, IResource, IRestrictedAccessDataset, IService, RenewAccessToResourceResponse } from '../models';

@Injectable()
export class UserServicesService {

  constructor(
    private _http: HttpClient,
    private _elasticsearchService: ElasticsearchService,
  ) { }

  getUserResources(): Observable<IResource[]> {
    return this._http.get<IResource[]>(
      `${APP_CONFIG.backendUrls.middlewareLegacyAuth}user/resources`,
      { withCredentials: true },
    );
  }

  getSlugsFromUuid(uuids: string[]) {
    return this._elasticsearchService.getSlugsFromUuid(uuids).pipe(
      map((response) => {
        // {
        //   uuid: {'slug': xx, 'title': yy }
        //
        // }
        const slugUuids = {};
        if (response.hits.hits && response.hits.hits.length > 0) {
          response.hits.hits.forEach((element) => {
            slugUuids[element._source['metadata-fr']['geonet:info']['uuid']] = {
              slug: element._source['slug'],
              title: element._source['metadata-fr']['title'],
            };
          });
        }

        return slugUuids;
      }),
    );
  }

  getRestrictedAccessDatasets(): Observable<IRestrictedAccessDataset[]> {
    // tslint:disable-next-line:max-line-length
    return this._http.get<IRestrictedAccessDataset[]>(`${APP_CONFIG.backendUrls.middlewareLegacyAuth}restrictedAccessDatasets`);
  }

  getServices(): Observable<IService[]> {
    return this._http.get<IService[]>(`${APP_CONFIG.backendUrls.middlewareLegacyAuth}services`);
  }

  requestAccessToResource(resource) {
    return this._http.post<AccessRequestToResourceResponse>(
      `${APP_CONFIG.backendUrls.middlewareLegacyAuth}user/resources/add`,
      resource,
      { withCredentials: true },
    );
  }

  renewAccessToResource(resource) {
    return this._http.post<RenewAccessToResourceResponse>(
      `${APP_CONFIG.backendUrls.middlewareLegacyAuth}user/resources/renew`,
      resource,
      { withCredentials: true },
    );
  }

  deleteAccessToResource(resource) {
    return this._http.request<DeleteAccessToRessourceResponse>(
      'delete', // HTTP verb
      `${APP_CONFIG.backendUrls.middlewareLegacyAuth}user/resources/delete`, // url
      { body: resource, withCredentials: true }, // options
    );
  }
}
