import { UserServicesService } from '../services/user-services.service';
import { UserService } from '../services/user.service';

export { UserServicesService, UserService };

// tslint:disable-next-line:variable-name
export const UserServices = [
  UserServicesService,
  UserService,
];
