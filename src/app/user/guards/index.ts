import { AuthenticatedGuard } from './authenticated.guard';
import { UnauthenticatedGuard } from './unauthenticated.guard';

// tslint:disable-next-line:variable-name
export const UserGuards = [
  AuthenticatedGuard,
  UnauthenticatedGuard,
];
