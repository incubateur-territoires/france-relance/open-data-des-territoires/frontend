import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AppRoutes } from '../../routes';
import { UserService } from '../services';
import { NotificationService } from '../../core/services';
import { notificationMessages } from '../../../i18n/traductions';

@Injectable()
export class UnauthenticatedGuard implements CanActivate {

  constructor(
    private _userService: UserService,
    private _router: Router,
    private _notificationService: NotificationService,
  ) {}

  // This guard is used to restrict the access to unauthenticated user on particular routes
  // example: login, signup, resetPassword...
  // It also notify the user that he is already logged in
  canActivate(): boolean {
    if (this._userService.userIsSignedIn) {
      this._router.navigate(['/', AppRoutes.home.uri]);
      this._notificationService.notify({
        type: 'warning',
        message: notificationMessages.alreadyAuthenticated,
      });
      return false;
    }
    return true;
  }
}
