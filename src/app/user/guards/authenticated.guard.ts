import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AppRoutes } from '../../routes';
import { UserService } from '../services';

@Injectable()
export class AuthenticatedGuard implements CanActivate {

  constructor(public _userService: UserService, public router: Router) {}

  canActivate(): boolean {
    if (!this._userService.userIsSignedIn) {
      this.router.navigate(['/', AppRoutes.signin.uri]);
      return false;
    }
    return true;
  }
}
