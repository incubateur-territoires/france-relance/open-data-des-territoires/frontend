import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutes } from '../routes';
import { AuthenticatedGuard } from './guards/authenticated.guard';
import {
  UserProfilComponent, LoginComponent, SignUpComponent, UserServicesComponent, PasswordForgottenComponent,
  PasswordResetComponent, AccessManagementComponent, AvailableDataComponent, UserInfoComponent,
  UserPasswordUpdateComponent
} from './components';
import { UnauthenticatedGuard } from './guards/unauthenticated.guard';

export const routes: Routes = [
  {
    path: AppRoutes.signin.uri,
    component: LoginComponent,
    canActivate: [UnauthenticatedGuard],
    data: {
      title: AppRoutes.signin.title,
    },
  },
  {
    path: AppRoutes.signup.uri,
    component: SignUpComponent,
    canActivate: [UnauthenticatedGuard],
    data: {
      title: AppRoutes.signup.title,
    },
  },
  {
    path: AppRoutes.passwordForgotten.uri,
    component: PasswordForgottenComponent,
    canActivate: [UnauthenticatedGuard],
    data: {
      title: AppRoutes.passwordForgotten.title,
    },
  },
  {
    path: AppRoutes.passwordReset.uri,
    component: PasswordResetComponent,
    canActivate: [UnauthenticatedGuard],
    data: {
      title: AppRoutes.passwordReset.title,
    },
  },
  {
    path: AppRoutes.accessManagement.uri,
    component: AccessManagementComponent,
    canActivate: [AuthenticatedGuard],
    children: [
      {
        path: '',
        redirectTo: `${AppRoutes.userServices.uri}`,
        pathMatch: 'full',
      },
      {
        path: AppRoutes.userServices.uri,
        component: UserServicesComponent,
        canActivate: [AuthenticatedGuard],
      },
      {
        path: AppRoutes.availableData.uri,
        component: AvailableDataComponent,
        canActivate: [AuthenticatedGuard],
      },
    ],
  },
  {
    path: AppRoutes.userProfil.uri,
    component: UserProfilComponent,
    canActivate: [AuthenticatedGuard],
    children: [
      {
        path: '',
        redirectTo: `${AppRoutes.userInfo.uri}`,
        pathMatch: 'full',
      },
      {
        path: AppRoutes.userInfo.uri,
        component: UserInfoComponent,
        canActivate: [AuthenticatedGuard],
      },
      {
        path: AppRoutes.userPasswordUpdate.uri,
        component: UserPasswordUpdateComponent,
        canActivate: [AuthenticatedGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule { }
