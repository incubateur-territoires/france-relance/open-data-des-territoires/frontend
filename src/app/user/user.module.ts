import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { UserServices } from './services';
import { UserComponents } from './components';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserGuards } from './guards';
import { SharedModule } from '../shared/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { XSRFTokenInterceptor } from './interceptors/xsrf-token.interceptor';
import { CookieService } from 'ngx-cookie-service';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  providers: [
    ...UserGuards,
    ...UserServices,
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: XSRFTokenInterceptor,
      multi: true,
    },
  ],
  declarations: [...UserComponents],
})
export class UserModule { }
