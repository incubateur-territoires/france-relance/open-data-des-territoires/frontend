import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { PasswordResetComponent } from '../components';

@Injectable()
export class XSRFTokenInterceptor implements HttpInterceptor {

  constructor(private cookieService: CookieService) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const xsrfToken = this.cookieService.get('XSRF-TOKEN');
    let request = req;

    if (xsrfToken) {
      if (!new RegExp(`^${APP_CONFIG.backendUrls.datasetUsageStatistics}`).test(req.url) &&
          !new RegExp(`^${APP_CONFIG.backendUrls.geocoder}`).test(req.url) ) {
        request = req.clone({
          headers: req.headers.set('x-xsrf-token', xsrfToken),
        });
      }
    }
    return next.handle(request);
  }
}
