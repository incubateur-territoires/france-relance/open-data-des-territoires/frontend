import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { filter, map } from 'rxjs/operators';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { environment } from '../environments/environment';
import { NavigationHistoryService } from './core/services';
import { AppRoutes } from './routes';
import { SeoSErvice } from './editorialisation/services';
import { pageTitles } from '../i18n/traductions';

declare var tarteaucitron: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  constructor(
    private _router: Router,
    private _navigationHistoryService: NavigationHistoryService,
    private _activatedRoute: ActivatedRoute,
    private _titleService: Title,
    private _seoSErvice: SeoSErvice,
  ) {
  }

  ngOnInit()
  {
    this._router.events.pipe(
      filter(e => e instanceof NavigationEnd),
    ).subscribe((e) => {
      this._navigationHistoryService.add(e['urlAfterRedirects']);
    });


    // Change the title using the title property passed in the data of each route
    this._router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => this._activatedRoute.snapshot),
      map((route) => {
        const lang = window.location.href.includes(environment.angularAppHost.en) ? 'en' : 'fr';
        let r = route;
        const titles = [AppRoutes.root.title[lang]];
        while (r.firstChild) {
          r = r.firstChild;
          if (r.outlet === 'primary' && r.data.title != null) {
            titles.unshift(r.data.title[lang]);
          }
        }
        return titles;
      }),
    ).subscribe((titles): void => {
      if (titles.length > 1) {
        const title = titles.join(' - ');
        this._seoSErvice.setRoutingSEO(title);
      }
      else {
        this._seoSErvice.setRoutingSEO('');
      }

    });
  }

  // Return true if origin url and destination url are both in datasetDetail
  isRoutingInsideDatasetDetail(origin, destination) {
    let res = false;
    if (origin.search(`${AppRoutes.datasets.uri}/`) !== -1 &&
      destination.search(`${AppRoutes.datasets.uri}/`) !== -1) {
      res = true;
    }
    return res;
  }
}
