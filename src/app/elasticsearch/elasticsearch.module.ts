import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElasticsearchService } from './services/elasticsearch.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [
    ElasticsearchService,
  ],
})
export class ElasticsearchModule { }
