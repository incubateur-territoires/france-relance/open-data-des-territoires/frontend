export interface ISortOption {
  'value': string;
  'order': string; // 'asc' or 'desc'
}
