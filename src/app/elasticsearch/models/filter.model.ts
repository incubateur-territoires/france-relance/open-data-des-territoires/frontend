import { geosource } from '../../../i18n/traductions';

interface IAggregationBucket {
  'key': string;
  'doc_count': number;
  'count_per_metadata': number;
  'isActive': boolean;
}

export class Aggregation {
  key: string;
  label: string;
  'doc_count': number;
  'count_per_metadata': number;
  isActive: boolean;
  troncatedKey: string; // Used for url parameters, without special characters
  troncatedLabel: string;
  subAggregations?: Aggregation[];
  parentAggregation?: Aggregation;
  field: string;
  type: string;

  constructor(aggBucket: IAggregationBucket) {
    this.troncatedKey = this.removeSpecialCharacters(aggBucket.key);
    this.key = aggBucket.key;
    this.doc_count = aggBucket.doc_count;
    this.count_per_metadata = aggBucket.count_per_metadata;
    this.isActive = aggBucket.isActive ? this.isActive : false;
    this.troncatedLabel = '';
    this.subAggregations = [];
    this.field = '';
    if (geosource.metadataCategories[this.key]) {
      this.label = geosource.metadataCategories[this.key];
    } else if (geosource.datasetTypes[this.key]) {
      this.label = geosource.datasetTypes[this.key];
    } else if (geosource.updateFrequencies[this.key]) {
      this.label = geosource.updateFrequencies[this.key];
    } else {
      this.label = aggBucket.key;
    }

    // The label display needs some treatment if too long or if have special pattern:
    // If the label is text1 (acronym1) / text2 (acronym) / text3
    // -> acronym1 / acronym2 / text3
    if (typeof this.label === 'string') {
      const splitLabel = this.label.split('/');
      if (splitLabel && splitLabel.length > 1) {
        let newLabel = '';
        splitLabel.forEach((element) => {
          const regex = new RegExp(/.*? \((.*?)\)/g);
          const execRegex = regex.exec(element);
          if (execRegex !== null) {
            const matches = Array.from(execRegex);
            newLabel += newLabel === '' ? matches[1] : ` / ${matches[1]}`;
          } else {
            newLabel += ` / ${element}`;
          }

        });
        this.troncatedLabel = newLabel;
      } else {
        this.troncatedLabel = this.label;
      }

      if (this.troncatedLabel.length > 30) {
        this.troncatedLabel = `${this.troncatedLabel.substring(0, 30)}...`;
      } else if (this.troncatedLabel === 'null') { // This is specific hack for ATMO provider
        this.troncatedLabel = 'Autres';
      }
    }

  }

  private removeSpecialCharacters(key: string) {
    // return (typeof key === 'string') ? key.replace(/[^a-zA-Z ]/g, '') : key;
    return (typeof key === 'string') ? key.replace(/[!@,#$%^&*]/g, '') : key;
  }

  findInSubAggregations(key: string) {
    const index = this.subAggregations.findIndex(el => el.key === key);
    return (index) ? this.subAggregations[index] : null;
  }

  isAllSubAggregationsActive() {
    let allActive = true;
    this.subAggregations.forEach((subAgg) => {
      if (!subAgg.isActive) {
        allActive = false;
      }
    });
    return allActive;
  }
}

export interface IFilter {
  field: string;
  subField?: string;
  nestedPath?: string;
  label: string;
  aggregations: Aggregation[];
  type: string;
  index: string;
  widget: string;
}
export class Filter {
  field: string;
  subField?: string;
  nestedPath?: string;
  label: string;
  aggregations: Aggregation[];
  type: string;
  index: string;
  widget: string;
  start: string;
  end: string;

  constructor(filter: IFilter) {
    this.field = filter.field;
    this.subField = (filter.subField) ? filter.subField : '';
    this.nestedPath = (filter.nestedPath) ? filter.nestedPath : '';
    this.label = filter.label;
    this.aggregations = (filter.aggregations) ? filter.aggregations : [];
    this.type = filter.type;
    this.index = filter.index;
    this.widget = filter.widget;
  }

  findInAggregationsAndSubAggregations(key: string, aggregationParentKey = '') {
    let aggregationFound = null;

    this.aggregations.forEach((aggregation) => {
      if (aggregation.key === key && aggregationParentKey === '') {
        aggregationFound = aggregation;
      } else if (aggregation.subAggregations.length > 0) {
        const index = aggregation.subAggregations.findIndex(el => el.key === key);
        const subAggregation = aggregation.subAggregations[index];
        if (subAggregation) {
          if (subAggregation.parentAggregation &&
            subAggregation.parentAggregation.key === aggregationParentKey) {
            aggregationFound = subAggregation;
          }
        }
      }
    });

    return aggregationFound;
  }

  setAggregationAndSubaggregation(aggregation: Aggregation, value: boolean) {
    if (aggregation) {
      aggregation.isActive = value;
      if (aggregation.subAggregations.length > 0) {
        aggregation.subAggregations.forEach((subAgg: Aggregation) => {
          subAgg.isActive = aggregation.isActive ? true : false;
        });
      }

      // If one subaggregation is updated:
      // - parent aggregation becomes active if all the sub aggregations are active
      // - parent aggregation becomes inactive if NOT all the sub aggregations are active
      if (aggregation.parentAggregation) {
        const parentAggregation = aggregation.parentAggregation;
        if (parentAggregation.isAllSubAggregationsActive()) {
          parentAggregation.isActive = true;
        } else {
          parentAggregation.isActive = false;
        }
      }
    }
  }

  findActiveAggregations() {
    const activeAgreggations = [];
    this.aggregations.forEach((aggregation) => {
      if (aggregation.isActive) {
        activeAgreggations.push(aggregation);
      } else if (aggregation.subAggregations.length > 0) {
        aggregation.subAggregations.forEach((subAgg) => {
          if (subAgg.isActive) {
            activeAgreggations.push(subAgg);
          }
        });
      }
    });
    return activeAgreggations;
  }
}

export interface IActiveFiltersTemplate {
  aggregation: Aggregation;
  filter: Filter;
}
