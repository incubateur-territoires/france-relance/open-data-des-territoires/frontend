export { Data, IData } from '../../shared/models';
// tslint:disable-next-line: max-line-length
export { ElasticsearchOptions, ICountScope, IElasticsearchOptions, IPostsESOptions, IScope } from './elasticsearch-options.model';
export { IElasticsearchHit, IElasticsearchResponse } from './elasticsearch-response.model';
export { Aggregation, Filter, IActiveFiltersTemplate, IFilter } from './filter.model';
export { ISortOption } from './sort-option.model';

