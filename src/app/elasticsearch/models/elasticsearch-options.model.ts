import { geosource } from '../../../i18n/traductions';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { scopesResearch, isRentertron } from '../../shared/variables';
import { Filter } from './filter.model';
import { ISortOption } from './sort-option.model';

export interface IPostsESOptions {
  type?: string;
  id?: string;
  slug?: string;
  content?: string;
  status?: string;
  shouldHighlight?: boolean;
  size?: number;
  sortDate?: boolean;
  includeStaticContent?: boolean;
  featured?: boolean;
  relatedDatasetSlug?: string;
}

export interface IScope {
  key: string;
  label: string;
  elasticType: string[];
  errorItem?: string;
  placeholder?: string;
}

export interface IElasticsearchOptions {
  pageIndex?: number;
  from?: number;
  pageSize?: number;
  sortOptions?: ISortOption;
  searchString?: string;
  filters?: Filter[];
  shouldAggregateResultCount: boolean;
  shouldAggregateFilters: boolean;
  scope: IScope;
  useCache: boolean;
}

export class ElasticsearchOptions {
  APP_CONFIG = APP_CONFIG;
  _pageIndex: number;
  from?: number;
  _pageSize: number;
  _sortOptions: ISortOption;
  _searchString: string;
  _filters: Filter[];
  _allfilters: Filter[];
  shouldAggregateResultCount: boolean;
  shouldAggregateFilters: boolean;
  fromAutocompletion: boolean;
  scope: IScope;
  useCache: boolean;
  otherLicenceAggregations: string[];

  constructor(data?) {
    this.from = (data && data.from != null) ? data.from : 0;
    this._pageSize = (data && data.pageSize != null) ? data.pageSize : 5;
    this._pageIndex = (data && data.pageIndex != null) ? data.pageIndex : 0;
    this._searchString = (data && data.searchString != null) ? data.searchString : '';
    this.shouldAggregateResultCount = true;
    this.shouldAggregateFilters = true;
    this.fromAutocompletion = false;
    this.scope = (data.scope) ? data.scope : scopesResearch.all;
    this.otherLicenceAggregations = [];
    this.useCache = (data && data.useCache != null) ? data.useCache : true;

    // Need to be done in order to avoid object reference copy
    this._sortOptions = {
      value: (data && data.searchString != null && this.scope.key != scopesResearch.post.key) ? 'relevance' : 'date',
      order: 'desc',
    };
    if (data && data.sortOptions != null) {
      this._sortOptions.value = data.sortOptions.value;
      this._sortOptions.order = data.sortOptions.order;
    }
    if (isRentertron) {
      this._sortOptions = {
        value: 'alphabetical',
        order: 'asc',
      };
    }

    let defaultFilterList=[
      new Filter({
        field: 'metadata-fr.topicCat',
        label: geosource.filterCategories.categories,
        aggregations: [],
        type: 'string',
        index: 'dataset',
        widget: 'checkbox',
      }),
      new Filter({
        field: 'metadata-fr.responsibleParty.organisationName',
        subField: 'metadata-fr.responsibleParty.individualName',
        nestedPath: 'metadata-fr.responsibleParty',
        label: geosource.filterCategories.dataProducers,
        aggregations: [],
        type: 'string',
        index: 'dataset',
        widget: 'combo',
      }),
      new Filter({
        field: 'metadata-fr.kind',
        label: geosource.filterCategories.type,
        aggregations: [],
        type: 'string',
        index: 'dataset',
        widget: 'checkbox',
      }),
      new Filter({
        field: 'metadata-fr.granularity',
        label: geosource.filterCategories.granularity,
        aggregations: [],
        type: 'string',
        index: 'dataset',
        widget: 'checkbox',
      }),
      new Filter({
        field: 'extras.observatoire',
        label: 'Observatoire',
        aggregations: [],
        type: 'string',
        index: 'dataset',
        widget: 'hidden',
      }),
      new Filter({
        field: 'metadata-fr.license',
        label: geosource.filterCategories.licences,
        aggregations: [],
        type: 'string',
        index: 'dataset',
        widget: 'checkbox',
      }),
      new Filter({
        field: 'metadata-fr.link.service',
        label: geosource.filterCategories.services,
        aggregations: [],
        type: 'string',
        index: 'dataset',
        widget: 'checkbox',
      }),
      new Filter({
        field: 'metadata-fr.link.formats',
        label: geosource.filterCategories.formats,
        aggregations: [],
        type: 'string',
        index: 'dataset',
        widget: 'checkbox',
      }),
      /*
      new Filter({
        field: 'metadata-fr.publicationDate',
        label: geosource.filterCategories.publicationYear,
        aggregations: [],
        type: 'date',
        index: 'dataset',
        widget: 'checkbox',
      }),
      */
      new Filter({
        field: 'metadata-fr.publicationDate',
        label: geosource.filterCategories.publicationDate,
        aggregations: [],
        type: 'dateRange',
        index: 'dataset',
        widget: 'dateRange',
      }),
      new Filter({
        field: 'metadata-fr.updateFrequency',
        label: geosource.filterCategories.updateFrequency,
        aggregations: [],
        type: 'string',
        index: 'dataset',
        widget: 'checkbox',
      }),
      new Filter({
        field: 'metadata-fr.territory',
        label: geosource.filterCategories.territory,
        aggregations: [],
        type: 'string',
        index: 'dataset',
        widget: 'checkbox',
      }),
    ];
    if(this.APP_CONFIG.filterConfig && this.APP_CONFIG.filterConfig.length>0){
      defaultFilterList=this.APP_CONFIG.filterConfig.map(x=>new Filter(x));
    }
    

    this._filters = (data && data.filters != null) ? data.filters : defaultFilterList;
    this._allfilters = this._filters;
  }

  setElasticsearchOptions(data: IElasticsearchOptions) {
    this.from = (data && data.from != null) ? data.from : 0;
    this._pageSize = (data && data.pageSize != null) ? data.pageSize : 5;
    this._pageIndex = (data && data.pageIndex != null) ? data.pageIndex : 0;
    this._searchString = (data && data.searchString != null) ? data.searchString : '';
    this.shouldAggregateResultCount = true;
    this.shouldAggregateFilters = true;
    this.fromAutocompletion = false;
    this.scope = (data.scope) ? data.scope : scopesResearch.all;
    this.otherLicenceAggregations = [];
    this.useCache = (data && data.useCache != null) ? data.useCache : true;
    // Need to be done in order to avoid object reference copy
    this._sortOptions = {
      value: (data && data.searchString != null && this.scope.key != scopesResearch.post.key) ? 'relevance' : 'date',
      order: 'desc',
    };
    if (data && data.sortOptions != null) {
      this._sortOptions.value = data.sortOptions.value;
      this._sortOptions.order = data.sortOptions.order;
    }
  }

  // Known licences
  get knownLicences() {
    return ['Licence Ouverte', 'Licence ODbL', 'Licence de réutilisation des données d\'intérêt général', 'Licence Mobilités'];
  }

  // Getters

  get pageIndex(): number {
    return this._pageIndex;
  }

  get pageSize(): number {
    return this._pageSize;
  }

  get sortOptions(): ISortOption {
    return this._sortOptions;
  }

  get searchString(): string {
    return this._searchString;
  }

  get filters(): Filter[] {
    return this._filters;
  }

  // Setter

  set pageIndex(page: number) {
    this._pageIndex = page;
  }

  set pageSize(size: number) {
    this._pageSize = size;
  }

  set sortOptions(options: ISortOption) {
    this._sortOptions = options;
  }

  set searchString(value: string) {
    this._searchString = value;
  }

  set filters(filters: Filter[]) {
    this._filters = filters;
  }
}

export interface ICountScope {
  scopeKey: string;
  count: number;
}
