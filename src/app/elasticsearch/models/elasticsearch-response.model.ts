export interface IElasticsearchHit {
  '_id': string;
  '_index': string;
  '_score': number;
  '_source': any;
  'uuid': string;
  'slug': string;
  'inner_hits': {
    'data': {
      'hits': {
        'total': number,
        'hits': any[],
      },
    },
  };
}

export interface IElasticsearchResponse {
  'took': number;
  'hits': {
    'total': number,
    'max_score': number,
    'hits': IElasticsearchHit[],
  };
  'suggest': any;
  'aggregations': any;
}
