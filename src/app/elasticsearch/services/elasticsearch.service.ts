import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { geosource, notificationMessages } from '../../../i18n/traductions';
import { ErrorService } from '../../core/services';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { scopesResearch } from '../../shared/variables';
import { Aggregation, ElasticsearchOptions, Filter, IElasticsearchResponse, IPostsESOptions } from '../models';

/*
  This service is in charge to get the content from Elasticsearch. This includes
  - all the metadata and data from datasets
  - editorial content from Ghost
*/

@Injectable()
export class ElasticsearchService {

  geosource = geosource;
  elasticSearchUrl: string;
  APP_CONFIG = APP_CONFIG;

  constructor(
    private _errorService: ErrorService,
    private _http: HttpClient,
  ) {
    this.elasticSearchUrl = `${APP_CONFIG.backendUrls.elasticsearch}` + '?request_cache=true';
  }
  headers:any = {
    'X-CSRFToken': (name => {
      var re = new RegExp(name + "=([^;]+)");
      var value = re.exec(document.cookie);
      return (value != null) ? unescape(value[1]) : null;
    })('csrftoken'),
  };

  getHeaders(){
    if(this.headers && this.headers['X-CSRFToken']==null){
      delete this.headers['X-CSRFToken'];
    }
    return this.headers;
  }
  /**
   *  This is the main elasticsearch (ES) request to get information from ES
   *  We have one ES options as parameter used for multiple purposes:
   *  - research, sorting, paginations, highlight, etc...
   */
  getAllEntities(options: ElasticsearchOptions): Observable<IElasticsearchResponse> {
    const requestOptions = this.constructElasticsearchRequest(options);
    
    requestOptions.headers=this.getHeaders();
    return this._http.request<IElasticsearchResponse>('POST', this.elasticSearchUrl, requestOptions).pipe(
      map((e) => {
        return e;
      }),
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getResults });
        },
      ),
    );
  }

  // Get the metadata. For this reason we add the 'metadata-fr' source where all the metadata content is.
  getDatasetMetadata(slugOrUuid: string): Observable<IElasticsearchResponse> {
    return this._http.request<IElasticsearchResponse>('POST', this.elasticSearchUrl, {
      headers:this.getHeaders(),
      withCredentials: true,
      body: {
        from: 0,
        size: 1,
        _source: ['metadata-fr', 'editorial-metadata', 'uuid', 'fields', 'slug', 'extras'],
        query: {
          bool: {
            should: [
              {
                term: {
                  'slug.keyword': slugOrUuid,
                },
              },
              {
                term: {
                  'metadata-fr.geonet:info.uuid.keyword': slugOrUuid,
                },
              },
            ],
          },
        },
      },
    }).pipe(
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getDatasetById });
        },
      ),
    );
  }

  // Each dataset have one unique slug and uuid. This method get the slugs
  // from a list of uuids. (this is useful for routing purpose for example)
  getSlugsFromUuid(uuids: string[]): Observable<IElasticsearchResponse> {
    return this._http.request<IElasticsearchResponse>('POST', this.elasticSearchUrl, {
      headers:this.getHeaders(),
      body: {
        from: 0,
        size: 1000,
        _source: ['slug', 'metadata-fr'],
        query: {
          terms: {
            'metadata-fr.geonet:info.uuid.keyword': uuids,
          },
        },
        collapse: {
          field: 'metadata-fr.geonet:info.uuid.keyword',
        },
      },
    }).pipe(
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getDatasetById });
        },
      ),
    );
  }

  // This retrieves the data of a dataset. The query is done by uuid.
  getDatasetData(id: number | string, options?: ElasticsearchOptions,query?: any): Observable<IElasticsearchResponse> {

    // Set the pagination
    const from = options ? options.from : 0;
    const size = options ? options.pageSize : 10;

    // Retrieve only the data (not the editorial-metadata, metadata, etc..)
    // This is done by setting 'data-fr' as _source.
    const body = {
      from,
      size,
      _source: ['data-fr'],
      query: {
        bool: {
          filter: {
            term: {
              'metadata-fr.geonet\:info.uuid.keyword': id,
            },
          },
        },
      },
    };

    if (options.sortOptions.value !== null) {
      const sort = {};
      const key = `data-fr.properties.${options.sortOptions.value}.sort`;
      sort[key] = {
        order: options.sortOptions.order,
        unmapped_type: 'string'
      };
      body['sort'] = [sort];
    }
    // Here the default_field is 'data', meaning that the query
    if (options.searchString !== '') {
      const searchString = this.escapeSpecialCharacters(options.searchString, options.fromAutocompletion, 'AND');
      body.query.bool['must'] = [
        {
          query_string: {
            query: searchString,
            /*default_field: '*',*/
            fields: query,
            analyzer: 'my_search_analyzer',
            fuzziness: 'AUTO',
            minimum_should_match: '90%',
          },
        },
      ];
    }

    // This is an option to use an nginx cache or not.
    // We usually use it, except for real time data when you want all the time the
    // latest and more fresh data.
    let header=this.getHeaders();
    if(!options.useCache){
      header.nocache='True';
    }

    return this._http.request<IElasticsearchResponse>('POST', this.elasticSearchUrl, {
      body,
      headers:header,
      withCredentials: true,
    }).pipe(
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getDatasetById });
        },
      ),
    );
  }

  /**
  * This request will get the completion values based on highlight request from one query text
  */
  getAutoComplete(text: string,filter:any=undefined): Observable<IElasticsearchResponse> {
    const regex = text.replace(/[\*\+\-=~><\"\?^\${}\(\)\:\!\/[\]\\\s]/g, '\\$&')
      .replace(/\|\|/g, '\\||') // replace ||
      .replace(/\&\&/g, '\\&&'); // replace &&

    const request = {
      _source: false, // We need only the highlight, not the hits
      query: {
        bool: {
          must: [
          ],
        },
      },
      highlight: {
        order: 'score',
        pre_tags: ['<b>'],
        post_tags: ['</b>'],
        highlight_query: {
          query_string: {
            query: regex,
            fields: ['data_and_metadata', 'content-fr.title', 'content-fr.plaintext'],
            analyzer: 'my_search_analyzer',
            fuzziness: 'AUTO',
            minimum_should_match: '90%',
          },
        },
        // Rules to construct the highlight fragments
        fields: {
          '*data-fr.*': {
            fragment_size: 50,
            fragmenter: 'span',
            type: 'unified',
            require_field_match: false,
          },
          'content-fr.title': {
            number_of_fragments: 0,
          },
          'content-fr.plaintext': {
            fragment_size: 50,
            number_of_fragments: 5,
          },
        },
      },
    };

    request.query.bool.must.push({
      multi_match: {
        query: regex,
        fields: ['content-fr.title', 'content-fr.plaintext', 'data_and_metadata'],
      },
    });
    if(filter){
      request.query.bool.must.push(filter);
    }


    return this._http.request<IElasticsearchResponse>('POST', this.elasticSearchUrl, {
      headers:this.getHeaders(),
      body: request,
      withCredentials: true,
    }).pipe(
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getAutocomplete });
        },
      ),
    );
  }

  /**
  * This request will get one phrase suggestion out of the query text
  * cf. https://www.elastic.co/guide/en/elasticsearch/reference/current/search-suggesters-phrase.html
  */
  getSuggestion(query: string): Observable<IElasticsearchResponse> {
    return this._http.request<IElasticsearchResponse>('POST', this.elasticSearchUrl, {
      headers:this.getHeaders(),
      withCredentials: true,
      body: {
        suggest: {
          text: query,
          suggestion: {
            phrase: {
              field: 'data_and_metadata.suggest',
              // as only the very first suggestion will be used, let's limit the size of the results to 1
              size: 1,
              max_errors: query.split(' ').length,
              highlight: {
                pre_tag: '<b><i>',
                post_tag: '</i></b>',
              },
              analyzer: 'my_search_analyzer',
              real_word_error_likelihood: 0.50,
              direct_generator: [{
                field: 'data_and_metadata.suggest',
                suggest_mode: 'missing',
                prefix_length: 1,
                min_word_length: 4,
              }],
            },
          },
        },
      },
    }).pipe(
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getSuggestion });
        },
      ),
    );
  }

  getTextQuery(regex:string) {
    // This defines the query text expression.
    // We attribute difference weight depending on the field.
    // For example if the keyword we look for is in the title of the metadata, the score
    // will be higher than if we find it in the abstract.
    return {
      bool:{
        should:
          [
            { 
              query_string:{
                query: regex,
                fields: ['data_and_metadata', 'metadata-fr.title^5', 'metadata-fr.abstract^3',
                  'content-fr.title^5', 'content-fr.excerpt^3', 'content-fr.plaintext'],
                analyzer: 'my_search_analyzer',
                fuzziness: 'AUTO',
                minimum_should_match: '90%',
                default_operator:'AND',
                boost:5
              },
            },
            { 
              query_string:{
                query: regex,
                fields: ['data_and_metadata', 'metadata-fr.title^5', 'metadata-fr.abstract^3',
                  'content-fr.title^5', 'content-fr.excerpt^3', 'content-fr.plaintext'],
                analyzer: 'my_search_analyzer',
                fuzziness: 'AUTO',
                minimum_should_match: '90%',
                default_operator:'OR',
                boost:1
              },
            },
        ]
      }
    }
  }

  constructElasticsearchRequest(options: ElasticsearchOptions) {
    const from = options.pageSize * options.pageIndex;

    const searchString = this.escapeSpecialCharacters(options.searchString, options.fromAutocompletion);

    const regex = (options.searchString !== '') ? searchString : '*';
    

    // Set query string options
    const requestOptions = {
      headers:this.getHeaders(),
      withCredentials: true,
      body: {
        from,
        size: options.pageSize,
        query: {
          bool: {
          },
        },
        "_source": {
          "exclude" : ["_dataset"]
        },
        // We collapse all the results by uuid
        collapse: {
          field: 'uuid.keyword',
          inner_hits: {
            name: 'data',
            size: 3,
            highlight: {
              pre_tags: ['<span class="highlighted">'],
              post_tags: ['</span>'],
              require_field_match: false,
              type: 'unified',
              fragmenter: 'span',
              order: 'score',
              highlight_query: this.getTextQuery(regex),
              /*{
                query_string: {
                  query: regex,
                  fields: ['data_and_metadata', 'metadata-fr.title', 'metadata-fr.abstract',
                    'content-fr.title', 'content-fr.plaintext'],
                  analyzer: 'my_search_analyzer',
                  fuzziness: 'AUTO',
                  minimum_should_match: '90%',
                },
              },*/
              fields: {
                '*data-fr.*': {
                  fragment_size: 100,
                  number_of_fragments: 1,
                },
                'metadata-fr.title': {
                  number_of_fragments: 0,
                },
                'content-fr.title': {
                  number_of_fragments: 0,
                },
                'content-fr.plaintext': {
                  fragment_size: 50,
                  number_of_fragments: 1,
                },
              },
            },
          },
        },
        aggregations: {
        },
      },
    };

    // Adding sort options
    // We look on multiple fields and add the unmapped_type property to avoid the fail request (because
    // the request is done for multiple indexes)
    switch (options.sortOptions.value) {
      case 'date':
        requestOptions.body['sort'] = [
          {
            'metadata-fr.publicationDate': {
              order: options.sortOptions.order,
              unmapped_type: 'date',
            },
          },
          {
            'content-fr.published_at': {
              order: options.sortOptions.order,
              unmapped_type: 'date',
            },
          },
        ];
        break;
      case 'alphabetical':
        requestOptions.body['sort'] = [
          {
            'metadata-fr.title.keyword': {
              order: options.sortOptions.order,
              unmapped_type: 'keyword',
            },
          },
          {
            'content-fr.title.keyword': {
              order: options.sortOptions.order,
              unmapped_type: 'keyword',
            },
          },
        ];
        break;
      case 'relevance':
        requestOptions.body['sort'] = [
          {
            _score: options.sortOptions.order,
          },
        ];
        break;
      default:
        break;
    }


    const mustExpression: Object[] = [];
    mustExpression.push(this.getTextQuery(regex));

    // Filters by aggregations
    let shouldExpression: Object[] = [];

    options.filters.forEach((filter) => {
      const activeAggregations = filter.findActiveAggregations();
      if(filter.type=='dateRange' && (filter.start||filter.end)) {
        console.log("range filter")
        let expression={
          range: {
            [filter.field]: {
              gte: undefined,
              lte: undefined,
            },
          },
        };
        if(filter.start && filter.start.length>0) {
          expression.range[filter.field].gte=new Date(filter.start+" 00:00:00").getTime();
        }
        if(filter.end && filter.end.length>0) {
          expression.range[filter.field].lte=new Date(filter.end+" 23:59:59").getTime();
        }

        shouldExpression.push(expression);
      } else if (activeAggregations.length > 0) {
        activeAggregations.forEach((activeAgg: Aggregation) => {
          let field = `${activeAgg.field}`;

          if (filter.type === 'date') {
            const date = new Date(Number(activeAgg.key));
            shouldExpression.push({
              range: {
                [field]: {
                  gte: date.getFullYear(),
                  lt: (date.getFullYear() + 1),
                },
              },
            });
          } else {
            field += '.keyword';
            if (filter.subField) {
              // if we have a nested field, we need to create a AND query.
              // ex: organizationName = "Métropole de Lyon" AND individualName = "DINSI".
              // For this, we add a must condition inside the should of the nested field
              let mustNested = null;
              if (activeAgg.parentAggregation) {
                const fieldParent = `${activeAgg.parentAggregation.field}.keyword`;

                mustNested = {
                  nested: {
                    path: filter.nestedPath,
                    ignore_unmapped: true,
                    query: {
                      bool: {
                        must: [
                          {
                            match: {
                              [field]: activeAgg.key,
                            },
                          },
                          {
                            match: {
                              [fieldParent]: activeAgg.parentAggregation.key,
                            },
                          },
                        ],
                      },
                    },
                  },
                };
              } else {
                mustNested = {
                  nested: {
                    path: filter.nestedPath,
                    ignore_unmapped: true,
                    query: {
                      term: {
                        [field]: activeAgg.key,
                      },
                    },
                  },
                };
              }
              shouldExpression.push(mustNested);
            } else {
              // Special treatment for the licences => désactivé pour PIGMA => && APP_CONFIG.theFunctionalitiesInterruptor.processLicences 
              if (filter.label === geosource.filterCategories.licences && APP_CONFIG.theFunctionalitiesInterruptor.processLicences && 
                !options.knownLicences.includes(activeAgg.label)) {
                options.otherLicenceAggregations.forEach((agg) => {
                  shouldExpression.push({
                    term: {
                      'metadata-fr.license.keyword': agg,
                    },
                  });
                });
              } else {
                if(activeAgg.key=='Non Renseigné'){
                  shouldExpression.push({
                    bool: {
                      "must_not": [
                        { "exists": { "field": filter.field}}
                      ]}
                  });
                } else {
                  shouldExpression.push({
                    term: {
                      [field]: activeAgg.key,
                    },
                  });
                }
              }
            }
          }
        });
      }

      // Filter is not taken into account for the scorring
      if (shouldExpression.length > 0) {
        mustExpression.push(
          {
            bool: {
              should: shouldExpression, // should expression act like an logic "OR"
            },
          },
        );
        shouldExpression = [];
      }
    });

    // Must is act like an logic "AND"
    requestOptions.body.query.bool['must'] = mustExpression;

    // Filter by scope (dataset, service, post, page). If the scope is 'all', we don't apply any filter
    const filtersScope = [];
    options.scope.elasticType.forEach((type) => {
      filtersScope.push(type);
    });
    // For the scopes we use post_filter instead of filter.
    // The difference is that the aggregations will be calculated before this filter is applied.
    requestOptions.body['post_filter'] = {
      terms: {
        'type.keyword': filtersScope,
      },
    };


    // Aggregations for the filter options
    // We have different filters determined by 'index' property.
    // We apply aggregations only to the concerned ES index.
    if (options.shouldAggregateFilters) {
      options.filters.forEach((filter) => {
        this.setAggregationFilter(requestOptions, filter);
      });
    }

    // Aggregations used to count the results for each type of result (eg. scopes)
    requestOptions.body['aggregations']['count_by_scope'] = {
      terms: {
        field: 'type.keyword',
        order: {
          _key: 'asc',
        },
        size: 500,
      },
      aggs: {
        count: {
          cardinality: {
            field: 'uuid.keyword',
          },
        },
      },
    };

    return requestOptions;

  }

  // CReate the filters depending on the type
  private setAggregationFilter(requestOptions: any, filter: Filter) {
    let field = filter.field;
    field = filter.type === 'string' ? `${field}.keyword` : field;
    const countPerMetadata = {
      cardinality: {
        field: 'uuid.keyword',
      },
    };
    if (filter.type === 'date') {
      requestOptions.body['aggregations'][filter.field] = {
        date_histogram: {
          field,
          interval: 'year',
          min_doc_count: 1,
          order: {
            _count: 'desc',
          },
        },
        aggs: {
          count_per_metadata: countPerMetadata,
        },
      };
    } else {
      const aggregationMaxSize = 1500;
      // If a subfield exists, we add one more agg depth
      if (filter.subField) {
        const countPerMetadata = {
          reverse_nested: {},
          aggregations: {
            uuid: {
              cardinality: {
                field: 'uuid.keyword',
              },
            },
          },
        };

        const subFieldAggName = `${filter.subField}`;
        
        requestOptions.body['aggregations'][filter.field] = {
          nested: {
            path: filter.nestedPath,
          },
          aggs: {
            [filter.field]: {
              terms: {
                field,
                order: { _key: 'asc' },
                size: aggregationMaxSize,
              },
              aggs: {
                count_per_metadata: countPerMetadata,
                [subFieldAggName]: {
                  terms: {
                    field: `${filter.subField}.keyword`,
                    order: {
                      _key: 'asc',
                    },
                    size: aggregationMaxSize,
                  },
                  aggs: {
                    count_per_metadata: countPerMetadata,
                  },
                },
              },
            },
          },

        };
      } else {
        requestOptions.body['aggregations'][filter.field] = {
          terms: {
            field,
            order: { _key: 'asc' },
            size: aggregationMaxSize,
          },
          aggs: {
            count_per_metadata: countPerMetadata,
          },
        };
      }
    }
  }

  /**
  * Escape special characters except logical operators.
  * */
  escapeSpecialCharacters(searchString: string, fromAutocompletion: boolean, joinOperator = '+') {

    let escapedSearchString = '';
    /** If the request:
    * - no contains logical operators (AND, OR and NOT) or
    * - comes from the autocompletion suggestion
    * =>  we do escape the "(" and ")" characters
    */
    if (fromAutocompletion ||
      (!searchString.toUpperCase().includes('AND ') &&
        !searchString.toUpperCase().includes('OR ') &&
        !searchString.toUpperCase().includes('KEYWORD') &&
        !searchString.toUpperCase().includes('NOT '))) {
      escapedSearchString = searchString
        .replace(/[\=~><\"\?^\${}\(\)\|\&\:\!\/[\]\\]/g, '\\$&');

      // We join each words with the '+' logical operator to accurate the results
      const words = escapedSearchString.split(/\s+/);
      escapedSearchString = words.join(` ${joinOperator} `);

      /** If not all this, we don't escape the "()" (and will be interpreted as logical characters by ES)
       *  but we still do escape some other special characters
      */
    } else {
      escapedSearchString = searchString
        .replace(/[\=~><\"\?^\${}\!\|\&\/[\]\\]/g, '\\$&');
    }

    return escapedSearchString;
  }

  // Get the number of datasets for by organizations using the aggregations.
  getNumberDatasetsByOrganization(): Observable<IElasticsearchResponse> {
    // Set query string options
    const requestOptions = {
      headers:this.getHeaders(),
      withCredentials: true,
      body: {
        from: 0,
        size: 0,
        aggregations: {
          nested_agg: {
            nested: {
              path: 'metadata-fr.responsibleParty',
            },
            aggs: {
              organisationName: {
                terms: {
                  field: 'metadata-fr.responsibleParty.organisationName.keyword',
                  size: 5000,
                },
                aggs: {
                  scope: {
                    reverse_nested: {},
                    aggs: {
                      type_agg: {
                        terms: {
                          field: 'metadata-fr.type.keyword',
                          size: 500,
                        },
                        aggs: {
                          count_per_metadata: {
                            reverse_nested: {},
                            aggs: {
                              cid: {
                                cardinality: {
                                  field: 'metadata-fr.geonet:info.uuid.keyword',
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };

    return this._http.request<IElasticsearchResponse>('POST', this.elasticSearchUrl, requestOptions).pipe(
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getResults });
        },
      ),
    );
  }

  getDatasetChildren(uuid: string): Observable<IElasticsearchResponse> {
    return this._http.request<IElasticsearchResponse>('POST', this.elasticSearchUrl, {
      headers:this.getHeaders(),
      withCredentials: true,
      body: {
        size: 500,
        _source: ['metadata-fr.title', 'metadata-fr.geonet:info.uuid', 'metadata-fr.image', 'slug'],
        query: {
          term: {
            'metadata-fr.parentId.keyword': uuid,
          },
        },
        // Collapse by children uuid in order not to count every data
        collapse: {
          field: 'metadata-fr.geonet:info.uuid.keyword',
        },
        // Count the number of different children uuid
        aggs: {
          children_number: {
            cardinality: {
              field: 'metadata-fr.geonet:info.uuid.keyword',
            },
          },
        },
      },
    }).pipe(
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getDatasetChildren });
        },
      ),
    );
  }

  getDatasetParentInfo(uuid: string): Observable<IElasticsearchResponse> {
    return this._http.request<IElasticsearchResponse>('POST', this.elasticSearchUrl, {
      headers:this.getHeaders(),
      withCredentials: true,
      body: {
        _source: ['metadata-fr.title', 'metadata-fr.image', 'slug'],
        query: {
          term: {
            'metadata-fr.geonet\:info.uuid.keyword': uuid,
          },
        },
      },
    }).pipe(
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getDatasetParentInfo });
        },
      ),
    );
  }

  // --------------------- CMS CONTENT -------------------------- //

  /**
  * Get raw articles with filter options (id, slug and content)
  */
  getPosts(options?: IPostsESOptions): Observable<IElasticsearchResponse> {
    const requestOptions = {
      headers:this.getHeaders(),
      withCredentials: true,
      body: {
        query: {
          bool: {
          },
        },
      },
    };
    if (options.type) {
      requestOptions.body.query.bool['filter'] = {
        term: {
          'type.keyword': options.type,
        },
      };
    }

    if (options.id) {
      requestOptions.body.query.bool['must'] = {
        term: {
          'uuid.keyword': options.id,
        },
      };
    } else if (options.slug) {
      requestOptions.body.query.bool['must'] = {
        term: {
          'content-fr.slug.keyword': options.slug,
        },
      };
    } else if (options.content) {
      requestOptions.body.query.bool['must'] = {
        match: {
          'content-fr.content': options.content,
        },
      };
    } else if (options.status) {
      requestOptions.body.query.bool['must'] = {
        match: {
          'content-fr.status': options.status,
        },
      };
    } else if (options.featured) {
      requestOptions.body.query.bool['must'] = {
        match: {
          'content-fr.featured': options.featured,
        },
      };
    }
    if (options.relatedDatasetSlug) {
      requestOptions.body.query.bool['must'] = {
        term: {
          'relations.datasets.slugs.keyword': options.relatedDatasetSlug,
        },
      };
    }

    if (options.size) {
      requestOptions.body['size'] = options.size;
    }

    if (options.sortDate) {
      requestOptions.body['sort'] = [
        {
          'content-fr.published_at': { order: 'desc', unmapped_type: 'date' },
        },
      ];
    }

    return this._http.request<IElasticsearchResponse>('POST', this.elasticSearchUrl, requestOptions).pipe(
      catchError(
        (error) => {
          throw this._errorService.handleError(error, { message: notificationMessages.edito.getPost });
        },
      ),
    );
  }
}
