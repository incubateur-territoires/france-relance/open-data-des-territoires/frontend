// tslint:disable-next-line:variable-name
export const AppRoutes = {
  root: {
    url: '',
    title: {
      fr: 'Pigma',
      en: 'Pigma',
    },
  },
  signin: {
    uri: 'connexion',
    title: {
      fr: 'Connexion',
      en: 'Log In',
    },
  },
  signup: {
    uri: 'inscription',
    title: {
      fr: 'Inscription',
      en: 'Sign up',
    },
  },
  passwordForgotten: {
    uri: 'mot-de-passe-oublie',
    title: {
      fr: 'Mot de passe oublié',
      en: 'Forgotten password',
    },
  },
  passwordReset: {
    uri: 'reinitialiser-mon-mot-de-passe',
    title: {
      fr: 'Reinitialiser mon mot de passe',
      en: 'Reset my password',
    },
  },
  accessManagement: {
    uri: 'gerer-les-acces',
    title: {
      fr: 'Gérer les accès',
      en: 'Manage accesses',
    },
  },
  userServices: {
    uri: 'mes-acces',
    title: {
      fr: 'Mes accès aux données',
      en: 'Data accesses',
    },
  },
  availableData: {
    uri: 'donnees-disponibles',
    title: {
      fr: 'Données disponibles',
      en: 'Available data',
    },
  },
  userProfil: {
    uri: 'mon-profil',
    title: {
      fr: 'Mon profil',
      en: 'My profile',
    },
  },
  userInfo: {
    uri: 'mes-informations',
    title: {
      fr: 'Mes informations',
      en: 'My information',
    },
  },
  userPasswordUpdate: {
    uri: 'modifier-mon-mot-de-passe',
    title: {
      fr: 'Modifier mon mot de passe',
      en: 'Change my password',
    },
  },
  home: {
    uri: 'accueil',
    title: {
      fr: 'Accueil',
      en: 'Home',
    },
  },
  approach: {
    uri: 'demarche',
    title: {
      fr: 'Démarche',
      en: 'Approach',
    },
  },
  documentation: {
    uri: 'documentation',
    title: {
      fr: 'Documentation',
      en: 'Documentation',
    },
  },
  credits: {
    uri: 'credits',
    title: {
      fr: 'Crédits',
      en: 'Credits',
    },
  },
  cgu: {
    uri: 'cgu',
    title: {
      fr: 'Conditions générales d\'utilisation',
      en: 'Terms of use',
    },
  },
  beginners: {
    uri: 'debutants',
    title: {
      fr: 'Débutants',
      en: 'Beginners',
    },
  },
  accessibility: {
    uri: 'accessibilite',
    title: {
      fr: 'Accessibilité',
      en: 'Accessibility',
    },
  },
  contact: {
    uri: 'contact',
    title: {
      fr: 'Contact',
      en: 'Contact',
    },
  },
  siteMap: {
    uri: 'plan-du-site',
    title: {
      fr: 'Plan du site',
      en: 'Site map',
    },
  },
  changelog: {
    uri: 'dernieres-evolutions',
    title: {
      fr: 'Dernières évolutions',
      en: 'Last changes',
    },
  },
  partners: {
    uri: 'partenaires',
    title: {
      fr: 'Partenaires',
      en: 'Partners',
    },
  },
  actors: {
    uri: 'acteurs',
    title: {
      fr: 'Acteurs',
      en: 'Actors',
    },
  },
  legalNotices: {
    uri: 'mentions-legales',
    title: {
      fr: 'Mentions légales',
      en: 'Legal notices',
    },
  },
  personalData: {
    uri: 'donnees-personnelles',
    title: {
      fr: 'Données personnelles',
      en: 'Personal data',
    },
  },
  drafts: {
    uri: 'brouillons',
    title: {
      fr: 'Brouillons',
      en: 'Drafts',
    },
  },
  research: {
    uri: 'recherche',
    title: {
      fr: 'Recherche',
      en: 'Research',
    },
  },
  datasets: {
    uri: 'jeux-de-donnees',
    title: {
      fr: 'Jeux de données',
      en: 'Datasets',
    },
  },
  info: {
    uri: 'info',
    title: {
      fr: 'Information',
      en: 'Information',
    },
  },
  data: {
    uri: 'donnees',
    title: {
      fr: 'Données',
      en: 'Data',
    },
  },
  resources: {
    uri: 'api',
    title: {
      fr: 'API',
      en: 'API',
    },
  },
  downloads: {
    uri: 'telechargements',
    title: {
      fr: 'Téléchargements',
      en: 'Downloads',
    },
  },
  otherResources: {
    uri: 'autre-ressources',
    title: {
      fr: 'Autre ressources',
      en: 'Other rResources',
    },
  },
  contribution: {
    uri: 'contribution',
    title: {
      fr: 'Contribution',
      en: 'Contribution',
    },
    opendata_url: {
      en: 'https://download.data.grandlyon.com/conventions/Version_EN_Convention_Cadre_Data_Partenaire_MetropoledeLyon_Donnees_Ouvertes_avec_Annexe.docx',
      fr: 'https://download.data.grandlyon.com/conventions/Convention_Cadre_Data_Partenaire_MetropoledeLyon_Donnees_Ouvertes_avec_Annexe.docx'
    },
    privatedata_url: {
      en: 'https://download.data.grandlyon.com/conventions/Version_EN_Convention_Cadre_Data_Partenaire_MetropoledeLyon_Donnees_Privees_avec_Annexe.docx',
      fr: 'https://download.data.grandlyon.com/conventions/Convention_Cadre_Data_Partenaire_MetropoledeLyon_Donnees_Privees_avec_Annexe.docx'
    },
  },
  reuses: {
    uri: 'reutilisations',
    title: {
      fr: 'Réutilisations',
      en: 'Reuses',
    },
  },
  news: {
    uri: 'actualites',
    title: {
      fr: 'Actualités',
      en: 'News'
    },
  },
  reusesDetail: {
    uri: 'reutilisations/:id',
    title: {
      fr: 'Détail réutilisation',
      en: 'Reuse detail',
    },
  },
  dataReuses: {
    uri: 'reutilisations',
    title: {
      fr: 'Réutilisations',
      en: 'Reuses',
    },
  },
  error: {
    uri: 'erreur',
    title: {
      fr: 'Erreur',
      en: 'Error',
    },
  },
  page404: {
    uri: 'page-404',
    title: {
      fr: 'Page non trouvée',
      en: 'Page not found',
    },
  },
};
