import { geosource } from '../../i18n/traductions';

export const isRentertron = /HeadlessChrome\//i.test(window.navigator.userAgent);

export const scopesResearch = {
  all: {
    key: 'all',
    label: geosource.researchScope.all,
    elasticType: ['dataset', 'nonGeographicDataset', 'series', 'nonGeographicSeries','service','post'],
    errorItem: geosource.errorItem.all,
  },
  datasets:
  {
    key: 'dataset',
    label: geosource.researchScope.datasets,
    elasticType: ['dataset', 'nonGeographicDataset', 'series', 'nonGeographicSeries'],
    errorItem: geosource.errorItem.datasets,
  },
  services:
  {
    key: 'service',
    label: geosource.researchScope.services,
    elasticType: ['service'],
    errorItem: geosource.errorItem.services,
  },
  post:
  {
    key: 'post',
    label: geosource.researchScope.articles,
    elasticType: ['post'],
    errorItem: geosource.errorItem.posts,
  },
  page:
  {
    key: 'page',
    label: geosource.researchScope.pages,
    elasticType: ['page'],
    errorItem: geosource.errorItem.pages,
  },
};
