import { PreventDefaultDirective } from './prevent-default.directive';
import { DynamicLinks } from './dynamic-links';
import { ClickOutsideDirective } from './click-outside.directive';

export { PreventDefaultDirective, DynamicLinks, ClickOutsideDirective };

// tslint:disable-next-line:variable-name
export const SharedDirectives = [
  PreventDefaultDirective,
  DynamicLinks,
  ClickOutsideDirective,
];
