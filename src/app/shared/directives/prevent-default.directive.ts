import { Directive } from '@angular/core';

@Directive({
  selector: '[preventDefault]',
  host: {
    '(click)': 'onClick($event)',
  },
})
export class PreventDefaultDirective {

  constructor() { }

  onClick(e) {
    e.preventDefault();
    e.stopPropagation();
  }
}
