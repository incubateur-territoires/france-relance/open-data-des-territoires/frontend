/* tslint:disable:member-ordering */
import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { Router } from '@angular/router';

@Directive({
  selector: '[dynamicLinks]',
})
export class DynamicLinks{
  linkClassName = 'routerLink';

  constructor(
    private el: ElementRef,
    private _router: Router,
  ) { }

  @HostListener('click', ['$event']) onClick(e) {
    if (e.target.classList.contains(this.linkClassName)) {
      const link: string = e.target.pathname;

      event.preventDefault();
      event.stopPropagation();

      this._router.navigateByUrl(link.trim());
    }
  }
}
