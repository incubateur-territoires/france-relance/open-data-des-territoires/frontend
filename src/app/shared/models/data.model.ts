export interface IData {
  geometry: IDataGeometry;
  type: string;
  properties: any;
}

interface IDataGeometry {
  coordinates: any[];
  type: string;
}

export class Data implements IData {
  geometry: IDataGeometry;
  type: string;
  properties: any;
  id: number;

  constructor(data?: IData) {
    if (data) {
      this.geometry = data.geometry;
      this.type = data.type;
      this.properties = data.properties;
      this.id = data.properties.gid ? data.properties.gid:data.properties.gid_int;
      this.id = this.id ? this.id:parseInt(data.properties.gid_str);
    }
  }
}
