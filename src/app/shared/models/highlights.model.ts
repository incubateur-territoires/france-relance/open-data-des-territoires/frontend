interface IHighlights {
  highlightedAbstract: string;
  highlightedTitle: string;
  highlightedDescription: string[];
}

export class Highlights implements IHighlights {
  highlightedAbstract: string;
  highlightedTitle: string;
  highlightedSubtitle?: string;
  highlightedDescription: string[];

  constructor(data?: IHighlights) {
    this.highlightedAbstract = (data && data.highlightedAbstract != null) ? data.highlightedAbstract : null;
    this.highlightedTitle = (data && data.highlightedTitle != null) ? data.highlightedTitle : null;
    this.highlightedDescription = (data && data.highlightedDescription != null) ? data.highlightedDescription : [];
  }
}
