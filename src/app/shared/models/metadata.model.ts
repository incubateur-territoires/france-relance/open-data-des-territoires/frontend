import { geosource } from '../../../i18n/traductions';
import { APP_CONFIG } from '../../core/services/app-config.service';

export interface IMetadata {
  'dataset_id'?: string;
  'dataset_index'?: string;
  'geonet\:info': IMetadataGeonet;
  'title': string;
  'abstract': string;
  'keyword': string[];
  'link': IMetadataLink[];
  'responsibleParty': IResponsibleParty[];
  'publicationDate': string;
  'lastUpdateDate':string;
  'topicCat': string[];
  'highlight'?: any;
  'legalConstraints': string[];
  'license': string;
  'rights': string[];
  'total_documents': number;
  'lineage': string;
  'type': string;
  'kind': string[];
  'denominator': number;
  'resolution': string;
  'href':string;
  'granularity':string;
  'usageRecommandation':string;
  'emailContact':string;
  'category': string[];
  'popularity': string;
  'updateFrequency': string;
  'crs': string;
  'native_crs': string;
  'native_bbox': string;
  'parentId'?: string;
  'image': [{
    url: string;
    type: string;
  }];
  spatialRepresentationType_text: string;
}

interface IMetadataGeonet {
  notify: string;
  schema: string;
  createDate: string;
  view: string;
  id: string;
  featured: string;
  download: string;
  changeDate: string;
  dynamic: string;
  '@xmlns:geonet': string;
  source: string;
  uuid: string;
  selected: string;
  isPublishedToAll: string;
}

export interface IResponsibleParty {
  logo: string;
  role: string;
  organisationName: string;
  individualName: string;
  positionName: string;
  address: string;
  email: string;
  appliesTo: string;
  telephone: string;
}

export interface IMetadataLink {
  'description': string;
  'url': string;
  'name': string;
  'file_name': string;
  'content-length': string;
  projections: string[];
  service: string;
  bbox_by_projection: any;
  formats: string[];
}

class MetadataCategory {
  category: string;
  labelCategory: string;
  constructor(category: string) {
    this.category = category;
    this.labelCategory = this.getCategory(this.category);
  }
  getCategory(id): string {
    if(geosource.metadataCategories[id])
      return geosource.metadataCategories[id];
    else 
      return id; 
  }
}

interface IMetadataCategory {
  'category': string;
}

/*
* This represents the differents link formats returned by elasticsearch (feed by Geosource)
* We use these constants in different components. If one day these values might change,
* we can do it here and no need for code changes in the components
*/
export const linkFormats = {
  wfs: 'WFS',
  wms: 'WMS',
  wcs: 'WCS',
  ws: 'WS',
  pdf: 'PDF',
  json: 'JSON',
  kml: 'KML',
};

export const typesMetadata = {
  dataset: 'dataset',
  nonGeographicalDataset: 'nonGeographicalDataset',
  service: 'service',
  series: 'series',
  nonGeographicalSeries: 'nonGeographicSeries',
};

export interface IParentDataset {
  id?: string;
  slug?: string;
  title?: string;
  imageUrl?: string;
}

export interface ILicense {
  matchingKeys?: string[];
  nameFr?: string;
  nameEn?: string;
  acronymFr?: string;
  acronymEn?: string;
  urlFr?: string;
  urlEn?: string;
}

export class License {
  nameFr: string;
  nameEn: string;
  acronymFr: string;
  acronymEn: string;
  urlFr: string;
  urlEn: string;


  constructor(data: ILicense) {
    this.nameFr = data && data.nameFr ? data.nameFr : null;
    this.nameEn = data && data.nameEn ? data.nameEn : null;
    this.acronymFr = data && data.acronymFr ? data.acronymFr : null;
    this.acronymEn = data && data.acronymEn ? data.acronymEn : null;
    this.urlFr = data && data.urlFr ? data.urlFr : null;
    this.urlEn = data && data.urlEn ? data.urlEn : this.urlFr;
  }
}

export class Metadata {
  'dataset_id'?: string;
  'dataset_index'?: string;
  title: string;
  subtitle: string;
  abstract: string;
  abstractTroncated: string;
  keyword: string[];
  contacts: IResponsibleParty[];
  link: IMetadataLink[];
  publicationDate: string;
  lastUpdateDate:string;
  'topicCat': string[];
  highlight: any;
  legalConstraints: string[];
  license: License;
  rights: string[];
  'total_documents': number;
  lineage: string;
  type: string;
  kind: string[];
  href:string;
  granularity:string;
  usageRecommandation:string;
  emailContact:string;
  denominator: number;
  resolution: string;
  categories: MetadataCategory[];
  geonet: IMetadataGeonet;
  popularity: string;
  'max_north': number;
  'max_south': number;
  'max_east': number;
  'max_west': number;
  'native_max_north': number;
  'native_max_south': number;
  'native_max_east': number;
  'native_max_west': number;
  updateFrequency: string;
  crs: string;
  native_crs: string;
  native_bbox: string;
  parentDataset: IParentDataset;
  providers: string[];
  'image': {
    url: string;
    type: string;
  };
  spatialRepresentationType: string;

  constructor(data?: IMetadata) {
    if (data) {
      this.dataset_id = data.dataset_id ? data.dataset_id : '';
      this.dataset_index = data.dataset_index ? data.dataset_index : '';

      // Set title and subtitle if exists.
      // ex.: Great data (Saturne metropole) (Version 1.0.0)
      // title -> Great data
      // subtitle -> Saturne metropole - Version 1.0.0

      this.setMetadataTitleAndSubtitle(data.title);

      this.abstract = data.abstract;
      if(this.abstract != undefined){
        this.abstractTroncated = this.abstract.slice(0, 345);
        if (this.abstract.length > 345) {
          this.abstractTroncated = `${this.abstractTroncated}...`;
        }
      }

      this.keyword = data.keyword;

      const providersToAdd = new Set<string>();
      this.contacts = [];
      if (data.responsibleParty) {
        data.responsibleParty.forEach((party) => {
          providersToAdd.add(party.organisationName);
          if (party.appliesTo === 'resource') {
            this.contacts.push(party);
          }
        });
      }
      this.providers = Array.from(providersToAdd) as string[];
      this.link = data.link ? data.link : [];
      this.link.forEach((link)=>{
        if(link.formats.indexOf("OGC:WMS")>=0 && link.service==undefined){
          link.service='WMS';
        }
        if(link.formats.indexOf("OGC:WFS")>=0 && link.service==undefined){
          link.service='WFS';
        }
      })
      this.publicationDate = data.publicationDate;
      this.lastUpdateDate = data.lastUpdateDate;
      this.topicCat = data.topicCat;
      this.highlight = data.highlight;
      this.legalConstraints = data.legalConstraints;
      this.license = this.setLicense(data.license);
      this.rights = data.rights;
      this.total_documents = data.total_documents;
      this.lineage = data.lineage;
      this.type = data.type;
      this.kind = data.kind;
      this.native_bbox = data.native_bbox;
      this.native_crs = data.native_crs;
      this.denominator = data.denominator;
      this.resolution = data.resolution;
      this.href = data.href;
      this.granularity = data.granularity;
      this.emailContact = data.emailContact;
      this.usageRecommandation = data.usageRecommandation;
      this.popularity = data.popularity;
      this.updateFrequency = data.updateFrequency;
      this.geonet = data['geonet\:info'];
      this.crs = data.crs;
      this.categories = [];
      if (data.category) {
        data.category.forEach((category) => {
          this.categories.push(new MetadataCategory(category));
        });
      }
      this.parentDataset = {};
      this.parentDataset.id = data.parentId;
      if (data.image) {
        data.image.forEach((image) => {
          if (image.type === 'thumbnail') {
            this.image = image;
          }
        });
      }
      this.spatialRepresentationType = data.spatialRepresentationType_text;
    }
  }

  setMetadataTitleAndSubtitle(title: string) {

    this.title= title;
    /* PIGMA simplification
    const splittedContent = title.split('(');
    this.title = splittedContent[0];

    const regExp = /\(([^)])+\)/g;
    const matches = title.match(regExp);
    if (matches && matches[0]) {
      this.subtitle = matches[0].substring(1, matches[0].length - 1);
      if (matches[1]) {
        this.subtitle += ' - ';
        this.subtitle += matches[1].substring(1, matches[1].length - 1);
      }
    }*/
  }
  
  hasWsLink(){
    return this.getWsLink()!=undefined;
  }

  getWsLink(){
    return this.link.find(x=>x.service==linkFormats.ws);
  }

  getCategories(): string[] {
    return this.categories.map((category) => {
      return category.labelCategory;
    });
  }

  getRasterService(){
    return { ...this.link.find((e) => { return e.service === linkFormats.wms || e.formats.indexOf('OGC:WMS')>=0; }) }
  }

  getVectorService(){
    return { ...this.link.find((e) => { return e.service === linkFormats.wfs; }) }
  }

  private setLicense(licenseLabel): License {
    if(licenseLabel && licenseLabel.length>0){
      const matchingLicense = APP_CONFIG.licenses.find(licence => licence.matchingKeys.find(item => licenseLabel.includes(item)));

      const license = matchingLicense ? new License(matchingLicense) : new License(
        { nameFr: licenseLabel, nameEn: licenseLabel, acronymFr: licenseLabel, acronymEn: licenseLabel }
      );
      return license;
    }
    

    return new License(
      { nameFr: "", nameEn: "", acronymFr: "", acronymEn: "" }
    );;
  }
}
