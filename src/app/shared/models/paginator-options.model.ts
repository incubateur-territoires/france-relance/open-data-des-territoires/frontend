export interface PaginatorOptions {
  pageIndex: number;
  length: number;
  pageSize: number;
  pageSizeOptions: number[];
}
