export interface IBasicTab {
  name: string;
  fullRouterLinkPath: string;
}