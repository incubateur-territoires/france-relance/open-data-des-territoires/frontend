export interface IEditorialMetadata {
  isAreal: boolean;
  isLinear: boolean;
  isOpenAccess: boolean;
  isPunctual: boolean;
  isQueryable: boolean;
  isRealTime: boolean;
  isSearchable: boolean;
  isSample: boolean;
}

export class EditorialMetadata {
  isAreal: boolean;
  isLinear: boolean;
  isOpenAccess: boolean;
  isPunctual: boolean;
  isQueryable: boolean;
  isRealTime: boolean;
  isSearchable: boolean;
  isSample: boolean;

  constructor(metadata?: IEditorialMetadata) {
    if (metadata) {
      this.isAreal = metadata.isAreal;
      this.isLinear = metadata.isLinear;
      this.isOpenAccess = metadata.isOpenAccess;
      this.isPunctual = metadata.isPunctual;
      this.isQueryable = metadata.isQueryable;
      this.isRealTime = metadata.isRealTime;
      this.isSearchable = metadata.isSearchable;
      this.isSample = metadata.isSample;
    }
  }
}
