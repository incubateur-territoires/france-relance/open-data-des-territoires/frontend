export interface IPageHeaderInfo {
  title: string;
  metadataSubtitle?: string;
  surtitle?: string;
  subtitle?: string;
  hasBetaStyle?: boolean;
  shareButtons?: {
    isActive: boolean;
    // uri in the seo service (outside of the angular app) that correspond to the type of element to be shared
    // ex: 'articles'
    matchingUriInSeoService: string;
  };
}

export interface ICMSPageLinks {
  title: string;
  url: string;
  img: string;
}
