import { CMSContent, Reuse } from '../../editorialisation/models';
import { Data } from './data.model';
import { EditorialMetadata, IEditorialMetadata } from './editorial-metadata.model';
import { Highlights } from './highlights.model';
import { IMetadata, linkFormats, Metadata } from './metadata.model';

export interface IDataset {
  _index?: string;
  slug?: string;
  highlights?: Highlights;
  metadata?: IMetadata;
  totalData?: number;
  data?: Data[];
  fields: IDatasetFields;
  editorialMetadata?: IEditorialMetadata;
  reuses?: Reuse[];
  relatedNews?: CMSContent[];
}

export interface IDatasetFields {
  list: string[];
  types: Object;
}

export class Dataset {
  uuid: string;
  slug: string;
  accessLevel:number;
  highlights?: Highlights;
  metadata: Metadata;
  totalData: number;
  data: Data[];
  fields: IDatasetFields;
  nbViews: number;
  editorialMetadata: EditorialMetadata;
  reuses: Reuse[];
  relatedNews?: CMSContent[];
  hasTable=false;

  constructor(data?: IDataset) {
    console.log(data);
    if (data) {
      this.uuid = data.metadata['geonet\:info'].uuid;
      this.slug = data.slug;
      this.highlights = data.highlights;
      this.metadata = new Metadata(data.metadata);
      this.fields = data.fields;
      this.data = data.data ? data.data : [];
      this.totalData = data.totalData ? data.totalData : 0;
      this.hasTable = data.data !=undefined && this.totalData>0;
      this.editorialMetadata = new EditorialMetadata(data.editorialMetadata);
      this.reuses = data.reuses ? data.reuses : [];
      this.relatedNews = data.relatedNews ? data.relatedNews : [];
    } else {
      this.uuid = null;
      this.slug = null;
      this.metadata = new Metadata();
      this.fields = {
        list: [],
        types: {},
      };
      this.data = [];
      this.totalData = 0;
      this.editorialMetadata = new EditorialMetadata();
      this.reuses = [];
      this.relatedNews = [];
    }
  }

  setHighlightedTitle(title: string) {
    // Set title and subtitle if exists.
    // ex.: Great data (Saturne metropole) (Version 1.0.0)
    // title -> Great data
    // subtitle -> Saturne metropole - Version 1.0.0
    const splittedContent = title.split('(');
    this.highlights.highlightedTitle = title;//splittedContent[0];
    /*
    const regExp = /\(([^)])+\)/g;
    const matches = title.match(regExp);
    if (matches && matches[0]) {
      this.highlights.highlightedSubtitle = matches[0].substring(1, matches[0].length - 1);
      if (matches[1]) {
        this.highlights.highlightedSubtitle += ' - ';
        this.highlights.highlightedSubtitle += matches[1].substring(1, matches[1].length - 1);
      }
    }*/
  }

  getIdField(){
    return this.fields.list.find(x=>x=='gid'||x=='id');
  }
  get hasMap() {
    if (!this.metadata || !this.metadata.link) {
      return false;
    }
    let rasterService=this.metadata.getRasterService();
    return rasterService && rasterService.url;
  }
}
