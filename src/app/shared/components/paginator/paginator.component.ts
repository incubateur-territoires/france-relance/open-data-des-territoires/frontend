import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent implements OnInit {

  @Input() pageIndex: number; // the current page
  @Input() length: number; // how many total items there are in all pages
  @Input() pageSize: number; // how many items we want to show per page
  @Input() pageSizeOptions: number[]; // possible values forpageSize
  @Input() pagesToShow: number; // how many pages between next/prev
  @Input() showFirstLastButtons: boolean; // show next/prev bbttons
  @Input() loading: boolean; // check if content is being loaded

  @Output() page = new EventEmitter<number>();
  @Output() pageSizeChanged = new EventEmitter<number>();

  pageSizeDropdownToggle = false;
  currentUrl: string;

  constructor(
    private _viewportScroller: ViewportScroller,
    private _router: Router
  ) { }

  ngOnInit() {
    const url = this._router.url.split('?');
    this.currentUrl = url[0];
  }

  onPrev() {
    this.scrollTop();
    if (this.pageIndex - 1 > 0) {
      this.page.emit(this.pageIndex - 1);
    }
  }

  onNext() {
    this.scrollTop();
    if (this.pageIndex + 1 <= this.totalPages()) {
      this.page.emit(this.pageIndex + 1);
    }
  }

  onPage(n: number): boolean {
    this.scrollTop();
    this.page.emit(n);

    return false;
  }

  totalPages(): number {
    return Math.ceil(this.length / this.pageSize) || 0;
  }

  lastPage(): boolean {
    return this.pageSize * this.pageIndex >= this.length;
  }

  changePageSize(size: number) {
    this.pageSize = size;
    this.pageSizeChanged.emit(this.pageSize);
  }

  getPages(): number[] {
    const totalP = Math.ceil(this.length / this.pageSize); // Total number of page
    const p = this.pageIndex || 1; // Current page index
    const midPage = Math.round(totalP / 2);
    const pagesToShow = 3; // Total number of pages to be displayed in the paginator
    const pages: number[] = []; // Page number to be displayed
    pages.push(p); // Push the current page index
    let times = pagesToShow; // Number of page to be added to the displayed page
    if (p !== 1) {
      // If current page is different from the first page remove one (first page automatically
      // added to the array at the end of the function)
      times -= 1;
    }
    if (p !== totalP) {
      // If current page is different from the last page remove one
      // (last page automatically added to the array at the end of the function)
      times -= 1;
    }
    for (let i = 0; i < times; i += 1) {
      let added = 0; // Count the number of page added in this loop
      // Take the min number in the array and add (n-1) to the array if n > 1
      if (pages.length < pagesToShow) {
        if (Math.min.apply(null, pages) > 1) {
          if (Math.min.apply(null, pages) - 1 > 1) {
            pages.push(Math.min.apply(null, pages) - 1);
            added += 1;
          }
        }
      }
      // Take the max number in the array and add (n+1) to the array if n > total number of pages
      if (pages.length < pagesToShow) {
        if (Math.max.apply(null, pages) < totalP) {
          if (Math.max.apply(null, pages) + 1 < totalP) {
            pages.push(Math.max.apply(null, pages) + 1);
            added += 1;
          }
        }
      }
      // If more than one page has been added then increase i by one more
      if (added === 2) {
        i += 1;
      }
    }
    // Adding first page if not already in the array
    if (pages.indexOf(1) === -1) {
      pages.push(1);
    }
    // Adding last page if not already in the array
    if (pages.indexOf(totalP) === -1) {
      pages.push(totalP);
    }
    // Adding half page if not already in the array
    if (pages.indexOf(midPage) === -1) {
      pages.push(midPage);
    }
    // Reordering the array
    pages.sort((a, b) => a - b);
    return pages;
  }

  // Return true if ellipsis has to be displayed
  displayEllipsis(i: number) {
    const pages = this.getPages();
    let result = false;
    // If second page (index start at 0)
    if (i === 1) {
      // If the two consecutive number are not separated by one need to add the elipsis
      if (pages[0] + 1 !== pages[1]) {
        result = true;
      }
    }
    // If last page
    if (i > 1 && i === pages.length - 1) {
      if (pages[i] - 1 !== pages[i - 1]) {
        result = true;
      }
    }

    return result;
  }

  scrollTop() {
    this._viewportScroller.scrollToPosition([0, 0]);
  }
}
