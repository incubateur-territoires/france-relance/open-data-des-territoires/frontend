import { Component, OnInit } from '@angular/core';
import { AppRoutes } from '../../../routes';

@Component({
  selector: 'app-restricted-access-banner',
  templateUrl: './restricted-access-banner.component.html',
  styleUrls: ['./restricted-access-banner.component.scss']
})
export class RestrictedAccessBannerComponent implements OnInit {

  AppRoutes = AppRoutes;

  constructor() { }

  ngOnInit() {
  }

}
