import { Component, EventEmitter, OnInit, Output, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../../environments/environment';
import { CMSContent } from '../../../editorialisation/models';
import { EditorialisationService } from '../../../editorialisation/services';
import { AppRoutes } from '../../../routes';
import { UserService } from '../../../user/services';
import { DatasetDetailService } from '../../../dataset-detail/services';

@Component({
  selector: 'app-cgu-modal',
  templateUrl: './cgu-modal.component.html',
  styleUrls: ['./cgu-modal.component.scss']
})
export class CguModalComponent implements OnInit {

  AppRoutes = AppRoutes;
  cguModalIsOpened: boolean;
  safePageContent: SafeHtml;
  isCguScrolledEntirely = false;
  shakeAnimation = false;

  @Output() cguAccepted = new EventEmitter();
  @ViewChild('cguAnchor', {static:true}) cguAnchor: ElementRef;

  constructor(
    private _editorialisationService: EditorialisationService,
    private sanitizer: DomSanitizer,
    private _router: Router,
    private _route: ActivatedRoute,
    private _cookieService: CookieService,
    private _userService: UserService,
    private _datasetDetailService: DatasetDetailService,
  ) { }

  ngOnInit() {
    this.cguModalIsOpened = (this._cookieService.get(environment.cguAcceptedCookieName) !== 'true')
      && !this._userService.userIsSignedIn;

    if (this.cguModalIsOpened) {
      this._editorialisationService.getPage(environment.cmsStaticPages[AppRoutes.cgu.uri]).subscribe((page) => {
        if (!(page instanceof CMSContent)) {
        } else {
          this.safePageContent = this.sanitizer.bypassSecurityTrustHtml(page.content.html);
        }
      });
    }
  }

  closeCguModal() {
    this.cguModalIsOpened = false;
    const mRoute = (this._datasetDetailService.dataset.hasMap || this._datasetDetailService.dataset.hasTable ? AppRoutes.data.uri : AppRoutes.info.uri);
    this._router.navigate([`../${mRoute}`], { relativeTo: this._route });
  }

  acceptCgu() {
    // The div containing the cgu must be scrolled to the bottom in order to be able to accept the cgu
    if (this.isCguScrolledEntirely) {
      this.shakeAnimation = false;
      const expDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
      this._cookieService.set(
        environment.cguAcceptedCookieName, // Key
        'true', // Value
        expDate,
        '/',
        document.location.hostname,
        false,
        'Strict',
      );
      this.cguModalIsOpened = false;
      this.cguAccepted.next();
    } else {
      this.shakeAnimation = true;
      // reset animation
      setTimeout(() => {
        this.shakeAnimation = false;
      }, 300);
    }
  }

  modalBodyScrolled(event) {

    const target = event.target || event.srcElement;
    if (Math.ceil(target.scrollTop) + target.offsetHeight >= target.scrollHeight) {
      this.isCguScrolledEntirely = true;
    }
    else {
      this.cguAnchor.nativeElement.scrollIntoView({ behavior: "smooth", block: "start", inline:'nearest' });
      const y = this.cguAnchor.nativeElement.getBoundingClientRect().top + window.pageYOffset -80;
      window.scrollTo({top: y, behavior: 'smooth'});
    }
  }
}
