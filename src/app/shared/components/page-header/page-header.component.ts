import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { buttonCopyLinkToCliboardMessages } from '../../../../i18n/traductions';
import { NavigationHistoryService } from '../../../core/services';
import { APP_CONFIG } from '../../../core/services/app-config.service';
import { AppRoutes } from '../../../routes';
import { IPageHeaderInfo } from '../../models';

declare var _paq: any;
@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent implements OnInit {

  constructor(
    private _navigationHistoryService: NavigationHistoryService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
  ) { }

  @Input() pageInfo: IPageHeaderInfo;
  @Input() customGoToPreviousPage: any;
  messageClipboard: string = buttonCopyLinkToCliboardMessages.hover;
  isMediaShareButtonsDisplayed = false;

  isSeoWsDefined = APP_CONFIG.backendUrls.seo && APP_CONFIG.backendUrls.seo.length > 0;
  
  ngOnInit() {
  }

  goToPreviousPage() {
    if (this.customGoToPreviousPage) {
      this.customGoToPreviousPage();
    } else {
      const index = 1; // Start to retrieve the previous element
      let url = this._navigationHistoryService.getFromLast(index);
      // If url is null then redirect to home page
      if (url == null) {
        url = `/${AppRoutes.home.uri}`;
      }
      this._router.navigateByUrl(url);
    }
  }

  copyToClipboard() {
    const el = document.createElement('input');     // Create a <input> element
    // Set its value to the string that you want copied
    // tslint:disable-next-line: max-line-length
    el.value = `${window.location.origin}${APP_CONFIG.backendUrls.seo}/${this.pageInfo.shareButtons.matchingUriInSeoService}/${this._activatedRoute.snapshot.params.id}`;
    el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
    el.style.position = 'absolute';
    el.style.left = '-9999px';                      // Move outside the screen to make it invisible
    document.body.appendChild(el);                  // Append the <input> element to the HTML document
    const selected =
      document.getSelection().rangeCount > 0        // Check if there is any content selected previously
        ? document.getSelection().getRangeAt(0)     // Store selection if found
        : false;                                    // Mark as false to know no selection existed before
    el.select();                                    // Select the <input> content
    document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
    document.body.removeChild(el);                  // Remove the <input> element
    if (selected) {                                 // If a selection existed before copying
      document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
      document.getSelection().addRange(selected);   // Restore the original selection
    }
    this.messageClipboard = buttonCopyLinkToCliboardMessages.copied;
    
    if(window['_paq']){
      _paq.push(['trackEvent', 'Edito', 'ShareArticle', window.location.href, 1]);
    }

    setTimeout(() => {
      this.messageClipboard = buttonCopyLinkToCliboardMessages.hover;
      // tslint:disable-next-line:align
    }, 2000);
  }

  share(socialMedia: string) {
    switch (socialMedia) {
      case 'facebook':
        window.open(
          // tslint:disable-next-line: max-line-length
          `https://www.facebook.com/sharer/sharer.php?u=${window.location.origin}${APP_CONFIG.backendUrls.seo}/${this.pageInfo.shareButtons.matchingUriInSeoService}/${this._activatedRoute.snapshot.params.id}`,
          'popUpWindow',
          'height=400,width=600,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,status=yes',
        );
        break;
      case 'twitter':
        window.open(
          // tslint:disable-next-line: max-line-length
          `https://twitter.com/intent/tweet?url=${window.location.origin}${APP_CONFIG.backendUrls.seo}/${this.pageInfo.shareButtons.matchingUriInSeoService}/${this._activatedRoute.snapshot.params.id}&hashtags=datagrandlyon`,
          'popUpWindow',
          'height=400,width=600,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,status=yes',
        );
        break;
      case 'linkedin':
        window.open(
          // tslint:disable-next-line: max-line-length
          `https://www.linkedin.com/shareArticle?mini=true&url=${window.location.origin}${APP_CONFIG.backendUrls.seo}/${this.pageInfo.shareButtons.matchingUriInSeoService}/${this._activatedRoute.snapshot.params.id}`,
          'popUpWindow',
          'height=400,width=600,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,status=yes',
        );
        break;
      default:
        break;
    }

  }

}
