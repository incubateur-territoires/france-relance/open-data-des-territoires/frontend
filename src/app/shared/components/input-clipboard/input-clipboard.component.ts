import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-input-clipboard',
  templateUrl: './input-clipboard.component.html',
  styleUrls: ['./input-clipboard.component.scss'],
})
export class InputClipboardComponent implements OnInit {

  @Input() textInput: string;
  messageClipboard: string;

  constructor() { }

  ngOnInit() {
  }

  onHoverIconCopy() {
    this.messageClipboard = 'Copy to clipboard';
  }

  copyToClipboard(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    this.messageClipboard = 'Copied';
  }

}
