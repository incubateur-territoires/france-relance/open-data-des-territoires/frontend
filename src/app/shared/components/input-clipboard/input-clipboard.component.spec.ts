import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InputClipboardComponent } from './input-clipboard.component';

describe('InputClipboardComponent', () => {
  let component: InputClipboardComponent;
  let fixture: ComponentFixture<InputClipboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [InputClipboardComponent],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputClipboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
