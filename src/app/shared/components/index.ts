import { DownloadButtonComponent } from '././download-button/download-button.component';
import { BasicTabsComponent } from './basic-tabs/basic-tabs.component';
import { CguModalComponent } from './cgu-modal/cgu-modal.component';
import { FormatIconComponent } from './format-icon/format-icon.component';
import { InputClipboardComponent } from './input-clipboard/input-clipboard.component';
import { LinkCopyIconComponent } from './link-copy-icon/link-copy-icon.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { RestrictedAccessBannerComponent } from './restricted-access-banner/restricted-access-banner.component';

// tslint:disable-next-line: max-line-length
export { PaginatorComponent, InputClipboardComponent, FormatIconComponent, DownloadButtonComponent, LinkCopyIconComponent, PageHeaderComponent, BasicTabsComponent, CguModalComponent, RestrictedAccessBannerComponent };

// tslint:disable-next-line:variable-name
export const SharedComponents = [
  PaginatorComponent,
  InputClipboardComponent,
  LinkCopyIconComponent,
  PageHeaderComponent,
  BasicTabsComponent,
  RestrictedAccessBannerComponent,
  CguModalComponent,
  FormatIconComponent,
  DownloadButtonComponent,
];
