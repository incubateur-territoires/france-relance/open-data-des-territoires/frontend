import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LinkCopyIconComponent } from './link-copy-icon.component';

describe('InputClipboardComponent', () => {
  let component: LinkCopyIconComponent;
  let fixture: ComponentFixture<LinkCopyIconComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [LinkCopyIconComponent],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkCopyIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
