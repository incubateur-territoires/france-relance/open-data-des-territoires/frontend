import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';
import { geosource } from '../../../../i18n/traductions';
import { APP_CONFIG } from '../../../core/services/app-config.service';

@Component({
  selector: 'app-link-copy-icon',
  templateUrl: './link-copy-icon.component.html',
  styleUrls: ['./link-copy-icon.component.scss'],
})
export class LinkCopyIconComponent implements OnChanges {

  @Input() linkUrl: string;
  @Input() linkToDisplay: SafeHtml = false;
  @Input() isRoot: boolean = false;
  @Input() displayLink:boolean = true;
  @Input() fileName: string;

  messageClipboard: string;

  constructor() { }


  ngOnChanges() {
    if (this.fileName) {
      this.messageClipboard = `${geosource.mapMessages.shareFor} ${this.fileName}`;
    }
    else {
      this.messageClipboard = geosource.mapMessages.share;
    }

    this.linkUrl = this.linkUrl.replace(
      `${APP_CONFIG.backendUrls.proxyQuery}/download`,
      `${APP_CONFIG.backendUrls.backendDomain}`,
    );
    this.linkUrl = this.linkUrl.replace(
      `${APP_CONFIG.backendUrls.proxyQuery}/download`,
      'https://download.recette.data.grandlyon.com',
    );
    if (this.linkToDisplay === false) {
      this.linkToDisplay = this.linkUrl;
    }
  }

  copyToClipboard() {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.height = '0';
    selBox.value = this.linkUrl;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    this.messageClipboard = geosource.mapMessages.copied;
    setTimeout(() => {
      this.messageClipboard = geosource.mapMessages.share;
      // tslint:disable-next-line: align
    }, 2000);
  }
}
