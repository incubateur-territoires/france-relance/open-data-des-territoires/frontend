import { HttpClient, HttpResponse } from '@angular/common/http';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { Component, Input, OnDestroy, OnInit, OnChanges } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { takeUntil, timeoutWith } from 'rxjs/operators';
import { geosource, notificationMessages } from '../../../../i18n/traductions';
import { NotificationService } from '../../../core/services';

@Component({
  selector: 'app-download-button',
  templateUrl: './download-button.component.html',
  styleUrls: ['./download-button.component.scss'],
})
export class DownloadButtonComponent implements OnInit, OnDestroy, OnChanges {

  @Input() url: string;
  @Input() fileName: string;

  loading: boolean = false;
  abortMessage: string = geosource.downloads.abort;
  downloadMessage: string ;

  // Use this subject in order to be able to cancel the http request at any time
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private _notificationService: NotificationService,
    private _http: HttpClient,
  ) { }

  ngOnInit() {
    this.downloadMessage = `${geosource.downloads.download}  "${this.fileName}"`;
  }
  ngOnChanges() {
    this.downloadMessage = `${geosource.downloads.download}  "${this.fileName}"`;
  }

  download() {
    this.loading = true;
    let downloadUrl=this.url;
    const requestTimeoutLogger = of({timeout:true});
    this._http.get<HttpResponse<Blob>>(downloadUrl, { responseType: 'blob', observe: 'response', withCredentials: false } as Object).pipe(
      takeUntil(this.ngUnsubscribe),
      timeoutWith(5000, requestTimeoutLogger)
    ).subscribe(
      (response:any) => {
        if(response.timeout){
          this.loading = false;
          window.open(downloadUrl,'target=_blank');
          return;
        }
        // Create a temporary link and click on it to launch the blob download
        const url = window.URL.createObjectURL(response.body);

        if (response.body.type == 'application/pdf') {
          var reader = new FileReader();
          reader.readAsDataURL(response.body);
          reader.onloadend = () => {
            const bytes = reader.result.toString();
            //what we realy have ?
            const header = bytes.substring(0,bytes.indexOf(',') +1);
            var str = bytes.substring(bytes.indexOf(',') + 1);

            const b64 = atob(str).replace('"','');

            var newData = '';
            try {
              atob(b64.replace('"',''));
              newData = header + b64.replace('"','');
            } catch (e) {
              newData = bytes;
            }
            finally {

              const a = document.createElement('a');
              a.href =  newData;
              a.rel = 'noopener';
              a.download = this.fileName;
              document.body.appendChild(a); // append the element to the dom -> otherwise it will not work in firefox
              a.click();
              a.remove();
              this.loading = false;
            }
          }
        }
        else {
          const a = document.createElement('a');
          a.href =  url;
          a.rel = 'noopener';
          a.download = this.fileName;
          document.body.appendChild(a); // append the element to the dom -> otherwise it will not work in firefox
          a.click();
          a.remove();
          this.loading = false;
        }
      },
      (err) => {
        let message = notificationMessages.general.failedDownloadFile;
        console.log(err)
        if (err && err.status === 401) {

          message = notificationMessages.general.failedDownloadFileUnauthenticated;
        }

        if (err && err.status === 403) {
          message = notificationMessages.general.failedDownloadFileUnauthorized;
        }

        if(err && err.status === 0){ // CORS ERROR
          window.open(downloadUrl,'target=_blank')
        }
        else{
          this._notificationService.notify(
            {
              message,
              type: 'error',
            },
          );
        }
        
        this.abortDownload();
      },
    );
  }

  abortDownload() {
    this.ngUnsubscribe.next();
    this.loading = false;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
