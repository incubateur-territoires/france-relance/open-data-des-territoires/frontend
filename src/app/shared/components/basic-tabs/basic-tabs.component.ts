import { Component, OnInit, Input } from '@angular/core';
import { IBasicTab } from '../../models';

@Component({
  selector: 'app-basic-tabs',
  templateUrl: './basic-tabs.component.html',
  styleUrls: ['./basic-tabs.component.scss'],
})
export class BasicTabsComponent implements OnInit {

  @Input() tabs: IBasicTab[];

  constructor() { }

  ngOnInit() {
  }
}
