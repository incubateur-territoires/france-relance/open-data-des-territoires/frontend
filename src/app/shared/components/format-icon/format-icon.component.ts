import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-format-icon',
  templateUrl: './format-icon.component.html',
  styleUrls: ['./format-icon.component.scss'],
})
export class FormatIconComponent implements OnInit {

  @Input() formatName: string;

  // used in case we want to display inside the type file icon, a label different from it's file extension
  // For example, a shapefile has a .zip extension, but we want to display the label 'SHP'
  labelTypeFile = {
    ShapeFile: 'SHP',
    GeoJSON: 'GeoJSON',
  };

  constructor() { }

  ngOnInit() {

  }

}
