import { EscapeHtmlPipe } from './keep-html.pipe';

export { EscapeHtmlPipe };

// tslint:disable-next-line:variable-name
export const SharedPipes = [
  EscapeHtmlPipe,
];
