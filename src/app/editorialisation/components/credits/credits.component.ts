import { Component, OnInit } from '@angular/core';
import { Credit } from '../../models';
import { AppRoutes } from '../../../routes';
import { IPageHeaderInfo } from '../../../shared/models';
import { pageTitles, notificationMessages } from '../../../../i18n/traductions';
import { NotificationService } from '../../../core/services';
import { CreditsService } from '../../services';

@Component({
  selector: 'app-credits',
  templateUrl: './credits.component.html',
  styleUrls: ['./credits.component.scss'],
})
export class CreditsComponent implements OnInit {

  AppRoutes = AppRoutes;
  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.credits,
  };
  credits: Credit[];
  constructor(
    private _creditsService: CreditsService,
    private _notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this._creditsService.getCredits().subscribe(
      (credits) => {
        this.credits = credits;
      },
      () => {
        this._notificationService.notify({
          type: 'error',
          message: notificationMessages.edito.getCredits,
        });
      },
    );
  }
}
