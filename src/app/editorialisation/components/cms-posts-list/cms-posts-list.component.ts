import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppRoutes } from '../../../routes';
import { CMSContent } from '../../models/cms-content.model';
import { IPageHeaderInfo } from '../../../shared/models';
import { pageTitles, introText, metaDescription } from '../../../../i18n/traductions';
import { PaginatorOptions } from '../../../shared/models';
import { ElasticsearchService } from '../../../elasticsearch/services/elasticsearch.service';
import { DatasetResearchService } from '../../../datasets/services';
import { scopesResearch } from '../../../shared/variables';
import { Meta } from '@angular/platform-browser';
import { APP_CONFIG } from '../../../core/services/app-config.service';

@Component({
  selector: 'app-cms-posts-list',
  templateUrl: './cms-posts-list.component.html',
  styleUrls: ['./cms-posts-list.component.scss'],
})
export class CMSPostsListComponent implements OnInit, OnDestroy {

  posts: CMSContent[];
  paginator: PaginatorOptions;
  countResults = [];
  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.news,
  };
  intro: string = introText.news.replace('{plateformeName}',APP_CONFIG.labels.plateformeName);

  private _pageIndex: number = 1;
  private _pageSize: number = 9;


  latestPosts: any;
  AppRoutes = AppRoutes;

  constructor(
    private _elasticsearchService: ElasticsearchService,
    private _datasetResearchService: DatasetResearchService,
    private _meta: Meta,
  ) { }

  ngOnInit() {

    this._meta.updateTag({ name: 'description', content: metaDescription.news.replace('{plateformeName}',APP_CONFIG.labels.plateformeName) });
    this._datasetResearchService.resetResearch();
    this._datasetResearchService.scopeReasearch = scopesResearch.post;
    const countResults = [];
    this._datasetResearchService.pageSize = 9;

    this.paginator = {
      pageIndex: this._datasetResearchService.pageIndex + 1,
      length: 0,
      pageSize: this._datasetResearchService.pageSize,
      pageSizeOptions: [5, 10, 20],
    };

    this.search();
  }

  // When pagination is changed by user, we update results with new pagination options
  changePagination(pageIndex) {
    this._datasetResearchService.shouldAggregateResultCount = false;
    this._datasetResearchService.shouldAggregateFilters = false;
    this._datasetResearchService.paginationChanged(this.paginator.pageSize, pageIndex - 1);

    this.search();
  }

  private search() {
    this._datasetResearchService.getResults().subscribe((posts) => {
      this.posts = [];
      posts.forEach((result) => {
        this.posts.push(result);
      });
      //const position = this._viewportScroller.getScrollPosition();
      this.countResults = [];
      this._datasetResearchService.resultsCount.forEach((item) => {
        this.countResults[item.scopeKey] = item.count || 0;
      });

      this.paginator.pageSize = this._datasetResearchService.pageSize;
      this.paginator.pageIndex = this._datasetResearchService.pageIndex + 1;

      // Set pagination depending on the selected scope
      this.paginator.length = this.countResults[scopesResearch.post.key];
      //this._viewportScroller.scrollToPosition(position);
    });
  }

  ngOnDestroy() {
    this._datasetResearchService.resetResearch();
  }
}
