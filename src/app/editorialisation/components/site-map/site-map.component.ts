import { Component, OnInit } from '@angular/core';
import { IPageHeaderInfo } from '../../../shared/models/cms-page.model';
import { pageTitles } from '../../../../i18n/traductions';
import { AppRoutes } from '../../../routes';
import { APP_CONFIG } from '../../../core/services/app-config.service';

@Component({
  selector: 'app-site-map',
  templateUrl: './site-map.component.html',
  styleUrls: ['./site-map.component.scss'],
})
export class SiteMapComponent implements OnInit {

  AppRoutes = AppRoutes;
  APP_CONFIG = APP_CONFIG;
  
  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.siteMap,
  };

  constructor() { }

  ngOnInit() {
  }

}
