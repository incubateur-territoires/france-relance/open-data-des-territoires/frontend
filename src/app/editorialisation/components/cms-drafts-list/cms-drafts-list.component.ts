import { Component, OnInit } from '@angular/core';
import { AppRoutes } from '../../../routes';
import { CMSContent, IESCMSResponse } from '../../models/cms-content.model';
import { ElasticsearchService } from '../../../elasticsearch/services/elasticsearch.service';

@Component({
  selector: 'app-cms-drafts-list',
  templateUrl: './cms-drafts-list.component.html',
  styleUrls: ['./cms-drafts-list.component.scss'],
})
export class CMSDraftsListComponent implements OnInit {

  drafts: CMSContent[];
  latestPosts: any;
  AppRoutes = AppRoutes;

  constructor(
    private _elasticsearchService: ElasticsearchService,
  ) { }

  ngOnInit() {

    const options = {
      status: 'draft',
      type: 'post',
    };
    this._elasticsearchService.getPosts(options).subscribe((drafts) => {
      this.drafts = [];
      drafts.hits.hits.forEach((result) => {
        const source = result._source as IESCMSResponse;
        const cmsPost = new CMSContent(source);
        this.drafts.push(cmsPost);
      });
    });
  }
}
