import { Component, OnInit } from '@angular/core';
import { IPageHeaderInfo } from '../../../shared/models';
import { ActivatedRoute } from '@angular/router';
import { pageTitles, contactTrad } from '../../../../i18n/traductions';
import { AppRoutes } from '../../../routes';
import { SeoSErvice } from '../../services';

@Component({
  selector: 'app-contribution',
  templateUrl: './contribution.component.html',
  styleUrls: ['./contribution.component.scss'],
})
export class ContributionComponent implements OnInit {

  AppRoutes = AppRoutes;
  contactTrad = contactTrad;
  pageHeaderInfo: IPageHeaderInfo = {
    title: '',
  };

  constructor(
    private _route: ActivatedRoute,
    private _seoService: SeoSErvice,
  ) {
  }

  ngOnInit() {
    this._route.data.subscribe((data) => {
      this.pageHeaderInfo.hasBetaStyle = data.hasBetaStyle;
      this.pageHeaderInfo.title = pageTitles.contribution;
    });
  }

}
