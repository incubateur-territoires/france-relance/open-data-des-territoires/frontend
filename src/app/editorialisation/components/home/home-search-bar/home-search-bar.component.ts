import { ViewportScroller } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { geosource } from '../../../../../i18n/traductions';
import { SearchBarComponent } from '../../../../datasets/components/search-bar/search-bar.component';
import { SearchCompletion } from '../../../../datasets/models';
import { DatasetResearchService } from '../../../../datasets/services';
import { IScope } from '../../../../elasticsearch/models';

@Component({
  selector: 'home-search-bar',
  templateUrl: './home-search-bar.component.html',
  styleUrls: ['./home-search-bar.component.scss'],
})
export class HomeSearchBarComponent extends SearchBarComponent /*implements OnInit*/ {


  searchPlaceholder = ' Chercher par mot-clé: ' + geosource.placeholders.all;






}
