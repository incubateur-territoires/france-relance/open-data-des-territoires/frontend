import { Component, OnInit } from '@angular/core';
import { Commune } from '../../../../core/models/commune.model';
import { CommunesService } from '../../../../core/services';
import { AppRoutes } from '../../../../routes';

@Component({
  selector: 'app-home-communes',
  templateUrl: './home-communes.component.html',
  styleUrls: ['./home-communes.component.scss']
})
export class HomeCommunesComponent implements OnInit {
  AppRoutes = AppRoutes;
  mapHover: string = '';
  listHover: string = '';
  
  communes:Commune[];

  constructor(private communesService:CommunesService) { }

  ngOnInit(): void {
    this.communesService.getCommunes().subscribe(communes=>this.communes=communes);
  }

  selectPartner(e) {
    this.mapHover = e.target.id;
  }

  getQueryParamForCommune(communeName){
    const c =this.communes.find(commune=>commune.name==communeName);
    if(c){
      return {portail: c.elasticSearchName};
    }
    return {portail: "NotFound"};
  }
}
