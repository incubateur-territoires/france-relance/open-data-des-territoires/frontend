import { Component, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { environment } from '../../../../environments/environment';
import { MapOptions } from '../../../map/models/map-options';
import { metaDescription, notificationMessages } from '../../../../i18n/traductions';
import { Notification } from '../../../core/models';
import { NotificationService } from '../../../core/services';
import { AppConfigService, APP_CONFIG } from '../../../core/services/app-config.service';
import { DatasetResearchService } from '../../../datasets/services';
import { ElasticsearchService } from '../../../elasticsearch/services/elasticsearch.service';
import { AppRoutes } from '../../../routes';
import { scopesResearch } from '../../../shared/variables';
import { CMSContent, IESCMSResponse } from '../../models/cms-content.model';
import { Metadata,linkFormats } from '../../../shared/models';
import { map } from 'rxjs/internal/operators/map';
import { ReusesService } from '../../services/reuses.service';
import { OrganizationsService } from '../../../core/services/organizations.service';

declare var _paq: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  AppRoutes = AppRoutes;
  APP_CONFIG = APP_CONFIG;
  environment = environment;
  notificationMessages = notificationMessages;

  triggerEasterEgg: boolean = false;
  numberOfClickEgg: number = 0;

  latestPosts: CMSContent[];

  defaultImage: string = 'assets/img/pigma/home_image.jpg';
  finalImage: string = 'assets/img/pigma/home_image.jpg';

  mapOptions: MapOptions;

  lastDataSets = [];

  //{topicCat: 'thematic.label'}
  thematics=[
    {icon:'agriculture-alimentation.svg',label:'Agriculture, Alimentation',searchParam:{topicCat: 'Agriculture Alimentation'}},
    {icon:'culture-tourisme.svg',label:'Culture, Tourisme',searchParam:{topicCat: 'Culture Tourisme'}},
    {icon:'economie-insertion-emploi.svg',label:'Économie, Insertion, Emploi',searchParam:{topicCat: 'Économie Insertion Emploi'}},
    {icon:'enfance-education.svg',label:'Enfance, Éducation',searchParam:{topicCat: 'Enfance Éducation'}},
    {icon:'environnement-energie-climat.svg',label:'Environnement, Énergie, Climat',searchParam:{topicCat: 'Environnement Énergie Climat'}},
    {icon:'institution-administration.svg',label:'Institution, Administration',searchParam:{topicCat: 'Institution Administrationn'}},
    {icon:'population-citoyennete.svg',label:'Population, Citoyenneté',searchParam:{topicCat: 'Population Citoyenneté'}},
    {icon:'sante-social-solidarite.svg',label:'Sante, Social, Solidarité',searchParam:{topicCat: 'Sante Social Solidarité'}},
    {icon:'sport-nature.svg',label:'Sport,<br>Nature',searchParam:{topicCat: 'Sport Nature'}},
    {icon:'territoires-infrastructures.svg',label:'Territoires, Infrastructures',searchParam:{topicCat: 'Territoires Infrastructures'}},
    {icon:'transport-mobilite.svg',label:'Transport, Mobilite',searchParam:{topicCat: 'Transport Mobilite'}},
    {icon:'urbanisme-habitat.svg',label:'Urbanisme, Habitat',searchParam:{topicCat: 'Urbanisme Habitat'}},
  ]
  reuses=[];


  partners=[]

  randomizedPartners=[];

  jddList:any=[];

  currentJDDOnmap:Metadata;

  currentJDDOnmapSlug='';

  currentJDDIndex = 0 ;

  constructor(
    private _elasticsearchService: ElasticsearchService,
    private _appConfigService: AppConfigService,
    private _datasetResearchService: DatasetResearchService,
    private _notificationService: NotificationService,
    private _organizationService: OrganizationsService,
    private _router: Router,
    private _reusesService:ReusesService,
    private _meta: Meta,
  ) { }

  ngOnInit() {
    this.mapOptions = {
      showScaleBar:false,
      showCopyright:false,
      navControlPosition:'top-right'
    } as MapOptions;
    this.mapOptions.bbox=[2,40,5,50];
    this.mapOptions.rasterService={url:"",name:"",bbox_by_projection:{}};
    console.log(this.mapOptions);
    // Set the title and description for the home page
    this._meta.updateTag({ name: 'description', content: metaDescription.home.replace('{plateformeName}',APP_CONFIG.labels.plateformeName) });

    this._datasetResearchService.resetResearch(false);
    this._datasetResearchService.getLastPublishedDatasets().subscribe((retour) => {
      this.lastDataSets = retour;
    });

    this._appConfigService.loadHomeJddList().subscribe((retour:any) => {
      this.jddList = retour.uuid_list;
      if(this.jddList && this.jddList.length>0){
        this.loadJddOnMap(this.jddList[this.currentJDDIndex]);
      }
    });

    this.latestPosts = [];
    if(this.APP_CONFIG.urls.homeImage && this.APP_CONFIG.urls.homeImage.length>0){
      this.defaultImage=this.APP_CONFIG.urls.homeImage;
      this.finalImage=this.APP_CONFIG.urls.homeImage;
    }
    if(this.APP_CONFIG.theFunctionalitiesInterruptor.news){
      this.getPosts();
    }
    
    this._organizationService.getOrganizations(true).subscribe((orgas)=>{
      this.partners = orgas;
      this.randomizedPartners = this.shuffle(this.partners).slice(0,6);
    });
    this._reusesService.getReuses().subscribe(
      (reuses) => {
        this.reuses = reuses.slice(0, 3);
      },
      () => {
        this._notificationService.notify({
          type: 'error',
          message: notificationMessages.edito.getReuses,
        });
      },
    );
  }

  nextJDD(){
    this.currentJDDIndex=(this.currentJDDIndex+1)%this.jddList.length;
    this.loadJddOnMap(this.jddList[this.currentJDDIndex]);
  }

  loadJddOnMap(slugOrUuid){
    this._elasticsearchService.getDatasetMetadata(slugOrUuid).pipe(
      map((e) => {
        if (e.hits.hits.length > 0) {
          const metadata = new Metadata(e.hits.hits[0]._source['metadata-fr']);
          this.currentJDDOnmap=metadata;
          this.currentJDDOnmapSlug=e.hits.hits[0]._source.slug;
          
          let vectorService=metadata.getVectorService();
          let rasterService=metadata.getRasterService();
          let wgsbbox = rasterService.bbox_by_projection["EPSG:4326"];
          this.mapOptions={
              showScaleBar:false,
              showCopyright:false,
              initOptions:true,
              isMVT : true,
              mvtUrl: `${APP_CONFIG.backendUrls.proxyQuery}/map/mvt`,
              navControlPosition:'top-right',
              bbox: [wgsbbox.minx, wgsbbox.miny, wgsbbox.maxx, wgsbbox.maxy],
              rasterService:rasterService,
              vectorService:vectorService,
              dataType:{
                isLinear: false,
                isPunctual: true,
                isAreal: false,
              }
            } as MapOptions;
        }}
        )
      ).subscribe();
  }

  getPosts(){
    // We get the 3 latests posts.
    // Then we check if one post is prioritary. 2 possibilities
    // - one prioritary + 2 latest posts
    // - 3 latest posts
    const optionsLatest = {
      size: 4,
      sortDate: true,
      status: 'published',
      type: 'post',
    };
    const optionSticky = {
      size: 1,
      featured: true,
      type: 'post',
    };

    forkJoin([
      this._elasticsearchService.getPosts(optionsLatest),
      this._elasticsearchService.getPosts(optionSticky),
    ]).subscribe(
      (results) => {
        const posts = results[0];
        const featuredPost = results[1];

        if (featuredPost.hits.hits && featuredPost.hits.hits.length > 0) {
          const source = featuredPost.hits.hits[0]._source as IESCMSResponse;
          this.latestPosts.push(new CMSContent(source));
        }

        posts.hits.hits.forEach((result) => {
          const source = result._source as IESCMSResponse;
          if (this.latestPosts.length < 4) {
            if (this.latestPosts.length === 0 || source.uuid !== this.latestPosts[0].uuid) {
              const cmsPost = new CMSContent(source);
              this.latestPosts.push(cmsPost);
            }
          }
        });
      },
      (err) => {
        this._notificationService.notify(new Notification({
          type: 'error',
          message: `${notificationMessages.edito.getPosts}`,
        }));
      },
    );
    this._elasticsearchService.getPosts(optionsLatest).subscribe((posts) => {

    });
  }
  // Update the research service with the scope selected, and set the string value to ''.
  // Then navigate to the results view.
  goToPostsResearch() {
    this._datasetResearchService.resetResearch();
    this._datasetResearchService.scopeReasearch = scopesResearch.post;
    this._router.navigate(['/', AppRoutes.research.uri]);
  }

  trackExploreButtonEvent() {
    if(window['_paq']){
      _paq.push(['trackEvent', 'HomePage', 'ExploreButton', 'Explore', 1]);
    }
  }

  shuffle(array) {
    let currentIndex = array.length,  randomIndex;
    // While there remain elements to shuffle.
    while (currentIndex != 0) {
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
    return array;
  }

  get isReady() {
    return this.mapOptions!=undefined;//(this.mapOptions && this.mapOptions.isMVT) ||
      //(this.mapOptions && !this.mapOptions.isMVT && this.mapOptions.geojson);
  }
}
