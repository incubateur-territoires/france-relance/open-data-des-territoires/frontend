import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeReuseCardComponent } from './home-reuse-card.component';

describe('HomeReuseCardComponent', () => {
  let component: HomeReuseCardComponent;
  let fixture: ComponentFixture<HomeReuseCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeReuseCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeReuseCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
