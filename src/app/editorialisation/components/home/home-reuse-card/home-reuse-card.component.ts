import { Component, OnInit, Input } from '@angular/core';
import { map } from 'rxjs/operators';
import { ElasticsearchService } from '../../../../elasticsearch/services/elasticsearch.service';
import { AppRoutes } from '../../../../routes';
import { Metadata } from '../../../../shared/models';
import { Reuse } from '../../../models';

@Component({
  selector: 'app-home-reuse-card',
  templateUrl: './home-reuse-card.component.html',
  styleUrls: ['./home-reuse-card.component.scss']
})
export class HomeReuseCardComponent implements OnInit {
  AppRoutes = AppRoutes;
  
  @Input() reuse: Reuse = null;
  @Input() displaytype: string;
  
  jddMap={};
  constructor(private _elasticsearchService:ElasticsearchService) { }

  ngOnInit(): void {
    console.log(this.reuse);
    if(this.displaytype=="main"){
      this._elasticsearchService.getDatasetMetadata(this.reuse.datasetsUsed[0]).pipe(
        map((e) => {
          if (e.hits.hits.length > 0) {
            const metadata = new Metadata(e.hits.hits[0]._source['metadata-fr']);
            this.jddMap[this.reuse.datasetsUsed[0]]=metadata;
          }
          else {
            this.jddMap[this.reuse.datasetsUsed[0]]={title:'Titre inconnu'};
          }
          }
          )
        ).subscribe();

        this._elasticsearchService.getDatasetMetadata(this.reuse.datasetsUsed[1]).pipe(
          map((e) => {
            if (e.hits.hits.length > 0) {
              const metadata = new Metadata(e.hits.hits[0]._source['metadata-fr']);
              this.jddMap[this.reuse.datasetsUsed[1]]=metadata;
            }
            else {
              this.jddMap[this.reuse.datasetsUsed[1]]={title:'Titre inconnu'};
            }
          }
            
            )
          ).subscribe();

    }
    

  }

  getReuseTypeLabel(reuseTypes) {
    return reuseTypes.map((type) => {
      switch (type) {
        case 'app':
          return 'Application'
        case 'web':
          return 'Site web'
        case 'article':
          return 'Article de presse'
      }
    }).join(', ')
  }
}
