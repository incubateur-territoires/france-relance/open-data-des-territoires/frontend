import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { map } from 'rxjs/operators';
import { ToolsService } from '../../../../core/services/tools.service';
import { ElasticsearchService } from '../../../../elasticsearch/services/elasticsearch.service';
import { AppRoutes } from '../../../../routes';
import { Metadata } from '../../../../shared/models';
import { CMSContent, Reuse } from '../../../models';

@Component({
  selector: 'app-home-news-card',
  templateUrl: './home-news-card.component.html',
  styleUrls: ['./home-news-card.component.scss']
})
export class HomeNewsCardComponent implements OnInit {
  AppRoutes = AppRoutes;
  
  @Input() news: CMSContent = null;
  @Input() displaytype: string;
  
  safePageContent: SafeHtml;
  
  constructor(private _elasticsearchService:ElasticsearchService,
    private sanitizer: DomSanitizer,
    private _toolsService: ToolsService) { }

  ngOnInit(): void {
    console.log(this.news);
    let textContent = this.news.content.html;
    if(textContent.length>200){
      textContent = textContent.substring(0,200)+'...';
    }
    
    this.safePageContent = this.sanitizer.bypassSecurityTrustHtml(this._toolsService.decorateRichText(textContent));
  }
}
