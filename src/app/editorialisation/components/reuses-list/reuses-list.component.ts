import { Component, OnInit } from '@angular/core';
import { Reuse } from '../../models';
import { AppRoutes } from '../../../routes';
import { IPageHeaderInfo } from '../../../shared/models';
import { pageTitles, notificationMessages, reusesTypes, contactTrad } from '../../../../i18n/traductions';
import { NotificationService } from '../../../core/services';
import { ReusesService } from '../../services';
import { APP_CONFIG } from '../../../core/services/app-config.service';

@Component({
  selector: 'app-reuses-list',
  templateUrl: './reuses-list.component.html',
  styleUrls: ['./reuses-list.component.scss'],
})
export class ReusesListComponent implements OnInit {

  AppRoutes = AppRoutes;
  contactTrad = contactTrad;
  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.reuses,
  };
  reuses: Reuse[];

  plateformeName = APP_CONFIG.labels.plateformeName;
  
  constructor(
    private _reusesService: ReusesService,
    private _notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this._reusesService.getReuses().subscribe(
      (reuses) => {
        this.reuses = reuses;
      },
      () => {
        this._notificationService.notify({
          type: 'error',
          message: notificationMessages.edito.getReuses,
        });
      },
    );
  }

  formatReusesTypes(types: string[]) {
    return types.map(e => reusesTypes[e] ? reusesTypes[e] : null).filter(e => e !== null).join(', ');
  }
}
