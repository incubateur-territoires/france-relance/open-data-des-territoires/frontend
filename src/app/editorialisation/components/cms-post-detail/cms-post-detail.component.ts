import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { notificationMessages } from '../../../../i18n/traductions';
import { ToolsService } from '../../../core/services/tools.service';
import { ElasticsearchService } from '../../../elasticsearch/services/elasticsearch.service';
import { SeoSErvice } from '../../services/seo.service';
import { AppRoutes } from '../../../routes';
import { IPageHeaderInfo, Metadata, typesMetadata } from '../../../shared/models';
import { CMSContent } from '../../models/cms-content.model';
import { TarteAuCitronService } from '../../../core/services';

@Component({
  selector: 'app-cms-post-detail',
  templateUrl: './cms-post-detail.component.html',
  styleUrls: ['./cms-post-detail.component.scss'],
})
export class CMSPostDetailComponent implements OnInit, AfterViewInit {

  AppRoutes = AppRoutes;
  post: CMSContent;
  safePostContent: SafeHtml;
  pageHeaderInfo: IPageHeaderInfo = {
    title: '',
    shareButtons: {
      isActive: true,
      matchingUriInSeoService: 'articles',
    },
  };
  relations: {
    datasetsMetadata: Metadata[];
  } = { datasetsMetadata: [] };

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _datePipe: DatePipe,
    private _sanitizer: DomSanitizer,
    private _elasticSearchService: ElasticsearchService,
    private _seoService: SeoSErvice,
    private _toolsService: ToolsService,
    private _tacService: TarteAuCitronService,
  ) {
  }

  ngOnInit() {
    this.post = this._route.snapshot.data['post'];

    if (this.post) {

      // Set the title and description for a CMS post
      this._seoService.setPostSEO(this.post);

      this.safePostContent = this._sanitizer.bypassSecurityTrustHtml(this._toolsService.decorateRichText(this.post.content.html));

      // Construct header informations: (Date + tags) and title
      this.pageHeaderInfo.title = this.post.content.title;
      let res = this._datePipe.transform(this.post.content.publicationDate, 'dd.MM.yyy');
      if (!res) {
        res = '-';
      }
      const category = this.post.content.category ?
        ` ${notificationMessages.edito.news}: ${this.post.content.category}` : '';
      this.pageHeaderInfo.surtitle = `${res} ${category}`;

      // Get the related dataset info
      this.post.relations.datasets.slugs.forEach((slug) => {
        this._elasticSearchService.getDatasetMetadata(slug).subscribe((response) => {
          if (response.hits.hits.length > 0) {
            const metadata = new Metadata(response.hits.hits[0]._source['metadata-fr']);
            metadata.parentDataset.slug = slug;
            // If the dataset doesn't have any image set the default one
            if (!(metadata.image && metadata.image.url)) {
              this.setBackupImage(metadata);
            }
            this.relations.datasetsMetadata.push(metadata);
          }
        });
      });
    } else {
      this._router.navigate(['/', AppRoutes.page404.uri])
    }
  }

  ngAfterViewInit(): void {
    this._tacService.triggerTarteaucitronYoutube();
  }

  setBackupImage(metadata: Metadata) {
    // Init the image property to make sure that 'url' is accessible in the following lines
    metadata.image = {
      url: null,
      type: null,
    };
    if (metadata.type === typesMetadata.series) {
      metadata.image.url = './assets/img/picto_collection.svg';
    } else {
      metadata.image.url = metadata.type === typesMetadata.dataset ?
        './assets/img/picto_geographical_dataset.svg' : './assets/img/picto_non_geographical_dataset.svg';
    }
  }
}
