import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { map, mergeMap, tap } from 'rxjs/operators';
import { notificationMessages, pageTitles } from '../../../../i18n/traductions';
import { NotificationService } from '../../../core/services';
import { DatasetResearchService } from '../../../datasets/services';
import { Aggregation } from '../../../elasticsearch/models';
import { ElasticsearchService } from '../../../elasticsearch/services/elasticsearch.service';
import { AppRoutes } from '../../../routes';
import { environment } from '../../../../environments/environment';
import { IPageHeaderInfo } from '../../../shared/models';
import { scopesResearch } from '../../../shared/variables';
import { Organization } from '../../models';
import { OrganizationsService } from '../../../core/services/organizations.service';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.scss'],
})
export class OrganizationsComponent implements OnInit {

  AppRoutes = AppRoutes;
  
  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.partners,
  };

  organizations: Organization[];

  opendata_url: string = '';

  privatedata_url: string ='';

  hasScrolled: boolean = false;

  orgaToFocus = this._route.snapshot.fragment;

  constructor(
    private _organizationsService: OrganizationsService,
    private _elasticSearchService: ElasticsearchService,
    private _datasetResearchService: DatasetResearchService,
    private _notificationService: NotificationService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _location: Location


  ) { }

  ngOnInit() {

    this.opendata_url = AppRoutes.contribution.opendata_url.fr;
    this.privatedata_url = AppRoutes.contribution.privatedata_url.fr;

    if (window.location.href.includes(environment.angularAppHost.en)) {
      this.opendata_url = AppRoutes.contribution.opendata_url.en;
      this.privatedata_url = AppRoutes.contribution.privatedata_url.en;
    }

    this._organizationsService.getOrganizations(true).pipe(
      tap((organizations) => {
        this.organizations = organizations;
      }),
      mergeMap(() => {
        return this._elasticSearchService.getNumberDatasetsByOrganization().pipe(
          map((e) => {
            const aggregations = e.aggregations['nested_agg']['organisationName'].buckets;
            return aggregations;
          }),
        );
      }),
    ).subscribe(
      (aggregations) => {
        aggregations.forEach((aggregation: Aggregation) => {
          this.organizations.forEach((organization) => {
            if (aggregation.key === organization.elasticSearchName) {
              const aggregationsType = aggregation['scope']['type_agg'].buckets;

              aggregationsType.forEach((type) => {
                if (type.key === scopesResearch.services.key) {
                  organization.servicesCount += type['count_per_metadata']['cid'].value;

                } else {
                  organization.datasetsCount += type['count_per_metadata']['cid'].value;
                }
              });
            }
          });
        });
      },
      () => {
        this._notificationService.notify({
          type: 'error',
          message: notificationMessages.edito.getDataProducers,
        });
      },
    );
  }

  ngAfterViewChecked() { // element isn't visible just after data was fetched, thus using AfterContent hook
    if (this.orgaToFocus && !this.hasScrolled) {
      const el = document.getElementById(this.orgaToFocus)
      if (el) {
        el.scrollIntoView();
        this.hasScrolled = true;
      }
    }
  }

  researchDatasetsWithOrganizationFilter(name: string) {
    this._datasetResearchService.resetResearch(true);

    this._datasetResearchService.getResults().subscribe(() => {
      this._datasetResearchService.updateAggregation('metadata-fr.responsibleParty.organisationName', name, true);
      this._datasetResearchService.scopeReasearch = scopesResearch.datasets;
      this._router.navigateByUrl(this._location.path());
    });
  }

  researchServicesWithOrganizationFilter(name: string) {
    this._datasetResearchService.resetResearch(true);

    this._datasetResearchService.getResults().subscribe(() => {
      this._datasetResearchService.updateAggregation('metadata-fr.responsibleParty.organisationName', name, true);
      this._datasetResearchService.scopeReasearch = scopesResearch.services;
      this._router.navigateByUrl(this._location.path());
    });

  }
}
