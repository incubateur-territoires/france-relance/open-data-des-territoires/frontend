import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { forkJoin } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { ElasticsearchService } from '../../../elasticsearch/services/elasticsearch.service';
import { AppRoutes } from '../../../routes';
import { IPageHeaderInfo, Metadata, typesMetadata } from '../../../shared/models';
import { Reuse } from '../../models';
import { ReusesService, SeoSErvice } from '../../services';

@Component({
  selector: 'app-reuse-detail',
  templateUrl: './reuse-detail.component.html',
  styleUrls: ['./reuse-detail.component.scss']
})
export class ReuseDetailComponent implements OnInit {

  pageHeaderInfo: IPageHeaderInfo = {
    title: null,
  };
  reuse: Reuse = new Reuse();
  datasetsUsed: Metadata[] = [];
  AppRoutes = AppRoutes;

  constructor(
    private _reusesService: ReusesService,
    private _route: ActivatedRoute,
    private _elasticSearchService: ElasticsearchService,
    private _seoSErvice: SeoSErvice,
  ) { }

  ngOnInit() {
    this._route.paramMap.pipe(
      filter((paramMap: ParamMap) => (paramMap.get('id') !== null)),
      switchMap((paramMap: ParamMap) => this._reusesService.getReuse(paramMap.get('id'))),
    ).subscribe((reuse: Reuse) => {
      this.pageHeaderInfo.title = reuse.name;
      this.pageHeaderInfo.subtitle = reuse.creator;
      this.reuse = reuse;
      const calls = this.reuse.datasetsUsed.map((slug) => {
        return this._elasticSearchService.getDatasetMetadata(slug);
      });

      this._seoSErvice.setReuseSEO(this.reuse);

      forkJoin(calls).pipe(
        map((response) => {
          return response.map((e, i) => {
            let metadata = null;
            if (e && e.hits.hits.length > 0) {
              metadata = new Metadata(e.hits.hits[0]._source['metadata-fr']);
              // If the dataset doesn't have any image set the default one
              metadata.parentDataset.slug = this.reuse.datasetsUsed[i];
              if (!(metadata.image && metadata.image.url)) {
                this.setBackupImage(metadata);
              }
            }
            return metadata;
          });
        }),
        map(array => array.filter(value => value !== null)),
      ).subscribe((res) => {
        this.datasetsUsed = res;
      });
    });
  }

  setBackupImage(metadata: Metadata) {
    // Init the image property to make sure that 'url' is accessible in the following lines
    metadata.image = {
      url: null,
      type: null,
    };
    if (metadata.type === typesMetadata.series) {
      metadata.image.url = './assets/img/vignette_collection.png';
    } else {
      metadata.image.url = metadata.type === typesMetadata.dataset ?
        './assets/img/vignette-data-geo.png' : './assets/img/vignette-data.png';
    }
  }

}
