import { Component, OnInit } from '@angular/core';
import { IPageHeaderInfo } from '../../../shared/models';
import { pageTitles } from '../../../../i18n/traductions';
import { AppRoutes } from '../../../routes';
import { Changelog } from '../../models';
import { ChangelogsService } from '../../services';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-changelog',
  templateUrl: './changelog.component.html',
  styleUrls: ['./changelog.component.scss']
})
export class ChangelogComponent implements OnInit {

  AppRoutes = AppRoutes;
  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.changelog,
    hasBetaStyle: true,
  };

  changelogs: Changelog[];

  constructor(
    private _changelogService: ChangelogsService,
  ) { }

  ngOnInit() {

    this._changelogService.getChangelogs().subscribe((changelogs) => {
      if (window.location.href.includes(environment.angularAppHost.en)) {
        this.changelogs = changelogs.filter((changelog) => {
          return changelog.language === 'EN';
        });
      } else {
        this.changelogs = changelogs.filter((changelog) => {
          return changelog.language === 'FR';
        });
      }
    });
  }

}
