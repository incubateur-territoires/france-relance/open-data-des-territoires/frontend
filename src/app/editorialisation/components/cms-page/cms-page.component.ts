import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DomSanitizer, Meta, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { TarteAuCitronService } from '../../../core/services';
import { ToolsService } from '../../../core/services/tools.service';
import { AppRoutes } from '../../../routes';
import { IPageHeaderInfo } from '../../../shared/models';
import { CMSContent } from '../../models';
import { EditorialisationService, SeoSErvice } from '../../services';

@Component({
  selector: 'app-cms-page',
  templateUrl: './cms-page.component.html',
  styleUrls: ['./cms-page.component.scss'],
})
export class CMSPageComponent implements OnInit, AfterViewInit {
  page: CMSContent;
  safePageContent: SafeHtml;
  pageHeaderInfo: IPageHeaderInfo = {
    title: '',
  };

  constructor(
    private _route: ActivatedRoute,
    private _editorialisationService: EditorialisationService,
    private sanitizer: DomSanitizer,
    private _toolsService: ToolsService,
    private _router: Router,
    private _meta: Meta,
    private _seoService: SeoSErvice,
    private _tacService: TarteAuCitronService,
  ) {
  }

  ngOnInit() {
    this._route.data.subscribe((data) => {
      this.pageHeaderInfo.hasBetaStyle = data.hasBetaStyle;
      const pageName = data.uri;
      let page = environment.cmsStaticPages[pageName];
      if (page === undefined) {
        page = '';
      }

      this._editorialisationService.getPage(page).subscribe((page) => {
        this.page = page;

        // Set the title and description for a CMS page
        this._meta.updateTag({ name: 'description', content: this.page.content.excerpt });

        if (!(this.page instanceof CMSContent)) {
          this._router.navigate(['/', AppRoutes.page404.uri]);
        } else {
          this.safePageContent = this.sanitizer.bypassSecurityTrustHtml(this._toolsService.decorateRichText(this.page.content.html));
          this.pageHeaderInfo.title = this.page.content.title;

          this._seoService.setPostSEO(this.page);
        }
      });
    });
  }

  ngAfterViewInit(): void {
    this._tacService.triggerTarteaucitronYoutube();
  }
}
