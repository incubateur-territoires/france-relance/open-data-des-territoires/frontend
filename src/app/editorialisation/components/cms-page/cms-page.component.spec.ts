import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CMSPageComponent } from './cms-page.component';
import { MockComponent } from 'ng2-mock-component';
import { DebugElement } from '@angular/core';
import { EscapeHtmlPipe } from '../../../shared/pipes';
import { CMSPage } from '../../models';
import { By, SafeHtml, DomSanitizer, BrowserModule } from '@angular/platform-browser';
import { of, Observable } from 'rxjs';
import { ActivatedRoute, Data } from '@angular/router';
import { EditorialisationService } from '../../services';

export class EditorialisationServiceMock {
  getPage(page) {
    return of(new CMSPage({
      content: {
        rendered: 'Page content',
      },
      status: 'Published',
      title: {
        rendered: 'Page title',
      },
    }));
  }
}

export class ActivatedRouteMock {
  constructor() { }

  get data() {
    return of();
  }
}

describe('CMSPageComponent', () => {
  describe('Template', () => {
    let component: CMSPageComponent;
    let fixture: ComponentFixture<CMSPageComponent>;
    let debugElement: DebugElement;

    beforeEach(waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          BrowserModule,
        ],
        declarations: [
          CMSPageComponent,
          MockComponent({ selector: 'app-cms-page-error' }),
          EscapeHtmlPipe,
        ],
        providers: [
          {
            provide: ActivatedRoute,
            useClass: ActivatedRouteMock,
          },
          {
            provide: EditorialisationService,
            useClass: EditorialisationServiceMock,
          },
        ],
      }).compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(CMSPageComponent);
      component = fixture.componentInstance;
      debugElement = fixture.debugElement;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should render page information', () => {
      // Given
      const page = new CMSPage({
        content: {
          rendered: 'Page content',
        },
        status: 'Published',
        title: {
          rendered: 'Page title',
        },
      });
      component.pageError = false;
      component.page = page;

      // When
      fixture.detectChanges();
      const title = debugElement.query(By.css('.wp-page-title'));
      // const status = debugElement.query(By.css('.page-status'));
      const errorTemplate = debugElement.nativeElement.querySelector('app-cms-page-error');
      // Then
      expect(title.nativeElement.innerText).toBe(page.title);
      // expect(status.nativeElement.innerText).toBe(`(${page.status})`);
      expect(errorTemplate).toBeFalsy();
    });

    it('should render error template', () => {
      // Given
      component.pageError = true;

      // When
      fixture.detectChanges();
      const title = debugElement.query(By.css('.page-title'));
      // const status = debugElement.query(By.css('.page-status'));
      const errorTemplate = debugElement.nativeElement.querySelector('app-cms-page-error');
      // Then
      expect(title).toBeFalsy();
      // expect(status).toBeFalsy();
      expect(errorTemplate).toBeTruthy();
    });
  });

  describe('Component.ts', () => {
    let component: CMSPageComponent;
    let activatedRouteMock: ActivatedRoute;
    let editorialisationServiceMock: EditorialisationService;
    const wpPage = new CMSPage({
      content: {
        rendered: 'Page content',
      },
      status: 'Published',
      title: {
        rendered: 'Page title',
      },
    });

    beforeEach(() => {
      activatedRouteMock = {
        data: of({ uri: '' } as Data),
      } as ActivatedRoute;

      const domSanitizer = {
        bypassSecurityTrustHtml: (value: string) => {
          return 'safe' as SafeHtml;
        },
      } as DomSanitizer;

      editorialisationServiceMock = {
        getPage(page) {
          return of(wpPage);
        },
      } as EditorialisationService;

      component = new CMSPageComponent(activatedRouteMock, editorialisationServiceMock, domSanitizer);

    });

    describe('ngOnInit()', () => {
      it('should let page error to false if page is instance of CMSPage', () => {
        // Given
        component.page = wpPage;

        // When
        component.ngOnInit();

        // Then
        expect(component.pageError).toBeFalsy();
      });

      it('should set page error to true if page is not instance of CMSPage', () => {
        // Given
        spyOn(editorialisationServiceMock, 'getPage').and.returnValue(of(null));

        // When
        component.ngOnInit();

        // Then
        expect(component.pageError).toBeTruthy();
      });
    });
  });
});
