import { CMSPageComponent } from './cms-page/cms-page.component';
import { CMSDraftsListComponent } from './cms-drafts-list/cms-drafts-list.component';
import { CMSPostsListComponent } from './cms-posts-list/cms-posts-list.component';
import { HomeComponent } from './home/home.component';
// tslint:disable-next-line:max-line-length
import { CMSPostDetailComponent } from './cms-post-detail/cms-post-detail.component';
import { OrganizationsComponent } from './organizations/organizations.component';
import { CreditsComponent } from './credits/credits.component';
import { SiteMapComponent } from './site-map/site-map.component';
import { ChangelogComponent } from './changelog/changelog.component';
import { ContributionComponent } from './contribution/contribution.component';
import { ReusesListComponent } from './reuses-list/reuses-list.component';
import { ReuseDetailComponent } from './reuse-detail/reuse-detail.component';
import { HomeCommunesComponent } from './home/home-communes/home-communes.component';
import { HomeReuseCardComponent } from './home/home-reuse-card/home-reuse-card.component';
import { HomeSearchBarComponent } from './home/home-search-bar/home-search-bar.component';
import { HomeNewsCardComponent } from './home/home-news-card/home-news-card.component';


export {
  CMSPageComponent,
  CMSDraftsListComponent,
  CMSPostsListComponent,
  CMSPostDetailComponent,
  HomeComponent,
  HomeSearchBarComponent,
  OrganizationsComponent,
  CreditsComponent,
  SiteMapComponent,
  ChangelogComponent,
  ContributionComponent,
  ReusesListComponent,
  ReuseDetailComponent,
  HomeCommunesComponent,
  HomeReuseCardComponent,
  HomeNewsCardComponent,
};

// tslint:disable-next-line:variable-name
export const EditorialisationComponents = [
  CMSPageComponent,
  CMSDraftsListComponent,
  CMSPostsListComponent,
  CMSPostDetailComponent,
  HomeComponent,
  OrganizationsComponent,
  CreditsComponent,
  SiteMapComponent,
  ChangelogComponent,
  ContributionComponent,
  ReusesListComponent,
  ReuseDetailComponent,
  HomeCommunesComponent,
  HomeReuseCardComponent,
  HomeNewsCardComponent,
  HomeSearchBarComponent,
];
