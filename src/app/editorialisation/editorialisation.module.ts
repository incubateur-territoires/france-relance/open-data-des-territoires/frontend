import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { intersectionObserverPreset, LazyLoadImageModule } from 'ng-lazyload-image';
import { DatasetsModule } from '../datasets/datasets.module';
import { ElasticsearchModule } from '../elasticsearch/elasticsearch.module';
import { MapModule } from '../map/map.module';
import { SharedModule } from '../shared/shared.module';
import { EditorialisationComponents } from './components';
import { EditorialisationRoutingModule } from './editorialisation-routing.module';
import { EditorialisationResolvers } from './resolvers';
import { EditorialisationServices } from './services';
import { HomeReuseCardComponent } from './components/home/home-reuse-card/home-reuse-card.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ElasticsearchModule,
    DatasetsModule,
    EditorialisationRoutingModule,
    SharedModule,
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset,
    }),
    MapModule,
  ],
  declarations: [...EditorialisationComponents, HomeReuseCardComponent],
  providers: [...EditorialisationServices, EditorialisationResolvers],
})
export class EditorialisationModule { }
