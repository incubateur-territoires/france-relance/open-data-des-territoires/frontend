import { EditorialisationService } from '../services';
import { CMSPost } from '../models';
import { of, Observable, throwError } from 'rxjs';
import { ActivatedRouteSnapshot, Params } from '@angular/router';
import { PostDetailResolver } from './post-detail.resolver';

describe('PostDetailResolver', () => {
  let postDetailResolver: PostDetailResolver;
  let editorialisationServiceMock: EditorialisationService;
  let observable: Observable<CMSPost>;
  const wpPost = new CMSPost({
    uuid: '1',
    title: 'test post',
    status: 'published',
    categories: [],
    tags: [],
    publicationDate: '05-07-2018',
    modificationDate: '05-07-2018',
    content: 'Post content',
    featuredMedia: '0',
    excerpt: '',
    author: '',
  });

  beforeEach(() => {
    observable = of(wpPost);
    editorialisationServiceMock = {
      getPostById(id) {
        return observable;
      },
    } as EditorialisationService;
    postDetailResolver = new PostDetailResolver(editorialisationServiceMock);
  });

  describe('resolve()', () => {
    it('should return an observable of CMSPost', () => {
      // Given
      const id = 1;
      const activatedRouteSnapshotMock = {
        params: { id } as Params,
      } as ActivatedRouteSnapshot;
      const getPostByIdSpy = spyOn(editorialisationServiceMock, 'getPostById').and.callThrough();

      // When
      const result = postDetailResolver.resolve(activatedRouteSnapshotMock);

      // Then
      expect(getPostByIdSpy).toHaveBeenCalledWith(id);
      expect(result instanceof Observable).toBeTruthy();

      result.subscribe((res) => {
        expect(res).toBe(wpPost);
      });
    });

    it('should return an observable containing null if an error occures', () => {
      // Given
      const id = 1;
      const activatedRouteSnapshotMock = {
        params: { id } as Params,
      } as ActivatedRouteSnapshot;

      const getPostByIdSpy = spyOn(editorialisationServiceMock, 'getPostById').and.callFake(() => {
        return throwError(new Error('Fake error'));
      });

      // When
      const result = postDetailResolver.resolve(activatedRouteSnapshotMock);

      // Then
      expect(getPostByIdSpy).toHaveBeenCalledWith(id);
      expect(result instanceof Observable).toBeTruthy();

      result.subscribe(
        (res) => {
          expect(res).toBe(null);
        },
      );
    });
  });
});
