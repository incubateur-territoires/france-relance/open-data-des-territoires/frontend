import { CMSContent } from '../models/cms-content.model';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { EditorialisationService } from '../services/editorialisation.service';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class PostDetailResolver implements Resolve<CMSContent> {
  constructor(private editorialisationService: EditorialisationService) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.editorialisationService.getPostBySlug(route.params.id).pipe(
      catchError(() => of(null)),
    );
  }
}
