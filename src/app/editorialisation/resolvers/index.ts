import { PostDetailResolver } from './post-detail.resolver';

export {
  PostDetailResolver,
};

// tslint:disable-next-line:variable-name
export const EditorialisationResolvers = [
  PostDetailResolver,
];
