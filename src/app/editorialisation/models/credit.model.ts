export interface ICredit {
  id: number;
  name: string;
  description: string;
  links: ILink[];
  logo: string;
}

interface ILink {
  id: number;
  name: string;
  url: string;
  creditId: number;
}

class Link {
  id: number;
  name: string;
  url: string;
  urlLabel: string;
  creditId: number;

  constructor(iLink: ILink) {
    this.id = iLink.id;
    this.name = iLink.name;
    this.url = iLink.url;
    this.urlLabel = iLink.url.replace(/^(https?:|)\/\//, '');
    this.creditId = iLink.creditId;
  }
}

export class Credit implements ICredit {
  id: number;
  name: string;
  description: string;
  links: ILink[];
  logo: string;

  constructor(credit: ICredit) {
    this.id = credit.id;
    this.name = credit.name;
    this.description = credit.description;
    this.links = [];
    credit.links.forEach((link) => {
      this.links.push(new Link(link));
    });
    this.logo = credit.logo;
  }
}
