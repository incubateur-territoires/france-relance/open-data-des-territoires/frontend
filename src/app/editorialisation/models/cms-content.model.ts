export interface IESCMSResponse {
  'content-fr': IGhostContentResponse;
  type: string;
  uuid: string;
  slug: string;
  relations: IESCMSRelations;
  extras:any;
}

export interface IESCMSRelations {
  datasets: {
    slugs: string[];
  };
  posts: {
    slugs: string[];
  };
  pages: {
    slugs: string[];
  };
}

export interface IGhostContentResponse {
  id: string;
  // iso8601 format
  published_at: string;
  slug: string;
  updated_at: string;
  title: string;
  status: string;
  html: string;
  excerpt: string;
  feature_image: string;
  primary_author: IGhostAuthor;
  featured: boolean;
  tags: string[];
  highlight?: IHighlight[];
  url:string;
  plaintext:string;
  meta_description: string;
  meta_title?: string;
  og_title?: string;
  og_description?: string;
  og_image?: string;
}

export interface IHighlight {
  label: string;
  value: string;
}

interface IGhostAuthor {
  id: number;
  name: string;
}


export class CMSRelations {
  datasets: {
    slugs: string[];
  };
  posts: {
    slugs: string[];
  };
  pages: {
    slugs: string[];
  };

  constructor(data: IESCMSRelations) {
    this.pages = data && data.pages && data.pages.slugs ? data.pages : { slugs: [] };
    this.posts = data && data.posts && data.posts.slugs ? data.posts : { slugs: [] };
    this.datasets = data && data.datasets && data.datasets.slugs ? data.datasets : { slugs: [] };
  }
}

export class CMSContent {
  content: GhostContentResponse;
  type: string;
  uuid: string;
  slug: string;
  relations: CMSRelations;
  extras:any;
  constructor(data: IESCMSResponse) {
    this.content = (data) ? new GhostContentResponse(data['content-fr']) : null;
    this.type = (data.type) ? data.type : '';
    this.uuid = (data.uuid) ? data.uuid : '';
    this.slug = (data['content-fr'].slug) ? data['content-fr'].slug : '';
    this.relations = new CMSRelations(data.relations);
    if(data.extras){
      this.extras = data.extras;
    }
  }
}

export class GhostContentResponse {
  id: string;
  title: string;
  status: string;
  html: string;
  excerpt: string;
  featureImage: string;
  author: IGhostAuthor;
  tags: string[];
  category: string;
  featured: boolean;
  publicationDate: number;
  modificationDate: number;
  highlight?: string[];
  titleHighlight?: string;
  plaintext?: string;
  url?: string;
  meta_description?: string;
  meta_title?: string;
  og_title?: string;
  og_description?: string;
  og_image?: string;

  constructor(data: IGhostContentResponse)
  {
    this.id = data.id;
    this.status = (data.status != null) ? data.status : '';
    this.title = (data.title != null) ? data.title : '';
    this.html = (data.html != null) ? data.html : '';

    this.meta_title = (data.meta_title != null) ? data.meta_title: this.title;
    this.og_title=(data.og_title != null) ? data.og_title : this.title;

    this.excerpt = (data.excerpt != null) ? data.excerpt : '';
    this.meta_description = data.meta_description ;
    this.og_description=(data.og_description != null) ? data.og_description : this.meta_description;

    this.featureImage = data.feature_image;
    this.og_image = this.featureImage;

    this.publicationDate = (data.published_at != null) ? Date.parse(data.published_at) : 0;
    this.modificationDate = (data.updated_at != null) ? Date.parse(data.updated_at) : 0;
    this.plaintext = (data.plaintext != null) ? data.plaintext : '';
    this.url = (data.url != null) ? data.url : '';
    if(this.excerpt.length == 0){
      this.excerpt = this.plaintext;
    }

    // Ghost tags are separated in 2 ways:
    // - tags starting with '#' are tags (one or many per article)
    // - the tag not starting with '#' is the categorie (only one per article)
    this.tags = [];
    this.category = null;
    if (data.tags) {
      data.tags.forEach((tag) => {
          tag.startsWith('#') ? this.tags.push(tag) : this.category = tag;
      });
    }
    this.html = this.youtubeParser(this.html);

    this.author = (data.primary_author) ? data.primary_author : null;
    this.highlight = [];
  }

  private youtubeParser (content:string): string {
    // recherche de youtube
    if (content.search('iframe') > 0) {
      var parser = new DOMParser();
      var htmlContent = parser.parseFromString(content, 'text/html');

      var iframes = htmlContent.getElementsByTagName('iframe');

      for (var i = 0 ; i < iframes.length ; i++) {
        let src = iframes[i].getAttribute('src');
        var link = src.match(new RegExp(/(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/mgi));

        if (link.length > 0) {
          // on récupère la derniere portion
          var vid: string[]|string = link[0].match(new RegExp(/embed\/[a-zA-Z0-9]*/mgi) );
          vid = vid[0].slice(6);

          var youtubeDiv = document.createElement('div');
          youtubeDiv.setAttribute('class','youtube_player');
          youtubeDiv.setAttribute('videoID', vid);
          youtubeDiv.setAttribute('width', '100%');
          youtubeDiv.setAttribute('height','300px');


          iframes[i].parentNode.replaceChild(youtubeDiv,iframes[i]);
        }
      }
      content = htmlContent.getElementsByTagName('body')[0].innerHTML;
    }
    return content;
  }
}

export interface ICMSMedia {
  id: number;
  guid: {
    rendered: string;
  };
}

export interface ICMSCategory {
  id: number;
  name: string;
}
