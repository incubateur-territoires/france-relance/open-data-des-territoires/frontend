export interface IReuse {
  _id: number;
  name: string;
  creator: string;
  logo: string;
  website: string;
  reuseTypes: string[];
  datasetsUsed: string[];
  published: boolean;
  createDate: Date;
  updateDate: Date;
}

export class Reuse implements IReuse {
  _id: number;
  name: string;
  creator: string;
  logo: string;
  website: string;
  reuseTypes: string[];
  datasetsUsed: string[];
  published: boolean;
  createDate: Date;
  updateDate: Date;

  constructor(reuse?: IReuse) {
    this._id = reuse && reuse._id ? reuse._id : null;
    this.name = reuse && reuse.name ? reuse.name : null;
    this.creator = reuse && reuse.creator ? reuse.creator : null;
    this.logo = reuse && reuse.logo ? reuse.logo : null;
    this.website = reuse && reuse.website ? reuse.website : null;
    this.reuseTypes = reuse && reuse.reuseTypes ? reuse.reuseTypes : [];
    this.datasetsUsed = reuse && reuse.datasetsUsed ? reuse.datasetsUsed : [];
    this.published = reuse && reuse.published ? reuse.published : null;
    this.createDate = reuse && reuse.createDate ? reuse.createDate : null;
    this.updateDate = reuse && reuse.updateDate ? reuse.updateDate : null;
  }
}
