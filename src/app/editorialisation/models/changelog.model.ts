export interface IChangelog {
  _id?: string;
  version: string;
  createDate: Date;
  updateDate: Date;
  language: string;
  majorImprovements: string[];
  minorImprovements: string[];
  bugFixes: string[];
}

export class Changelog implements IChangelog {
  _id?: string;
  version: string;
  createDate: Date;
  updateDate: Date;
  language: string;
  majorImprovements: string[];
  minorImprovements: string[];
  bugFixes: string[];

  constructor(changelog: IChangelog) {
    this._id = changelog._id;
    this.version = changelog.version;
    this.createDate = changelog.createDate;
    this.updateDate = changelog.updateDate;
    this.language = changelog.language;
    this.majorImprovements = changelog.majorImprovements;
    this.minorImprovements = changelog.minorImprovements;
    this.bugFixes = changelog.bugFixes;
  }
}