import { EditorialisationService } from './editorialisation.service';
import { CreditsService } from './credits.service';
import { ChangelogsService } from './changelog.service';
import { ReusesService } from './reuses.service';
import { SeoSErvice } from './seo.service';

export {
  EditorialisationService,
  CreditsService,
  ChangelogsService,
  ReusesService,
  SeoSErvice,
};

// tslint:disable-next-line:variable-name
export const EditorialisationServices = [
  EditorialisationService,
  CreditsService,
  ChangelogsService,
  ReusesService,
  SeoSErvice,
];
