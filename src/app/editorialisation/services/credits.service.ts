import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICredit, Credit } from '../models';
import { map } from 'rxjs/operators';
import { APP_CONFIG } from '../../core/services/app-config.service';

@Injectable()
export class CreditsService {

  constructor(
    private _httpClient: HttpClient,
  ) { }

  getCredits() {
    return this._httpClient.get<ICredit[]>(APP_CONFIG.backendUrls.credits, { withCredentials: true }).pipe(
      map((creditsBack) => {
        const credits = [];
        creditsBack.forEach((credit) => {
          credits.push(new Credit(credit));
        });
        return credits;
      }));
  }
}
