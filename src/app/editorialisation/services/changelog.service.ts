import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IChangelog, Changelog } from '../models';
import { map } from 'rxjs/operators';
import { APP_CONFIG } from '../../core/services/app-config.service';

@Injectable()
export class ChangelogsService {

  constructor(
    private _httpClient: HttpClient,
  ) { }

  getChangelogs() {
    return this._httpClient.get<IChangelog[]>(`${APP_CONFIG.backendUrls.changelog}/changelog`).pipe(
      map((changelogsBack) => {
        const changelogs = [];
        changelogsBack.forEach((changelog) => {
          changelogs.push(new Changelog(changelog));
        });
        return changelogs;
      }));
  }
}
