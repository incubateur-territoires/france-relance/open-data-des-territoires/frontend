import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { metaDescription, pageTitles } from '../../../i18n/traductions';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { AppRoutes } from '../../routes';
import { Metadata } from '../../shared/models';
import { CMSContent, Reuse } from '../models';


@Injectable()
export class SeoSErvice {

  private _lang: string;
  private _defaultImg: string;
  private _defaultDesc: string;
  private _defaultTitle: string;

  constructor(
      private _titleService: Title,
      private _metaService: Meta
  ) {
    this._lang = window.location.href.includes(environment.angularAppHost.en) ? 'en' : 'fr';
    
    if(this._metaService.getTag("property='og:image'")){
      this._defaultImg = this._metaService.getTag("property='og:image'").content;
    }
    
    this._defaultDesc = metaDescription.home.replace('{plateformeName}',APP_CONFIG.labels.plateformeName);
    this._defaultTitle = this._titleService.getTitle();
  }

  setPostSEO(post:CMSContent)
  {
    const imageUrl = (post.content.og_image != null) ?
        post.content.og_image : post.content.featureImage;

    var description: string = '';
    if (post.content.meta_description) {
      description = post.content.meta_description;
    }
    else {
      description = post.content.excerpt;
      if (description.length > 140) {
        description = `${description.substr(0, description.substr(0,136).lastIndexOf(' '))}...`;
      }
    }

    const meta = [
      { name: 'description', content: description },
      { property: 'og:title', content: post.content.og_title },
      { property: 'og:description', content: post.content.og_description },
      { property: 'og:image', content: imageUrl },
    ];

    this._setMeta(meta);
    this._setTitle(post.content.meta_title);
  }

  setDatasetSEO(data: Metadata)
  {
    const imageUrl = (data.image != null) ?
        data.image.url : this._defaultImg;

    const description = data.abstractTroncated;

    const meta = [
      { name: 'description', content: description },
      { property: 'og:title', content: data.title },
      { property: 'og:description', content: description},
      { property: 'og:image', content: imageUrl },
    ];

    this._setMeta(meta);
    this._setTitle(data.title);
  }

  setRoutingSEO(title:string)
  {
    if (title=='') {
      title=pageTitles.home;
    }

    const meta = [
      { name: 'description', content: this._defaultDesc },
      { property: 'og:title', content: title },
      { property: 'og:description', content: this._defaultDesc },
      { property: 'og:image', content: this._defaultImg }
    ];
    this._setMeta(meta);
    this._titleService.setTitle(title);
  }

  setReuseSEO (reuse: Reuse)
  {
    const title:string = reuse.name;
    const meta = [
      { name: 'description', content: '' },
      { property: 'og:title', content: title },
      { property: 'og:description', content: '' },
      { property: 'og:image', content: reuse.logo }
    ];

    this._setTitle(title);
    this._setMeta(meta);
  }

  private _setTitle(title:string) {
    this._titleService.setTitle(`${title} - ${AppRoutes.root.title[this._lang]}` );
  }

  private _setMeta(metatags:{name?: string, property?:string, content:string}[]) {

    metatags.forEach((meta) => {
      if (meta.content!= null) {
        meta.content = meta.content.replace('\n', ' ');
        this._metaService.updateTag(meta);
      }
    });
  }

}
