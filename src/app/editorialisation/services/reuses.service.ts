import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { IReuse, Reuse } from '../models';

@Injectable()
export class ReusesService {

  constructor(
    private _httpClient: HttpClient,
  ) { }

  getReuses() {
    return this._httpClient.get<IReuse[]>(`${APP_CONFIG.backendUrls.reuses}/reuses`, { withCredentials: true }).pipe(
      map((reusesBack) => {
        return reusesBack.map(reuse => new Reuse(reuse));
      }));
  }

  getReuse(id: string) {
    return this._httpClient.get<IReuse>(`${APP_CONFIG.backendUrls.reuses}/reuses/${id}`, { withCredentials: true }).pipe(
      map(reuse => new Reuse(reuse)),
    );
  }
  
  getDataSetReuses(slug : string) {
    return this._httpClient.get<IReuse[]>(`${APP_CONFIG.backendUrls.reuses}/datasets/${slug}/reuses`, { withCredentials: true }).pipe(
      map((reusesBack) => {
        return reusesBack.map(reuse => new Reuse(reuse));
      }),
    );
  }
}
