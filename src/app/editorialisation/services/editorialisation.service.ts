
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { notificationMessages } from '../../../i18n/traductions';
import { ErrorService } from '../../core/services';
import { ElasticsearchService } from '../../elasticsearch/services/elasticsearch.service';
import { CMSContent, IESCMSResponse } from '../models/cms-content.model';

@Injectable()
export class EditorialisationService {

  constructor(
    private _errorService: ErrorService,
    private _elasticSearchService: ElasticsearchService,
  ) { }

  /*
  * Get a single cms page by slug
  */
  getPage(slug: string): Observable<CMSContent> {
    return this._elasticSearchService.getPosts({ slug }).pipe(
      map((res) => {
        const resultHits = res.hits.hits;
        if (resultHits.length === 0) {
          return null;
        }
        const post = new CMSContent(resultHits[0]._source as IESCMSResponse);
        return post;
      }),
      catchError((error) => {
        throw this._errorService.handleError(error, { message: notificationMessages.edito.getPost });
      }),
    );
  }

  getPostBySlug(slug: string): Observable<CMSContent> {
    return this._elasticSearchService.getPosts({ slug }).pipe(
      map((res) => {
        const resultHits = res.hits.hits;
        if (resultHits.length === 0) {
          return null;
        }
        const post = new CMSContent(resultHits[0]._source as IESCMSResponse);
        return post;
      }),
      catchError((error) => {
        throw this._errorService.handleError(error, { message: notificationMessages.edito.getPost });
      }),
    );
  }

  /*
  * This methods calls a unique post in the WP backend: the video post for the home page
  */
  getPostVideoHome(): Observable<CMSContent> {
    return this._elasticSearchService.getPosts({ slug: environment.cmsContent.slugPostVideo }).pipe(
      map((res) => {
        const resultHits = res.hits.hits;
        if (resultHits.length === 0) {
          return null;
        }
        const post = new CMSContent(resultHits[0]._source as IESCMSResponse);
        return post;
      }),
      catchError((error) => {
        throw this._errorService.handleError(error, { message: notificationMessages.edito.getHomeVideoPost });
      }),
    );
  }

  // Retrieve the cms posts by text search filter (in the post content)
  getPostsByTextSearch(tag: string): Observable<CMSContent[]> {
    return this._elasticSearchService.getPosts({ content: tag }).pipe(
      map((res) => {
        const postsList = [];
        const resultHits = res.hits.hits;
        resultHits.forEach((hit) => {
          const wpResponse = hit._source as IESCMSResponse;
          postsList.push(new CMSContent(wpResponse));
        });
        return postsList;
      }),
      catchError((error) => {
        throw this._errorService.handleError(error, { message: notificationMessages.edito.getPosts });
      }),
    );
  }

}
