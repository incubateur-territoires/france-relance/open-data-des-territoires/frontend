import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppConfigService } from '../../core/services/app-config.service';

@Injectable()
export class RedirectGuard implements CanActivate {

  constructor(public router: Router, private appConfigService:AppConfigService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
   
    console.log('RedirectGuard');
    let config =this.appConfigService.getAppConfig();
    if(config.externalUrls[route.routeConfig.path] && config.externalUrls[route.routeConfig.path].length > 0){
      window.location.href = config.externalUrls[route.routeConfig.path];
      return false;
    }
    return true;
  }
}
