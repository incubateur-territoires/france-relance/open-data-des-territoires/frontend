import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppConfigService } from '../../core/services/app-config.service';
import { AppRoutes } from '../../routes';
import { APP_CONFIG } from '../../core/services/app-config.service';

@Injectable()
export class HomeGuard implements CanActivate {

  constructor(public router: Router, private appConfigService:AppConfigService) { }

  canActivate(): boolean {
    if (APP_CONFIG.theFunctionalitiesInterruptor.nohomepage) {
      this.router.navigate(['/', AppRoutes.research.uri]);
      return false;
    }
    return true;
  }
}
