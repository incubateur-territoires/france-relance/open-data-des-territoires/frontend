import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AppRoutes } from '../../routes';
import { APP_CONFIG } from '../../core/services/app-config.service';

@Injectable()
export class PartnersActivatedGuard implements CanActivate {

  constructor(public router: Router) { }

  canActivate(): boolean {
    if (!APP_CONFIG.theFunctionalitiesInterruptor.partners) {
      this.router.navigate(['/', AppRoutes.page404.uri]);
      return false;
    }
    return true;
  }
}
