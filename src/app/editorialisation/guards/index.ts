import { CreditsActivatedGuard } from "./credits-activated.guard";
import { HomeGuard } from "./home.guard";
import { PartnersActivatedGuard } from "./partners-activated.guard";
import { RedirectGuard } from "./redirect.guard";
import { ReusesActivatedGuard } from "./reuses-activated.guard";

// tslint:disable-next-line:variable-name
export {
  CreditsActivatedGuard,
  PartnersActivatedGuard,
  ReusesActivatedGuard,
  RedirectGuard,
  HomeGuard,
};

// tslint:disable-next-line: variable-name
export const EditorialisationGuards = [
  CreditsActivatedGuard,
  PartnersActivatedGuard,
  ReusesActivatedGuard,
  RedirectGuard,
  HomeGuard,
];
