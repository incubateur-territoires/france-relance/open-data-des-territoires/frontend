import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '../routes';
import { ChangelogComponent, CMSDraftsListComponent, CMSPageComponent, CMSPostsListComponent,
  ContributionComponent, HomeComponent, OrganizationsComponent,
  ReuseDetailComponent, ReusesListComponent, SiteMapComponent } from './components';
import { CMSPostDetailComponent } from './components/cms-post-detail/cms-post-detail.component';
import { CreditsComponent } from './components/credits/credits.component';
import { CreditsActivatedGuard, EditorialisationGuards, PartnersActivatedGuard, RedirectGuard,HomeGuard, ReusesActivatedGuard } from './guards';
import { PostDetailResolver } from './resolvers/post-detail.resolver';

export const routes: Routes = [
  {
    path: AppRoutes.home.uri,
    canActivate: [HomeGuard],
    component: HomeComponent,
    data: {
      title: AppRoutes.home.title,
    },
  },
  {
    path: AppRoutes.approach.uri,
    component: CMSPageComponent,
    data: {
      title: AppRoutes.approach.title,
      uri: AppRoutes.approach.uri,
    },
  },
  {
    path: AppRoutes.cgu.uri,
    component: CMSPageComponent,
    data: {
      title: AppRoutes.cgu.title,
      uri: AppRoutes.cgu.uri,
    },
  },
  {
    path: AppRoutes.partners.uri,
    component: OrganizationsComponent,
    data: {
      title: AppRoutes.partners.title,
    },
    canActivate: [PartnersActivatedGuard,RedirectGuard],
  },
  {
    path: AppRoutes.accessibility.uri,
    component: CMSPageComponent,
    data: {
      title: AppRoutes.accessibility.title,
      uri: AppRoutes.accessibility.uri,
    },
  },
  {
    path: AppRoutes.siteMap.uri,
    component: SiteMapComponent,
    data: {
      title: AppRoutes.siteMap.title,
    },
  },
  {
    path: AppRoutes.changelog.uri,
    component: ChangelogComponent,
    data: {
      title: AppRoutes.changelog.title,
    },
  },
  {
    path: AppRoutes.legalNotices.uri,
    component: CMSPageComponent,
    data: {
      title: AppRoutes.legalNotices.title,
      uri: AppRoutes.legalNotices.uri,
    },
    canActivate: [RedirectGuard],
  },
  {
    path: AppRoutes.personalData.uri,
    component: CMSPageComponent,
    data: {
      title: AppRoutes.personalData.title,
      uri: AppRoutes.personalData.uri,
    },
  },
  {
    path: AppRoutes.personalData.uri,
    component: CMSPageComponent,
    data: {
      title: AppRoutes.personalData.title,
      uri: AppRoutes.personalData.uri,
    },
  },
  {
    path: AppRoutes.documentation.uri,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: CMSPageComponent,
        data: {
          title: AppRoutes.documentation.title,
          uri: AppRoutes.documentation.uri,
        },
      },
      {
        path: AppRoutes.beginners.uri,
        component: CMSPageComponent,
        data: {
          title: AppRoutes.beginners.title,
          uri: AppRoutes.beginners.uri,
        },
      },
    ],
  },
  {
    path: AppRoutes.credits.uri,
    component: CreditsComponent,
    data: {
      title: AppRoutes.credits.title,
    },
    canActivate: [CreditsActivatedGuard,RedirectGuard],
  },
  {
    path: AppRoutes.reuses.uri,
    component: ReusesListComponent,
    data: {
      title: AppRoutes.reuses.title,
    },
    canActivate: [ReusesActivatedGuard],
  },
  {
    path: AppRoutes.reusesDetail.uri,
    component: ReuseDetailComponent,
    data: {
      title: AppRoutes.reusesDetail.title,
    },
    canActivate: [ReusesActivatedGuard],
  },
  {
    path: AppRoutes.news.uri,
    component: CMSPostsListComponent,
    data: {
      title: AppRoutes.news.title
    },
  },
  {
    path: `${AppRoutes.news.uri}/:id`,
    component: CMSPostDetailComponent,
    resolve: {
      post: PostDetailResolver,
    },
    data: {
      title: AppRoutes.news.title,
    },
  },
  {
    path: AppRoutes.drafts.uri,
    component: CMSDraftsListComponent,
    data: {
      title: AppRoutes.drafts.title,
    },
  },
  {
    path: AppRoutes.contribution.uri,
    component: ContributionComponent,
    data: {
      title: AppRoutes.contribution.title,
      uri: AppRoutes.contribution.uri,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [...EditorialisationGuards]
})
export class EditorialisationRoutingModule { }
