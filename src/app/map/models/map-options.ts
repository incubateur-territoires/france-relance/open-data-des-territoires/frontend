export interface MapOptions {
  vectorService: GeoService;
  rasterService: GeoService;
  bbox: [number, number, number, number];
  initOptions: boolean;
  isMVT: boolean;
  mvtUrl?: string;
  geojson? : GeoJSON.FeatureCollection;
  dataType: DataType;
  showScaleBar?: boolean;
  showCopyright?: boolean;
  navControlPosition?:string;
}

export interface DataType {
  isLinear: boolean;
  isPunctual: boolean;
  isAreal: boolean;
}

export interface GeoService {
  name: string;
  url: string;
  bbox_by_projection: {};
}
