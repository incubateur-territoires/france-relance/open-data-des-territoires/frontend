import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { MapService } from './services/map.service';
import { GeocoderService } from './services/geocoder.service';
import { MapComponents } from './components';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  declarations: [MapComponents],
  providers: [
    MapService,
    GeocoderService,
    DatePipe,
  ],
  exports: [
    MapComponents,
  ],
})
export class MapModule { }
