import { MapComponent } from './map.component';
import { SearchAddressComponent } from './search-address/search-address.component';

export { MapComponent, SearchAddressComponent };

// tslint:disable-next-line:variable-name
export const MapComponents = [
  MapComponent,
  SearchAddressComponent,
];
