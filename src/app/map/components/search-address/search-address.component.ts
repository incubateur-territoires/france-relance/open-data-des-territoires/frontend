import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Input, EventEmitter, Output } from '@angular/core';
import { geosource } from '../../../../i18n/traductions';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ViewportScroller } from '@angular/common';

@Component({
  selector: 'app-search-address',
  templateUrl: './search-address.component.html',
  styleUrls: ['./search-address.component.scss'],
})
export class SearchAddressComponent implements OnInit {

  @Input() optionsAutocomplete: any;
  @Output() searchAddress = new EventEmitter();
  @Output() clearAddress = new EventEmitter();
  @Output() addressSelected = new EventEmitter();
  @ViewChild('inputSearch', { static: true }) inputSearch: ElementRef;

  searchValue: string;
  geosource = geosource;

  // Autocomplete
  currentAutocompleteFocus = -1;
  displayAutocomplete = false;
  autocompleteIsValid = true;

  keyUpSubject: Subject<any> = new Subject<any>();

  constructor(
  ) { }

  ngOnInit() {

    /**
     * We create this subject to have specific behaviour for the keyup event.
     * When keyup event, this subject will emit (see below in the switch of the searchChanged() function)
     * an event and will be catched here.
     * Then we do API call only if users didn't type the last 800ms to avoid
     * multiple api calls.
     */
    this.keyUpSubject.pipe(debounceTime(600)).subscribe(() => {
      this.requestAutocomplete();
    });
  }

  // This function is triggered when:
  // - the click press key Enter or select option with the mouse:
  // we make a search query & clean the completion list
  // - any keyup in the search bar :
  // we make the autocomplete query
  // - arrow up and down buttons:
  // we change the focus item in the completion list
  keyUpChanged(key: string) {
    switch (key) {
      // These 2 keys are to go up and down in the autocomplete list items
      case 'ArrowDown':
        if (this.currentAutocompleteFocus < 4) {
          this.currentAutocompleteFocus = this.currentAutocompleteFocus + 1;
        }
        break;
      case 'ArrowUp':
        if (this.currentAutocompleteFocus > 0) {
          this.currentAutocompleteFocus = this.currentAutocompleteFocus - 1;
        }
        break;
      case 'Enter':
        // When Enter is pressed and that we are inside the autocomplete list items
        if (this.currentAutocompleteFocus > -1) {
          this.searchValue = this.optionsAutocomplete[this.currentAutocompleteFocus].caption;
          this.selectOption(this.optionsAutocomplete[this.currentAutocompleteFocus]);
        }
        this.displayAutocomplete = false;
        this.inputSearch.nativeElement.blur();
        break;
      default:
        this.keyUpSubject.next();
        break;
    }
  }

  // When one autocomplete option is selected
  selectOption(option: any) {
    this.currentAutocompleteFocus = -1;
    this.searchValue = option.caption;
    this.addressSelected.emit(option);
  }

  searchChanged() {
    this.optionsAutocomplete = [];
  }

  /*
  * Display the autocomplete list: if the list is empty, we request it to ES.
  */
  displayAutocompleteList() {
    this.displayAutocomplete = true;
    if (this.optionsAutocomplete.length < 1) {
      this.requestAutocomplete();
    }
  }

  requestAutocomplete() {
    this.searchAddress.emit(this.searchValue);
  }

  resetResearch() {
    this.searchValue = '';
    this.searchChanged();
    this.clearAddress.emit();
  }
}
