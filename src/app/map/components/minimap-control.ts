import * as mapboxgl from 'mapbox-gl';
import centroid from '@turf/centroid';
import { polygon } from '@turf/helpers';

/*
 * A `Minimap` control contains a minimap with a draggable rectangle
 * Handle bearing and pitch changes.
*/
export class Minimap {

  _ticking = false;
  _parentMap = null;
  _isDragging = false;
  _isCursorOverFeature = false;
  _previousPoint = [0, 0];
  _currentPoint = [0, 0];
  _trackingRectCoordinates = [[[], [], [], [], []]];
  _miniMap: mapboxgl.Map;
  _container: any;
  _trackingRect: any;
  _miniMapCanvas: any;

  options = {
    id: 'mapboxgl-minimap',
    position: 'bottom-right',
    width: '300px',
    height: '170px',
    style: '',
    center: [0, 0] as mapboxgl.LngLatLike,
    zoom: 6,
    bounds: 'parent',
    classes: '',
    lineColor: '#08F',
    lineWidth: 1,
    lineOpacity: 1,
    fillColor: '#F80',
    fillOpacity: 0.25,
  };

  constructor(options) {
    Object.assign(this.options, options);
  }

  /*
  * This methods is needed to be extended for the control be used by tha main map
  * Returns a container with the minimap
  */
  onAdd(parentMap) {
    this._parentMap = parentMap;
    this._container = this.createContainer(parentMap);
    this._miniMap = new mapboxgl.Map({
      container: this._container,
      attributionControl: false,
      style: this.options.style,
      zoom: this.options.zoom,
      center: this.options.center,
    });

    this._miniMap.on('load', () => {
      this.load();
    });

    return this._container;
  }

  onRemove() {
    this._container.parentNode.removeChild(this._container);
    this._miniMap = null;

    return this;
  }

  getDefaultPosition() {
    return 'bottom-right';
  }

  load() {
    // Remove some interactions for the minimap
    const interactions = [
      'dragPan', 'scrollZoom', 'boxZoom', 'dragRotate',
      'keyboard', 'doubleClickZoom', 'touchZoomRotate',
    ];
    interactions.forEach((i) => {
      if (this.options[i] !== true) {
        this._miniMap[i].disable();
      }
    });

    this.options.bounds = this._parentMap.getBounds();

    this._miniMap.addSource('trackingRect', {
      type: 'geojson',
      data: {
        type: 'Feature',
        properties: {
          name: 'trackingRect',
        },
        geometry: {
          type: 'Polygon',
          coordinates: this._trackingRectCoordinates,
        },
      },
    });

    // Needed for dragging
    this._miniMap.addLayer({
      id: 'trackingRectFill',
      type: 'fill',
      source: 'trackingRect',
      layout: {},
      paint: {
        'fill-color': this.options.fillColor,
        'fill-opacity': this.options.fillOpacity,
        'fill-outline-color': this.options.lineColor,
      },
    });
    this._trackingRect = this._miniMap.getSource('trackingRect');

    // --- Events----
    // Change the cursor on the rectangle
    this._miniMap.on('mouseover', 'trackingRectFill', () => {
      this._miniMap.getCanvas().style.cursor = 'pointer';
    }).on('mouseleave', 'trackingRectFill', () => {
      this._miniMap.getCanvas().style.cursor = '';
    });

    // Update the minimap when move on the main map
    this._parentMap.on('move', this.update.bind(this));

    // Events to manage the draggable rectangle
    this._miniMap
      .on('mousemove', this.mouseMove.bind(this))
      .on('mousedown', this.mouseDown.bind(this))
      .on('mouseup', this.mouseUp.bind(this))
      .on('touchmove', this.mouseMove.bind(this))
      .on('touchstart', this.mouseDown.bind(this))
      .on('touchend', this.mouseUp.bind(this));

    this._miniMapCanvas = this._miniMap.getCanvasContainer();
    this._miniMapCanvas.addEventListener('wheel', this.preventDefault.bind(this));
    this._miniMapCanvas.addEventListener('mousewheel', this.preventDefault.bind(this));

    this.update();
  }

  // Set draggable variable to true when mouse down on the rectangle
  mouseDown(e) {
    const features = this._miniMap.queryRenderedFeatures(e.point, {
      layers: ['trackingRectFill'],
    });
    this._isCursorOverFeature = features.length > 0;

    if (this._isCursorOverFeature) {
      this._isDragging = true;
      this._previousPoint = this._currentPoint;
      this._currentPoint = [e.lngLat.lng, e.lngLat.lat];
    } else {
      this._miniMapCanvas.style.cursor = this._isCursorOverFeature ? 'move' : '';
    }
  }

  // Update rectangle layer + main map center
  mouseMove(e) {
    this._ticking = false;

    if (this._isDragging) {
      this._previousPoint = this._currentPoint;
      this._currentPoint = [e.lngLat.lng, e.lngLat.lat];

      // Offset = difference between old point and current point
      const offset = [
        this._previousPoint[0] - this._currentPoint[0],
        this._previousPoint[1] - this._currentPoint[1],
      ];

      this.moveTrackingRect(offset);

      // Use Turf to calculate the centroid of the polygon on the minimap, and center
      // the main map with it
      const polygonArea = polygon(this._trackingRectCoordinates);
      const center = centroid(polygonArea);
      this._parentMap.setCenter(center.geometry.coordinates);
    }
  }

  mouseUp() {
    this._isDragging = false;
    this._ticking = false;
    this.update();
  }

  // Update the rectangle coordinates with the offset
  moveTrackingRect(offset) {
    const data = this._trackingRect._data;
    const bounds = data.geometry.coordinates;

    // We need to change only 4 points of the polygon (and ignore the 5th)
    bounds[0][0][0] -= offset[0];
    bounds[0][0][1] -= offset[1];
    bounds[0][1][0] -= offset[0];
    bounds[0][1][1] -= offset[1];
    bounds[0][2][0] -= offset[0];
    bounds[0][2][1] -= offset[1];
    bounds[0][3][0] -= offset[0];
    bounds[0][3][1] -= offset[1];

    this._trackingRectCoordinates = bounds;
    this._trackingRect.setData(data);

    return bounds;
  }

  // Update the minimap after moving on the main map
  update() {
    if (this._isDragging) {
      return;
    }

    // To update the rectangle, we need consider the bearing and pitch values
    // that can change. Mapxgl.Map.getBounds() no consider this, so we need to
    // use the canvas and unproject it to get the points of the boundaries.
    const canvas = this._parentMap.getCanvas();
    const topLeft = this._parentMap.unproject([0, 0]).toArray();
    const topRight = this._parentMap.unproject([canvas.width, 0]).toArray();
    const bottomRight = this._parentMap.unproject([canvas.width, canvas.height]).toArray();
    const bottomLeft = this._parentMap.unproject([0, canvas.height]).toArray();

    const data = this._trackingRect._data;
    data.geometry.coordinates = [[topLeft, topRight, bottomRight, bottomLeft, topLeft]];
    this._trackingRect.setData(data);

    this._trackingRectCoordinates = [[topLeft, topRight, bottomRight, bottomLeft, topLeft]];

    this.zoomAdjust();
  }

  // For now this is hard coded: a difference of 3 between the main map and minimap zoom
  zoomAdjust() {
    this._miniMap.setZoom(parseInt(this._parentMap.getZoom(), 10) - 3);
    this._miniMap.setCenter(this._parentMap.getCenter());
  }

  createContainer(parentMap) {
    const container = document.createElement('div');

    container.className = `mapboxgl-ctrl-minimap mapboxgl-ctrl ${this.options.classes}`;
    container.setAttribute('style', `width: ${this.options.width}; height: ${this.options.height};`);
    container.addEventListener('contextmenu', this.preventDefault);

    parentMap.getContainer().appendChild(container);

    if (this.options.id !== '') {
      container.id = this.options.id;
    }

    return container;
  }

  preventDefault(e) {
    e.preventDefault();
  }

  switchLayer(baseLayer) {
    // Change map baseLayer with the new file
    if (this._miniMap.getZoom() > baseLayer.maxzoom) {
      this._miniMap.setZoom(baseLayer.maxzoom - 1);
    }
    this._miniMap.setMaxZoom(baseLayer.maxzoom);

    this._miniMap.setStyle(`assets/mapbox-gl-styles/${baseLayer.fileName}`);

    this._miniMap.on('style.load', () => {
      this.load();
    });
  }

}
