import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { Subscription } from 'rxjs';
import { geosource } from '../../../i18n/traductions';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { linkFormats } from '../../shared/models';
import { MapOptions } from '../models/map-options';
import { GeocoderService } from '../services/geocoder.service';
import { MapService } from '../services/map.service';
import { settings } from '../settings';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})

export class MapComponent implements OnInit, OnDestroy,OnChanges {

  @Input() mapOptions: MapOptions;

  @Input() isHomeMap = false;

  settings = settings;
  linkFormats = linkFormats;
  lang: string; // (fr or en)

  featuresToChange: Subscription;

  map: mapboxgl.Map;

  shareMessage: string = geosource.mapMessages.share;

  // Key of the lay to be displayed from baseLayers in environment files
  selectedBaseLayer = this.settings.baseLayers[this.settings.defaultBaseLayer];
  displayPitchSlider = false;
  displayButton3d = false;
  display3d = false;

  // Attributes to manage the display of the pudate map button
  totalData = 0;

  previousDatasetId: string;
  availableLayers: string[];

  geolocation = false;

  searchLocationResult = [];
  markerLocation: mapboxgl.Marker;

  constructor(
    private _mapService: MapService,
    private _geocoderService: GeocoderService,
  ) { }

  ngOnInit() {
    this.shareMessage = geosource.mapMessages.share;

    // Geolocation availability in the browser
    if ('geolocation' in navigator) {
      this.geolocation = true;
    }

    this.initilizeLanguage();
    // this.initAvailableLayers();
    this._mapService.destroyMap();
    this.constructMap();
  }
  ngOnChanges(changes: SimpleChanges): void{
    this._mapService.destroyMap();
    this.constructMap();
  }
  /* This method is used when the container of the map has changed in width,
  *  Natively, Mapbox re-center when a resize() is done, but for the user experience is not great.
  *  For this, we resize the map but we keed the old center.
  */
  reDraw() {
    if (this.map) {
      const nw = this.map.getBounds().getNorthWest();
      this.map.resize();
      const newNw = this.map.getBounds().getNorthWest();
      const currentCenter = this.map.getCenter();
      const shiftVector = { x: newNw.lng - nw.lng, y: newNw.lat - nw.lat };
      const newCenter = new mapboxgl.LngLat(currentCenter.lng - shiftVector.x, currentCenter.lat - shiftVector.y);
      this.map.setCenter(newCenter);
    }
  }

  // To avoid call the constructMap when we left the component
  // (because this is a custom observable, this is not automatically unsubscribed
  // when the component is destroyed)
  ngOnDestroy() {
    this._mapService.destroyMap();
  }

  searchAdress(value: string) {
    // Subscribe to search location input
    const item = this.searchLocationResult.find(e => e.caption === value);
    if (item) {
      this.flyTo(item.value);
    } else {
      this._geocoderService.searchLocation(value).subscribe((response) => {
        this.searchLocationResult = response;
      });
    }
  }

  flyTo(option) {
    if (this.markerLocation) {
      this.markerLocation.remove();
    }

    this.map.flyTo({
      center: option.value,
      zoom: 15,
      speed: 1,
      curve: 1,
    });

    this.addMarkerLocation(option.value);
  }

  addMarkerLocation(position) {
    // create a DOM element for the marker
    const el = document.createElement('div');
    el.className = 'marker';
    el.style.backgroundImage = 'url(./assets/img/position.svg)';
    el.style.backgroundRepeat = 'no-repeat';
    el.style.backgroundPosition = 'center';
    el.style.width = '50px';
    el.style.height = '50px';
    this.markerLocation = new mapboxgl.Marker(el).setLngLat(position).addTo(this.map);
  }

  removeMarker() {
    this.markerLocation.remove();
  }

  centerToMyPosition() {
    // Remove the previous marker if exists (in case the user has moved)
    if (this.markerLocation) {
      this.removeMarker();
    }

    navigator.geolocation.getCurrentPosition((position) => {
      this.map.flyTo({
        center: [position.coords.longitude, position.coords.latitude],
        zoom: 15,
        speed: 1,
        curve: 1,
      });

      this.addMarkerLocation([position.coords.longitude, position.coords.latitude]);
    });

  }

  // When we get a metadata, we display the following:
  // - load Mapbox style file to get the styles and the base layers (vector, plan, satellite)
  constructMap() {

    if (this.mapOptions) {

      // Set the basic and default options
      const mapboxOptions = {
        container: 'map',
        center: [4.85, 45.75] as mapboxgl.LngLatLike,
        // minZoom: 9,
        zoom: 12,
      };

      // Check if in the url we have map options
      const urlSplited = window.location.href.split('?map=');
      const url = urlSplited[0];
      const parameters = urlSplited.length > 1 ? decodeURIComponent(urlSplited[1]).split('/') : [];
      // The order of parameters are:
      // 0: zoom level
      // 1: latitude
      // 2: longitude
      // 3: bearing
      // 4: pitch
      // 5: selectedBaseLayerId
      // If parameters inside the url exists (from permalink), we set options form it
      if (parameters.length === 6) {
        mapboxOptions.zoom = parseFloat(parameters[0]);
        mapboxOptions.center = [parseFloat(parameters[2]), parseFloat(parameters[1])];
        mapboxOptions['bearing'] = parseFloat(parameters[3]);
        mapboxOptions['pitch'] = parseFloat(parameters[4]);
        const baseLayer = this.settings.baseLayers.find(e => e.id === parseInt(parameters[5], 10));
        if (baseLayer !== undefined) {
          this.selectedBaseLayer = baseLayer;
        }
      }
      // If the map is already constructed inside the service (for example go to Info tab and come back to Map)
      // we set the mapboxOptions from the existing map
      // If the datasetId is different from previous navigation (for example the user comes from the result page),
      // we will display the map with default settings.
      // tslint:disable-next-line:brace-style
      else if (this._mapService.mapIsConstructed && !this.mapOptions.initOptions) {
        mapboxOptions.zoom = this._mapService.map.getZoom();
        mapboxOptions.zoom = this._mapService.map.getZoom();
        mapboxOptions.center = [this._mapService.map.getCenter().lng, this._mapService.map.getCenter().lat];
        mapboxOptions['bearing'] = this._mapService.map.getBearing();
        mapboxOptions['pitch'] = this._mapService.map.getPitch();
        this.selectedBaseLayer = this._mapService.selectedBaseLayer;
      } else {
        // If default options with no zoom or center values, we set the bbox
        const bounds = new mapboxgl.LngLatBounds(this.mapOptions.bbox);
        mapboxOptions['bounds'] = bounds;
      }

      mapboxOptions['style'] = `assets/mapbox-gl-styles/${this.selectedBaseLayer.fileName}`;

      // Create the map with the associated style Mapbox file
      // tslint:disable-next-line: max-line-length
      this.map = this._mapService.createMap(this.mapOptions, url, this.selectedBaseLayer, mapboxOptions);
      this.map.on('styledata', () => {
        this._mapService.addLayers();
      });

      // Manage the 3d layer button only if config contains the url 3d layer
      if (APP_CONFIG.layer3d) {
        this.map.on('zoomend', () => {
          if (this.map.getZoom() < 14) {
            this.displayButton3d = false;

            if (this.map.getLayer('3d-layer')) {
              // this.display3d = false;
              this.map.removeLayer('3d-layer');
            }

          } else if (this.map.getZoom() > 14) {
            this.displayButton3d = true;
            if (this.display3d) {
              this._mapService.switch3DLayer();
            }
          }
        });
      }
    }
  }

  switchLayer(baseLayer) {
    this.selectedBaseLayer = baseLayer;
    this._mapService.switchLayer(baseLayer);
    this.display3d = false;
  }

  // [WARNING] This toggle only works with two base layers
  toggleLayer() {
    if (this.selectedBaseLayer.id === this.settings.baseLayers[0].id) {
      this.selectedBaseLayer = this.settings.baseLayers[1];
    } else {
      this.selectedBaseLayer = this.settings.baseLayers[0];
    }
    this._mapService.switchLayer(this.selectedBaseLayer);
    this.display3d = false;
  }

  // Looks for the language to be used, if not indicated in the url takes the navigator default language
  initilizeLanguage() {
    let language = window.location.pathname.split('/')[1];
    if (!Array('fr', 'fr-FR', 'fr-CA').includes(language) && !Array('en', 'en-US').includes(language)) {
      language = navigator.language;
    }
    if (Array('fr', 'fr-FR', 'fr-CA').includes(language)) {
      this._mapService.lang = 'fr';
      this.lang = 'fr';
    } else {
      this._mapService.lang = 'en';
      this.lang = 'en';
    }
  }

  switch3DLayer() {
    if (this.map.isStyleLoaded()) {
      if (this.map.getSource('3d-source')) {
        this.display3d = !this.display3d;
        this._mapService.switch3DLayer();
      }
    }
  }

  copyMaplink(inputElement) {
    inputElement.select();
    document.execCommand('copy');

    this.shareMessage = geosource.mapMessages.copied;
    setTimeout(() => {
      this.shareMessage = geosource.mapMessages.share;
      // tslint:disable-next-line:align
    }, 2000);
  }

  mapUrl() {
    return this._mapService.getPermalinkUrl();
  }
}
