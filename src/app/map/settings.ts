export const settings = {

  // Map
  defaultBaseLayer: 0,
  baseLayers: [
    {
      id: 2,
      maxzoom: 21,
      fileName: 'etalab.json',
      labels: {
        fr: 'Carte',
        en: 'Map',
      },
    },
    {
      id: 4,
      maxzoom: 21,
      fileName: 'satellite_pigma.json',
      labels: {
        fr: 'Satellite',
        en: 'Satellite',
      },
    },
  ],
};
