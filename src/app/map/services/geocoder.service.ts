import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { } from 'geojson';
import { map } from 'rxjs/operators';
import { APP_CONFIG } from '../../core/services/app-config.service';

@Injectable()
export class GeocoderService {

  geoServiceUrl = APP_CONFIG.backendUrls.geocoder;
  constructor(private http: HttpClient) {
  }

  searchLocation(term) {
    // tslint:disable-next-line: max-line-length
    return this.http.get<GeoJSON.FeatureCollection>(
      // tslint:disable-next-line:max-line-length
      `${this.geoServiceUrl}${term}`).pipe(
        map((res) => {
          return res.features.map((item) => {
            // const stop = item.properties.osm_value === 'bus_stop' ? 'Arrêt ' : '';
            const housenumber = item.properties.housenumber ? item.properties.housenumber : null;
            const name = item.properties.name ? item.properties.name : '';
            const street = item.properties.street ? item.properties.street : '';
            const city = item.properties.city ? item.properties.city : '';
            const postalCode = item.properties.postcode ? item.properties.postcode : '';

            let caption = null;

            if (housenumber && street) {
              caption = `${housenumber} ${street}, ${city} ${postalCode}`;
            } else if (name) {
              caption = `${name}, ${city} ${postalCode}`;
            } else if (street) {
              caption = `${street}, ${city} ${postalCode}`;
            }

            if (!caption) {
              return null;
            }

            const obj = {
              caption,
              value: (item.geometry as GeoJSON.Point).coordinates,
            };
            return obj;
          });
        }),
      );
  }

  // reverseGeocode(coords: number[]) {
  //   // tslint:disable-next-line:max-line-length
  //   return this.http.get<FeatureCollection>(this.geoServiceUrl + `/reverse?distance_sort=true&lon=${coords[0]}&lat=${coords[1]}`).map(res => {
  //     const item = res.features[0];
  //     const stop = item.properties.osm_value === 'stop' ? 'Arrêt ' : '';
  //     const housenumber = item.properties.housenumber ? item.properties.housenumber + ', ' : '';
  //     const name = item.properties.name ? item.properties.name + ' ' : '';
  //     const street = item.properties.street ? item.properties.street : '';
  //     const city = item.properties.city;
  //     return `${stop}${housenumber}${name}${street}, ${city}`;
  //   }
  //   );
  // }
}
