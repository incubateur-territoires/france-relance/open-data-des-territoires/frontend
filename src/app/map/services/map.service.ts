import { Injectable } from '@angular/core';
import centroid from '@turf/centroid';
import * as mapboxgl from 'mapbox-gl';
import { fromEvent, Observable, Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { notificationMessages } from '../../../i18n/traductions.fr';
import { Notification } from '../../core/models';
import { NotificationService } from '../../core/services';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { MapOptions } from '../models/map-options';

@Injectable()
export class MapService {

  lang: string;

  private _map: mapboxgl.Map;
  private url: string;
  selectedBaseLayer: any;
  mapOptions: MapOptions;

  eventPopupAdded = false;
  eventsData = false;
  mapIsConstructed: boolean = false;
  // popup: mapboxgl.Popup;

  // Map features colors
  featureColor: string = '#4668ab';
  featureHoverColor: string = '#E19190';
  featureHighlightedColor: string = '#da322f'; // Tomato color

  hoveredFeatureId: string;
  highlightedFeatureId: string;

  private _mapSubject = new Subject<any>();
  private _errorSubscription: Subscription;

  selectedFeature; // Contains the gid of the selected feature

  // Properties used to send information to the component
  // about the features state (need to update or not)
  private _mapToUpdate = new Subject<any>();
  private _featureSelected = new Subject<mapboxgl.MapboxGeoJSONFeature>();

  // Mapbox expression. Is set differently depending the data type (circle, fill, line)
  private paintExpression = {};
  private paintColorPropertyName = '';

  constructor(
    private _notificationService: NotificationService,
  ) { }

  // Init the map with basic options for controls, transform request etc...
  createMap(
    mapOptions: MapOptions,
    url: string, baseLayer: any, options?: mapboxgl.MapboxOptions): mapboxgl.Map {
    this.mapOptions = mapOptions;
    this.url = url;

    // Reset to false in ordre to set event listener
    this.eventPopupAdded = false;

    this.selectedBaseLayer = baseLayer;

    options.transformRequest = (url, resourceType) => {
      if (resourceType === 'Tile' && url.startsWith(this.mapOptions.mvtUrl)) {
        return {
          url,
          credentials: 'include',
        };
      }
      if (resourceType === 'Tile' && url.includes('ign')) {
        return {
          url,
        };
      }
    };

    this._map = new mapboxgl.Map(options);

    if(mapOptions.showScaleBar==undefined || mapOptions.showScaleBar!=false)
    {
      const scale = new mapboxgl.ScaleControl({
        maxWidth: 80,
        unit: 'metric',
      });
      this._map.addControl(scale, 'bottom-right');
    }


    const nav = new mapboxgl.NavigationControl();
    let navControlPosition:any='bottom-right';
    if(mapOptions.navControlPosition!=undefined){
      navControlPosition=mapOptions.navControlPosition;
    }
    this._map.addControl(nav, navControlPosition);

    // Create an observable from the map error event and only emit after 1s in order
    // to avoid to many error at the same time
    const errorObservable = fromEvent(this._map, 'error').pipe(debounceTime(1000));

    // Subscribe to the error observable and send a notification
    this._errorSubscription = errorObservable.subscribe((v:any) => {
      console.log(v);
      let ignorError = false;
      if(v.error.status==400 && v.error.url.indexOf("GetTile")){
        ignorError = true;
      }
      if(!ignorError){
        this._notificationService.notify(
          new Notification({
            message: notificationMessages.geosource.mapError,
            type: 'error',
          }),
        );
      }

    });

    this._mapSubject.next();

    this.mapIsConstructed = true;

    return this._map;
  }

  // This adds 2 layers:
  // - a WMS layer to display the visual part of the features (WMS service send a png or jpeg)
  // - a data layer, created from a geojson or an MVT service. It is used for the features interaction (hover, click)
  addLayers() {

    // Raster layer in charge of displaying the data
    this.addWMSLayer();

    // Add the 3d source. Constructed with MVT tiles from the 'fpc_fond_plan_communaut.fpctoit' dataset
    if (APP_CONFIG.layer3d) {
      const url = APP_CONFIG.layer3d.url;
      this._map.addSource('3d-source', {
        type: 'vector',
        tiles: [url],
      });
    }

    // Add the data layer only if it comes from PIGMA
    if (this.mapOptions.rasterService.url.includes(`pigma`)
    ||this.mapOptions.rasterService.url.includes(`onegeo`)
    ||this.mapOptions.rasterService.url.includes(`grandlyon`)
    ||this.mapOptions.rasterService.url.includes(`localhost`)) {
      // There is two ways to add tha data layers: from a geojson or from a MVT url
      if (!this.mapOptions.isMVT) {
        if (this.mapOptions.geojson && this._map.getSource('vector-source')==undefined) {
          const url = `${this.mapOptions.vectorService.url}?SERVICE=WFS&VERSION=2.0.0&request=GetFeature&typename=${this.mapOptions.vectorService.name}&outputFormat=application/json`;
          this._map.addSource('vector-source', {
              type: 'geojson',
              tolerance:1,
              data: url
            });
            this.addDataLayer();
        }
     } else {
        if (this.mapOptions.vectorService && this.mapOptions.vectorService.url && this._map.getSource('vector-source')==undefined) {
          //this.mapOptions.vectorService.url='https://dev.pigma.neogeo.fr/geoserver/gwc/service/wmts';
          //this.mapOptions.vectorService.url='http://localhost/geos_proxy/gwc/service/wmts';
          this.mapOptions.vectorService.url=APP_CONFIG.backendUrls.proxyWmts;
          const tileMatrix='EPSG:900913';
          const url =`${this.mapOptions.vectorService.url}?LAYERS` +
          `=${this.mapOptions.vectorService.name}&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&layer=${this.mapOptions.vectorService.name}&TILEMATRIX=${tileMatrix}:{z}&TILEMATRIXSET=${tileMatrix}&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y} `;   
          this._map.addSource('vector-source', {
            type: 'vector',
            promoteId:'gid',//TODO trouver le moyen de récupérer le nom de l'attibut qui sert d'ID
            tiles: [url],
          });

          this.addDataLayer();

        }
      }
    }

    // If satellite baselayer, then add a line layer to draw in black the frontier
    // between Métropole and IGN view images
    if (this.selectedBaseLayer.fileName === 'satellite.json') {
      this.addSeparationTerritoryLayer();
      this.addIgnBaseLayer();
    }
  }

  addIgnBaseLayer() {

    // Add this layer below all the other layers (as first in the stack)
    const firstLayer = this._map.getStyle().layers[0];

    this._map.addSource('raster-tiles-ign', {
      type: 'raster',
      tiles: [
        `${APP_CONFIG.backendUrls.proxyQuery}/map/ign?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1` +
        '&request=GetMap&srs=EPSG:3857&width=512&height=512&transparent=true&layers=ORTHOIMAGERY.ORTHOPHOTOS.BDORTHO&styles=normal',
      ],
      maxzoom: 21,
    });

    this._map.addLayer({
      id: 'satellite-tiles-ign',
      type: 'raster',
      source: 'raster-tiles-ign',
      paint: {
        'raster-saturation': -0.5,
      },
      // tslint:disable-next-line: align
    }, firstLayer.id);
  }

  addSeparationTerritoryLayer() {
    // Add the Metropole polygon layer
    this._map.addSource('polygon-metropole', {
      type: 'geojson',
      lineMetrics: true,
      data: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              name: 'Métropole',
            },
            geometry: {
              type: 'LineString',
              coordinates: [[4.664889456606686, 45.546883237913455], [4.677293671678624, 45.96069089388331],
              [4.844965771994277, 45.958104771794908], [4.843771328197183, 45.922124012308018],
              [4.908214607359763, 45.921064482585919], [4.906980046164964, 45.885084194120161],
              [5.113048045465636, 45.881451508470512], [5.107934333793898, 45.746532535709612],
              [5.172169234171356, 45.745323877108788], [5.166227999823527, 45.592416594858285],
              [4.884346903146746, 45.597449627708009], [4.882529381431865, 45.543477090694203],
              [4.664889456606686, 45.546883237913455]],
            },
          },
        ],
      },
    });

    this._map.addLayer({
      id: 'polygon-metropole',
      type: 'line',
      source: 'polygon-metropole',
      layout: {
        'line-join': 'round',
        'line-cap': 'round',
      },
      paint: {
        'line-color': 'black',
        'line-width': 3,
        'line-offset': -1,
      },
      minzoom: 9,
    });

    this._map.on('mouseenter', 'polygon-metropole', (e) => {
      // Change the cursor style as a UI indicator.
      this._map.getCanvas().style.cursor = 'pointer';
    });

    // If not already done, add all the events listeners
    if (!this.eventPopupAdded) {
      this._map.on('click', 'polygon-metropole', (e) => {

        // Check the browser language to display the popup message
        const popupText = this.lang === 'fr' ?
          '<p>Deux orthophotographies constituent la vue aérienne du territoire :</p>' +
          '<ul><li>À grande échelle : vue aérienne à très haute résolution réalisée par la Métropole de Lyon en 2018</li>' +
          '<li>À moyenne échelle : vue aérienne à haute résolution réalisée par l’IGN en 2017</ul></p>' :
          '<p> Two orthophotographs constitute the aerial view of the territory: </p> ' +
          '<ul> <li> On a large scale: very high resolution aerial view taken by the Métropole de Lyon in 2018 </li>' +
          '<li> On a middle scale: high-resolution aerial view produced by IGN in 2017 </ul> </p>';

        const coordinates = e.lngLat;
        // Populate the popup and set its coordinates
        // based on the feature found.
        // Create a popup, but don't add it to the map yet.
        new mapboxgl.Popup({ className: 'popup', maxWidth: '350px' })
          .setLngLat(coordinates)
          .setHTML(popupText)
          .addTo(this._map);
      });

      this._map.on('mouseleave', 'polygon-metropole', () => {
        this._map.getCanvas().style.cursor = '';
      });
      this.eventPopupAdded = true;
    }
  }

  addClickEventOnLayer(layer: string) {
    this._map.on('click', layer, (e) => {
      // Set a variable that is verified on the map click event (evey click not only layers)
      // when set to tru it will prevent the action on the map click event to be executed
      e.stopPropagation = true;

      this.selectedFeature = e.features[0].id;

      // Reset the hover and highglithed state for the current and previous feature
      this.changeFeatureState(this.hoveredFeatureId, { highlight: false });
      this.changeFeatureState(this.highlightedFeatureId, { highlight: false });

      // Set highlited style for the current feature
      this.highlightedFeatureId = this.selectedFeature;
      this.changeFeatureState(this.highlightedFeatureId, { highlight: true });

      if (e.point.x > (this._map.getCanvas().width - 400)) {
        // If the screen is not mobile the dataset data details panel push the map that is then smaller
        // therefore we make sure the point/surface/line is still in the view box by recentering the map on the point
        // 768 correspond to 768px which is the mobile breakpoint defined by Bulma our css framework
        if (window.innerWidth > 768) {
          // 70 is the width of the dataset data details panel divided by 4 (by 2 because the map and the table
          // share the remaining space and again by 2 because we want to center the point so there is half on the
          // left, half on the right)
          this._map.panTo(e.lngLat, {
            duration: 500,
            // 70 is the width of the dataset data details panel divided by 4 (by 2 because the map and the table
            // share the remaining space and again by 2 because we want to center the point so there is half on the
            // left, half on the right)
            offset: new mapboxgl.Point(-70, 0),
          },
          );
        }
      }

      this._featureSelected.next(e.features[0]);
    });
  }

  // Change the state of one feature.
  // State has the following format:{state: value}
  // Ex: {hover: true, highlight: false}
  changeFeatureState(featureId: string, state) {
    let stateSource = {} as mapboxgl.FeatureIdentifier;
    const name=this.mapOptions.vectorService.name.split(':')[1];
    if (!this.mapOptions.isMVT) {
      stateSource = {
        source: 'vector-source',
        id: featureId,
      };
    } else {
      stateSource = {
        source: 'vector-source',
        sourceLayer: name,
        id: featureId,
      };
    }
    if (featureId) {
      const oldState = this._map.getFeatureState(stateSource);

      this._map.setFeatureState(
        stateSource,
        { ...oldState, ...state });
    }
  }

  // Add the raster (WMS) layer
  addWMSLayer() {
    console.log('addWMSLayer')
    const name=this.mapOptions.rasterService.name;
    if(name && name.length>0 && this.mapOptions.rasterService.url.length>0 && this._map.getSource('wms-source')==undefined )
    {
      this._map.addSource('wms-source', {
        type: 'raster',
        tiles: [
          `${this.mapOptions.rasterService.url}?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&` +
          `srs=EPSG:3857&width=256&height=256&transparent=true&layers=${name}`,
        ],
        tileSize: 256,
      });
  
      this._map.addLayer({
        id: 'wms-layer',
        type: 'raster',
        source: 'wms-source',
        paint: {},
      });
    }


  }

  // Add the data layer (from geojson or MVT)
  addDataLayer() {

    let layerOptions = {};

    // Set the paint options depending the geometry type
    // For 'Polygon' and 'MultiPolygon' features
    if (this.mapOptions.dataType.isAreal) {
      this.paintExpression = {
        'fill-color': ['case',
          ['boolean', ['feature-state', 'highlight'], false],
          this.featureHighlightedColor,
          ['boolean', ['feature-state', 'hover'], false],
          this.featureHoverColor,
          'transparent',
        ],
        'fill-opacity': 0.7,
        'fill-outline-color': 'transparent',
      };
      this.paintColorPropertyName = 'fill-color';

      layerOptions = {
        type: 'fill',
        paint: this.paintExpression,
      };
    }

    // For 'Line' and 'MultiLine' features
    if (this.mapOptions.dataType.isLinear) {

      this.paintExpression = {
        'line-color': ['case',
          ['boolean', ['feature-state', 'highlight'], false],
          this.featureHighlightedColor,
          ['boolean', ['feature-state', 'hover'], false],
          this.featureHoverColor,
          'transparent',
        ],
        'line-width': [
          'interpolate', ['linear'], ['zoom'],
          12, 2,
          13, 4,
        ],
        'line-opacity': 0.8,
      };
      this.paintColorPropertyName = 'line-color';

      layerOptions = {
        type: 'line',
        paint: this.paintExpression,
      };
    }

    // For "Point" features
    if (this.mapOptions.dataType.isPunctual) {

      // Set the color expression
      this.paintExpression = {
        'circle-radius': ['case',
          ['boolean', ['feature-state', 'highlight'], false],
          2.5,
          5,
        ],
        'circle-color': ['case',
          ['boolean', ['feature-state', 'highlight'], false],
          'white',
          ['boolean', ['feature-state', 'hover'], false],
          this.featureHoverColor,
          'transparent',
        ],
        'circle-stroke-width': ['case',
          ['boolean', ['feature-state', 'highlight'], false],
          5,
          0,
        ],
        'circle-stroke-color': ['case',
          ['boolean', ['feature-state', 'highlight'], false],
          this.featureHighlightedColor,
          ['boolean', ['feature-state', 'visited'], false],
          'white',
          'transparent',
        ],
      };
      this.paintColorPropertyName = 'circle-color';

      layerOptions = {
        type: 'circle',
        paint: this.paintExpression,
      };
    }

    const name=this.mapOptions.vectorService.name.split(':')[1];
    this._map.addLayer(
      {
        id: 'data-layer',
        source: 'vector-source',
        ...layerOptions,
        ...(this.mapOptions.isMVT ? { 'source-layer': name } : {}), // if from MVT, this property is needed
      });

    // If not already done, add all the events listeners
    if (!this.eventsData) {

      // Manage the cursor and feature state for data layer when mouse events
      this._map.on('mousemove', 'data-layer', (e) => {
        this.manageFeatureOnMouseMove(e.features);
      });
      this._map.on('mouseleave', 'data-layer', (e) => {
        this.manageFeatureOnMouseEnd();
      });
      this.addClickEventOnLayer('data-layer');

      // [WARNING] The click event on the map has to be declared after the click event on layers
      // in order for the stopPropagation to work properly
      this._map.on('click', (e) => {
        // If the click was on a layer the stopPropagation property is set
        // we only want to execute the code when the click was not on a feature of a layer
        if (!e.stopPropagation) {
          this.resetSelectedState();
          setTimeout(
            () => {
              const nw = this._map.getBounds().getNorthWest();
              this._map.resize();
              const newNw = this.map.getBounds().getNorthWest();
              const currentCenter = this._map.getCenter();
              const shiftVector = { x: newNw.lng - nw.lng, y: newNw.lat - nw.lat };
              const newCenter = new mapboxgl.LngLat(
                currentCenter.lng - shiftVector.x,
                currentCenter.lat - shiftVector.y,
              );
              this._map.setCenter(newCenter);
            },
            600,
          );
        }
      });
      this.eventsData = true;
    }
  }

  manageFeatureOnMouseMove(features: any) {
    if (features.length > 0) {
      this._map.getCanvas().style.cursor = 'pointer';
      if (this.hoveredFeatureId) {
        this.changeFeatureState(this.hoveredFeatureId, { hover: false });
      }

      this.hoveredFeatureId = features[0].id;

      this.changeFeatureState(this.hoveredFeatureId, { hover: true });
    }
  }

  manageFeatureOnMouseEnd() {
    if (this.hoveredFeatureId) {
      this._map.getCanvas().style.cursor = '';
      this.changeFeatureState(this.hoveredFeatureId, { hover: false });
    }
    this.hoveredFeatureId = null;
  }

  destroyMap() {
    if(this._map){
      try{
        this._map.remove();
      }
      catch(e){
        console.error(e);
      }
      
    }
    this.eventsData = false;
    if(this._errorSubscription){
      this._errorSubscription.unsubscribe();
    }

  }

  switch3DLayer() {
    if (!this._map.getLayer('3d-layer') && this._map.getSource('3d-source')) {

      this._map.addLayer(
        {
          id: '3d-layer',
          type: 'fill-extrusion',
          source: '3d-source',
          'source-layer': 'fpc_fond_plan_communaut.fpctoit',
          paint: {
            'fill-extrusion-color': '#E0E4EF',
            'fill-extrusion-height': {
              property: 'htotale',
              type: 'identity',
            },
            'fill-extrusion-opacity': 0.7,
          },
        });
    } else {
      this._map.removeLayer('3d-layer');
    }
  }

  // Generate the url with map options
  // http://hostName#zoom/latitude/longitude/bearing/pitch/selectedBaseLayerId
  getPermalinkUrl() {
    return `${this.url}?map=${this._map.getZoom()}/${this._map.getCenter().lat}/${this._map.getCenter().lng}` +
      `/${this._map.getBearing()}/${this._map.getPitch()}/${this.selectedBaseLayer.id}`;
  }

  switchLayer(baseLayer) {
    // Set selected base layer
    this.selectedBaseLayer = baseLayer;
    // Change map baseLayer with the new file
    if (this._map.getZoom() > this.selectedBaseLayer.maxzoom) {
      this._map.setZoom(this.selectedBaseLayer.maxzoom - 1);
    }
    this._map.setMaxZoom(this.selectedBaseLayer.maxzoom);

    this._map.setStyle(`assets/mapbox-gl-styles/${this.selectedBaseLayer.fileName}`);    
  }

  setSelectedFeature(selectedFeature) {
    if (selectedFeature && this.selectedFeature !== selectedFeature.id) {
      this.selectedFeature = selectedFeature.id;

      // Reset the hover and highglithed state for the current and previous feature
      this.changeFeatureState(this.hoveredFeatureId, { highlight: false });
      this.changeFeatureState(this.highlightedFeatureId, { highlight: false });

      // Set highlited style for the current feature
      this.highlightedFeatureId = this.selectedFeature;
      this.changeFeatureState(this.highlightedFeatureId, { highlight: true });

      const pointCenter = selectedFeature.geometry.type === 'Point' ?
        selectedFeature.geometry.coordinates : centroid(selectedFeature).geometry.coordinates;
      this._map.panTo(pointCenter, {
        duration: 500,
        // 70 is the width of the dataset data details panel divided by 4 (by 2 because the map and the table
        // share the remaining space and again by 2 because we want to center the point so there is half on the
        // left, half on the right)
        offset: new mapboxgl.Point(-70, 0),
      },
      );

    }
  }

  /*
  * When the search value has been changed, we add a text expression
  * that filters the features containing this text value in at least one of its properties.
  * If there is a match we:
  * - decrease the opacity for the raster layer (WMS) which displays all the features
  * - show the found features from our data layer (WMT or GeoJSON)
  */
  filterBySearchValue(searchValue: string, properties: string[]) {

    if (searchValue) {

      const escapedSearchString = searchValue.replace(/[\=~><\"\?^\${}\(\)\|\&\:\!\/[\]\\]/g, '\\$&');
      const words = escapedSearchString.split(/\s+/);

      // Some basic explanations for the operators expression we use:
      // - "all": returns true if all the conditions are true
      // - "any": returns true if one of the the conditions are true
      // - "in":  can be used in many context, here it looks for a substring in a string. To make it case insensitive, set
      // both the search value and the property value to uppercase with the operator 'upcase'.
      // To learn more about it: https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions
      // Here is the format of the expression, for example with 2 words:
      // ['all',
      //   ['any',
      //      ['in', ['upcase', 'word1'], ['upcase', ['to-string', ['get', property1]]]],
      //      ['in', ['upcase', 'word1'], ['upcase', ['to-string', ['get', property2]]]],
      //      ['in', ['upcase', 'word1'], ['upcase', ['to-string', ['get', property3]]]]
      //   ],
      //   ['any',
      //      ['in', ['upcase', 'word2'], ['upcase', ['to-string', ['get', property1]]]],
      //      ['in', ['upcase', 'word2'], ['upcase', ['to-string', ['get', property2]]]],
      //      ['in', ['upcase', 'word2'], ['upcase', ['to-string', ['get', property3]]]]
      //   ],
      // ]
      const anyFilter = [];
      words.forEach((word) => {
        const propertiesFilters = [];
        properties.forEach((property) => {
          propertiesFilters.push(['in', ['upcase', word], ['upcase', ['to-string', ['get', property]]]]);
        });
        anyFilter.push(['any', ...propertiesFilters]);
      });

      // Once this search filter expression is done, we add it to the existing paint options (for hover, selected)
      // of the data-layer
      const copyPaintOptions = [... this._map.getPaintProperty('data-layer', this.paintColorPropertyName)];
      copyPaintOptions.splice(copyPaintOptions.length - 1, 0, ['all', ...anyFilter], this.featureColor);

      this._map.setFilter('data-layer', ['all', ...anyFilter]);
      this._map.setPaintProperty('data-layer', this.paintColorPropertyName, copyPaintOptions);
      this._map.setPaintProperty('wms-layer', 'raster-opacity', 0.3);

    } else {
      // If value is empty, remove the search expression and set the opacity for the raster layer back to 1.
      this._map.setFilter('data-layer', null);
      this._map.setPaintProperty('data-layer', this.paintColorPropertyName, this.paintExpression[this.paintColorPropertyName]);
      this._map.setPaintProperty('wms-layer', 'raster-opacity', 1);

    }
  }

  get map$(): Observable<void> {
    return this._mapSubject.asObservable();
  }

  get map() {
    return this._map;
  }

  resetSelectedState() {
    this.changeFeatureState(this.highlightedFeatureId, { highlight: false });
    this.selectedFeature = null;
    this.highlightedFeatureId = null;
    this._featureSelected.next(null);
  }

  get featuresToUpdate$(): Observable<void> {
    return this._mapToUpdate.asObservable();
  }

  get featureSelected$(): Observable<any> {
    return this._featureSelected.asObservable();
  }

}
