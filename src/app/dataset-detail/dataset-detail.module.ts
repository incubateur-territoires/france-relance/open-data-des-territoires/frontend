import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { InlineSVGModule } from 'ng-inline-svg';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ElasticsearchModule } from '../elasticsearch/elasticsearch.module';
import { MapModule } from '../map/map.module';
import { SharedModule } from '../shared/shared.module';
import { DatasetDetailComponents } from './components';
import { DatasetDetailRoutingModule } from './dataset-detail-routing.module';
import { DatasetDetailResolvers } from './resolvers';
import { DatasetDetailServices } from './services';

@NgModule({
  declarations: [...DatasetDetailComponents],
  imports: [
    CommonModule,
    DatasetDetailRoutingModule,
    ElasticsearchModule,
    FormsModule,
    ReactiveFormsModule,
    MapModule,
    SharedModule,
//    InlineSVGModule.forRoot(),
    InfiniteScrollModule,
  ],
  providers: [...DatasetDetailServices, ...DatasetDetailResolvers],
})
export class DatasetDetailModule { }
