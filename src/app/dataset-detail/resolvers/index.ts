import { DatasetDetailResolver } from './dataset-detail.resolver';

export { DatasetDetailResolver };

// tslint:disable-next-line:variable-name
export const DatasetDetailResolvers = [
  DatasetDetailResolver,
];
