import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Dataset } from '../../shared/models';
import { DatasetDetailService } from '../services';

@Injectable()
export class DatasetDetailResolver implements Resolve<Dataset> {
  constructor(private datasetDetailService: DatasetDetailService) { }

  resolve(route: ActivatedRouteSnapshot) {
    let toBeReturned;
    if (
      !(route.params.id === this.datasetDetailService.dataset.slug) &&
      !(route.params.id === this.datasetDetailService.dataset.uuid)
    ) {
      toBeReturned = this.datasetDetailService.initializeDataset(route.params.id).pipe(
        catchError(() => of(null)),
      );
    } else {
      toBeReturned = of(null);
    }

    return toBeReturned;
  }
}
