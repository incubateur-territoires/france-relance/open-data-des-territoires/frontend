import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import lodashClonedeep from 'lodash.clonedeep';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { IMetadataLink, linkFormats, Metadata } from '../../shared/models';
import { IResource, Resource } from '../models';
import { IProjection, Projection } from '../models/projection.model';
@Injectable()
export class ResourcesService {

  linkFormats = linkFormats;
  resources: Resource[];
  projections: Projection[];

  constructor(
    private _httpClient: HttpClient,
  ) { }

  getResources() {
    if (this.resources && this.resources.length > 0) {
      return of(this.resources);
    }

    return this._httpClient.get<IResource[]>(`${APP_CONFIG.backendUrls.resources}/resources`).pipe(
      map((resourcesBack) => {
        this.resources = resourcesBack.map(r => new Resource(r));
        return this.resources;
      }),
    );
  }

  getProjections() {
    if (this.projections && this.projections.length > 0) {
      return of(this.projections);
    }

    return this._httpClient.get<IProjection[]>(`${APP_CONFIG.backendUrls.resources}/projections`).pipe(
      map((projections) => {
        this.projections = projections.map(r => new Projection(r));
        return this.projections;
      }),
    );
  }

  getQueryableResources(links: IMetadataLink[]) {
    const queryableResources = {};

    links.forEach((link) => {
      const linkID = link.description !== 'null' ? link.description : link.name;
      if (link.service) {
        this.resources.forEach((resource) => {
          const resourceCopy = Object.assign({}, resource);
          resourceCopy.metadataLink = link;
          if (link.service === resource.type && resourceCopy.isQueryable) {
            if (!queryableResources[linkID]) {
              queryableResources[linkID] = [];
            }
            queryableResources[linkID].push(resourceCopy);
          }
        });
      }
    });
    return queryableResources;
  }

  getResourcesForDownload(metadata: Metadata): [any, any, IMetadataLink[]] {
    const downloadableResources = {};
    const staticResources = [];
    const otherResources: IMetadataLink[] = [];

    const links = lodashClonedeep(metadata.link);
    links.forEach((link) => {
      const linkID = link.description !== 'null' ? link.description : link.name;

      if (link.service) {
        this.resources.forEach((resource) => {
          const resourceCopy = Object.assign({}, resource);

          resourceCopy.metadataLink = link;
          if (link.service === resource.type && resourceCopy.isQueryable) {

            // We exclude Shapefile if from WFS. The Shapefile from WS has priority.
            if (resourceCopy.type === this.linkFormats.wfs) {
              resourceCopy.formats = resourceCopy.formats.filter(f => f.name !== 'ShapeFile');
            }

            // For non-geographical data, the Shapefile is not available for the WS service
            if (resourceCopy.type === 'WS' && metadata.type === 'nonGeographicDataset') {
              resourceCopy.formats = resourceCopy.formats.filter(f => f.name !== 'ShapeFile');
            }

            // WE exclude image formats for WMS and WCS. (https://redmine.neogeo.fr/issues/12382)
            if (resourceCopy.type === this.linkFormats.wms || resourceCopy.type === this.linkFormats.wcs) {
              resourceCopy.formats = resourceCopy.formats.filter(
                f => f.name !== 'PNG' && f.name !== 'JPEG' && f.name !== 'TIFF'
                      && f.name !== 'GIF' && f.name !== 'AAIGrid');
            }

            if (resourceCopy.formats.length > 0 ) {
              if (!downloadableResources[linkID]) {
                downloadableResources[linkID] = [];
              }

              // Instead of using the direct link to the files we go through the dedicated proxy
              // that is adding credentials of a technical account if the user has access to the requested file/data
              resourceCopy.metadataLink.url = resourceCopy.metadataLink.url.replace(
                APP_CONFIG.backendUrls.backendDomain,
                `${APP_CONFIG.backendUrls.proxyQuery}/download`,
              );
              resourceCopy.metadataLink.url = resourceCopy.metadataLink.url.replace(
                'https://download.recette.data.grandlyon.com',
                `${APP_CONFIG.backendUrls.proxyQuery}/download`,
              );

              downloadableResources[linkID].push(resourceCopy);
            }

          }
        });
      } else if (link.service === undefined) {
        //if (!link.name.includes('Licence')) { Custo PIGMA
          if (link.formats) {
            
            if(link.formats.length>1 && link.formats.indexOf('HTTP-LINK')>=0){
              link.formats =link.formats.filter(x=>x!='HTTP-LINK');
            }
            /*if (staticResources.find(x=>x.key==link.description)==undefined) { Custo PIGMA
              staticResources.push(
                  {'key':link.description,
                   'value': []
                  });
            }

            // Instead of using the direct link to the files we go through the dedicated proxy
            // that is adding credentials of a technical account if the user has access to the requested file/data
            link.url = link.url.replace(
              APP_CONFIG.backendUrls.backendDomain,
              `${APP_CONFIG.backendUrls.proxyQuery}/download`,
            );
            link.url = link.url.replace(
              'https://download.recette.data.grandlyon.com',
              `${APP_CONFIG.backendUrls.proxyQuery}/download`,
            );

            staticResources.find(x=>x.key==link.description).value.push(link);
            */
            staticResources.push(link);
          } else {
            otherResources.push(link);
          }
        }
      //}
    });

    return [downloadableResources, staticResources, otherResources];
  }
}
