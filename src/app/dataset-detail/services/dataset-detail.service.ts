import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { catchError, finalize, flatMap, map, tap } from 'rxjs/operators';
import { notificationMessages } from '../../../i18n/traductions';
import { ErrorService, MatomoService } from '../../core/services';
import { Reuse } from '../../editorialisation/models';
import { CMSContent } from '../../editorialisation/models/cms-content.model';
import { ReusesService } from '../../editorialisation/services';
import { ElasticsearchOptions, IElasticsearchResponse } from '../../elasticsearch/models';
import { ElasticsearchService } from '../../elasticsearch/services/elasticsearch.service';
// tslint:disable-next-line: max-line-length
import { Data, Dataset, EditorialMetadata, Metadata } from '../../shared/models';
import { DatasetChild, IDatasetRawChild } from '../models';

@Injectable()
export class DatasetDetailService {

  private _dataset = new Dataset();
  private _children: {
    total: number,
    data: DatasetChild[],
  } = { total: null, data: [] };
  private _datasetSubject = new Subject<any>();
  private _datasetDataSearchChanged = new Subject<string>();
  private _sampleDataLoaded = new Subject<Data[]>();
  private _initialScrollOptions = {
    from: 0,
    pageSize: 30,
    sortOptions: {
      value: null,
      order: 'asc',
    },
    searchString: '',
  };
  private _researchMaxResult: number;
  // Copy the initial value to be able to reinitialize at any moment
  private _elasticSearchOptions: ElasticsearchOptions;

  // Used to avoid to construct the map multiple times for the same datasetId
  private _lastDatasetIdForMap: string;
  private _pendingRequests: number = 0;


  constructor(
    private _errorService: ErrorService,
    private _elasticsearchService: ElasticsearchService,
    private _matomoService: MatomoService,
    private _reusesService: ReusesService,
    private _http: HttpClient,
  ) { }

  initializeDataset(slugOrUuid: string): Observable<Dataset> {
    this._dataset.data = [];
    this._dataset.nbViews = null;
    this._elasticSearchOptions = new ElasticsearchOptions(this._initialScrollOptions);

    this.newAsyncRequest();

    this._matomoService.getPageMetadataPageMetrics(slugOrUuid).pipe(
      catchError(() => of(null)),
    ).subscribe((nbViews) => {
      this._dataset.nbViews = nbViews;
    });
    // Get the metadata
    return this._elasticsearchService.getDatasetMetadata(slugOrUuid).pipe(
      map((e:any) => {
        if (e.hits.hits.length > 0) {
          const metadata = new Metadata(e.hits.hits[0]._source['metadata-fr']);
          this._dataset.uuid = metadata.geonet.uuid;
          this._dataset.slug = e.hits.hits[0]._source['slug'];
          this._dataset.editorialMetadata = e.hits.hits[0]._source['editorial-metadata'];
          //retrieve permissions on the dataset
          this._dataset.accessLevel=0;
          this._http.get("/fr/dataset/permissions/?dataset="+this._dataset.uuid)
          .subscribe((data:any) => {
            this._dataset.accessLevel=data.level;
          }); 
          this._dataset.fields = e.hits.hits[0]._source['fields'] ? e.hits.hits[0]._source['fields'] : {
            list: [],
            types: {},
          };
          metadata.dataset_id = e.hits.hits[0]._id;
          const bbox = e.hits.hits[0]._source['metadata-fr']['bbox'];
          if (bbox !== undefined && bbox['coordinates'] !== undefined) {
            const coordinates = e.hits.hits[0]._source['metadata-fr']['bbox']['coordinates'][0];
            metadata.max_west = Math.min.apply(Math, coordinates.map((point) => {
              return point[0];
            }));
            metadata.max_east = Math.max.apply(Math, coordinates.map((point) => {
              return point[0];
            }));
            metadata.max_north = Math.max.apply(Math, coordinates.map((point) => {
              return point[1];
            }));
            metadata.max_south = Math.min.apply(Math, coordinates.map((point) => {
              return point[1];
            }));
          }
          const native_bbox = e.hits.hits[0]._source['metadata-fr']['native_bbox'];
          if (native_bbox !== undefined && native_bbox['coordinates'] !== undefined) {
            const coordinates = native_bbox['coordinates'][0];
            metadata.native_max_west = Math.min.apply(Math, coordinates.map((point) => {
              return point[0];
            }));
            metadata.native_max_east = Math.max.apply(Math, coordinates.map((point) => {
              return point[0];
            }));
            metadata.native_max_north = Math.max.apply(Math, coordinates.map((point) => {
              return point[1];
            }));
            metadata.native_max_south = Math.min.apply(Math, coordinates.map((point) => {
              return point[1];
            }));
          }

          metadata.dataset_index = e.hits.hits[0]._index;
          if ((<any>e.hits.total).value) {
            this._dataset.totalData = (<any>e.hits.total).value;
          }
          else {
            this._dataset.totalData = e.hits.total;
          }

          this._dataset.metadata = metadata;
          this._dataset.editorialMetadata = new EditorialMetadata(e.hits.hits[0]._source['editorial-metadata']);
          // Disable cache for real time dataset
          this._elasticSearchOptions.useCache = this.datasetEditorialMetadata.isRealTime ? false : true;
        } else {
          this._dataset.metadata = new Metadata();
        }
        return this._dataset;
      }),
      catchError((err) => {
        throw this._errorService.handleError(err, {
          message: notificationMessages.geosource.getDatasetById, redirect: true,
        });
      }),
      flatMap((dataset) => {
        // Get reuses
        return this._reusesService.getDataSetReuses(dataset.slug).pipe(
          map((reuses) => {
            dataset.reuses = reuses;
            return dataset;
          }),
          catchError(() => {
            return of(dataset);
          }),
        );
      }),
      flatMap((dataset) => {
        // Get related articles
        return this.retrieveRelatedPosts(dataset.slug).pipe(
          map((posts) => {
            dataset.relatedNews = posts;
            return dataset;
          }),
          catchError(() => {
            return of(dataset);
          }),
        );
      }),
      // Get the parent dataset if there is one
      flatMap((dataset) => {
        if (dataset.metadata.parentDataset.id) {
          return this._elasticsearchService.getDatasetParentInfo(dataset.metadata.parentDataset.id).pipe(
            map((parent) => {
              dataset.metadata.parentDataset.title = parent.hits.hits[0]._source['metadata-fr'].title;
              dataset.metadata.parentDataset.slug = parent.hits.hits[0]._source['slug'];
              // Pick the thumbnail image among the parent dataset images
              const thumbnail = parent.hits.hits[0]._source['metadata-fr'].image.find(
                image => image.type === 'thumbnail',
              );
              if (thumbnail) {
                dataset.metadata.parentDataset.imageUrl = thumbnail.url;
              }
              return dataset;
            }),
            catchError(() => {
              return of(dataset);
            }),
          );
          // tslint:disable-next-line:no-else-after-return
        } else {
          return of(dataset);
        }
      }),
      flatMap((dataset) => {
        // Get the dataset's children
        return this._elasticsearchService.getDatasetChildren(this._dataset.uuid).pipe(
          map((res) => {
            this._children.total = res.aggregations.children_number.value;
            // Reinit the array ini case the dataset doesn't have children
            this._children.data = [];
            res.hits.hits.forEach((child) => {
              const source = child._source as IDatasetRawChild;
              const datasetChild = new DatasetChild(source);
              datasetChild.slug = child._source['slug'];
              this._children.data.push(datasetChild);
            });
            // reorder children alphabetically
            this._children.data.sort((a,b) => {
              var child1 = a.slug.toUpperCase();
              var child2 = b.slug.toUpperCase();

              return (child1 < child2) ? -1 : (child1 > child2) ? 1 : 0;
            });

            return dataset;
          }),
          catchError((err) => {
            throw this._errorService.handleError(err, { message: notificationMessages.geosource.getDatasetChildren });
          }),
        );
      }),
      finalize(() => {
        this.asyncRequestDone();
      }),
    );

  }

  // Get the next page of data, add it to the current array and return the array with the updated data
  retrieveMoreDatasetData(): Observable<Data[]> {
    this.newAsyncRequest();
    const fieldList = this._dataset.fields.list.filter((x)=>x.indexOf('date')<0).map((x)=>'data-fr.properties.'+x+'_str');
    return this._elasticsearchService.getDatasetData(this._dataset.uuid, this._elasticSearchOptions,fieldList).pipe(
      tap((res) => {
        this.setDatasetData(res);
      }),
      map((res) => {
        return this._dataset.data;
      }),
      catchError((err) => {
        throw this._errorService.handleError(err, { message: notificationMessages.geosource.getDatasetData });
      }),
      finalize(() => {
        this.asyncRequestDone();
      }),
    );
  }

  // Initialize the dataset data array with an empty array and push the data retreived
  retrieveDatasetData(): Observable<Data[]> {
    this.newAsyncRequest();
    const types = this._dataset.fields.types;
    const fieldList = this._dataset.fields.list.filter((x)=>x.indexOf('date')<0).map((x)=>'data-fr.properties.'+x+'_str');//+types[x]
    //const fieldList = this._dataset.fields.list.map((x)=>'data-fr.properties.'+x+'_str');
    return this._elasticsearchService.getDatasetData(this._dataset.uuid, this._elasticSearchOptions,fieldList).pipe(
      tap((res) => {
        this._dataset.data = [];
        console.log("retrieve dataset")
        //Gestion des différentes version d'ES 
        if((<any>res.hits.total).value != undefined) {
          this._researchMaxResult = (<any>res.hits.total).value;
        }
        else {
          this._researchMaxResult = res.hits.total;
        }
        if(this._researchMaxResult>0 && res.hits.hits[0]._source['data-fr'] !== undefined){
          this._dataset.hasTable=true;
        }
        this.setDatasetData(res);
      }),
      map((res) => {
        return this._dataset.data;
      }),
      catchError((err) => {
        throw this._errorService.handleError(err, { message: notificationMessages.geosource.getDatasetData });
      }),
      finalize(() => {
        this.asyncRequestDone();
      }),
    );
  }

  retrieveRelatedPosts(slug: string) {
    this.newAsyncRequest();

    const options = {
      sortDate: true,
      type: 'post',
      relatedDatasetSlug: slug,
    };

    return this._elasticsearchService.getPosts(options).pipe(
      map((posts) => {
        return posts.hits.hits.map(result => new CMSContent(result._source));
      }),
      catchError((err) => {
        throw this._errorService.handleError(err, { message: notificationMessages.edito.getDatasetRelatedPosts });
      }),
      finalize(() => {
        this.asyncRequestDone();
      }),
    );
  }

  setDatasetData(res: IElasticsearchResponse): void {
    res.hits.hits.forEach((e) => {
      if (e._source['data-fr'] !== undefined) {
        this._dataset.data.push(
          new Data(e._source['data-fr']),
        );
        // Set the properties order based on the natural order from the data provider (set in 'dataset.fields.list')
        this._dataset.data.forEach((data) => {
          this.orderProperties(data, this._dataset.fields.list);
        });
      }
    });
    if (this._dataset.editorialMetadata.isSample) {
      this._sampleDataLoaded.next(this._dataset.data);
    }
  }

  getKeyType(key: string) {
    return  key+'_'+ eval(`this._dataset.fields.types.${key}`);
  }

  orderProperties(data: any, orderedProperties: string[]) {
    const newDataPropertiesOrder = {};
    orderedProperties.forEach((field) => {
      // Adds type to field. First time, the field with type is used, and attributed to field without type
      // force _json detection since the fieldtype is not listed. (used for complex data)
      var fieldtype = field;
      try{
        fieldtype = field + '_' + eval(`this._dataset.fields.types["${field}"]`);
      }catch(e){
        console.error(e)
      }
      
      newDataPropertiesOrder[field] = data.properties[fieldtype] ? data.properties[fieldtype] :
          data.properties[field] ? data.properties[field] : data.properties[`${field}_json`] ?
          data.properties[`${field}_json`] : data.properties[`${field}_str`] ?
          data.properties[`${field}_str`] : '';
    });


    data.properties = newDataPropertiesOrder;
  }

  getAttributesFromWS(){
    const wsLink=this.datasetMetadata.getWsLink();
    let url = wsLink.url+"/"+wsLink.name+".json";
    return this._http.get(url);
  }

  get datasetData(): Data[] {
    return this._dataset.data;
  }

  get datasetMetadata(): Metadata {
    return this._dataset.metadata;
  }

  get dataset(): Dataset {
    return this._dataset;
  }

  get lastDatasetIdForMap(): string {
    return this._lastDatasetIdForMap;
  }

  set lastDatasetIdForMap(datasetId: string) {
    this._lastDatasetIdForMap = datasetId;
  }

  get datasetEditorialMetadata(): EditorialMetadata {
    return this._dataset.editorialMetadata;
  }

  get dataset$(): Observable<void> {
    return this._datasetSubject.asObservable();
  }

  get datasetDataSearchChanged$(): Observable<string> {
    return this._datasetDataSearchChanged.asObservable();
  }

  get sampleDataLoaded$(): Observable<Data[]> {
    return this._sampleDataLoaded.asObservable();
  }

  get datasetChildren() {
    return this._children;
  }

  get datasetDataNumber(): number {
    return this._dataset.totalData;
  }

  get datasetNumberOfViews(): number {
    return this._dataset.nbViews;
  }

  get scrollOptionsSum(): number {
    return this._elasticSearchOptions.pageSize + this._elasticSearchOptions.from;
  }

  get sortValue(): string {
    return this._elasticSearchOptions.sortOptions.value;
  }

  get sortOrder(): string {
    return this._elasticSearchOptions.sortOptions.order;
  }

  get searchString() {
    return this._elasticSearchOptions.searchString;
  }

  get researchMaxResult() {
    return this._researchMaxResult;
  }

  get datasetIndex() {
    return this._dataset.uuid;
  }
  get datasetReuses(): Reuse[] {
    return this._dataset.reuses;
  }

  sortBy(val: string): void {
    // Reset the from of the query to start again from the beggining
    this._elasticSearchOptions.from = this._initialScrollOptions.from;
    // If same value jsut reverse the order
    if (this._elasticSearchOptions.sortOptions.value === val) {
      this.reverseSortOrder();
    } else {
      // Set the new value and take initial order
      this._elasticSearchOptions.sortOptions.value = val;
      this._elasticSearchOptions.sortOptions.order = this._initialScrollOptions.sortOptions.order;
    }
  }

  searchChanged(value: string) {
    // Reset the from of the query to start again from the beggining
    this._elasticSearchOptions.from = this._initialScrollOptions.from;
    this._elasticSearchOptions.searchString = value;
    this._datasetDataSearchChanged.next(value);
  }

  reverseSortOrder(): void {
    if (this._elasticSearchOptions.sortOptions.order === 'asc') {
      this._elasticSearchOptions.sortOptions.order = 'desc';
    } else {
      this._elasticSearchOptions.sortOptions.order = 'asc';
    }
  }

  putDataAtBeginningOfDatasetDataArray(data: Data) {
    if (data) {
      this._dataset.data = this._dataset.data.filter(item => item.id !== data.id);
      this._dataset.data.unshift(data);
    }
  }

  datasetChanged(): void {
    this._datasetSubject.next();
  }

  nextPage(): void {
    // Increase the from value by 'size' (think of a pagination)
    // The next request that will use this value will get the 'size'next elements
    this._elasticSearchOptions.from += this._elasticSearchOptions.pageSize;
  }

  // Helpers for knowing if there are pending requests or not
  // Increments the number of curretly running async request by one
  newAsyncRequest() {
    this._pendingRequests += 1;
  }

  // Decrement the number of async requests by one
  asyncRequestDone() {
    this._pendingRequests -= 1;
  }

  // Return true if http queries are being made
  get isLoading() {
    return this._pendingRequests > 0 ? true : false;
  }

}
