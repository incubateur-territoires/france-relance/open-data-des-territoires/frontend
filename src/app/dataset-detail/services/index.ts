import { DatasetDetailService } from './dataset-detail.service';
import { ResourcesService } from './resources.service';
import { UsageStatisticsService } from './usage-statistics.service';

export { DatasetDetailService, ResourcesService };

// tslint:disable-next-line:variable-name
export const DatasetDetailServices = [
  DatasetDetailService,
  ResourcesService,
  UsageStatisticsService,
];
