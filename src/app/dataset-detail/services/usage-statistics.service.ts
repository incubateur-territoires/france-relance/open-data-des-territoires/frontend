import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { DatasetUsageStatistics } from '../models';

@Injectable()
export class UsageStatisticsService {

  constructor(
    private _httpClient: HttpClient,
  ) { }

  getDatasetLastYearStats(uuid: string): Observable<DatasetUsageStatistics> {

    // The statistics graph displays monthly usage of each service available for the dataset
    // It doesn't display the current month information but stops at the previous month
    // However the months in javascript dates goes from 0 to 11 so when passing start and end dates, we don't need to subsctract a month
    // The backend service expect a 2 digits month while javascript date does not give 'O1' but only '1'. This is while we use a small trick
    // to make sure there always two digits for the month in the date
    const monthListArray = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    const today = new Date();
    today.setMonth(today.getMonth() - 1);
    const aMonthAgo = `${today.getFullYear()}-${monthListArray[today.getMonth()]}-${today.getDate()}`;
    today.setFullYear(today.getFullYear() - 1);
    const aMonthAndAYearAgo = `${today.getFullYear()}-${monthListArray[today.getMonth()]}-${today.getDate()}`;
    const url = `${APP_CONFIG.backendUrls.datasetUsageStatistics}?start=${aMonthAndAYearAgo}&uuid=${uuid}&granularity=month&end=${aMonthAgo}`;

    return this._httpClient.get(url).pipe(
      map((data: any[]) => {
        // As the service doesn't return anything for a month if no requests to the service has been made,
        // we can directly use the date from the services answers, instead we use this functions that generates
        // the dates ('YYYY-MM-01') just as the service would do but making sure that there is no missing months
        const dates = this.generateMonthlyDate();
        // Collecting all the possible values of the service property for the dataset and removing duplicates
        const services = [...new Set(data.map(e => e.service))];

        const sortedData = {};

        // For each service of the dataset fill an array of value, each item of the array corresponding to the count
        // of requests to the services for the month at the same position in the dates array
        // ex: sortedData['wms'][0] = 100, dates[0] = "2019-04-01" means that during April 2019 the wms service of this dataset
        // has been requested 100 times
        services.forEach((service) => {
          // Looking in the data array if an item match the service and date of the currect iteration, if not set value to 0
          sortedData[service] = dates.map((date) => {
            const value = data.find(d => d.service === service && d.date === date);
            return value ? value.count : 0;
          });
        });

        return {
          dates,
          data: sortedData,
        };
      }),
    );

  }

  // Generates an array of dates string formatted as follow 'YYYY-MM-01', starting a year and a month ago until a month ago
  // and using monthly intervals
  private generateMonthlyDate = (): string[] => {
    const today = new Date();
    today.setMonth(today.getMonth() - 1); // Starting a month before the current date
    const dates = [];

    for (let i = 0; i < 12; i += 1) {
      dates.push(`${today.getFullYear()}-${`0${today.getMonth() + 1}`.slice(-2)}-01`);
      today.setMonth(today.getMonth() - 1);
    }

    return dates.reverse();
  }
}
