export interface DatasetUsageStatistics {

  dates: string[]; // This contains an array of date string formatted like this 'YYYY-MM-DD'
  data: any; // data is an object with many properties possible. The key of each property correspond to service name
  // and the value is an array of numbers corresponding of the number of request for this service. The order of the numbers 
  // in the array correspond to the order of the dates
}