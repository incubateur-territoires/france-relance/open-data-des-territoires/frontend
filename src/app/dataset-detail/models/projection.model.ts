export interface IProjection {
  id: number;
  name: string;
  commonName: string;
  description: string;
}

export class Projection {
  id: number;
  name: string;
  commonName: string;
  description: string;

  constructor(resource?: IProjection) {
    this.id = resource.id;
    this.name = resource.name;
    this.commonName = resource.commonName;
    this.description = resource.description;
  }
}
