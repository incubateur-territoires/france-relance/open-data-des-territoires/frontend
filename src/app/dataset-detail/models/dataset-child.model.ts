export interface IDatasetRawChild {
  'metadata-fr': {
    'geonet:info': {
      uuid: string;
    },
    title: string;
    image: IDatasetImage[];
  };
}

export class DatasetChild {
  uuid: string;
  slug: string;
  title: string;
  imageUrl: string;

  constructor(data?: IDatasetRawChild) {
    if (data) {
      this.uuid = data['metadata-fr']['geonet\:info'].uuid;
      this.slug = data['slug']['geonet\:info'];
      this.title = data['metadata-fr'].title;
      if (data['metadata-fr'].image) {
        const image = data['metadata-fr'].image.find(i => i.type === 'thumbnail');
        this.imageUrl = image ? image.url : null;
      }
    }
  }
}

interface IDatasetImage {
  type: string;
  url: string;
}
