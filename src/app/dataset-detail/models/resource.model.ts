import { IMetadataLink } from '../../shared/models';

export interface IFormat {
  id: number;
  name: string;
  mapServerType: string;
  fileExtension: string;
}

export class Format implements IFormat {
  id: number;
  name: string;
  mapServerType: string;
  fileExtension: string;
  isCuttable: number;
  isProjectable: number;

  constructor(resourceFormat?: IResourceFormat) {
    this.id = resourceFormat.format.id;
    this.name = resourceFormat.format.name;
    this.mapServerType = resourceFormat.format.mapServerType;
    this.fileExtension = resourceFormat.format.fileExtension;
    this.isCuttable = resourceFormat.isCuttable;
    this.isProjectable = resourceFormat.isProjectable;
  }
}

export interface IResource {
  id: number;
  name: string;
  acronym: string;
  type: string;
  description: string;
  isQueryable: number;
  isDownloadable: number;
  isStandard: number;
  parametersUrl: string;
  messageWarningFr: string;
  messageWarningEn: string;
  resourceFormats: IResourceFormat[];
}

export class Resource {
  id: number;
  name: string;
  acronym: string;
  type: string;
  description: string;
  isQueryable: number;
  isDownloadable: number;
  isStandard: number;
  parametersUrl: string;
  messageWarningFr: string;
  messageWarningEn: string;
  metadataLink: IMetadataLink;
  formats: Format[];

  constructor(resource?: IResource) {
    this.id = resource.id;
    this.name = resource.name;
    this.acronym = resource.acronym;
    this.type = resource.type;
    this.description = resource.description;
    this.isQueryable = resource.isQueryable;
    this.isDownloadable = resource.isDownloadable;
    this.isStandard = resource.isStandard;
    this.parametersUrl = resource.parametersUrl;
    this.messageWarningFr = resource.messageWarningFr;
    this.messageWarningEn = resource.messageWarningEn;
    if (resource.resourceFormats) {
      this.formats = resource.resourceFormats.map((resourceFormat) => {
        return new Format(resourceFormat);
      });
    } else {
      this.formats = [];
    }
  }
}

export interface IResourceFormat {
  id: string;
  resourceId: string;
  format: IFormat;
  isProjectable: number;
  isCuttable: number;
}
