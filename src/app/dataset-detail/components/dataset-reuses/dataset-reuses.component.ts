import { Component, OnInit } from '@angular/core';
import { reusesTypes } from '../../../../i18n/traductions';
import { CMSContent } from '../../../editorialisation/models/cms-content.model';
import { AppRoutes } from '../../../routes';
import { DatasetDetailService } from '../../services';

@Component({
  selector: 'app-dataset-reuses',
  templateUrl: './dataset-reuses.component.html',
  styleUrls: ['./dataset-reuses.component.scss'],
})
export class DatasetReusesComponent implements OnInit {
  AppRoutes = AppRoutes;
  hasRelatedPosts: boolean = false;

  constructor(
    private _datasetDetailService: DatasetDetailService,
  ) { }

  ngOnInit() {
    if (this.relatedNews.length > 0) {
      this.hasRelatedPosts = true;
    }
  }

  get relatedNews(): CMSContent[] {
    return this._datasetDetailService.dataset.relatedNews;
  }

  get reuses() {
    return this._datasetDetailService.dataset.reuses;
  }

  formatReusesTypes(types: string[]) {
    return types.map(e => reusesTypes[e] ? reusesTypes[e] : null).filter(e => e !== null).join(', ');
  }

}
