import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { communeInsee } from '../../../../../../assets/resources/commune-insee';
import { geosource } from '../../../../../../i18n/traductions';
import { APP_CONFIG } from '../../../../../core/services/app-config.service';
import { Dataset, IMetadataLink, linkFormats, Metadata } from '../../../../../shared/models';
import { Format, Projection, Resource } from '../../../../models';
import { DatasetDetailService } from '../../../../services';

@Component({
  selector: 'app-resource-download-item',
  templateUrl: './resource-download-item.component.html',
  styleUrls: ['./resource-download-item.component.scss']
})
export class ResourceDownloadItemComponent implements OnInit {

  linkFormats = linkFormats;
  APP_CONFIG = APP_CONFIG;
  
  @Input() format?: Format;
  @Input() resource?: Resource;
  @Input() projections?: Projection[];
  @Input() dataset?: Dataset;
  
  @Input() link: IMetadataLink;
  @Input() isQueryable: boolean;
  @Output() saveEvent = new EventEmitter();
  @Output() abortEvent = new EventEmitter();

  metadata?: Metadata;

  abortMessage: string = geosource.downloads.abort;
  downloadMessage: string = geosource.downloads.download;

  _queryableUrl = '';

  isOptionOpen = false;

  projectionList;
  // Dropdown projection
  dropDownProjectionToggle: false;
  selectedProjection: {
    name: string,
    bbox: any,
  };

  inseeLabel: string;
  // Dropdown insee
  dropDownInseeToggle: false;
  selectedInsee: {
    commune: '',
    insee: '',
  };
  communeInseeList = communeInsee;

  labelLayer = {
    WFS: 'typename',
    WMS: 'layers',
    WCS: 'identifiers',
  };

  labelFormat = {
    WFS: 'outputFormat',
    WMS: 'FORMAT',
    WCS: 'FORMAT',
  };

  shpProjections = {
    'EPSG:4171': 'EPSG:4171',
    'EPSG:3946': 'EPSG:3946',
    'EPSG:2154': 'EPSG:2154',
    'EPSG:4326': 'EPSG:4326',
    'EPSG:4258': 'EPSG:4258',
  };

  queryableParameters = {
    baseUrl: '',
    baseParameters: '',
    layer: '',
    outputFormat: '',
    outputFormatActive: false,
    projection: '',
    projectionActive: false,
    bbox: '',
    insee: '',
    inseeActive: false,
    numberFeatures: false,
    fromFeature: false,
  };

  constructor(
    private _datasetDetailService: DatasetDetailService,
  ) { }

  ngOnInit() {
    this.initItems();
  }

  initItems() {
    if(this.dataset){
      this.metadata=this.dataset.metadata;
    }
    
    // Get projections and set the default one (depending on the resource)
    if (this.resource) {

      this.projectionList = [];
      if (this.resource.metadataLink.projections && (this.resource.type === linkFormats.wms ||
          this.resource.type === linkFormats.wcs)
          ) {
        this.resource.metadataLink.projections.forEach((key) => {
          this.projectionList.push({
            name: key,
            bbox: this.resource.metadataLink.bbox_by_projection[key],
          });
        });
        this.selectedProjection = this.projectionList[0];
      } else if (this.resource.type === linkFormats.wfs && this.resource.metadataLink.projections) {
        this.resource.metadataLink.projections.forEach((key) => {
          this.projectionList.push({
            name: key,
            bbox: key,
          });
        });
        this.selectedProjection = this.projectionList[0];
      } else if (this.resource.type === linkFormats.ws) {
        this.projectionList = this.transformObjectToArray(this.shpProjections);
        this.selectedProjection = this.projectionList[0];
      }

      // Set the insee property label for this dataset
      if (this._datasetDetailService.datasetData[0]) {
        this.setInseeProperties();
      } else {
        this._datasetDetailService.sampleDataLoaded$.subscribe((data) => {
          this.setInseeProperties();
        });
      }

      this.setProjection(this.selectedProjection);
    }

  }
  setInseeProperties ()
  {
    if(APP_CONFIG.theFunctionalitiesInterruptor.inseeFilter){
      const properties = Object.keys(this._datasetDetailService.datasetData[0].properties);
      this.inseeLabel = properties.find((prop) => { return prop.includes('insee'); });
      this.communeInseeList.sort((a, b) => (a.commune.localeCompare(b.commune)));
    }
  }

  getFormatName() {
    return this.format ? this.format.name : this.link.formats[0];
  }


  isHttpLink() {
    return this.getFormatName()=='HTTP-LINK' || this.getFormatName()=='Site internet' ;
  }

  get queryableUrl() {
    let queryableUrl = '';
    if (this.isQueryable) {
      queryableUrl = this.resource.metadataLink.url;
      let layer = '';
      let outputFormat = '';
      if (this.resource.isStandard) {
        layer = `&${this.labelLayer[this.resource.type]}=${this.resource.metadataLink.name}`;
        outputFormat = `&${this.labelFormat[this.resource.type]}=` +
          `${this.format.mapServerType}`;
        const projectionAndBbox = this.getProjectionAndBbox(this.format);

        if (Array.isArray(this.metadata.providers) && this.metadata.providers.length > 0 && this.metadata.providers[0].match(/ATMO/g)) {
          if (this.format.mapServerType.match(/application\/json/)) {
            outputFormat = `&${this.labelFormat[this.resource.type]}=GEOJSON`;
          }
        }

        const baseParameters = this.resource.parametersUrl ? `?${this.resource.parametersUrl}` : '';
        queryableUrl += baseParameters +
          layer +
          outputFormat +
          projectionAndBbox.projection +
          projectionAndBbox.bboxRequest;

        // Set the dimension of the image based no the bbox aspect ratio
        if (this.selectedProjection && this.resource.type === linkFormats.wms) {
          const width = 700;
          const bbox = this.resource.metadataLink.bbox_by_projection[this.selectedProjection.name];
          let longDiff = bbox.maxx - bbox.minx;
          let latDiff = bbox.maxy - bbox.miny;
          if(this.selectedProjection.name==='EPSG:4326' || this.selectedProjection.name=='EPSG:4171'){
             longDiff = bbox.maxy - bbox.miny;
             latDiff = bbox.maxx - bbox.minx;
          }
        
          const ratio = latDiff / longDiff;
          const height = Math.round(width * ratio);
          queryableUrl += `&WIDTH=${width}&HEIGHT=${height}`;
        }
        else if (this.resource.type === linkFormats.wfs) {
          
          queryableUrl += `&sortBy=${this.dataset.getIdField()}`;
        }
      }  else if (this.resource.type === linkFormats.ws) {
        if (this.format.name === 'JSON') {
          queryableUrl += `/${this.resource.metadataLink.name}/all.json?maxfeatures=-1`;
        } else if (this.format.name === 'CSV') {
          queryableUrl += `/${this.resource.metadataLink.name}/all.csv?maxfeatures=-1`;
        } else {
          const projectionAndBbox = this.getProjectionAndBbox(this.format);

          queryableUrl += `/${this.resource.metadataLink.name}.shp?${projectionAndBbox.projection}`;
        }
      } else if (this.resource.type === linkFormats.kml) {
        queryableUrl += `?${this.resource.parametersUrl}` +
          `&typename=${this.resource.metadataLink.name}`;
      }
    } else {
      queryableUrl = this.link.url;
    }
    queryableUrl += this.queryableParameters.insee;
    return queryableUrl;
  }

  get fileName() {
    return this.resource ? `${this.resource.metadataLink.name}.${this.format.fileExtension}` : this.link.file_name ? this.link.file_name:this.link.name;
  }

  getProjectionLabel(name: string) {
    const projection = this.projections.find((projection) => {
      return projection.name === name;
    });
    return projection ? `${projection.commonName} (${projection.name})` : name;
  }

  // Set the selected projection and set the queryable parameters
  // depending on the resource
  setProjection(projection) {
    this.selectedProjection = projection;
    this.queryableParameters.projectionActive = true;
    // this.updateQueryableUrl();
  }

  // Set the selected insee and set the queryable parameters
  // depending on the resource
  setInsee(insee) {

    this.selectedInsee = insee;
    if (!this.selectedInsee || !this.inseeLabel) {
      this.queryableParameters.insee = '';
    } else {
      if (this.resource.type === linkFormats.wfs) {
        this.queryableParameters.insee = `&CQL_FILTER=${this.inseeLabel}=${this.selectedInsee.insee}`;
          /*'&filter=Filter=<Filter> <PropertyIsLike wildcard="*" ' +
          'singleChar=\'.\' escape=\'_\'> <PropertyName>' +
          `${this.inseeLabel}</PropertyName><Literal>${this.selectedInsee.insee}` +
          '</Literal></PropertyIsLike> </Filter>';*/
      } else if (this.resource.type === linkFormats.ws) {
        this.queryableParameters.baseUrl = this.resource.metadataLink.name.replace('\/all.json', '');
        const match = (/\/ws\/([^\/]+)/g).exec(this.resource.metadataLink.url);
        const serviceName = match[1] ? match[1] : '';
        this.queryableParameters.insee = `&mask_db=${serviceName}&mask_layer=adr_voie_lieu.adrcommune` +
          `&mask_field=insee&mask_value=${this.selectedInsee.insee}`;
      }
      this.queryableParameters.inseeActive = true;
    }

    this.updateQueryableUrl();
  }

  getProjectionAndBbox(format: Format) {
    let projection = '';
    let bboxRequest = '';
    if (this.selectedProjection) {

      if (this.resource.type === linkFormats.wfs) {
        projection = `&SRSNAME=${this.selectedProjection.name}`;
      } else if (this.resource.type === linkFormats.wms || this.resource.type === linkFormats.wcs) {
        projection = `&CRS=${this.selectedProjection.name}`;
        const bbox = this.resource.metadataLink.bbox_by_projection[this.selectedProjection.name];
        if(this.selectedProjection.name=='EPSG:4326' || this.selectedProjection.name=='EPSG:4171'){ // inversion des coordonnées en 4326
          bboxRequest = `&BBOX=${bbox.miny},` +
          `${bbox.minx},` +
          `${bbox.maxy},` +
          `${bbox.maxx}`;
        }else{
          bboxRequest = `&BBOX=${bbox.minx},` +
          `${bbox.miny},` +
          `${bbox.maxx},` +
          `${bbox.maxy}`;
        }

      } else if (this.resource.type === linkFormats.ws) {
        if (format.name !== 'JSON') {
          projection = `srsname=${this.selectedProjection.name}`;
        }
      }
    }

    return {
      projection,
      bboxRequest,
    };
  }

  // When the user changes parameters (or when view init), we update the url provided
  // to the <a> link
  updateQueryableUrl() {
    this._queryableUrl = this.queryableParameters.baseUrl +
      this.queryableParameters.baseParameters;
    this._queryableUrl += this.queryableParameters.layer +
      this.queryableParameters.outputFormat +
      this.queryableParameters.insee +
      this.queryableParameters.projection +
      this.queryableParameters.bbox
      ;
  }

  transformObjectToArray(object) {
    const array = [];
    let keys;

    if (object) {
      keys = Object.keys(object);

      keys.forEach((key) => {
        array.push({
          name: key,
          bbox: object[key],
        });
      });
    }

    return array;
  }

}
