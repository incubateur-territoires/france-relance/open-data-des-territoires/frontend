import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { forkJoin, Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { notificationMessages } from '../../../../i18n/traductions';
import { Notification } from '../../../core/models';
import { NotificationService } from '../../../core/services';
import { Dataset, IMetadataLink, linkFormats, Metadata } from '../../../shared/models';
import { UserService } from '../../../user/services';
import { Projection, Resource } from '../../models';
import { DatasetDetailService, ResourcesService } from '../../services';

@Component({
  selector: 'app-dataset-downloads',
  templateUrl: './dataset-downloads.component.html',
  styleUrls: ['./dataset-downloads.component.scss'],
})
export class DatasetDownloadsComponent implements OnInit {

  linkFormats = linkFormats;

  metadata: Metadata;
  sub: Subscription;
  downloadableResources; // Each property of the object is an array of Resource;
  staticResources; // Each property of the object is an array of IMetadataLink;
  otherResources: IMetadataLink[];

  cguModalIsOpened: boolean;

  isSample = false;
  resources: Resource[];
  projections: Projection[];
  hasBeenInitialized = false;

  dataset:Dataset;

  constructor(
    private _datasetDetailService: DatasetDetailService,
    private _resourcesService: ResourcesService,
    private _notificationService: NotificationService,
    private _cookieService: CookieService,
    private _userService: UserService,

  ) { }

  ngOnInit() {
    this.resources = [];
    this.projections = [];
    this.downloadableResources = {};
    this.staticResources = [];
    this.otherResources = [];

    this.cguModalIsOpened = false;// (this._cookieService.get(environment.cguAcceptedCookieName) !== 'true') &&
    //  !this._userService.userIsSignedIn;

    this.isSample = this._datasetDetailService.dataset.editorialMetadata.isSample;

    forkJoin([
      this._resourcesService.getProjections(),
      this._resourcesService.getResources(),
    ]).subscribe(
      (response) => {
        this.projections = response[0];
        this.resources = response[1];

        this.initialize();
      },
      (err) => {
        this._notificationService.notify(new Notification({
          type: 'error',
          message: `${notificationMessages.resources.initializationError}`,
        }));
      },
    );

    this.sub = this._datasetDetailService.dataset$.subscribe(() => {
      this.initialize();
    });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  // Resources are organized on 2 levels :
  // -> 1st level: by metadata link description
  // -> 2nd level: for each link description in 3 categories:
  //    - queryable
  //    - downloadable
  //    - other
  // How to organize: we compare the type of one resource from our resource service
  // and the ES metadata link property. If there is a match, the service will let us know if the resource
  // is queryable or downloadable. If there is no match and the ES resource cannot be found in the service,
  // we put it in "others" resources catagory.
  initialize() {
    this.metadata = this._datasetDetailService.datasetMetadata;
    this.dataset = this._datasetDetailService.dataset;
    if (this.metadata && this.resources.length > 0 && !this.hasBeenInitialized) {
      this.hasBeenInitialized = true;
      if (this.metadata.link) {
        [this.downloadableResources, this.staticResources, this.otherResources] = this._resourcesService.getResourcesForDownload(this.metadata);
      }
    }
  }

  get hasDownloadableResources() {
    return Object.keys(this.downloadableResources).length > 0;
  }

  get hasStaticResources() {
    return this.staticResources.length > 0;
  }

  get hasOtherResources() {
    return Object.keys(this.otherResources).length > 0;
  }

  cguAccepted() {
    this.cguModalIsOpened = false;
  }

}
