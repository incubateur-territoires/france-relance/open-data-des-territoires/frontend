import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Data } from '../../../elasticsearch/models';
import { DatasetDetailService } from '../../services';
import { DatasetMapComponent } from '../dataset-map/dataset-map.component';
import { ActivatedRoute, Router, Scroll } from '@angular/router';
import { AppRoutes } from '../../../routes';
import { linkFormats } from '../../../shared/models';
import { APP_CONFIG } from '../../../core/services/app-config.service';
import { UserService } from '../../../user/services';

@Component({
  selector: 'app-dataset-table-map',
  templateUrl: './dataset-table-map.component.html',
  styleUrls: ['./dataset-table-map.component.scss'],
})
export class DatasetTableMapComponent implements OnInit, OnDestroy {

  @ViewChild(DatasetMapComponent, { static: false }) datasetMapComponent: DatasetMapComponent;
  AppRoutes = AppRoutes;

  APP_CONFIG = APP_CONFIG;
  // Component state
  fullscreen = false;
  displayColumnsMenu = false;
  mapIsEnabled = true;
  tableIsEnabled = true;
  mobileMapIsEnabled = false;
  mobileTableIsEnabled = true;
  mobileResearchIsDisplayed = false;
  displayDataDetails = false; // Wether the data details should be displayed or not

  selectedProperties = {}; // Object containing the properties as key and the display state as value
  selectedData: Data | mapboxgl.MapboxGeoJSONFeature = null; // Contains the properties of the selected feature
  dataDetailsShouldBeAnimated = false; // Used to prevent animation on first load of the component

  subscriptions: Subscription[] = [];

  dataLoaded: boolean = false;

  // search
  searchInput = new FormControl('');

  constructor(
    private _datasetDetailService: DatasetDetailService,
    public _userService:UserService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.initComponent();

    this.subscriptions.push(
      // If the dataset has at least a table or a map then initialize the component
      // else redirect to the info tab
      this._datasetDetailService.dataset$.subscribe(() => {
        this.initComponent();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => {
      sub.unsubscribe();
    });
  }

  initComponent() {
    this.properties.forEach(prop => {
      if(prop==="gid"||prop ==="geometry"){
        this.selectedProperties[prop] = false;
      }
      else{
        this.selectedProperties[prop] = true;
      }

    });
    // Intialize the search value with the value from the service in order to keep it when navigating in tabs
    this.searchInput.setValue(this._datasetDetailService.searchString);
    //acces aux metadonnées uniquement
    if(this._datasetDetailService.dataset.accessLevel<=1){
       this._router.navigate([
          `/${AppRoutes.datasets.uri}/${this._datasetDetailService.dataset.slug}/${AppRoutes.info.uri}`,
        ], {replaceUrl: true});
    }
    if (this.hasTable) {
      if (this._datasetDetailService.datasetData){
        this.dataLoaded = true;
      }
      else {
        // If dataset has table then preload some data for the table component
        this._datasetDetailService.sampleDataLoaded$.subscribe((data) => {
          this.dataLoaded = true;
        }).unsubscribe();
      }
    }
  }

  get isMobile() {
    // 768 (in px) is the mobile breakpoint defined by bulma (CSS framework used)
    return window.innerWidth <= 768;
  }

  get isSample(): boolean {
    return this._datasetDetailService.dataset.editorialMetadata.isSample;
  }

  get hasMap() {
    return this._datasetDetailService.dataset.hasMap;
  }

  get hasTable() {
    return this._datasetDetailService.dataset.hasTable;
  }

  get selectedFeatureId() {
    return this.selectedData && this.selectedData.id ?
      this.selectedData.id : null;
  }

  get selectedFeatureProperties() {
    return this.selectedData ? this.selectedData.properties : null;
  }

  // Get the number of documents in this dataset
  get datasetDataNumber() {
    return this._datasetDetailService.datasetDataNumber;
  }

  get properties() {
    return this._datasetDetailService.dataset.fields.list;
  }

  get researchMaxResult() {
    return this._datasetDetailService.researchMaxResult;
  }

  get datasetDataNumberPercentage() {
    return Math.ceil(this.researchMaxResult / this.datasetDataNumber * 100);
  }

  get isLoading() {
    return this._datasetDetailService.isLoading;
  }

  createNewMap() {
    let metadata=this._datasetDetailService.datasetMetadata;
    let vectorService = { ...metadata.link.find((e) => { return e.service === linkFormats.wfs; }) };
    let rasterService = { ...metadata.link.find((e) => { return e.service === linkFormats.wms; }) };
    
    if(rasterService && APP_CONFIG.externalUrls.maps_default_map){
      let url=APP_CONFIG.externalUrls.maps_default_map+"?addLayer=";
      url+=btoa(JSON.stringify(rasterService));
      window.open(url,'target=_blank');
    }
    
   
  }

  toogleFullscreen() {
    this.fullscreen = !this.fullscreen;
    // Redraw the map
    setTimeout(
      () => {
        this.datasetMapComponent.reDraw();
      },
      1,
    );
  }

  toogleMap() {
    if (this.hasTable && this.tableIsEnabled) {
      this.mapIsEnabled = !this.mapIsEnabled;
    }

    /* IMPORTANT
    *  The 600ms timeout to redraw the map is not random. In the page we have CSS animations to close and open
    *  components that last 500ms. So to wait the DOM has finished to be modified, we set a 600ms timeout to make a
    *  proper resize() of the map.
    */
    if (this.mapIsEnabled) {
      setTimeout(
        () => {
          this.datasetMapComponent.reDraw();
        },
        600,
      );
    }
  }

  toogleTable() {
    if (this.hasMap && this.mapIsEnabled) {
      this.tableIsEnabled = !this.tableIsEnabled;

      /* IMPORTANT
      *  The 600ms timeout to redraw the map is not random. In the page we have CSS animations to close and open
      *  components that last 500ms. So to wait the DOM has finished to be modified, we set a 600ms timeout to make a
      *  proper resize() of the map.
      */
      setTimeout(
        () => {
          this.datasetMapComponent.reDraw();
        },
        600,
      );
    }
  }

  toogleColumsMenu() {
    this.displayColumnsMenu = !this.displayColumnsMenu;
  }

  closeColumsMenu() {
    this.displayColumnsMenu = false;
  }

  closeDataDetails() {
    this.displayDataDetails = false;

    /* IMPORTANT
    *  The 600ms timeout to redraw the map is not random. In the page we have CSS animations to close and open
    *  components that last 500ms. So to wait the DOM has finished to be modified, we set a 600ms timeout to make a
    *  proper resize() of the map.
    */
    setTimeout(
      () => {
        this.datasetMapComponent.reDraw();
      },
      600,
    );
  }

  toogleColumn(name: string) {
    this.selectedProperties[name] = !this.selectedProperties[name];
    setTimeout(
      () => {
        this.datasetMapComponent.reDraw();
      },
      1,
    );
  }

  resetResearch() {
    this.searchInput.setValue('');
    this._datasetDetailService.searchChanged('');
  }

  searchChanged() {
    this._datasetDetailService.searchChanged(this.searchInput.value);
  }

  onDataSelected(data: Data) {
    this.dataDetailsShouldBeAnimated = true;
    this.selectedData = data;
    this.displayDataDetails = true;
  }

  onTableWidthModified() {
    setTimeout(
      () => {
        if(this.datasetMapComponent){
          this.datasetMapComponent.reDraw();
        }
      },
      1,
    );
  }

  onFeatureClicked(data: any) {
    if (data) {
      this._datasetDetailService.putDataAtBeginningOfDatasetDataArray(data);
    }
    this.dataDetailsShouldBeAnimated = true;
    this.selectedData = data;
    this.displayDataDetails = true;
  }

  //  Returns the index if found in the array, -1 if not found
  columnIsSelected(name: string) {
    return this.selectedProperties[name];
  }

  toogleMapMobile() {
    if (!this.mobileMapIsEnabled) {
      this.mobileMapIsEnabled = !this.mobileMapIsEnabled;
      this.mobileTableIsEnabled = !this.mobileTableIsEnabled;

      /* IMPORTANT
    *  The 600ms timeout to redraw the map is not random. In the page we have CSS animations to close and open
    *  components that last 500ms. So to wait the DOM has finished to be modified, we set a 600ms timeout to make a
    *  proper resize() of the map.
    */
      if (this.mobileMapIsEnabled) {
        setTimeout(
          () => {
            this.datasetMapComponent.reDraw();
          },
          600,
        );
      }
    }
  }

  toogleTableMobile() {
    if (!this.mobileTableIsEnabled) {
      this.mobileMapIsEnabled = !this.mobileMapIsEnabled;
      this.mobileTableIsEnabled = !this.mobileTableIsEnabled;

      /* IMPORTANT
      *  The 600ms timeout to redraw the map is not random. In the page we have CSS animations to close and open
      *  components that last 500ms. So to wait the DOM has finished to be modified, we set a 600ms timeout to make a
      *  proper resize() of the map.
      */
      if (this.mobileMapIsEnabled) {
        setTimeout(
          () => {
            this.datasetMapComponent.reDraw();
          },
          600,
        );
      }
    }
  }

  toogleMobileResearch() {
    this.mobileResearchIsDisplayed = !this.mobileResearchIsDisplayed;
  }
}
