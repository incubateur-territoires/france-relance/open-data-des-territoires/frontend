// tslint:disable-next-line: max-line-length
import { DatasetAPIComponent } from './dataset-api/dataset-api.component';
import { IconFormatComponent } from './dataset-api/icon-format/icon-format.component';
// tslint:disable-next-line:max-line-length
import { ResourceQueryableComponent } from './dataset-api/resources-queryable/resource-queryable/resource-queryable.component';
import { ResourcesQueryableComponent } from './dataset-api/resources-queryable/resources-queryable.component';
// tslint:disable-next-line: max-line-length
import { DatasetDataDetailPropertiesComponent } from './dataset-data-details/dataset-data-detail-properties/dataset-data-detail-properties.component';
import { DatasetDataDetailsComponent } from './dataset-data-details/dataset-data-details.component';
import { DatasetDetailComponent } from './dataset-detail/dataset-detail.component';
import { DatasetDownloadsComponent } from './dataset-downloads/dataset-downloads.component';
// tslint:disable-next-line: max-line-length
// tslint:disable-next-line: max-line-length
import { ResourceDownloadItemComponent } from './dataset-downloads/resource-download-item/resource-download-item/resource-download-item.component';
import { DatasetInfoComponent } from './dataset-info/dataset-info.component';
import { InfoContactComponent } from './dataset-info/info-contact/info-contact.component';
import { InfoPartnersComponent } from './dataset-info/info-partners/info-partners.component';
import { InfoSummaryComponent } from './dataset-info/info-summary/info-summary.component';
import { DatasetMapComponent } from './dataset-map/dataset-map.component';
import { DatasetReusesComponent } from './dataset-reuses/dataset-reuses.component';
import { DatasetTableMapComponent } from './dataset-table-map/dataset-table-map.component';
import { DatasetTableComponent } from './dataset-table/dataset-table.component';
import { InfoChartComponent } from './dataset-info/info-chart/info-chart.component';
import { InfoAttributesComponent } from './dataset-info/info-attributes/info-attributes.component';
// tslint:disable-next-line: max-line-length
export { DatasetDetailComponent, ResourceDownloadItemComponent, DatasetAPIComponent, DatasetDownloadsComponent, ResourceQueryableComponent, DatasetMapComponent, DatasetInfoComponent, DatasetTableMapComponent, DatasetTableComponent, ResourcesQueryableComponent, IconFormatComponent, DatasetDataDetailsComponent, DatasetDataDetailPropertiesComponent, DatasetReusesComponent, InfoContactComponent, InfoPartnersComponent, InfoSummaryComponent };
// tslint:disable-next-line:variable-name
export const DatasetDetailComponents = [
  DatasetDetailComponent,
  ResourceQueryableComponent,
  DatasetAPIComponent,
  DatasetMapComponent,
  DatasetInfoComponent,
  InfoContactComponent,
  InfoPartnersComponent,
  InfoAttributesComponent,
  InfoSummaryComponent,
  InfoChartComponent,
  DatasetDownloadsComponent,
  DatasetReusesComponent,
  DatasetTableMapComponent,
  DatasetTableComponent,
  ResourcesQueryableComponent,
  IconFormatComponent,
  DatasetDataDetailsComponent,
  DatasetDataDetailPropertiesComponent,
  ResourceDownloadItemComponent,
];
