import { isFormattedError } from '@angular/compiler';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { datatsetDataRepresentationType } from '../../../../i18n/traductions';
import { AppRoutes } from '../../../routes';
import { environment } from '../../../../environments/environment';
import { License, Metadata, typesMetadata } from '../../../shared/models';
import { DatasetDetailService } from '../../services';
import { UsageStatisticsService } from '../../services/usage-statistics.service';
import { ibsFormat } from 'ibs-format';

@Component({
  selector: 'app-dataset-info',
  templateUrl: './dataset-info.component.html',
  styleUrls: ['./dataset-info.component.scss'],
})
export class DatasetInfoComponent implements OnInit, OnDestroy {
  AppRoutes = AppRoutes;

  metadata: Metadata;
  descriptions: Object[];
  statistics;
  sub: Subscription;

  constructor(
    private _datasetDetailService: DatasetDetailService,
    private _usageStatisticsService: UsageStatisticsService,
  ) { }

  ngOnInit() {
    // Initilize component variables with value from the service
    this.initializeInfo();

    // Subcribe to changes in the service
    this.sub = this._datasetDetailService.dataset$.subscribe(() => {
      this.initializeInfo();
    });

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  initializeInfo() {
    this.metadata = this._datasetDetailService.datasetMetadata;

    this._usageStatisticsService.getDatasetLastYearStats(this._datasetDetailService.dataset.uuid).subscribe(
      (data) => {
        this.statistics = data;
      },
      (err) => {
        // Do something on error ...
      });
  }

  setBackupImage() {
    this.metadata.image.url = './assets/img/vignette-data-geo.png';
  }

  get parent() {
    return this.metadata.parentDataset;
  }

  get metadataUrl() {
    return this.metadata.href;
  }

  get children() {
    return this._datasetDetailService.datasetChildren.data;
  }

  get geographicalInfo(): IGeographicalInfo {
    let info = null;
    if (this.metadata.type === typesMetadata.dataset) {
      let denom:any='';
      if(this.metadata.denominator && this.metadata.denominator!=null){
        denom=this.metadata.denominator;
      }
      let resol:any='';
      if(this.metadata.resolution && this.metadata.resolution!=null){
        resol=this.metadata.resolution;
      }
      info = {
        representationType: this.getDatasetDataRepresentationType(),
        scale: `1:${denom}`,
        resolution: resol,
        geographicalExtend: {
          maxEast: `${this.metadata.native_max_east} est,`,
          maxWest: `${this.metadata.native_max_west} ouest`,
          maxSouth: `${this.metadata.native_max_south} sud,`,
          maxNorth: `${this.metadata.native_max_north} nord,`,
        },
        coordinatesSystem: this.metadata.native_crs,
      };
    }

    return info;
  }

  get abstract() {
    return ibsFormat(this.metadata.abstract,[],{detectLinks: true, target: '_blank'});
  }

  get lineage() {
    return ibsFormat(this.metadata.lineage,[], {detectLinks: true, target: '_blank'} )
  }

  getDatasetDataRepresentationType(): string {
    let type = '';
    if (this._datasetDetailService.datasetEditorialMetadata.isPunctual) {
      type = datatsetDataRepresentationType.short.punctual;
    }

    if (this._datasetDetailService.datasetEditorialMetadata.isLinear) {
      type = datatsetDataRepresentationType.short.linear;
    }

    if (this._datasetDetailService.datasetEditorialMetadata.isAreal) {
      type = datatsetDataRepresentationType.short.areal;
    }

    return type;
  }

  get categories() {
    return this.metadata.getCategories();
  }

  get license(): License {
    return this.metadata.license;
  }
  
  get usageRecommandation(): String {
    return this.metadata.usageRecommandation;
  }

  get granularity(): String {
    return this.metadata.granularity;
  }

  get keywords() {
    return this.metadata.keyword;
  }

  isFr(): boolean {
    return window.location.href.includes(environment.angularAppHost.en) ?
      false : true;
  }
}



interface IGeographicalInfo {
  representationType: string;
  scale: string;
  resolution: string;
  geographicalExtend: {
    maxEast: string;
    maxSouth: string;
    maxNorth: string;
    maxWest: string;
  };
  coordinatesSystem: string;
}
