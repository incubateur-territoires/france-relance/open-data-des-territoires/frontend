import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Metadata, IResponsibleParty } from '../../../../shared/models';
import { DatasetDetailService } from '../../../services';

@Component({
  selector: 'app-info-partners',
  templateUrl: './info-partners.component.html',
  styleUrls: ['./info-partners.component.scss']
})
export class InfoPartnersComponent implements OnInit {
  metadata: Metadata;
  descriptions: Object[];
  sub: Subscription;

  constructor(
    private _datasetDetailService: DatasetDetailService,
    ) { }

  ngOnInit() {
    // Initilize component variables with value from the service
    this.initializeInfo();

    // Subcribe to changes in the service
    this.sub = this._datasetDetailService.dataset$.subscribe(() => {
      this.initializeInfo();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  initializeInfo() {
    this.metadata = this._datasetDetailService.datasetMetadata;
  }
  get producers(): IResponsibleParty[] {
    let contact = this.metadata.contacts
    .reduce(function (acc, curr) {
      let existingOrga=acc.find(x=>x.organisationName==curr.organisationName);
      if (!existingOrga){
        acc.push(curr);
      }
      else if(!existingOrga.positionName && curr.positionName){
        const index = acc.indexOf(existingOrga);
        // Si l'orga a une addresse et pas le contact on récupère l'addresse de l'orga
        if(existingOrga.address && !curr.address){
          curr.address=existingOrga.address;
        }
        if (index > -1) { 
          acc.splice(index, 1); 
        }

        acc.push(curr);
      }
        
    return acc;
    }, []);
    return contact;
  }
}
