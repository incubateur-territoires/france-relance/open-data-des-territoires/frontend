import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { APP_CONFIG } from '../../../../core/services/app-config.service';
import { AppRoutes } from '../../../../routes';
import { Metadata } from '../../../../shared/models';
import { DatasetDetailService } from '../../../services';

@Component({
  selector: 'app-info-contact',
  templateUrl: './info-contact.component.html',
  styleUrls: ['./info-contact.component.scss']
})
export class InfoContactComponent implements OnInit, OnDestroy {
  AppRoutes = AppRoutes;
  metadata: Metadata;
  descriptions: Object[];
  sub: Subscription;
  APP_CONFIG=APP_CONFIG;

  constructor(
    private _datasetDetailService: DatasetDetailService,
  ) { }

  ngOnInit() {
    // Initilize component variables with value from the service
    this.initializeInfo();

    // Subcribe to changes in the service
    this.sub = this._datasetDetailService.dataset$.subscribe(() => {
      this.initializeInfo();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  get mailTo(){
    return 'mailTo:'+this.metadata.emailContact;
  }
  
  initializeInfo() {
    this.metadata = this._datasetDetailService.datasetMetadata;
  }
}
