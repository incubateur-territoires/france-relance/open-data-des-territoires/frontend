import { Component, OnInit } from '@angular/core';
import { Metadata } from '../../../../shared/models';
import { Subscription } from 'rxjs';
import { DatasetDetailService } from '../../../services';
import { geosource } from '../../../../../i18n/traductions';

@Component({
  selector: 'app-info-summary',
  templateUrl: './info-summary.component.html',
  styleUrls: ['./info-summary.component.scss']
})
export class InfoSummaryComponent implements OnInit {
  metadata: Metadata;
  descriptions: Object[];
  sub: Subscription;

  constructor(
    private _datasetDetailService: DatasetDetailService,
    ) { }

  ngOnInit() {
    // Initilize component variables with value from the service
    this.initializeInfo();

    // Subcribe to changes in the service
    this.sub = this._datasetDetailService.dataset$.subscribe(() => {
      this.initializeInfo();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  initializeInfo() {
    this.metadata = this._datasetDetailService.datasetMetadata;
  }
  get kinds(){
    return this.metadata.kind && this.metadata.kind.length>0 ?
      this.metadata.kind.join(', ') :' ';
  }
  get hasUpdateDate():boolean {
    return this.summaryInfo.lastUpdateDate && Number(this.summaryInfo.lastUpdateDate)>0;
  }
  get summaryInfo(): ISummaryInfo {
    return {
      // tslint:disable-next-line:max-line-length
      isOpenAccess: this._datasetDetailService.datasetEditorialMetadata.isOpenAccess,
      // tslint:disable-next-line:max-line-length
      restriction: this._datasetDetailService.datasetEditorialMetadata.isOpenAccess ? geosource.accessRestriction.open : geosource.accessRestriction.restricted,
      publicationDate: this.metadata.publicationDate,
      lastUpdateDate: this.metadata.lastUpdateDate,
      viewsNumber: this._datasetDetailService.datasetNumberOfViews,
      updateFrequency: this.metadata.updateFrequency ?
        geosource.updateFrequencies[this.metadata.updateFrequency] : geosource.updateFrequencies.other,
      dataType: geosource.datasetTypes[this.metadata.type],
      topicCat:this.metadata.topicCat
    };
  }
}
interface ISummaryInfo {
  isOpenAccess: boolean;
  restriction: string;
  publicationDate: string;
  lastUpdateDate: string;
  viewsNumber: number;
  updateFrequency: string;
  dataType: string;
  topicCat:any;
}
