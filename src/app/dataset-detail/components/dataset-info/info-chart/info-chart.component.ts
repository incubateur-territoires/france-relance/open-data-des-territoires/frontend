import { Component, Input, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import * as ChartAnnotation from 'chartjs-plugin-annotation';
import { datasetStatistics } from '../../../../../i18n/traductions';
import { element } from 'protractor';

@Component({
  selector: 'app-info-chart',
  templateUrl: './info-chart.component.html',
  styleUrls: ['./info-chart.component.scss']
})
export class InfoChartComponent implements OnInit {

  @Input('statistics') statistics;
  chart;

  constructor() { }

  barWidth = 20;

  ngOnInit(): void {
    let barColors : string = '#7BC0F5';
    let namedChartAnnotation = ChartAnnotation;
    namedChartAnnotation["id"]="annotation";
    Chart.pluginService.register( namedChartAnnotation);
    Chart.defaults.global.defaultFontFamily = 'Titillium Web';
    Chart.defaults.global.defaultFontSize = 14;

    this.chart = new Chart('myChart', {
      type: 'bar',
      data: {
        labels: this.statistics.dates.map(date => new Date(date).toLocaleString('default', { month: 'short' })),
        datasets: [
          {
            data: this.aggregateOtherServicesStats(this.statistics.data),
            label: datasetStatistics.otherServices,
            backgroundColor: barColors,
            hoverBackgroundColor: '#7BC0F5',
            hoverBorderColor: '#DA322F',
            hoverBorderWidth: 0,
            ttBackgroundColor: '#F2C774',
            barThickness: this.barWidth,
            stack: 'a',
          },
          {
            data: this.statistics.data.ws || [],
            label: 'WS',
            backgroundColor:  barColors,
            hoverBackgroundColor: '#7BC0F5',
            hoverBorderColor: '#DA322F',
            hoverBorderWidth: 0,
            ttBackgroundColor: '#78D7D7',
            barThickness: this.barWidth,
            stack: 'a',
          },
          {
            data: this.statistics.data.wms || [],
            label: 'WMS',
            backgroundColor: barColors,
            hoverBackgroundColor: '#7BC0F5',
            hoverBorderColor: '#DA322F',
            hoverBorderWidth: 0,
            ttBackgroundColor: '#7BC0F5',
            barThickness: this.barWidth,
            stack: 'a',
          },
          {
            data: this.statistics.data.wfs || [],
            label: 'WFS',
            backgroundColor: barColors,
            hoverBackgroundColor: '#7BC0F5',
            hoverBorderColor: '#DA322F',
            hoverBorderWidth: 0,
            ttBackgroundColor: '#E5A1F1',
            barThickness: this.barWidth,
            stack: 'a',
          },
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        title: {
          display: true,
          text: '',
          position: 'bottom',
        },
        tooltips: {
          mode: 'label',
          xPadding: 15,
          yPadding: 15,
          cornerRadius: 0,
          position: 'nearest',
          displayColors: false,
          backgroundColor: '#464A57',
          callbacks: {
            title: (tooltipItem, data) => {
              let total = 0;
              // Get the total amount of requests
              tooltipItem.forEach((tooltip) => {
                total += tooltip.yLabel;
              });
              if (total <= 1) {
                return `${total.toLocaleString()} ${datasetStatistics.request}`;
              }
              return `${total.toLocaleString()} ${datasetStatistics.requests}`;
            },
            labelTextColor: (tooltipItem, data) => {
              // for each tooltiplabel get the bar color
              return data.data.datasets[tooltipItem.datasetIndex].ttBackgroundColor.toString();
            },
            label: (tooltipItem, data) => {
              var value:number = Math.round(tooltipItem.yLabel * 100 ) / 100;
              if (value === 0) {
                return;
              }

              var label:string = value.toLocaleString();
              label += ' ' + data.datasets[tooltipItem.datasetIndex].label || '';

              return label;

            }
          },
        },
        legend: {
          display: false
        },
        annotation: {
          drawTime: 'beforeDatasetsDraw',
          annotations: [{
            type: 'box',
            xScaleID: 'x-axis-0',
            xMin: -1,
            xMax: 12,
            borderColor: 'rgb(242,242,242)',
            borderWidth:2,
            backgroundColor: 'rgba(121,121,121,0.10 )',
          }]
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              precision: 0,
              suggestedMax: 2,
              callback: (value: number, index: number, values: number[]):string => {
                if (value > 1000000) {
                  value = value / 1000000;
                  return value.toLocaleString() + ' M'
                }
                if (value >= 1000) {
                  value = value / 1000;
                  return value.toLocaleString() + ' k';
                }
                return value.toLocaleString();
              }
            },
          }],
          xAxes: [{
            ticks: {
              beginAtZero: true,
            },
            gridLines: {
              color: '#fff',
            },
          }],
        },
      },
    });
  }

  aggregateOtherServicesStats(data) {
    let array: number[] = [];

    for (const service in data) {
      if (service !== 'wms' && service !== 'wfs' && service !== 'ws') {
        array = data[service].map((a, i) => array[i] ? a + array[i] : a);
      }
    }

    return array;

  }
}
