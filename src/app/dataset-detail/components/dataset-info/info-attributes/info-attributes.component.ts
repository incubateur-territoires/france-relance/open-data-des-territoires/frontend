import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppRoutes } from '../../../../routes';
import { IMetadataLink, Metadata } from '../../../../shared/models';
import { DatasetDetailService } from '../../../services';

@Component({
  selector: 'app-info-attributes',
  templateUrl: './info-attributes.component.html',
  styleUrls: ['./info-attributes.component.scss']
})
export class InfoAttributesComponent implements OnInit, OnDestroy {
  AppRoutes = AppRoutes;
  metadata: Metadata;
  sub: Subscription;
  wsLink:IMetadataLink;
  displayedProperties=["Nom","Type","Description"];
  sortValue='Nom';
  attributesDescr:[];
  constructor(
    private _datasetDetailService: DatasetDetailService,
  ) { }

  ngOnInit() {
    // Initilize component variables with value from the service
    this.initializeInfo();

    // Subcribe to changes in the service
    this.sub = this._datasetDetailService.dataset$.subscribe(() => {
      this.initializeInfo();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getStyle() {
    const gridColumns = this.displayedProperties.length !== 1 ?
      `repeat(${this.displayedProperties.length - 1}, max-content) 1fr` : '1fr';
    return {
      // 'grid-template-columns': `repeat(${this.displayedProperties.length}, minmax(50px, 1fr)`,
      'grid-template-columns': gridColumns,
    };
  }

  initializeInfo() {
    this.metadata = this._datasetDetailService.datasetMetadata;
    this._datasetDetailService.getAttributesFromWS().subscribe((data:any)=>{
      console.log(data);
      this.attributesDescr=data.results;
    });
    this.wsLink = this.metadata.getWsLink();

  }

}
