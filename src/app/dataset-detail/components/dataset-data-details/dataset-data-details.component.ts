import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DatasetDetailService } from '../../services';

@Component({
  selector: 'app-dataset-data-details',
  templateUrl: './dataset-data-details.component.html',
  styleUrls: ['./dataset-data-details.component.scss'],
})
export class DatasetDataDetailsComponent implements OnInit {

  private _properties;
  @Input()
  set properties(properties) {
    // Making sure the complex properties values are under objects or array form and not string
    // When properties comme from the map MVT the types are lost and they are always represented as string
    for (const key in properties) {
      if (properties.hasOwnProperty(key)) {
        if (!this._datasetDetailService.dataset.fields.types[key] && typeof properties[key] === 'string') {
          try {
            const parsed = JSON.parse(properties[key]);
            properties[key] = parsed;
          } catch (err) { }
        }
      }
      else if (Array.isArray(properties[key])) {
        try {
          const parsed = JSON.parse(properties[key]);
          properties[key] = parsed;
        } catch (err) { }
      }
    }
    this._properties = properties;
  }

  @Output() close = new EventEmitter<boolean>();

  constructor(
    private _datasetDetailService: DatasetDetailService,
  ) { }

  ngOnInit() {
  }

  get properties() {
    return this._properties;
  }

  closeSelf() {
    this.close.emit(true);
  }

}
