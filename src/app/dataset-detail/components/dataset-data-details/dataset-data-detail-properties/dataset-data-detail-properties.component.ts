import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dataset-data-detail-properties',
  templateUrl: './dataset-data-detail-properties.component.html',
  styleUrls: ['./dataset-data-detail-properties.component.scss'],
})
export class DatasetDataDetailPropertiesComponent implements OnInit {

  private _properties: Object;
  @Input()
  set properties(properties) {
    // In order to allow the potential scroll on the properties div, we need to wait for the translation animation
    // on that component to be finished before updating the properties. Refere to the animation duration declared in
    // dataset-table-map-component.scss
    setTimeout(
      () => {
        this._properties = properties;
        // Initilize the array with the keys of the json object received in the input
        this.keys = this.getKeys(properties);
        this.isOpenProperties = this.keys.reduce((acc, it) => {
          acc[it] = (this.getTypeOfProperty(it) === 'object' || Array.isArray(this.properties[it])) ? false : true;
          return acc;
          // tslint:disable-next-line:align
        }, {});
      },
      200,
    );
  }
  // This input is used to display complex objects.
  // When the value is an array or object, we will hide in first place, then allow to toggle the element.
  // But (because we use recurcivity, this compnent can call himself, cf template) if the value of the value
  // is still an array or an object, we don't reproduce this behavior to hide. This toggle system is used
  // only for the first data-detail component, not for the children.
  @Input() isParent: boolean;
  keys: string[];
  isOpenProperties = {};

  constructor() { }

  ngOnInit() {
  }

  get properties() {
    return this._properties;
  }

  getKeys(obj) {
    let keys = [];
    if (obj) {
      keys = Object.keys(obj);
    }
    return keys;
  }
  // Return the type of the 'key' property of the input object
  getTypeOfProperty(key: string) {
    const type = typeof this.properties[key];
    let res: string;
    if (type === 'object' && Array.isArray(this.properties[key])) {
      res = 'array';
    } else {
      res = type;
    }
    return res;
  }

  isPropertyObjectOrArray(key: string) {
    return this.getTypeOfProperty(key) === 'object' || Array.isArray(this.properties[key]);
  }

  // Return true if the 'object' Input of the component is an array
  inputIsArray() {
    return Array.isArray(this.properties);
  }

  toggleProperty(key: string) {
    if (this.getTypeOfProperty(key) === 'object' || Array.isArray(this.properties[key])) {
      this.isOpenProperties[key] = !this.isOpenProperties[key];
    }
  }

  isJson(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  isUrl(value: any) {
    const expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
    const regex = new RegExp(expression);
    const isStringAndNotNull = value ? typeof value === 'string' : false;
    return isStringAndNotNull ? value.match(regex) : false;
  }

  isFloat(n: any) {
    return Number(n) === n && n % 1 !== 0;
  }

  roundTo(num: number, places: number) {
    const factor = 10 ** places;
    return Math.round(num * factor) / factor;
  }
}
