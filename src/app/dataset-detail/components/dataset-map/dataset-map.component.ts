import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { APP_CONFIG } from '../../../core/services/app-config.service';
import { MapComponent } from '../../../map/components';
import { MapOptions } from '../../../map/models/map-options';
import { MapService } from '../../../map/services/map.service';
import { Data, linkFormats, Metadata } from '../../../shared/models';
import { DatasetDetailService } from '../../services';

@Component({
  selector: 'app-dataset-map',
  templateUrl: './dataset-map.component.html',
  styleUrls: ['./dataset-map.component.scss'],
})

export class DatasetMapComponent implements OnInit, OnDestroy {
  @Input() isDisplayed: boolean;
  @Input()
  set selectedFeature(selectedFeature: number) {
    this._selectedFeature = selectedFeature;
    // Indicates to the map what feature should be highlighted
    this._mapService.setSelectedFeature(this._selectedFeature);
  }

  @Output() featureClicked = new EventEmitter<mapboxgl.MapboxGeoJSONFeature>();

  @ViewChild(MapComponent, { static: false }) mapComponent: MapComponent;
  private _selectedFeature = null; // Contains the gid of the selected feature
  private subscriptions: Subscription[] = [];

  mapLoaded: boolean;
  isSample = false;
  datasetSub: Subscription; // Subscription to dataset change
  metadata: Metadata;
  mapOptions: MapOptions;

  constructor(
    private _mapService: MapService,
    private _datasetDetailService: DatasetDetailService,
  ) { }

  ngOnInit() {
    this.metadata = this._datasetDetailService.datasetMetadata;
    this.constructMapOptions();

    this.subscriptions.push(
      // Subcribe to the dataset changes in the service. When the dataset is loaded
      // (with the metadata), we construct the map and display the features
      this._datasetDetailService.dataset$.subscribe(() => {
        this.metadata = this._datasetDetailService.datasetMetadata;
        this.constructMapOptions();
      }),
      this._mapService.map$.subscribe(() => {
        this.mapLoaded = true;
      }),
      this._mapService.featureSelected$.subscribe((feature: mapboxgl.MapboxGeoJSONFeature) => {
        this.featureClicked.next(feature);
      }),
      this._datasetDetailService.datasetDataSearchChanged$.subscribe((searchValue) => {
        this._mapService.filterBySearchValue(searchValue, this._datasetDetailService.dataset.fields.list);
      }),
    );

    this.isSample = this._datasetDetailService.dataset.editorialMetadata.isSample;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => {
      sub.unsubscribe();
    });
  }

  constructMapOptions() {
    console.log("constructMapOptions")
    const isSample = this._datasetDetailService.dataset.editorialMetadata.isSample;
    if (this.metadata && !this.mapOptions) {
      this.mapOptions = {} as MapOptions;
      this.mapOptions.isMVT = this._datasetDetailService.dataset.hasTable;
      this.mapOptions.mvtUrl = `${APP_CONFIG.backendUrls.proxyQuery}/map/mvt`;
      this.mapOptions.vectorService =  this.metadata.getVectorService();
      this.mapOptions.rasterService = this.metadata.getRasterService();
      const dataType = {
        isLinear: false,
        isPunctual: false,
        isAreal: false,
      };
      if (this._datasetDetailService.dataset.editorialMetadata.isLinear) {
        dataType.isLinear = true;
      } else if (this._datasetDetailService.dataset.editorialMetadata.isPunctual) {
        dataType.isPunctual = true;
      } else if (this._datasetDetailService.dataset.editorialMetadata.isAreal) {
        dataType.isAreal = true;
      }
      this.mapOptions.dataType = dataType;

      // For authentication reason, we do not use the open url of download.data for the WMS service.
      // We use a kong endpoint for the url to be proxyfied
      if (!this.mapOptions.rasterService) {
        return;
      }

      // If download data, use config url.
      if (this.mapOptions.rasterService && this.mapOptions.rasterService.url &&
        (this.mapOptions.rasterService.url.includes('download.data') || this.mapOptions.rasterService.url.includes('download.recette.data'))) {
        const domain = this.mapOptions.rasterService.url.split('wms')[1];
        this.mapOptions.rasterService.url = `${APP_CONFIG.backendUrls.proxyQuery}/map/wms${domain}`;
      }

      // If the data are just a sample, we will use this sample as geojson to display the interaction layer on the map.
      // Otherwise we use normally the MVT service.
      if (isSample) {
        this.mapOptions.isMVT = false;
        if (this._datasetDetailService.dataset.data) {
          const geojson = this.createGeojson(this._datasetDetailService.dataset.data);
          // Use WFS instead of WMT for the interaction
          this.mapOptions.geojson = geojson;
        } else {
          this._datasetDetailService.sampleDataLoaded$.subscribe((data) => {
            const geojson = this.createGeojson(data);
            this.mapOptions.geojson = geojson;
          });
        }
      }

      // tslint:disable-next-line: max-line-length
      if(this.metadata.max_east){
        this.mapOptions.bbox = [this.metadata.max_east, this.metadata.max_south, this.metadata.max_west, this.metadata.max_north];
      }
      else{
        // TODO mettre une emprise par defaut en conf
        this.mapOptions.bbox = [-2,42,5,45];
      }
      

      // This is used to remember what is the previous dataset that the map displayed.
      // The reason is to know if we need to display the same map settings (for example the user went to another
      // tab), or if we display with default settings (if the user changed the dataset and go to the map).
      // This is used by the map.component
      if (this._datasetDetailService.lastDatasetIdForMap === this.metadata.dataset_id) {
        this.mapOptions.initOptions = false;
      } else {
        this._datasetDetailService.lastDatasetIdForMap = this.metadata.dataset_id;
        this.mapOptions.initOptions = true;
      }
    }
  }

  // Create a geojson object from an array of data (a sample here)
  private createGeojson(data: Data[]): GeoJSON.FeatureCollection {
    const featureCollection = {
      type: 'FeatureCollection',
      name: this.mapOptions.vectorService.name,
      features: [],
    };
    featureCollection.features = data;

    const newFeatures = [];
    // If the features are 'MultiPoint' type, explode it into multiple 'Point'
    featureCollection.features.forEach((feature, index) => {
      feature.properties['_featureId'] = index;
      if (feature.geometry.type === 'MultiPoint') {
        feature.geometry.coordinates.forEach((point) => {
          const newFeature = Object.assign(feature);
          newFeature.geometry.coordinates = point;
          newFeature.geometry.type = 'Point';
          newFeatures.push(newFeature);
        });
      } else {
        newFeatures.push(feature);
      }
    });
    featureCollection.features = newFeatures;
    return featureCollection as GeoJSON.FeatureCollection;
  }

  get isReady() {
    return this.mapOptions!=undefined;//(this.mapOptions && this.mapOptions.isMVT) ||
      //(this.mapOptions && !this.mapOptions.isMVT && this.mapOptions.geojson);
  }

  reDraw() {
    if (this.mapComponent) {
      this.mapComponent.reDraw();
    }
  }

}
