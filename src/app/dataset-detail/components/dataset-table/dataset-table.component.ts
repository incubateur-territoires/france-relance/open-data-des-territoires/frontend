// tslint:disable-next-line: max-line-length
import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { Subscription } from 'rxjs';
import { geosource } from '../../../../i18n/traductions';
import { Data } from '../../../elasticsearch/models';
import { AppRoutes } from '../../../routes';
import { DatasetDetailService } from '../../services';

@Component({
  selector: 'app-dataset-table',
  templateUrl: './dataset-table.component.html',
  styleUrls: ['./dataset-table.component.scss'],
})
export class DatasetTableComponent implements OnInit, OnDestroy {
  @Input() isDisplayed: boolean;
  // Indicates what columns (properties of the dataset) should be displayed in the table
  @Input() selectedProperties;
  @Input()
  set selectedFeature(id: number) {
    // Making sure that the property will always be a number
    this._selectedFeature = id;
    if (id) {
      this.addVisitedItem(id);
    }
  }
  // This outputs emits the selected data (when the user clicks on an element of the table)
  // By subscribing to this event, the parent component is able to know when a data is selected in the table
  @Output() dataSelected = new EventEmitter<Data>();

  @Output() tableWidthModified = new EventEmitter<boolean>();

  @ViewChildren('itemsToCollapse', { read: ElementRef }) itemsToCollapseList: QueryList<ElementRef>;

  private _selectedFeature: number;
  private subscriptions: Subscription[] = [];

  // Contains the list of gid of the features that have been clicked
  visitedFeatures: number[] = [];

  AppRoutes = AppRoutes;

  // Infinite scroll
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  message: string = null;
  maximumNumberOfData = 10000;

  constructor(
    private _datasetDetailService: DatasetDetailService,
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      // Subscribe to input changes
      this._datasetDetailService.datasetDataSearchChanged$.subscribe(() => {
        this.getSearchResults();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => {
      sub.unsubscribe();
    });
  }

  getStyle() {
    const gridColumns = this.displayedProperties.length !== 1 ?
      `repeat(${this.displayedProperties.length - 1}, max-content) 1fr` : '1fr';
    return {
      // 'grid-template-columns': `repeat(${this.displayedProperties.length}, minmax(50px, 1fr)`,
      'grid-template-columns': gridColumns,
    };
  }

  //  Method called when the scrolled down
  onScrollDown() {
    // Only call the service if the actual number of data is smaller than the
    // total number of data found for this reseach
    if (this.data.length < this.researchMaxResult) {
      // Increase the from value to get more data
      this._datasetDetailService.nextPage();
      // Make sure from + size (in the service) dont exceed maximumNumberOfData, limit set by elasticsearch
      // if a request is made with a size and from and the sum of those is over maximumNumberOfData it will
      // cause an internal error in elaticsearch and the request will fail
      if (this._datasetDetailService.scrollOptionsSum <= this.maximumNumberOfData) {
        // Update the array of data with the newly retrieved data (size shoul have increased by 'size' value)
        this._datasetDetailService.retrieveMoreDatasetData().subscribe();
      } else {
        this.message = `${geosource.datasetData.maxDataDisplayedMessage}${this.maximumNumberOfData}.`;
      }
    } else {
      // console.log('Already retrieved all occurences');
    }
  }

  sortBy(key: string) {
    const keyType = this._datasetDetailService.getKeyType(key);
    // Set the new sort value in the service
    this._datasetDetailService.sortBy(keyType);
    //  Retrieve date sorted with the new value
    this.getSearchResults();
  }

  getSearchResults() {
    // Get the new data
    this._datasetDetailService.retrieveDatasetData().subscribe(() => {
      this.tableWidthModified.emit(true);
    });
  }

  emitSelectedData(data: Data) {
    this.addVisitedItem(data.id);
    this.dataSelected.emit(data);
  }

  isVisited(id) {
    return this.visitedFeatures.includes(id);
  }

  get data(): Data[] {
    return this._datasetDetailService.datasetData;
  }

  get sortValue(): String {
    var sortValue: String = this._datasetDetailService.sortValue;
    return sortValue ? sortValue:'' ;
  }

  get sortOrder() {
    return this._datasetDetailService.sortOrder;
  }

  get researchMaxResult() {
    return this._datasetDetailService.researchMaxResult;
  }

  get selectedFeature() {
    return this._selectedFeature;
  }

  get displayedProperties() {
    const props = [];
    Object.keys(this.selectedProperties).forEach((key) => {
      if (this.selectedProperties[key]) props.push(key);
    });
    return props;
  }

  // Return the type of the 'key' property of the input object
  isPropertyComplex(element, key: string) {
    const type = typeof element.properties[key];
    return type === 'object' || Array.isArray(element.properties[key]);
  }

  isUrl(value: any) {
    const expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
    const regex = new RegExp(expression);
    const isStringAndNotNull = value ? typeof value === 'string' : false;
    return isStringAndNotNull ? value.match(regex) : false;
  }

  addVisitedItem(id: number) {
    this.visitedFeatures.push(id);
    // Make there are no duplicates in the array
    this.visitedFeatures = [...new Set(this.visitedFeatures)]
  }

  isFloat(n: any) {
    return Number(n) === n && n % 1 !== 0;
  }

  roundTo(num: number, places: number) {
    const factor = 10 ** places;
    return Math.round(num * factor) / factor;
  }
}
