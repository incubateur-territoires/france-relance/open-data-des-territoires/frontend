import { Component, OnDestroy, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { notificationMessages } from '../../../../i18n/traductions';
import { Notification } from '../../../core/models';
import { NotificationService } from '../../../core/services';
import { Metadata } from '../../../shared/models';
import { UserService } from '../../../user/services';
import { Resource } from '../../models';
import { DatasetDetailService, ResourcesService } from '../../services';

@Component({
  selector: 'app-dataset-api',
  templateUrl: './dataset-api.component.html',
  styleUrls: ['./dataset-api.component.scss'],
})
export class DatasetAPIComponent implements OnInit, OnDestroy {

  metadata: Metadata;
  resources: Resource[];
  displayLicenseModal = false;
  sub: Subscription;
  hasBeenInitialized = false;

  cguModalIsOpened: boolean;

  isSample = false;

  // To display in the template
  queryableResources; // each property of this object is an Array of Resource;

  constructor(
    private _datasetDetailService: DatasetDetailService,
    private _resourcesService: ResourcesService,
    private _notificationService: NotificationService,
    private _cookieService: CookieService,
    private _userService: UserService,
  ) { }

  ngOnInit() {
    this.resources = [];
    this.queryableResources = {};
    this.isSample = this._datasetDetailService.dataset.editorialMetadata.isSample;
    this.cguModalIsOpened = false; // (this._cookieService.get(environment.cguAcceptedCookieName) !== 'true') &&
      // !this._userService.userIsSignedIn;

    this._resourcesService.getResources().subscribe(
      (results) => {
        this.resources = results;

        this.initialize();
      },
      (err) => {
        this._notificationService.notify(new Notification({
          type: 'error',
          message: `${notificationMessages.resources.initializationError}`,
        }));
      },
    );

    this.sub = this._datasetDetailService.dataset$.subscribe(() => {
      this.initialize();
    });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  // Resources are organized on 2 levels :
  initialize() {
    this.metadata = this._datasetDetailService.datasetMetadata;
    if (this.metadata && this.resources.length > 0 && !this.hasBeenInitialized) {
      this.hasBeenInitialized = true;
      if (this.metadata.link) {
        this.queryableResources = this._resourcesService.getQueryableResources(this.metadata.link);
      }
    }
  }

  toggleLicenseModal() {
    this.displayLicenseModal = !this.displayLicenseModal;
  }

  get hasQueryableResources() {
    return Object.keys(this.queryableResources).length > 0;
  }

  cguAccepted() {
    this.cguModalIsOpened = false;
  }
}
