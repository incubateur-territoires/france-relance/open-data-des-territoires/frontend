import { OnInit, Input, Component } from '@angular/core';

@Component({
  selector: 'app-icon-format',
  templateUrl: './icon-format.component.html',
  styleUrls: ['./icon-format.component.scss'],
})
export class IconFormatComponent implements OnInit {

  @Input() resourceType: string;
  @Input() size?: string;

  ngOnInit() {
  }
}
