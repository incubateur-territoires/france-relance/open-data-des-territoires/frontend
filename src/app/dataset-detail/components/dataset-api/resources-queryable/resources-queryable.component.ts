import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { Metadata } from '../../../../shared/models';
import { Projection, Resource } from '../../../models';
import { ResourcesService } from '../../../services';

@Component({
  selector: 'app-resources-queryable',
  templateUrl: './resources-queryable.component.html',
  styleUrls: ['./resources-queryable.component.scss'],
})
export class ResourcesQueryableComponent implements OnInit {
  @Input() resources: Resource[];
  @Input() metadata: Metadata;
  @Input() openFirst: boolean;
  selectedApis: {};
  projections: Projection[];

  constructor(
    private _resourcesService: ResourcesService,
  ) {
  }

  ngOnInit() {
    this.selectedApis = {};
    this._resourcesService.getProjections().subscribe((projections) => {
      this.projections = projections;
      if (this.openFirst) {
        this.selectedApis[this.resources[0].id] = this.resources[0];
      }
    });
  }

  updateSelectedResource(idResource: string) {
    this.selectedApis[idResource] = !this.selectedApis[idResource];

  }

  getMessageWarning(resource: Resource) {
    return window.location.href.includes(environment.angularAppHost.en) ?
      resource.messageWarningEn :
      resource.messageWarningFr;
  }
}
