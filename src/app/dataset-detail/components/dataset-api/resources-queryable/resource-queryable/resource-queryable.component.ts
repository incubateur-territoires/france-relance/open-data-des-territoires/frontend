import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { communeInsee } from '../../../../../../assets/resources/commune-insee';
import { geosource } from '../../../../../../i18n/traductions';
import { APP_CONFIG } from '../../../../../core/services/app-config.service';
import { Dataset, linkFormats, Metadata } from '../../../../../shared/models';
import { Format, Projection, Resource } from '../../../../models';
import { DatasetDetailService } from '../../../../services';

@Component({
  selector: 'app-resource-queryable',
  templateUrl: './resource-queryable.component.html',
  styleUrls: ['./resource-queryable.component.scss'],
})
export class ResourceQueryableComponent implements OnInit {
  linkFormats = linkFormats;
  APP_CONFIG = APP_CONFIG;
  @Input() resource: Resource;
  @Input() metadata: Metadata;
  @Input() projections: Projection[];

  downloadMessage = geosource.downloads.download;


  queryableUrl: string;
  queryableUrlToDisplay: SafeHtml;
  allSettingsOK: boolean = true;

  shpProjections = {
    'EPSG:4171': 'EPSG:4171',
    'EPSG:3946': 'EPSG:3946',
    'EPSG:2154': 'EPSG:2154',
    'EPSG:4326': 'EPSG:4326',
    'EPSG:4258': 'EPSG:4258',
  };

  labelFormat = {
    WFS: 'outputFormat',
    WMS: 'FORMAT',
    WCS: 'FORMAT',
  };
  labelLayer = {
    WFS: 'typename',
    WMS: 'layers',
    WCS: 'identifiers',
  };

  inseeLabel: string;

  // Dropdown format
  dropDownFormatsToggle: false;
  selectedFormat: Format;

  // Dropdown projection
  dropDownProjectionToggle: false;
  selectedProjection: {
    name: '',
    bbox: any,
  };
  projectionList;

  // Dropdown CSV's field format
  csvFormatToggle: false;
  selectedFieldsSeparator: '';
  csvFieldsSeparatorList;

  // Dropdown CSV's decimal format
  csvDecimalFormatToggle: false;
  csvDecimalSeparatorList;
  selectedDecimalseparator: '';

  // Dropdown insee
  dropDownInseeToggle: false;
  selectedInsee: {
    commune: '',
    insee: '',
  };
  communeInseeList = communeInsee;

  // number of features (for WFS)
  numberFeatures: number = 100;
  inputCountFeatures: number = 100;
  isAllFeaturesChecked = false;

  fromFeature: number = 0;
  minFromFeature: number = 0;

  // Object that will manage the display of the queryable url
  queryableParameters = {
    baseUrl: '',
    baseParameters: '',
    layer: '',
    outputFormat: '',
    outputFormatActive: false,
    projection: '',
    projectionActive: false,
    bbox: '',
    insee: '',
    inseeActive: false,
    numberFeatures: false,
    fromFeature: false,
    decimalSeparator: '',
    decimalSeparatorActive: false,
    fieldSeparator: '',
    fieldSeparatorActive: false
  };
  
  dataset?:Dataset;

  constructor(
    private _datasetDetailService: DatasetDetailService,
    private _sanitizer: DomSanitizer,
  ) { }

  // reset the values when the input( selectedResource) is changed
  ngOnChanges() {

    this.queryableParameters = {
      baseUrl: '',
      baseParameters: '',
      layer: '',
      outputFormat: '',
      outputFormatActive: false,
      projection: '',
      projectionActive: false,
      bbox: '',
      insee: '',
      inseeActive: false,
      numberFeatures: false,
      fromFeature: false,
      decimalSeparator: '',
      decimalSeparatorActive: false,
      fieldSeparator: '',
      fieldSeparatorActive: false
    };

    this.initSettings();
  }

  initSettings() {
    this.dataset=this._datasetDetailService.dataset;
    // By default we display for each resource the root URL.
    // Then we check if all necessary information are there to init the custom URL tool
    this.allSettingsOK = true;
    if ((this.resource.formats && this.resource.formats.length < 1) ||
      (this.resource.type === linkFormats.wms && !this.resource.metadataLink.bbox_by_projection) ||
      (this.resource.type === linkFormats.wcs && !this.resource.metadataLink.bbox_by_projection) ||
      this.resource.type === linkFormats.wfs &&
      (!this.resource.metadataLink.projections || this.resource.metadataLink.projections.length < 1)) {
      this.allSettingsOK = false;
    }

    if (this.allSettingsOK) {
      // This is the starting index feature. For WS: 1, WFS: 0
      if (this.resource.type === linkFormats.ws) {
        this.minFromFeature = this.fromFeature = 1;
      } else if (this.resource.type === linkFormats.wfs) {
        this.minFromFeature = this.fromFeature = 0;
      }
      this.isAllFeaturesChecked = false;
      this.inputCountFeatures = 100;
      this.numberFeatures = 100;
      this.initURLCustomization();
    }
  }

  ngOnInit() {
    this.initSettings();
  }

  initInseeLabel(datasetData) {
    if(APP_CONFIG.theFunctionalitiesInterruptor.inseeFilter){
      const properties = Object.keys(datasetData[0].properties);
      this.inseeLabel = properties.find((prop) => { return prop.includes('insee'); });
    }
  }

  initURLCustomization() {
    this.projectionList = [];

    this.csvFieldsSeparatorList = [',',';'];
    this.csvDecimalSeparatorList = ['.',','];


    // Set the insee property label for this dataset
    if (this._datasetDetailService.datasetData[0]) {
      this.initInseeLabel(this._datasetDetailService.datasetData);
    }
    else {
      this._datasetDetailService.sampleDataLoaded$.subscribe((data) => {
        this.initInseeLabel(this._datasetDetailService.datasetData);
      });
    }

    this.selectedFormat = this.resource.formats[0];
    this.communeInseeList.sort((a, b) => (a.commune > b.commune) ? 1 : -1);

    // Get projections and set the default one (depending on the resource)
    if (this.resource.type === linkFormats.wms 
          || this.resource.type === linkFormats.wcs) {
      this.resource.metadataLink.projections.forEach((key) => {
          this.projectionList.push({
            name: key,
            bbox: this.resource.metadataLink.bbox_by_projection[key],
          });
        });
      this.selectedProjection = this.projectionList[0];
    }
    else if (this.resource.type === linkFormats.wfs) {
      this.resource.metadataLink.projections.forEach((key) => {
        this.projectionList.push({
          name: key,
          bbox: key,
        });
      });
      this.selectedProjection = this.projectionList[0];
    } else if (this.resource.type === linkFormats.ws) {
      this.projectionList = this.transformObjectToArray(this.shpProjections);
      this.selectedProjection = this.projectionList[0];
    }

    // Init the url query parameters that will not be changed by the user
    this.queryableParameters.baseUrl = this.resource.metadataLink.url;

    if (this.resource.type === linkFormats.ws) {
      // Exception for this service, we modify the root URL
      // this.resource.metadataLink.url += '/all.json';
      this.queryableParameters.baseUrl += `/${this.resource.metadataLink.name}/all.json`;
    }
    this.queryableParameters.baseParameters = this.resource.parametersUrl ? `?${this.resource.parametersUrl}` : '';

    if (this.resource.type === linkFormats.kml) {
      this.queryableParameters.baseParameters = `?${this.resource.parametersUrl}` +
        `&typename=${this.resource.metadataLink.name}`;
    }

    // Set the dimension of the image based no the bbox aspect ratio
    if (this.resource.type === linkFormats.wms) {
      const width = 700;
      const bbox = this.resource.metadataLink.bbox_by_projection[this.selectedProjection.name];
      const longDiff = bbox.maxx - bbox.minx;
      const latDiff = bbox.maxy - bbox.miny;
      const ratio = longDiff / latDiff;
      const height = Math.round(width * ratio);
      this.queryableParameters.baseParameters += `&WIDTH=${width}&HEIGHT=${height}`;
    }

    this.setProjection(this.selectedProjection);

    if (this.resource.isStandard) {
      this.queryableParameters.layer = `&${this.labelLayer[this.resource.type]}=${this.resource.metadataLink.name}`;
      this.queryableParameters.outputFormat = `&${this.labelFormat[this.resource.type]}=` +
        `${this.selectedFormat.mapServerType}`;
    }

    this.updateQueryableUrl();
    this.updateQueryableurlToDisplay();
  }

  // When the user changes parameters (or when view init), we update the url provided
  // to the <a> link
  updateQueryableUrl() {
    this.queryableUrl = this.queryableParameters.baseUrl +
      this.queryableParameters.baseParameters;
    this.queryableUrl += this.queryableParameters.layer +
      this.queryableParameters.outputFormat +
      this.queryableParameters.projection +
      this.queryableParameters.insee +
      this.queryableParameters.bbox
      ;


    if (this.resource.type === linkFormats.wfs) {
      this.queryableUrl += `&startIndex=${this.fromFeature}&sortyBy=${this.dataset.getIdField()}`;
    } else if (this.resource.type === linkFormats.ws) {
      if (this.selectedFormat.isProjectable === 1) {
        this.queryableUrl += `&`;
      } else {
        this.queryableUrl += `?`;
      }
      this.queryableUrl += `maxfeatures=${this.numberFeatures}&start=${this.fromFeature}`;
    }

    if (this.queryableParameters.decimalSeparator !== '') {
      this.queryableUrl += `&ds=${this.queryableParameters.decimalSeparator}`;
    }
    if (this.queryableParameters.fieldSeparator !== '') {
      this.queryableUrl += `&separator=${this.queryableParameters.fieldSeparator}`;
    }

    if (!this.isAllFeaturesChecked && this.resource.type === linkFormats.wfs) {
      this.queryableUrl += `&count=${this.numberFeatures}`;
    }

  }

  updateQueryableurlToDisplay() {
    let queryableUrlToDisplay: string = '';

    queryableUrlToDisplay = this.queryableParameters.baseUrl
      + this.queryableParameters.baseParameters
      + this.queryableParameters.layer;

    // Project & bbox
    if (this.queryableParameters.projectionActive) {
      queryableUrlToDisplay += `<span class="is-active-parameter">${this.queryableParameters.projection}`
        + `${this.queryableParameters.bbox}</span>`;
    } else {
      queryableUrlToDisplay += this.queryableParameters.projection + this.queryableParameters.bbox;
    }

    // Format
    if (this.selectedFormat) {
      if (this.queryableParameters.outputFormatActive) {
        queryableUrlToDisplay +=
          `<span class="is-active-parameter">${this.queryableParameters.outputFormat}</span>`;
      } else {
        queryableUrlToDisplay += this.queryableParameters.outputFormat;
      }
    }

    // Insee
    if (this.selectedInsee) {
      if (this.queryableParameters.inseeActive) {
        queryableUrlToDisplay +=
          `<span class="is-active-parameter">${this.queryableParameters.insee}</span>`;
      } else {
        queryableUrlToDisplay += this.queryableParameters.insee;
      }
    }

    // Number features WFS
    if (!this.isAllFeaturesChecked && this.resource.type === this.linkFormats.wfs) {
      if (this.queryableParameters.numberFeatures) {
        queryableUrlToDisplay +=
          `<span class="is-active-parameter">&count=${this.numberFeatures}</span>`;
      } else {
        queryableUrlToDisplay += `&count=${this.numberFeatures}`;
      }
    }

    // Number features WS
    if (this.resource.type === this.linkFormats.ws) {
      if (this.queryableParameters.numberFeatures) {
        queryableUrlToDisplay += this.selectedFormat.isProjectable === 0 ?
          `<span class="is-active-parameter">?maxfeatures=${this.numberFeatures}</span>` :
          `<span class="is-active-parameter">&maxfeatures=${this.numberFeatures}</span>`;
      } else {
        queryableUrlToDisplay += this.selectedFormat.isProjectable === 0 ?
          `?maxfeatures=${this.numberFeatures}` :
          `&maxfeatures=${this.numberFeatures}`;
      }
    }
    if (this.queryableParameters.decimalSeparator !== '' && this.queryableParameters.decimalSeparator != undefined) {
      queryableUrlToDisplay += `<span class="is-active-parameter">&ds=${this.queryableParameters.decimalSeparator}</span>`;
    }
    if (this.queryableParameters.fieldSeparator !== '' && this.queryableParameters.fieldSeparator != undefined) {
      queryableUrlToDisplay += `<span class="is-active-parameter">&separator=${this.queryableParameters.fieldSeparator}</span>`;
    }

    // From feature WFS
    if (this.resource.type === this.linkFormats.wfs) {
      if (this.queryableParameters.fromFeature) {
        queryableUrlToDisplay +=
          `<span class="is-active-parameter">&startIndex=${this.fromFeature}</span>&sortyBy=${this.dataset.getIdField()}`;
      } else {
        queryableUrlToDisplay += `&startIndex=${this.fromFeature}&sortyBy=${this.dataset.getIdField()}`;
      }
    }

    // From feature WS
    if (this.resource.type === this.linkFormats.ws) {
      if (this.queryableParameters.fromFeature) {
        queryableUrlToDisplay +=
          `<span class="is-active-parameter">&start=${this.fromFeature}</span>`;
      } else {
        queryableUrlToDisplay += `&start=${this.fromFeature}`;
      }
    }

    this.queryableUrlToDisplay = this._sanitizer.bypassSecurityTrustHtml(queryableUrlToDisplay);
  }

  // Set the selected format and set the queryable parameters
  // depending on the resource
  setFormat(format: Format) {
    console.log (format);
    this.selectedFormat = format;
    this.resetUrlHilightedParameters();
    if (this.resource.type !== linkFormats.ws) {
      this.queryableParameters.outputFormat = `&${this.labelFormat[this.resource.type]}=` +
        `${this.selectedFormat.mapServerType}`;
      this.queryableParameters.outputFormatActive = true;
      this.queryableParameters.inseeActive = false;
      if (!this.selectedFormat.isProjectable) {
        this.queryableParameters.insee = '';
      }
    } else {
      // Specific treatment for the WS service. We need manipulate the url
      // depending the json or shapefile format wanted
      if (this.selectedFormat.name === 'JSON') {
        const specificRootURL = this.resource.metadataLink.url.replace('all.json', '');
        this.queryableParameters.baseUrl = `${specificRootURL}/${this.resource.metadataLink.name}/all.json`;
        this.selectedInsee = null;
        this.queryableParameters.insee = '';
        this.queryableParameters.projection = '';
      } else if (this.selectedFormat.name === 'CSV') {
        const specificRootURL = this.resource.metadataLink.url.replace('all.json', '');
        this.queryableParameters.baseUrl = `${specificRootURL}/${this.resource.metadataLink.name}/all.csv`;
        this.selectedInsee = null;
        this.queryableParameters.insee = '';
        this.queryableParameters.projection = '';
        this.queryableParameters.decimalSeparator = '';
        this.queryableParameters.fieldSeparator = '';

        //this.setFieldsSeparator(this.selectedFieldsSeparator);
        //this.setDecimalSeparator(this.selectedDecimalseparator);
      }
      else {
        this.queryableParameters.baseUrl = this.resource.metadataLink.url.replace('/all.json', '');
        this.queryableParameters.baseUrl += `/${this.resource.metadataLink.name}.shp?`;
        if (this.selectedFormat.isProjectable) {
          this.setProjection(this.selectedProjection);
        }
      }
    }

    this.updateQueryableUrl();
    this.updateQueryableurlToDisplay();
  }

  // Set the selected insee and set the queryable parameters
  // depending on the resource
  setInsee(insee) {
    this.selectedInsee = insee;
    if (!this.selectedInsee || !this.inseeLabel) {
      this.queryableParameters.insee = '';
    } else {
      this.resetUrlHilightedParameters();
      if (this.resource.type === linkFormats.wfs) {
        this.queryableParameters.insee = `&CQL_FILTER=${this.inseeLabel}=${this.selectedInsee.insee}`;
        /*='&filter=Filter=<Filter> <PropertyIsLike wildcard="*" ' +
          'singleChar=\'.\' escape=\'_\'> <PropertyName>' +
          `${this.inseeLabel}</PropertyName><Literal>${this.selectedInsee.insee}` +
          '</Literal></PropertyIsLike> </Filter>';*/
      } else if (this.resource.type === linkFormats.ws) {
        this.queryableParameters.baseUrl = this.resource.metadataLink.url.replace('\/all.json', '');
        const match = (/\/ws\/([^\/]+)/g).exec(this.resource.metadataLink.url);
        this.queryableParameters.baseUrl += `/${this.resource.metadataLink.name}.shp?`;
        const serviceName = match[1] ? match[1] : '';
        this.queryableParameters.insee = `&mask_db=${serviceName}&mask_layer=adr_voie_lieu.adrcommune` +
          `&mask_field=insee&mask_value=${this.selectedInsee.insee}`;
      }
      this.queryableParameters.inseeActive = true;
    }

    this.updateQueryableUrl();
    this.updateQueryableurlToDisplay();
  }

  // Set the selected projection and set the queryable parameters
  // depending on the resource
  setProjection(projection) {
    this.selectedProjection = projection;
    this.resetUrlHilightedParameters();
    if (this.resource.type === linkFormats.wfs) {
      this.queryableParameters.projection = `&SRSNAME=${projection.name}`;
    } else if (this.resource.type === linkFormats.wms || this.resource.type === linkFormats.wcs) {
      this.queryableParameters.projection = `&CRS=${projection.name}`;
      this.queryableParameters.bbox = `&BBOX=${projection.bbox.minx},` +
        `${projection.bbox.miny},` +
        `${projection.bbox.maxx},` +
        `${projection.bbox.maxy}`;
    } else if (this.resource.type === linkFormats.ws) {
      if (this.selectedFormat.isProjectable === 1 ) {
        this.queryableParameters.projection = `srsname=${projection.name}`;
      }
    }

    this.queryableParameters.projectionActive = true;
    this.updateQueryableUrl();
    this.updateQueryableurlToDisplay();
  }

  setDecimalSeparator(decimalSeparator) {
    this.selectedDecimalseparator = decimalSeparator;
    this.resetUrlHilightedParameters();

    this.queryableParameters.decimalSeparator = `${ decimalSeparator }`;
    this.queryableParameters.decimalSeparatorActive = true;

    this.updateQueryableUrl();
    this.updateQueryableurlToDisplay();
  }

  setFieldsSeparator(fieldsSeparator) {
    this.selectedFieldsSeparator = fieldsSeparator;
    this.resetUrlHilightedParameters();
    this.queryableParameters.fieldSeparator = `${fieldsSeparator}`;
    this.queryableParameters.fieldSeparatorActive = true;

    this.updateQueryableUrl();
    this.updateQueryableurlToDisplay();
  }

  setActiveCountFeatures() {
    this.numberFeatures = this.inputCountFeatures;
    this.resetUrlHilightedParameters();
    this.queryableParameters.numberFeatures = true;
    this.updateQueryableUrl();
    this.updateQueryableurlToDisplay();
  }

  setActiveFromFeature() {
    this.resetUrlHilightedParameters();
    this.queryableParameters.fromFeature = true;
    this.updateQueryableUrl();
    this.updateQueryableurlToDisplay();
  }

  setCSVFormat() {
    this.resetUrlHilightedParameters();
    this.updateQueryableUrl();
    this.updateQueryableurlToDisplay();
  }

  checkAllFeatures() {
    this.numberFeatures = this.isAllFeaturesChecked ?
      -1 : this.inputCountFeatures;
    this.updateQueryableUrl();
    this.updateQueryableurlToDisplay();
  }

  // Reset the variables used to highlight some parameters in the
  // personnalized url
  resetUrlHilightedParameters() {
    this.queryableParameters.projectionActive = false;
    this.queryableParameters.inseeActive = false;
    this.queryableParameters.outputFormatActive = false;
    this.queryableParameters.fromFeature = false;
    this.queryableParameters.numberFeatures = false;
  }

  transformObjectToArray(object) {
    const array = [];
    const keys = Object.keys(object);
    keys.forEach((key) => {
      array.push({
        name: key,
        bbox: object[key],
      });
    });
    return array;
  }

  get fileName() {
    return `${this.resource.metadataLink.name}.${this.selectedFormat.fileExtension}`;
  }

  get totalData() {
    return this._datasetDetailService.dataset.totalData;
  }

  getProjectionLabel(name: string) {

    const projection = this.projections.find((projection) => {
      return projection.name === name;
    });
    return projection ? `${projection.commonName} (${projection.name})` : name;
  }

  get downloadUrl() {
    return this.queryableUrl.replace(
      APP_CONFIG.backendUrls.backendDomain,
      `${APP_CONFIG.backendUrls.proxyQuery}/download`,
    );
  }
}
