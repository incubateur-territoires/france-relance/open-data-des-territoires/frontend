import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Scroll } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ViewportScroller } from '@angular/common';
import { environment } from '../../../../environments/environment';
import { datatsetDataRepresentationType, geosource, reusesTypes } from '../../../../i18n/traductions';
import { NavigationHistoryService } from '../../../core/services';
import { APP_CONFIG } from '../../../core/services/app-config.service';
import { AppRoutes } from '../../../routes';
import { IPageHeaderInfo, Metadata, typesMetadata } from '../../../shared/models';
import { isRentertron } from '../../../shared/variables';
import { DatasetDetailService } from '../../services';
import { SeoSErvice } from '../../../editorialisation/services';


@Component({
  selector: 'app-dataset-detail',
  templateUrl: './dataset-detail.component.html',
  styleUrls: ['./dataset-detail.component.scss'],
})
export class DatasetDetailComponent implements OnInit, OnDestroy {
  APP_CONFIG = APP_CONFIG;
  AppRoutes = AppRoutes;
  isRendertron = isRentertron;

  pageHeaderInfo: IPageHeaderInfo = {
    title: '',
    shareButtons: {
      isActive: true,
      matchingUriInSeoService: 'datasets',
    },
  };
  readonly typesMetadata = typesMetadata;
  position: any;
  scrollSubscription: Subscription;
  isLoading = false;

  constructor(
    private _route: ActivatedRoute,
    private _datasetDetailService: DatasetDetailService,
    private _router: Router,
    private _scroller: ViewportScroller,
    private _navigationHistoryService: NavigationHistoryService,
    private _seoService: SeoSErvice,
  ) { }

  ngOnInit() {
    this.position = [0, 0];
    // Keep scroll position between tabs navigation
    this.scrollSubscription = this._router.events.pipe(
      // Filter on scroll event
      filter(e => e instanceof Scroll),
      // Filter on navigation event children of Approutes.datasets+'/' ('datasets/')
      filter(e => e['routerEvent'].url.search(`${AppRoutes.datasets.uri}/`) !== -1),
    ).subscribe((e) => {
      this._scroller.scrollToPosition(this.position);
    });

    this._route.params.subscribe((params) => {
      // Set the title and description for the research page
      this.isLoading = true;
      this.initDatasetInfo();
    });
  }

  ngOnDestroy() {
    this.scrollSubscription.unsubscribe();
  }

  get metadata(): Metadata {
    return this._datasetDetailService.datasetMetadata;
  }

  // Number of data found by elasticsearch
  get datasetDataNumber() {
    return this._datasetDetailService.datasetDataNumber;
  }
  get datasetLicense() {
    if (window.location.href.includes(environment.angularAppHost.en)) {
      return this.metadata.license.acronymEn;
    }
    return this.metadata.license.acronymFr;
  }

  get datasetUpdateFrequency() {
    const keyUpdate = this.metadata.updateFrequency ? this.metadata.updateFrequency : 'other';
    return geosource.updateFrequencies[keyUpdate];
  }

  get datasetDataRepresentationType(): string {
    let type = '';
    if (this._datasetDetailService.datasetEditorialMetadata.isPunctual) {
      type = datatsetDataRepresentationType.long.punctual;
    }

    if (this._datasetDetailService.datasetEditorialMetadata.isLinear) {
      type = datatsetDataRepresentationType.long.linear;
    }

    if (this._datasetDetailService.datasetEditorialMetadata.isAreal) {
      type = datatsetDataRepresentationType.long.areal;
    }

    return type;
  }

  get datasetServicesList() {
    const list = this._datasetDetailService.datasetMetadata.link.filter(l => l.service).map(l => l.service);
    let formatted: string;

    if (list.length > 4) {
      formatted = `${list.slice(0, 4).join(', ')} ...`; // Keep the first 4 elements of the list
    } else {
      formatted = list.join(', ');
    }

    return formatted;
  }

  get otherResourceList() {
    const links = this._datasetDetailService.datasetMetadata.link;

    let list = links.map((link) => {
      let formats: string[] ;
      // si le lien n'as pas de format, on lui en ajoute un
      if (!link.formats) {
        if (link.name.toUpperCase().includes('API') || link.name.toUpperCase().includes('SERVICE')) {
          formats = ['API'];
        }
        else {
          formats = ['link'];
        }
      }

      let filteredList = formats;
      if (formats) {
        filteredList = formats.filter((f) => {
          let validFormat = true;
          if (f !== 'API' && f !== 'link') {
            validFormat = false;
          }
          return validFormat;
        });
      }
      return filteredList;
    });

    return this.filterFormatList(list);
  }

  // Get the formats available to download
  get datasetFormatsList() {

    const links = this._datasetDetailService.datasetMetadata.link;

    let blockedFormat = ['PNG','JPEG','TIFF','GIF','AAIGrid'];

    let list = links.map((link) => {
      let filteredList = link.formats;
      if (link.formats) {
        filteredList = link.formats.filter((f) => {
          let validFormat = true;
          if (!f) {
            validFormat = false;
          }
          // If nongeographical dataset, there is no shapefile format
          // tslint:disable-next-line: ter-indent
          // tslint:disable-next-line: brace-style
          else if (this._datasetDetailService.datasetMetadata.type === 'nonGeographicDataset'
            && f === 'ShapeFile') {
            validFormat = false;
          } else if (f === 'PDF' && link.name.includes('Licence')) { // Remove PDF because it's Licence
            validFormat = false;
          }
          /*if (link.service === 'WFS') {
            if (f === 'ShapeFile') {
              validFormat = false;
            }
          }*/

          if (link.service === 'WMS' || link.service === 'WCS') {
            if (blockedFormat.includes(f)) {
              validFormat = false;
            }
          }

          return validFormat;
        });
      }
      if(filteredList.length>1 && filteredList.indexOf('HTTP-LINK')>=0){
        filteredList =filteredList.filter(x=>x!='HTTP-LINK');
      }
      return filteredList;
    });

    return this.filterFormatList(list);
  }

  get hasMap() {
    return this._datasetDetailService.dataset.hasMap;
  }

  get accessLevel() {
    return this._datasetDetailService.dataset.accessLevel;
  }

  get hasTable() {
    return this._datasetDetailService.dataset.hasTable;
  }

  get reusesTypes(): string {
    let types = [];
    if (this._datasetDetailService.dataset.reuses.length > 0) {

      types = [
        // Using a Set in order to filter out duplicates values
        ...new Set(
          // Generate an array of all the reuses types (there can be duplicate)
          [].concat(...this._datasetDetailService.dataset.reuses.map(reuse => reuse.reuseTypes))
        ),
      ]
        // Replace the keys by the labels
        .map(e => reusesTypes[e]);
    }

    if (this._datasetDetailService.dataset.relatedNews.length > 0) {
      types.push(reusesTypes['news']);
    }

    // Contact the value into a string (values separated by a coma)
    return types.join(', ');
  }

  initDatasetInfo() {
    this.pageHeaderInfo = {
      ...this.pageHeaderInfo,
      title: this.metadata.title,
      metadataSubtitle: this.metadata.subtitle,
      subtitle: this.metadata.contacts.map(c => c.organisationName)
        .reduce(function (acc, curr) {
        if (!acc.includes(curr))
            acc.push(curr);
        return acc;
        }, [])
        .join(', '),
    };

    this.isLoading = false;
    this._seoService.setDatasetSEO(this.metadata);
    console.log("initDatasetInfos")
    // If a tab (info, data...) is not specified in the url then redirect to the data tab if the dataset
    // as a table or a map or to the info tab in the failing case
    if (!this._route.snapshot.firstChild) {
      if ((this.hasTable || this.hasMap) && this.accessLevel>1 ) {
        this._router.navigate([
          `/${AppRoutes.datasets.uri}/${this._datasetDetailService.dataset.slug}/${AppRoutes.info.uri}`,
          // tslint:disable-next-line: align
        ], { queryParamsHandling: 'preserve', replaceUrl: true });
      } else {
        this._router.navigate([
          `/${AppRoutes.datasets.uri}/${this._datasetDetailService.dataset.slug}/${AppRoutes.info.uri}`,
        ], {replaceUrl: true});
        
      }
    } else {
      // Making that the url contains the slug and not the uuid of the dataset
      this._router.navigate([
        // tslint:disable-next-line: max-line-length
        `/${AppRoutes.datasets.uri}/${this._datasetDetailService.dataset.slug}/${this._route.snapshot.firstChild.url[0]}`,
      ],
        // tslint:disable-next-line: align
        { queryParamsHandling: 'preserve', replaceUrl: true });
    }

    // Emit event to indicate to child component that the dataset has changed
    this._datasetDetailService.datasetChanged();
    // move subscription from ngOnInit
    this._datasetDetailService.retrieveDatasetData().subscribe();
  }

  setPosition() {
    this.position = this._scroller.getScrollPosition();
  }

  goToPreviousPage() {
    let index = 1; // Start to retrieve the previous element
    let url = this._navigationHistoryService.getFromLast(index);
    // While the previous element still has /jeux-de-donnees or is not null (no previous element)
    // get the previous element of the previous element...
    while (url !== null && url.search(`${AppRoutes.datasets.uri}/`) !== -1) {
      // the method getFromLast deletes every history after the index required therefor as we already entered
      // once in the function we need now to retrieve the last element
      index = 0;
      url = this._navigationHistoryService.getFromLast(index);
    }

    // If url is null then redirect to research page
    if (url == null) {
      url = `/${AppRoutes.research.uri}`;
    }

    this._router.navigateByUrl(url);
  }

  filterFormatList(list: string[][]) {
    // Flat the arrays
    list = [].concat(...list);
    // Remove duplicates
    list = [...new Set(list)];

    list = list.filter((el) => {
      return el !== undefined;
    });

    let formatted: string;

    if (list.length > 4) {
      formatted = `${list.slice(0, 4).join(', ')} ...`; // Keep the first 4 elements of the list
    } else {
      formatted = list.join(', ');
    }

    return formatted;
  }
}
