import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '../routes';
// tslint:disable-next-line: max-line-length
import { DatasetAPIComponent, DatasetDetailComponent, DatasetDownloadsComponent, DatasetInfoComponent, DatasetTableMapComponent, DatasetReusesComponent } from './components';
import { DatasetDetailResolver } from './resolvers';
export const routes: Routes = [
  // Some of the parteners already used the english version of this routing so in order to not break again there link
  // we implemented a fallback routes that redirects to the new routing
  {
    path: '',
    component: DatasetDetailComponent,
    resolve: {
      dataset: DatasetDetailResolver,
    },
    children: [
      {
        path: AppRoutes.info.uri,
        component: DatasetInfoComponent,
      },
      {
        path: AppRoutes.data.uri,
        component: DatasetTableMapComponent,
      },
      {
        path: AppRoutes.resources.uri,
        component: DatasetAPIComponent,
      },
      {
        path: AppRoutes.downloads.uri,
        component: DatasetDownloadsComponent,
      },
      {
        path: AppRoutes.otherResources.uri,
        component: DatasetDownloadsComponent,
      },
      {
        path: AppRoutes.dataReuses.uri,
        component: DatasetReusesComponent,
      },
    ],
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)],
})
export class DatasetDetailRoutingModule { }
