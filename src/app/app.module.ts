import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { EditorialisationModule } from './editorialisation/editorialisation.module';
import { UserModule } from './user/user.module';
import { UserService } from './user/services';
import { AppConfigService, NotificationService, TarteAuCitronService } from './core/services';
import { timeout } from 'rxjs/operators';
import { notificationMessages } from '../i18n/traductions';

// Function used by APP_INITIALIZER before the app start: init user info / statut (expect a promise)
export function initUserService(authService: UserService, notificationService: NotificationService) {
  return (): Promise<any> => {
    return new Promise<void>((resolve, reject) => {
      authService.initializeService().pipe(timeout(3000)).subscribe(
        () => {
          resolve();
        },
        (err) => {
          if (err.status !== 401) {
            notificationService.notify({
              type: 'error',
              message: notificationMessages.userInfo.userInit,
            });
          }
          resolve();
        });
    });
  };
}

export function initTarteaucitronService(tarteaucitronService: TarteAuCitronService) {
  return (): Promise<void> => {
    return new Promise((resolve, reject) => {
      tarteaucitronService.load();
      resolve();
    });
  };
}

export function initAppConfig(appConfigService: AppConfigService) {
  return (): Promise<any> => {
    return new Promise<void>((resolve, reject) => {
      appConfigService.load();
      resolve();
    });
  };
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CoreModule,
    EditorialisationModule,
    UserModule,
    AppRoutingModule,
  ],
  providers: [
    // The order is important as the InitUserService require the configuration of the app
    {
      provide: APP_INITIALIZER,
      useFactory: initAppConfig,
      deps: [AppConfigService],
      multi: true,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initTarteaucitronService,
      deps: [TarteAuCitronService],
      multi: true,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initUserService,
      deps: [UserService, NotificationService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
