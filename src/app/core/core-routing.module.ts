import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '../routes';
import { ContactComponent } from './components';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: AppRoutes.home.uri,
    pathMatch: 'full',
  },
  {
    path: AppRoutes.page404.uri,
    component: PageNotFoundComponent,
    data: {
      title: AppRoutes.page404.title,
    }
  },
  {
    path: AppRoutes.contact.uri,
    component: ContactComponent,
    data: {
      title: AppRoutes.contact.title,
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoreRoutingModule { }
