// http-service-interceptor.ts
// tslint:disable-next-line: max-line-length
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserService } from '../../user/services';

@Injectable()
export class HttpErrorResponseInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private _userService: UserService,
  ) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(tap(
      (event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          return event;
        }
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          switch (err.status) {
            case 401:
              // console.log('HTTP ERROR: Unauthenticated');
              this._userService.resetAuth();
              break;
            case 403:
              // console.log('HTTP ERROR: Forbidden');
              break;
            case 404:
              // console.log('HTTP ERROR: Not Found');
              break;
            case 500:
            case 502:
            case 503:
            case 504:
              // console.log('HTTP ERROR: Server Error');
              break;
            default:
              // console.log(`HTTP ERROR: Status code ${err.status}`);
          }
        }
      }),
    );
  }
}
