import { HeaderComponent } from './main/header/header.component';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './main/footer/footer.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { ContactComponent } from './contact/contact.component';
import { SideMenuComponent } from './main/side-menu/side-menu.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

export {
  HeaderComponent,
  MainComponent,
  FooterComponent,
  NotificationsComponent,
  ContactComponent,
  FeedbackComponent,
  PageNotFoundComponent
};

// tslint:disable-next-line:variable-name
export const CoreComponents = [
  HeaderComponent,
  MainComponent,
  FooterComponent,
  NotificationsComponent,
  ContactComponent,
  SideMenuComponent,
  FeedbackComponent,
  PageNotFoundComponent,
];
