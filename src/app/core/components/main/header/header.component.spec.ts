import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HeaderComponent } from './header.component';
import { ErrorService, NotificationService } from '../../../services';
import { Router } from '@angular/router';
import { DatasetResearchService, ElasticsearchService } from '../../../../geosource/services';
import { MockComponent } from 'ng2-mock-component';
import { StorageService } from '../../../services/storage.service';
import { HttpClientModule } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../../../../user/models';
import { UserService } from '../../../../user/services';
import { InlineSVGModule } from 'ng-inline-svg';
import { InlineSVGService } from 'ng-inline-svg/lib/inline-svg.service';

describe('HeaderComponent', () => {

  describe('Template', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;
    let debugElement: DebugElement;

    beforeEach(waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          HeaderComponent,
          MockComponent({ selector: 'app-search-bar', inputs: ['searchOnChangeScope'] }),
        ],
        imports: [
          RouterTestingModule,
          HttpClientModule,
          InlineSVGModule,
        ],
        providers: [
          UserService,
          DatasetResearchService,
          ElasticsearchService,
          ErrorService,
          StorageService,
          NotificationService,
          InlineSVGService,
        ],
      })
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(HeaderComponent);
      component = fixture.componentInstance;
      debugElement = fixture.debugElement;

      fixture.detectChanges();
    });

    it('should create component', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Component.ts', () => {
    let component: HeaderComponent;
    const routerMock = {
      url: '',
      navigate: (params: string[]) => {
        return Promise.resolve(true);
      },
      events: new Observable((observer) => {
        observer.next(this.start);
        observer.next(this.end);
        observer.complete();
      }),
    } as Router;

    const datasetResearchServiceMock = {
      get searchChange$(): Observable<string> {
        return of('');
      },
    } as DatasetResearchService;

    const userServiceMock = {
      resetAuth() { },
      user: new User({}),
      get userIsSignedIn() {
        return false;
      },
    } as UserService;

    beforeEach(() => {
      component = new HeaderComponent(datasetResearchServiceMock, routerMock, userServiceMock);
    });

    it('ngOnInit()', () => {
      // When
      component.ngOnInit();

      // Then
      expect(component.isBurgerActive).toBe(false);
    });
  });
});
