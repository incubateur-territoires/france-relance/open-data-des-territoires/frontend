import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { environment } from '../../../../../environments/environment';
import { AppRoutes } from '../../../../routes';
import { UserService } from '../../../../user/services';
import { AppStateService, CommunesService } from '../../../services';
import { APP_CONFIG } from '../../../services/app-config.service';
import { Commune, ICommune } from '../../../../core/models/commune.model';


declare var _paq: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit, OnChanges {
  @Input() commune: ICommune;
  @Input() isPortalLightTheme: boolean = false;

  isBurgerActive = false;
  env = environment;
  isHomePage = true;
  userDropdownIsOpened = false;

  AppRoutes = AppRoutes;

  APP_CONFIG = APP_CONFIG;

  communes:Commune[];
  loginUrl = '';
  constructor(
    private _router: Router,
    public _userService: UserService,
    private _appStateService: AppStateService,
    private _communesService:CommunesService
  ) { }
  
  ngOnChanges(changes: SimpleChanges) {
    // changes.prop contains the old and the new value...
    console.log(changes);
  }
  
  ngOnInit() {
    this._router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isHomePage = (event.url === `/${AppRoutes.home.uri}` || event.url === '/') ? true : false;
        this.initLoginUrl();
      }
    });

    this._appStateService.menuStateChanged$.subscribe((state) => {
      this.isBurgerActive = state;
    });
    this._communesService.getCommunes().subscribe(communes=>this.communes=communes);
    this.initLoginUrl();
  }
  
  initLoginUrl(){
    if(window.location.href.endsWith("fr/accueil"))
    {
      this.loginUrl='/onegeo-login/signin?next=/portail/fr/recherche';
    }
    else{
      this.loginUrl='/onegeo-login/signin?next='+window.location.href;
    }
  }

  getQueryParamForCommune(communeName){
    // console.log(communeName)
    const c =this.communes.find(commune=>commune.name==communeName);
    if(c){
      return {portail: c.elasticSearchName};
    }
    return {portail: "NotFound"};
    
  }
  currentLanguage() {
    return window.location.href.includes(environment.angularAppHost.en) ?
      environment.angularAppHost.en :
      environment.angularAppHost.fr;
  }

  changeLanguage(lang: string) {
    window.location.href = environment.angularAppHost[lang] + this._router.url;
  }

  toggleBurger() {
    this.isBurgerActive = !this.isBurgerActive;
    this._appStateService.changeMenuState(this.isBurgerActive);
    if(window['_paq']){
      _paq.push(['trackEvent', 'Navigation', 'BurgerMenu', 'BurgerManu', 1]);
    }

  }

  toggleUserDropdown() {
    this.userDropdownIsOpened = !this.userDropdownIsOpened;
  }

  toogleFeedback(event) {
    event.stopPropagation(); // Avoid to trigger clickOutside directive
    this._appStateService.changeFeedbackState(!this._appStateService.feedbackOpened);
  }

  signOut() {
    this._userService.resetAuth();
    this.closeUserDropdown();
    this._router.navigate(['/', AppRoutes.home.uri]);
  }

  getMapsUrl() {
    if(APP_CONFIG.externalUrls.maps){
      return APP_CONFIG.externalUrls.maps;
    }
    return '/onegeo-maps/';
  }

  closeUserDropdown() {
    this.userDropdownIsOpened = false;
  }

  get userIsSignedIn() {
    return this._userService.userIsSignedIn;
  }

  get username() {
    return this._userService.user.firstName;
  }
}
