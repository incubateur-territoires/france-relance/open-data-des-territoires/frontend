import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppRoutes } from '../../../routes';
import { AppStateService } from '../../services';
import { APP_CONFIG } from '../../services/app-config.service';
import { ICommune } from '../../../core/models/commune.model';
import { CommunesService } from './../../../core/services';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit, OnDestroy {

  sidebarOpened = false;
  isHomePage = false;
  isDatasetDetailTableMapPage = false;
  subscriptions: Subscription[] = [];
  APP_CONFIG = APP_CONFIG;

  commune: ICommune;
  isPortalLightTheme: boolean;

  constructor(
    private _router: Router,
    private _appStateService: AppStateService,
    private _route: ActivatedRoute,
    private _communesService:CommunesService,
    public sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
    // Check if homepage. This is used to color the background (to make side-menu transition possible)
    this._router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isHomePage = (event.url === `/${AppRoutes.home.uri}` || (event.url === '/')) ? true : false;
        const regex = new RegExp(`${AppRoutes.datasets.uri}\/.+\/${AppRoutes.data.uri}`);
        this.isDatasetDetailTableMapPage = event.url.match(regex) !== null ? true : false;
      }
    });

    this.subscriptions.push(
      this._appStateService.menuStateChanged$.subscribe((state) => {
        this.sidebarOpened = state;
      }),
    );

    this._route.queryParams.subscribe((params) => {
      if (params.portail) {
         this._communesService.getCommuneByEsName(params.portail).subscribe((commune)=>{
          this.commune = commune;
          if (this.commune) {
            this.isPortalLightTheme = this.commune.text_color === '#000000';
          }
        });
        
      }
      else{
        this.commune=undefined;
        this.isPortalLightTheme=false;
      }
    });

  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => {
      sub.unsubscribe();
    });
  }

  closeSideMenu() {
    this.sidebarOpened = false;
    this._appStateService.changeMenuState(false);

  }

  get feedbackIsOpened() {
    return this._appStateService.feedbackOpened;
  }
  get isFeedBackActive(){
    return APP_CONFIG.theFunctionalitiesInterruptor.feedback;
  }
  get communeStyle() {
    if (this.commune && this.commune.color && this.commune.text_color) {
      return `--commune-color-var: ${this.commune.color}; --commune-text_color-var: ${this.commune.text_color};`;
    }
    return ''
  }
  public skipToContent() {
    setTimeout(() => {
      document.getElementById('site-content').focus();
    });
  }

  public skipToFooter() {
    setTimeout(() => {
      document.getElementById('site-footer').focus();
    });
  }
}
