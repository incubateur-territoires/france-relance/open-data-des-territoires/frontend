import { Component, OnInit } from '@angular/core';
import { AppRoutes } from '../../../../routes';
import { AppStateService } from '../../../services';
import { APP_CONFIG } from '../../../services/app-config.service';

declare var tarteaucitron: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  // Instanciate the object containing routes name
  AppRoutes = AppRoutes;
  APP_CONFIG = APP_CONFIG;
  catalogueUrl = APP_CONFIG.backendUrls.catalogue;

  constructor(
    private _appStateService: AppStateService,
  ) { }

  ngOnInit() { }

  openCookieModal() {
    tarteaucitron.userInterface.openPanel();
  }

}
