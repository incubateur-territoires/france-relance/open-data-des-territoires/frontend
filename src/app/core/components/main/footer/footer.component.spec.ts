import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { FormsModule } from '@angular/forms';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  describe('Template', () => {
    let component: FooterComponent;
    let fixture: ComponentFixture<FooterComponent>;
    let de: DebugElement;

    beforeEach(waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          FooterComponent,
        ],
        imports: [
          FormsModule,
          RouterTestingModule,
        ],
      })
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(FooterComponent);
      component = fixture.componentInstance;
      de = fixture.debugElement;

      fixture.detectChanges();
    });

    it('should create component', () => {
      expect(component).toBeTruthy();
    });
  });
});
