import { Directive, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[closeMenu]',
})
export class CloseMenuDirective {
  constructor(private _elementRef: ElementRef) {
  }

  @Output()
  public closeMenu = new EventEmitter();

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    const isHamburgerClick = targetElement && targetElement.className && typeof targetElement.className === 'string' ?
      targetElement.className.includes('hamburger') : false;
    const clickedInside = this._elementRef.nativeElement.contains(targetElement);
    if (!clickedInside && !isHamburgerClick) {
      this.closeMenu.emit(null);
    }
  }
}
