import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SideMenuComponent } from './side-menu.component';
import { MockComponent } from 'ng2-mock-component';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../../../../user/services';
import { ElasticsearchService } from '../../../../geosource/services';
import { ErrorService, NotificationService } from '../../../services';
import { StorageService } from '../../../services/storage.service';
import { HttpClientModule } from '@angular/common/http';

export class UserServiceMock {

  constructor() { }

  get userIsSignedIn() {
    return false;
  }

}

describe('SideMenuComponent', () => {
  let component: SideMenuComponent;
  let fixture: ComponentFixture<SideMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        HttpClientModule,
      ],
      declarations: [
        SideMenuComponent,
        MockComponent({ selector: 'app-search-bar' }),
      ],
      providers: [
        {
          provide: UserService,
          useClass: UserServiceMock,
        },
        ElasticsearchService,
        ErrorService,
        NotificationService,
        StorageService,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
