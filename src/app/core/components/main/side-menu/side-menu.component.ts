import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { ElasticsearchService } from '../../../../elasticsearch/services/elasticsearch.service';
import { AppRoutes } from '../../../../routes';
import { UserService } from '../../../../user/services';
import { AppStateService } from '../../../services';
import { APP_CONFIG } from '../../../services/app-config.service';

declare var tarteaucitronForceLanguage: any;

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent implements OnInit, OnDestroy {

  @Input() isOpened: boolean;
  @Input() isPortalLightTheme: boolean;

  AppRoutes = AppRoutes;
  APP_CONFIG = APP_CONFIG;
  env = environment;
  isHomePage: boolean;
  displayAdminBar = false;

  burgerStatus: boolean;

  private subscriptions: Subscription[] = [];

  constructor(
    private _router: Router,
    private _userService: UserService,
    private _elasticsearchService: ElasticsearchService,
    private _appStateService: AppStateService,
  ) { }

  ngOnInit() {
    this.burgerStatus = false;
    this.isHomePage = true;

    this._router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isHomePage = (event.url === `/${AppRoutes.home.uri}`) ? true : false;
      }
      if (event instanceof NavigationStart) {
        this.burgerStatus = false;
        this._appStateService.changeMenuState(this.burgerStatus);
      }
    });

    this.getDrafts();

    this.subscriptions.push(
      this._userService.userStatusChanged$.subscribe((isLogged) => {
        this.displayAdminBar = false;
        this.getDrafts();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => {
      sub.unsubscribe();
    });
  }

  getDrafts() {
    const options = {
      status: 'draft',
    };
    this._elasticsearchService.getPosts(options).subscribe((drafts) => {
      if (drafts.hits && drafts.hits.hits && drafts.hits.hits.length > 0) {
        this.displayAdminBar = true;
      }
    });
  }

  changeLanguage(lang: string) {
    tarteaucitronForceLanguage = lang;
    window.location.href = environment.angularAppHost[lang] + this._router.url;
  }

  currentLanguage() {
    return window.location.href.includes(environment.angularAppHost.en) ?
      environment.angularAppHost.en :
      environment.angularAppHost.fr;
  }

  signOut() {
    this._userService.resetAuth();
    this._router.navigate(['/', AppRoutes.signin.uri]);
  }

  get userIsSignedIn() {
    return this._userService.userIsSignedIn;
  }

  get username() {
    return `${this._userService.user.firstName} ${this._userService.user.lastName}`;
  }

}
