import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NotificationsComponent } from './notifications.component';
import { NotificationService } from '../../services';
import { DebugElement } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { By } from '@angular/platform-browser';
import { Notification } from '../../models';

export class NotificationServiceMock {

  _notification: BehaviorSubject<Notification> = new BehaviorSubject(null);

  constructor() { }

  notify(notification: Notification) {
    this._notification.next(notification);
  }

  get notification$() {
    return this._notification;
  }

}

describe('NotificationsComponent', () => {

  describe('Template', () => {
    let component: NotificationsComponent;
    let fixture: ComponentFixture<NotificationsComponent>;
    let debugElement: DebugElement;

    beforeEach(waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [
          {
            provide: NotificationService,
            useClass: NotificationServiceMock,
          },
        ],
        declarations: [NotificationsComponent],
      }).compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(NotificationsComponent);
      component = fixture.componentInstance;
      debugElement = fixture.debugElement;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should display an error notification', () => {
      // Given
      const errorMessage = 'error message';
      component.notifications = [{
        message: errorMessage,
        type: 'error',
      }];
      fixture.detectChanges();
      const notification = debugElement.query(By.css('.appNotification'));
      const notificationContent = debugElement.query(By.css('.notification-content')).nativeElement;

      // Then
      expect(notificationContent.innerText).toBe(errorMessage);
      expect(notification.classes.error).toBe(true);
    });

    it('should display a warning notification', () => {
      // Given
      const errorMessage = 'warning message';
      component.notifications = [{
        message: errorMessage,
        type: 'warning',
      }];

      // When
      fixture.detectChanges();
      const notification = debugElement.query(By.css('.appNotification'));
      const notificationContent = debugElement.query(By.css('.notification-content')).nativeElement;

      // Then
      expect(notificationContent.innerText).toBe(errorMessage);
      expect(notification.classes.warning).toBe(true);
    });

    it('should display a success notification', () => {
      // Given
      const errorMessage = 'success message';
      component.notifications = [{
        message: errorMessage,
        type: 'success',
      }];
      fixture.detectChanges();
      const notification = debugElement.query(By.css('.appNotification'));
      const notificationContent = debugElement.query(By.css('.notification-content')).nativeElement;

      // Then
      expect(notificationContent.innerText).toBe(errorMessage);
      expect(notification.classes.success).toBe(true);
    });

    it('should display 3 notifications', () => {
      // Given
      const errorMessage = 'warning message';
      component.notifications = [
        {
          message: errorMessage,
          type: 'error',
        },
        {
          message: errorMessage,
          type: 'warning',
        },
        {
          message: errorMessage,
          type: 'success',
        },
      ];

      fixture.detectChanges();
      const notificationNumber = debugElement.queryAll(By.css('.appNotification')).length;

      // Then
      expect(notificationNumber).toBe(3);
    });
  });

  describe('Component.ts', () => {
    let component: NotificationsComponent;
    let notificationServiceMock: NotificationService;

    beforeEach(() => {
      const subject = new BehaviorSubject(null);
      notificationServiceMock = {
        get notification$(): BehaviorSubject<Notification> {
          return subject;
        },
        notify(notification: Notification) {
          subject.next(notification);
        },
      } as NotificationService;
      component = new NotificationsComponent(notificationServiceMock);
    });

    describe('ngOnInit()', () => {
      it('should initialize the component\'s variables', () => {
        // Given
        const getNotificationSpy = spyOnProperty(notificationServiceMock, 'notification$').and.callThrough();

        // When
        component.ngOnInit();

        // Then);
        expect(getNotificationSpy).toHaveBeenCalled();

        notificationServiceMock.notify({ message: 'error', type: 'error' });

        expect(component.notifications.length).toBe(1);
      });
    });

    describe('hide(index)', () => {
      it('should remove one element from the notification array', () => {
        //  Given
        const n1 = new Notification({
          message: 'Awesome notification',
          type: 'success',
        });
        const n2 = new Notification({
          message: 'Error notification',
          type: 'error',
        });
        component.notifications.push(n1, n2);

        // When
        component.hide(1);

        // Then
        expect(component.notifications.length).toBe(1);
        expect(component.notifications[0]).toBe(n1);
      });
    });
  });
});
