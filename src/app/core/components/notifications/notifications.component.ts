import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../services';
import { Notification } from '../../models';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {
  notifications: Notification[] = [];

  constructor(
    private _notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this._notificationService.notification$.subscribe(
      (notification) => {
        if (notification) {
          this.notifications.push(notification);
          // Remove notification from array after 5 seconds
          // IMPORTANT (if you change this value also change the value of the css animation)
          setTimeout(() => this.notifications.shift(), 8000);
        }
      },
    );
  }

  hide(index) {
    this.notifications.splice(index, 1);
  }

}
