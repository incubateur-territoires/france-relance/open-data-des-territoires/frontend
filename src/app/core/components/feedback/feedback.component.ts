import { Component, Input, OnInit } from '@angular/core';
import { EmailService, NotificationService, AppStateService } from '../../services';
import { environment } from '../../../../environments/environment';
import { Feedback, Notification } from '../../models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { notificationMessages, feedback } from '../../../../i18n/traductions';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { AppRoutes } from '../../../routes';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
})
export class FeedbackComponent implements OnInit {
  @Input() isPortalCommune: boolean = false;

  AppRoutes = AppRoutes;
  isExpended: boolean;
  isHomePage = false;
  feedbackForm: FormGroup;
  hasBeenClicked = false; // Used to prevent the animation to be played when switching from mobile to tablet or desktop
  feedback = feedback;

  constructor(
    private _appStateService: AppStateService,
    private _emailService: EmailService,
    private _notificationService: NotificationService,
    private _fb: FormBuilder,
    private _router: Router,
  ) {
    this.isExpended = this._appStateService.feedbackOpened;
    this.feedbackForm = this._fb.group({
      message: ['', [Validators.required]],
      feeling: [null],
      email: [null],
      consent: [null]
    });
  }

  ngOnInit() {
    this._appStateService.feedbackStateChanged$.subscribe((state) => {
      this.hasBeenClicked = true; // This change has been triggered by a click of the user so set the var to true
      this.isExpended = state;
      // Reset the var state to false after the end of the animation
      setTimeout(() => { this.hasBeenClicked = false; }, 500);
    })
    this._router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this._appStateService.changeFeedbackState(false);
      }
      if (event instanceof NavigationEnd) {
        this.isHomePage = (event.url === `/${AppRoutes.home.uri}` || event.url === '/') ? true : false;
      }
    });
    this.setDynamiqueConsentValidators();
  }

  // Consent is required to true only if the user entered its email
  setDynamiqueConsentValidators() {
    const consent = this.feedbackForm.get('consent');

    this.feedbackForm.get('email').valueChanges.subscribe(
      (emailValue) => {
        if (emailValue === '' || emailValue === null ) {
          consent.setValue(false);
        }

        if (emailValue !== null && emailValue !== '') {
          consent.setValidators([Validators.requiredTrue]);
        } else {
          consent.setValidators(null);
        }

        consent.updateValueAndValidity();
      },
    );
  }

  sendFeedback() {
    if (this.feedbackForm.valid) {
      const feedback = new Feedback();

      feedback.url = window.location.href;
      feedback.version = environment.version;
      feedback.message = this.feedbackForm.get('message').value;
      feedback.feeling = parseInt(this.feedbackForm.get('feeling').value, 10);
      feedback.email = this.feedbackForm.get('email').value;

      this._emailService.sendFeedback(feedback).subscribe(
        (res) => {
          this.feedbackForm.reset();
          this.toogle();
          this._notificationService.notify(new Notification({
            type: 'success',
            message: notificationMessages.feedback.success,
          }));
        },
        (err) => {
          this._notificationService.notify(new Notification({
            type: 'error',
            message: notificationMessages.feedback.error,
          }));
        },
      );
    }
  }

  toogle() {
    this._appStateService.changeFeedbackState(!this._appStateService.feedbackOpened);
  }

  closeFeedback() {
    this._appStateService.changeFeedbackState(false);
  }

  closeFeedbackFromClickOutside() {
    this._appStateService.changeFeedbackState(false);
  }

  get feedbackHasEmail() {
    if (this.feedbackForm.get('email').value !== null && this.feedbackForm.get('email').value !== '') {
      return true;
    }
    else {
      return false;
    }
  }

  get shouldAnimateClosing() {
    return this.hasBeenClicked && !this.isExpended;
  }

}
