import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AppRoutes } from '../../../routes';
import { APP_CONFIG } from '../../services/app-config.service';
import { isRentertron } from '../../../shared/variables';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  APP_CONFIG = APP_CONFIG;
  public defaultImage: string = './assets/img/pigma/home_image.jpg';

  AppRoutes = AppRoutes;
  public safeURL;

  constructor(private sanitizer: DomSanitizer) { 
    this.safeURL=this.sanitizer.bypassSecurityTrustStyle(`url(${this.defaultImage})`);
  }

  ngOnInit() {
    if (isRentertron) {
      window.location.href = 'page-404';
    } else if(this.APP_CONFIG.urls.homeImage && this.APP_CONFIG.urls.homeImage.length>0){
      this.safeURL=this.sanitizer.bypassSecurityTrustStyle(`url(${this.APP_CONFIG.urls.homeImage})`);
    }
  }

}
