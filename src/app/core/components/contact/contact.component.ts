import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmailService, NotificationService, NavigationHistoryService } from '../../services';
import { Email } from '../../models';
import { subjects as Subjects, notificationMessages, pageTitles, contactTrad } from '../../../../i18n/traductions';
import { Router, ActivatedRoute } from '@angular/router';
import { AppRoutes } from '../../../routes';
import { User } from '../../../user/models';
import { UserService } from '../../../user/services';
import { IPageHeaderInfo } from '../../../shared/models';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {

  pageHeaderInfo: IPageHeaderInfo = {
    title: pageTitles.contact,
  };
  AppRoutes = AppRoutes;
  contactTrad = contactTrad;
  form: FormGroup;
  subjectDropdownState = false;
  subjects: Subject[] = Subjects; // Keep this line in order to access the subjects in the html template
  selectedSubject: Subject = null;
  user: User = null;

  constructor(
    private _fb: FormBuilder,
    private _emailService: EmailService,
    private _notificationService: NotificationService,
    private _navigationHistoryService: NavigationHistoryService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _userService: UserService,
  ) {
    this.form = this._fb.group(
      {
        firstname: ['', [
          Validators.required,
          Validators.pattern('([A-ZÀ-ÖØ-Ý][a-zà-öø-ýÿA-ZÀ-ÖØ-Ý]*)([ \'-][a-zA-ZÀ-ÖØ-öø-ýÿ]*)*'),
        ]],
        lastname: ['', [Validators.required,
          Validators.pattern('([A-ZÀ-ÖØ-Ý][a-zà-öø-ýÿA-ZÀ-ÖØ-Ý]*)([ \'-][a-zA-ZÀ-ÖØ-öø-ýÿ]*)*'),
        ]],
        email: ['', [Validators.required, Validators.email]],
        emailConfirmation: ['', [Validators.required, Validators.email]],
        subject: ['', Validators.required],
        text: ['', Validators.required],
        consent: ['', Validators.requiredTrue],
      });
  }

  ngOnInit() {
    // if user authenticated then initialize the form with its name and email
    this.completeFormWithUserInfo();
    this._route.queryParams.subscribe((params) => {
      if (params.subject) {
        this.selectedSubject = this.subjects.find(sub => sub.key === 'other');
        this.subject.patchValue(params.subject);
      } else {
        this.selectedSubject = null;
      }
    });
  }

  completeFormWithUserInfo() {
    if (this._userService.userIsSignedIn) {
      this.user = this._userService.user;
      this.firstname.patchValue(this.user.firstName);
      this.lastname.patchValue(this.user.lastName);
      this.email.patchValue(this.user.email);
      this.emailConfirmation.patchValue(this.user.email);
    } else {
      this.user = null;
    }
  }

  send() {
    if (!this.formIsInvalid) {
      const email = new Email(this.form.value);
      this.form.disable();
      this._emailService.send(email).subscribe(
        (res) => {
          this.form.enable();
          this._notificationService.notify(
            {
              type: 'success',
              message: notificationMessages.contact.success,
            },
          );
          this.form.reset();
          this.completeFormWithUserInfo();
        },
        (err) => {
          this.form.enable();
          this._notificationService.notify(
            {
              type: 'error',
              message: notificationMessages.contact.error,
            },
          );
        },
      );
    }
  }

  get emailConfirmationIsCorrect(): boolean {
    const value = this.email.value === this.emailConfirmation.value ? true : false;
    return value;
  }

  get emailConfirmationError() {
    // Display the message if
    // Input values have been modified
    // Input values have been modified but the email values are not similar
    return this.email.touched && this.emailConfirmation.touched && !this.emailConfirmationIsCorrect;
  }

  // Return true if one of the fields at least doesn't respect its validators
  // or if the confirmation email is different from the email
  get formIsInvalid() {
    const value = this.form.invalid || this.emailConfirmationError;
    return value;
  }

  fieldIsInvalid(field: string) {
    return (this.form.controls[field].touched) && this.form.controls[field].invalid;
  }

  fieldIsValid(field: string) {
    return (this.form.controls[field].touched) && this.form.controls[field].valid;
  }

  toggleSubject() {
    if (!this.formDisabled) {
      this.subjectDropdownState = !this.subjectDropdownState;
    }
  }

  closeSubjectDropdown() {
    this.subjectDropdownState = false;
  }

  setSubject(subject: Subject) {
    this.toggleSubject();
    this.selectedSubject = subject;
    if (this.selectedSubject.key === 'other') {
      this.subject.patchValue('');
      this.subject.reset();
    } else {
      this.subject.patchValue(this.selectedSubject.value);
    }
  }

  cancel() {
    const previous = this._navigationHistoryService.getFromLast(1);
    if (previous !== null) {
      this._router.navigateByUrl(previous);
    } else {
      this._router.navigateByUrl(AppRoutes.home.uri);
    }
  }

  get subjectLabelFor(): string {
    return this.displaySubjectInput ? 'subjectInput' : 'subjectDropdown';
  }

  get formDisabled(): boolean {
    return this.form.disabled;
  }

  get displaySubjectInput(): boolean {
    return (this.selectedSubject && this.selectedSubject.key) === 'other' ? true : false;
  }

  get userIsSignedIn() {
    return this._userService.userIsSignedIn;
  }

  toUppercase(controlName: string) {
    const input = this.form.controls[controlName].value;
    const uppercased = input.substring(0, 1).toUpperCase() + input.substring(1);
    this.form.controls[controlName].patchValue(uppercased);
  }

  get firstname() { return this.form.get('firstname'); }
  get lastname() { return this.form.get('lastname'); }
  get email() { return this.form.get('email'); }
  get emailConfirmation() { return this.form.get('emailConfirmation'); }
  get subject() { return this.form.get('subject'); }
  get text() { return this.form.get('text'); }
  get consent() { return this.form.get('consent'); }

}

interface Subject {
  key: string;
  value: string;
}
