import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ContactComponent } from './contact.component';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { EmailService, NotificationService, NavigationHistoryService } from '../../services';
import { BehaviorSubject, of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../../../user/services';

export class NotificationServiceMock {

  constructor() { }

}

export class EmailServiceMock {

  _notification: BehaviorSubject<Notification> = new BehaviorSubject(null);

  constructor() { }

  notify(notification: Notification) {
    this._notification.next(notification);
  }

  get notification$() {
    return this._notification;
  }

}

export class UserServiceMock {

  constructor() { }

  get userIsSignedIn() {
    return false;
  }

}

describe('ContactComponent', () => {
  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;
  let firstname: AbstractControl;
  let lastname: AbstractControl;
  let email: AbstractControl;
  let emailConfirmation: AbstractControl;
  let subject: AbstractControl;
  let text: AbstractControl;

  describe('Template', () => {
    beforeEach(waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          ReactiveFormsModule,
          RouterTestingModule,
        ],
        providers: [
          {
            provide: EmailService,
            useClass: EmailServiceMock,
          },
          {
            provide: UserService,
            useClass: UserServiceMock,
          },
          {
            provide: NotificationService,
            useClass: NotificationServiceMock,
          },
          NavigationHistoryService,
        ],
        declarations: [ContactComponent],
      })
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(ContactComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      firstname = component.form.controls.firstname;
      lastname = component.form.controls.lastname;
      email = component.form.controls.email;
      emailConfirmation = component.form.controls.emailConfirmation;
      subject = component.form.controls.subject;
      text = component.form.controls.text;
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    describe('Form validation', () => {

      it('form invalid when empty', () => {
        expect(component.form.valid).toBeFalsy();
      });

      describe('Firstname control validation', () => {
        beforeEach(() => {
          firstname.reset();
        });

        it('firstname control is valid with lowercase and uppercase letter', () => {
          // Given
          const value = 'John Doe';
          // When
          firstname.setValue(value);
          // Then
          expect(firstname.valid).toBeTruthy();
        });

        it('firstname control is valid with letter and space', () => {
          // Given
          const value = 'John Doe';
          // When
          firstname.setValue(value);
          // Then
          expect(firstname.valid).toBeTruthy();
        });

        it('firstname control is valid with letter and dash', () => {
          // Given
          const value = 'Jean-jacques';
          // When
          firstname.setValue(value);
          // Then
          expect(firstname.valid).toBeTruthy();
        });

        it('firstname control is valid with letter and apostrophe', () => {
          // Given
          const value = 'De\'Andre';
          // When
          firstname.setValue(value);
          // Then
          expect(firstname.valid).toBeTruthy();
        });

        it('firstname control accept any of those characters', () => {
          // Given
          let specialChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
          specialChars += 'abcdefghijklmnopqrstuvwxyz';
          specialChars += 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ';
          specialChars += 'àáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ';
          // When
          firstname.setValue(specialChars);
          // Then
          expect(firstname.valid).toBeTruthy();
        });

        it('firstname control is invalid if first letter is lowercased, has error "pattern"', () => {
          // Given
          const value = 'david';
          // When
          firstname.setValue(value);
          const errors = firstname.errors || {};
          // Then
          expect(firstname.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('firstname control is invalid if contains numbers', () => {
          // Given
          const value = '0123456789';
          // When
          firstname.setValue(value);
          const errors = firstname.errors || {};
          // Then
          expect(firstname.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('firstname control is invalid if contains special characters', () => {
          // Given
          const value = '"*$^!qjsdk+°)';
          // When
          firstname.setValue(value);
          const errors = firstname.errors || {};
          // Then
          expect(firstname.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('firstname control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          firstname.setValue(value);
          const errors = firstname.errors || {};
          // Then
          expect(firstname.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });
      });

      describe('Lastname control validation', () => {
        beforeEach(() => {
          lastname.reset();
        });

        it('lastname control is valid with lowercase and uppercase letter', () => {
          // Given
          const value = 'John Doe';
          // When
          lastname.setValue(value);
          // Then
          expect(lastname.valid).toBeTruthy();
        });

        it('lastname control is valid with letter and space', () => {
          // Given
          const value = 'John Doe';
          // When
          lastname.setValue(value);
          // Then
          expect(lastname.valid).toBeTruthy();
        });

        it('lastname control is valid with letter and dash', () => {
          // Given
          const value = 'Jean-jacques';
          // When
          lastname.setValue(value);
          // Then
          expect(lastname.valid).toBeTruthy();
        });

        it('lastname control is valid with letter and apostrophe', () => {
          // Given
          const value = 'De\'Andre';
          // When
          lastname.setValue(value);
          // Then
          expect(lastname.valid).toBeTruthy();
        });

        it('lastname control accept any of those characters', () => {
          // Given
          let specialChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
          specialChars += 'abcdefghijklmnopqrstuvwxyz';
          specialChars += 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ';
          specialChars += 'àáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ';
          // When
          lastname.setValue(specialChars);
          // Then
          expect(lastname.valid).toBeTruthy();
        });

        it('lastname control is invalid if first letter is lowercased, has error "pattern"', () => {
          // Given
          const value = 'david';
          // When
          lastname.setValue(value);
          const errors = lastname.errors || {};
          // Then
          expect(lastname.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('lastname control is invalid if contains numbers', () => {
          // Given
          const value = '0123456789';
          // When
          lastname.setValue(value);
          const errors = lastname.errors || {};
          // Then
          expect(lastname.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('lastname control is invalid if contains special characters', () => {
          // Given
          const value = '"*$^!qjsdk+°)';
          // When
          lastname.setValue(value);
          const errors = lastname.errors || {};
          // Then
          expect(lastname.valid).toBeFalsy();
          expect(errors['pattern']).toBeTruthy();
        });

        it('lastname control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          lastname.setValue(value);
          const errors = lastname.errors || {};
          // Then
          expect(lastname.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });
      });

      describe('Email control validation', () => {
        beforeEach(() => {
          email.reset();
        });

        it('Email control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          email.setValue(value);
          const errors = email.errors || {};
          // Then
          expect(email.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });

        it('Email control is invalid if the value is not a valid email', () => {
          // Given
          const value = 'toto.grandlyon.com';
          // When
          email.setValue(value);
          const errors = email.errors || {};
          // Then
          expect(email.valid).toBeFalsy();
          expect(errors['email']).toBeTruthy();
        });

        it('Email control is valid if the value is a valid email', () => {
          // Given
          const value = 'toto@grandlyon.com';
          // When
          email.setValue(value);
          const errors = email.errors || {};
          // Then
          expect(email.valid).toBeTruthy();
          expect(errors).toEqual({});
        });
      });

      describe('emailConfirmation control validation', () => {
        beforeEach(() => {
          emailConfirmation.reset();
        });

        it('emailConfirmation control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          emailConfirmation.setValue(value);
          const errors = emailConfirmation.errors || {};
          // Then
          expect(emailConfirmation.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });

        it('emailConfirmation control is invalid if the value is not a valid emailConfirmation', () => {
          // Given
          const value = 'toto.grandlyon.com';
          // When
          emailConfirmation.setValue(value);
          const errors = emailConfirmation.errors || {};
          // Then
          expect(emailConfirmation.valid).toBeFalsy();
          expect(errors['email']).toBeTruthy();
        });

        it('emailConfirmation control is valid if the value is a valid emailConfirmation', () => {
          // Given
          const value = 'toto@grandlyon.com';
          // When
          emailConfirmation.setValue(value);
          const errors = emailConfirmation.errors || {};
          // Then
          expect(emailConfirmation.valid).toBeTruthy();
          expect(errors).toEqual({});
        });
      });

      describe('subject control validation', () => {
        beforeEach(() => {
          subject.reset();
        });

        it('subject control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          subject.setValue(value);
          const errors = subject.errors || {};
          // Then
          expect(subject.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });
      });

      describe('text control validation', () => {
        beforeEach(() => {
          text.reset();
        });

        it('text control is invalid if the value is empty', () => {
          // Given
          const value = '';
          // When
          text.setValue(value);
          const errors = text.errors || {};
          // Then
          expect(text.valid).toBeFalsy();
          expect(errors['required']).toBeTruthy();
        });
      });
    });
  });
});
