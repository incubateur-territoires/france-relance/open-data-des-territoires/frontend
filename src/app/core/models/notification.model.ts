type errorType = 'error' | 'warning' | 'success';

export interface INotification {
  message: string;
  type: errorType;
}

export class Notification {
  message: string;
  type: errorType;

  constructor(notification: INotification) {
    this.message = notification.message;
    this.type = notification.type;
  }
}
