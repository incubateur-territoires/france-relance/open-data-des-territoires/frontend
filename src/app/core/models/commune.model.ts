

export interface ICommune {
  id:string;
  name: string;
  elasticSearchName: string;
  description: string;
  site_url: string;
  color:string;
  text_color:string; // noir ou blanc
  logo_url:string;
  stats:any;
}

export class Commune {
  id:string;
  name: string;
  elasticSearchName: string;
  description: string;
  site_url: string;
  logo_url:string;
  color:string;
  text_color:string; // noir ou blanc
  stats:any;

  constructor(commune: ICommune) {
    Object.assign(this, commune);
  }
}
