export interface IContactForm {
  email: string;
  emailConfirmation: string;
  subject: string;
  text: string;
  firstname: string;
  lastname: string;
  consent: boolean;
}

export class Email {
  email: string;
  subject: string;
  firstname: string;
  lastname: string;
  text: string;

  constructor(contactForm: IContactForm) {
    this.subject = contactForm.subject;
    this.text = contactForm.text;
    this.email = contactForm.email;
    this.firstname = contactForm.firstname;
    this.lastname = contactForm.lastname;
  }
}

export class Feedback {
  url: string;
  version: string;
  message: string;
  feeling?: number;
  email?: string;

  constructor() { }
}
