export interface IOrganization {
  id: number;
  display_name: string;
  codename: string;
  description?: string;
  website_url?: string;
  elasticSearchName?: string;
  thumbnail?: IThumbnail;
}

interface ILink {
  id: number;
  name: string;
  url: string;
  urlLabel: string;
  organizationId: number;
}

interface IThumbnail {
  name: string;
  url: string;
}

class Link {
  id: number;
  name: string;
  url: string;
  urlLabel: string;
  organizationId: number;

  constructor(iLink: ILink) {
    this.id = iLink.id;
    this.name = iLink.name;
    this.url = iLink.url;
    // For the label url to display: remove http|https from url
    this.urlLabel = iLink.url.replace(/^(https?:|)\/\//, '');
    this.urlLabel = this.urlLabel.replace(/\/$/, '');
    this.organizationId = iLink.organizationId;
  }
}

export class Organization {
  id: number;
  codename: string;
  name: string;
  description: string;
  links: ILink[];
  logo: string;
  elasticSearchName?: string;
  datasetsCount?: number;
  servicesCount?: number;

  constructor(organization: IOrganization) {
    this.id = organization.id;
    this.codename = organization.codename;
    this.name = organization.display_name;
    this.description = organization.description;
    this.links = [];
    if(organization.website_url && organization.website_url!=null){
      this.links.push(new Link({id:1,organizationId:organization.id,urlLabel:organization.website_url,name:organization.website_url, url:organization.website_url}));
    }
    
    if(organization.thumbnail){
      this.logo = organization.thumbnail.url;
    }
    this.elasticSearchName = organization.display_name;
    this.datasetsCount = 0;
    this.servicesCount = 0;
  }
}
