import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { notificationMessages } from '../../../i18n/traductions';
import { ErrorService } from './error.service';

@Injectable()
export class FileService {

  constructor(
    private http: HttpClient,
    private _errorService: ErrorService,
  ) { }

  downloadFile(url: string): Observable<any> {
    return this.http.get(`${url}`, { responseType: 'arraybuffer' }).pipe(
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.general.failedDownloadFile });
        },
      ),
    );
  }
}
