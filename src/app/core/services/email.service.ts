import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Email, Feedback } from '../models';
import { APP_CONFIG } from './app-config.service';

@Injectable()
export class EmailService {

  constructor(
    private _httpClient: HttpClient,
  ) {}

  send(email: Email) {
    return this._httpClient.post(`${APP_CONFIG.backendUrls.email}/contact`, email);
  }

  sendFeedback(feedback: Feedback) {
    return this._httpClient.post(`${APP_CONFIG.backendUrls.email}/feedback`, feedback);
  }
}
