
import { Injectable } from '@angular/core';

@Injectable()
export class ToolsService {

  constructor(
  ) { }

  /**
   * Détermine si une url est externe
   * @param url 
   * @return boolean
   */
  isExternalURL(url:string) {
    return new URL(url).host !== (location.host);
  }

  /**
   * Ajoute de nouveaux attributs html aux urls externes
   * @param html 
   */
  decorateRichText(html) {
    const domParser = new DOMParser()
    const document = domParser.parseFromString(html, `text/html`)
    const serializer = new XMLSerializer()
  
    // Handles external links
    const links = document.querySelectorAll(`a`);
    links.forEach((link) => {
      if (link.href) {
        if (this.isExternalURL(link.href)) {
          link.target = `_blank`
          link.rel = `noopener noreferrer`
        }
      }
    })
  
    return serializer.serializeToString(document)
  }
}