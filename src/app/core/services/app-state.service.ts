import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class AppStateService {

  private _menuOpened: boolean;
  private _menuStateChanged: Subject<boolean>;
  private _feedbackOpened: boolean;
  private _feedbackStateChanged: Subject<boolean>;
  private _cookieModalOpened: boolean;
  private _cookieModalStateChanged: Subject<boolean>;

  constructor() {
    this._menuStateChanged = new Subject<any>();
    this._feedbackStateChanged = new Subject<any>();
    this._cookieModalStateChanged = new Subject<any>();
    this._feedbackOpened = false;
    this._cookieModalOpened = false;
  }

  changeMenuState(state: boolean) {
    this._menuOpened = state;
    this._menuStateChanged.next(state);
  }

  changeFeedbackState(state: boolean) {
    if (this.feedbackOpened !== state) {
      this._feedbackOpened = state;
      this._feedbackStateChanged.next(state);
    }
  }

  changeCookieModalState(state: boolean) {
    if (this.cookieModalOpened !== state) {
      this._cookieModalOpened = state;
      this._cookieModalStateChanged.next(state);
    }
  }

  get menuStateChanged$(): Observable<boolean> {
    return this._menuStateChanged.asObservable();
  }

  get feedbackStateChanged$(): Observable<boolean> {
    return this._feedbackStateChanged.asObservable();
  }

  get cookieModalStateChanged$(): Observable<boolean> {
    return this._cookieModalStateChanged.asObservable();
  }

  get menuOpened() {
    return this._menuOpened;
  }

  get feedbackOpened() {
    return this._feedbackOpened;
  }

  get cookieModalOpened() {
    return this._cookieModalOpened;
  }
}
