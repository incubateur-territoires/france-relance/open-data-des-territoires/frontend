import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { filter, find, map } from 'rxjs/operators';
import { Metadata } from '../../shared/models';
import { Commune, ICommune } from '../models/commune.model';
import { Organization } from '../models/organization.model';
import { APP_CONFIG } from './app-config.service';
import { OrganizationsService } from './organizations.service';


@Injectable()
export class CommunesService {

  communes:Commune[];
  
  constructor(
    private _httpClient: HttpClient,
    private _organisationsService:OrganizationsService
  ) { 
    this.getCommunes().subscribe();
  }

  getCommunes():Observable<Commune[]> {
    if (this.communes && this.communes.length > 0) {
      return of(this.communes);
    }
    
    const calls=[
      this._httpClient.get<ICommune[]>(`./assets/resources/communes_infos.json`),
      this._organisationsService.getOrganizations()
    ];
    
    return forkJoin(calls).pipe(
      map((response) => {
        console.log(response)
        this.communes = response[0].map((r:any) => new Commune(r));
        const orgas = response[1];
        orgas.forEach(orga=> {
        const commune = this.communes.find((commune)=>commune.id === orga.codename);
          if(commune){
              commune.description=orga.description;
              commune.elasticSearchName=orga.elasticSearchName;
              if(orga.links && orga.links.length>0){
                commune.site_url = orga.links[0].url;
              }
          }
        });
        return this.communes;
      })
    );
  }

  getCommune(id: string):Observable<Commune> {
    return this.getCommunes().pipe(map((communes) => communes.find((commune)=>commune.id === id)));
  }
  getCommuneByEsName(id: string):Observable<Commune> {
    return this.getCommunes().pipe(map((communes) => communes.find((commune)=>commune.elasticSearchName === id)));
  }
}
