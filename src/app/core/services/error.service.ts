
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class ErrorService {

  constructor(
  ) { }

  handleError(err, opt): Error | HttpErrorResponse {
    const error = new Error(opt.message);
    console.error(err)
    if ((err instanceof HttpErrorResponse)) {
      switch (err.status) {
        default:
          error.stack = err.message;
      }
    } else {
      error.stack = err.stack;
    }
    return error;
  }
}
