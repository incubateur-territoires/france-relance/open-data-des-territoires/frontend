import { AppConfigService } from './app-config.service';
import { AppStateService } from './app-state.service';
import { CommunesService } from './communes.service';
import { EmailService } from './email.service';
import { ErrorService } from './error.service';
import { FileService } from './file.service';
import { MatomoService } from './matomo.service';
import { NavigationHistoryService } from './navigation-history.service';
import { NotificationService } from './notification.service';
import { OrganizationsService } from './organizations.service';
import { StorageService } from './storage.service';
import { TarteAuCitronService } from './tarteaucitron.service';
import { ToolsService } from './tools.service';

// tslint:disable-next-line: max-line-length
export { OrganizationsService,CommunesService,ErrorService, TarteAuCitronService, NotificationService, MatomoService, NavigationHistoryService, EmailService, FileService, AppConfigService, AppStateService, };

// tslint:disable-next-line:variable-name
export const CoreServices = [
  ErrorService,
  NotificationService,
  CommunesService,
  OrganizationsService,
  MatomoService,
  NavigationHistoryService,
  StorageService,
  EmailService,
  FileService,
  AppConfigService,
  TarteAuCitronService,
  AppStateService,
  ToolsService,
];
