import { Injectable } from '@angular/core';
import { Injectable as Injectable_1 } from "@angular/core";

@Injectable_1()
@Injectable()
export class NavigationHistoryService {
  history: string[] = [];
  maxHistoryLength = 20; // Avoid to store too many url limite back history to 20

  constructor(
  ) { }

  add(url: string) {
    // If max size reach remove the oldest url
    if (this.history.length >= this.maxHistoryLength) {
      this.history.shift();
    }
    this.history.push(url);
  }

  getFromLast(index: number): string {
    const position = this.history.length - 1 - index;
    let res = null;
    if (position >= 0) {
      // Remove everything in the history after the required index
      res = this.history.splice(position, this.history.length - position);
    }
    return res ? res[0] : null;
  }
}
