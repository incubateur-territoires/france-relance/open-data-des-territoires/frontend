import { Injectable } from "@angular/core";
import { APP_CONFIG } from "./app-config.service";
import { environment } from "../../../environments/environment";

declare var tarteaucitron: any;
declare var tarteaucitronForceLanguage: any;
declare var tarteaucitronForceExpire: any;

@Injectable()
export class TarteAuCitronService {

  public load () {
    return new Promise<void>((resolve, reject) => {
      tarteaucitronForceLanguage = this.getCurrentLanguageKey();
      tarteaucitronForceExpire = '365';

      tarteaucitron.init({
        "privacyUrl":"",

        "hashtag" : "#tarteaucitron",
        "cookieName" : "tarteaucitron",

        "orientation" : "bottom",

        "showAlertSmall" : false,
        "cookieslist" : true,
        "iconPosition": "BottomLeft",
        "showIcon": false,

        "AcceptAllCta" : true,
        "DenyAllCta": true,
        "highPrivacy" : true,

        "adblocker" : false,

        "handleBrowserDNTRequest" : false,

        "removeCredit" : true,
        "moreInfoLink": false,

        "useExternalCss" : false,
        "readmoreLink": "",

      });
      if(APP_CONFIG.statistics){
        tarteaucitron.user.matomoId = APP_CONFIG.statistics.matomoId;
        tarteaucitron.user.matomoHost = APP_CONFIG.statistics.matomoHost;
  
        (tarteaucitron.job = tarteaucitron.job || []).push('matomo');
        (tarteaucitron.job = tarteaucitron.job || []).push('youtube');
  
        tarteaucitron.load();
      }

      resolve();
    });
  }

  public initEvents() {
    if (tarteaucitron) {
      tarteaucitron.initEvents.loadEvent();
    }
  }

  public triggerTarteaucitronYoutube(): void {
    if (undefined !== tarteaucitron.services.youtube) {
      if (
        typeof tarteaucitron.state.youtube !== 'undefined'
        && tarteaucitron.state.youtube
        && typeof tarteaucitron.services['youtube'] !== 'undefined'
      ) {
        tarteaucitron.services['youtube'].js();
      } else {
        tarteaucitron.services['youtube'].fallback();
        tarteaucitron.triggerJobsAfterAjaxCall();
      }
    }
  }

  protected getCurrentLanguageKey() {
    return window.location.href.includes(environment.angularAppHost.en) ?
      'en' :
      'fr';
  }
}
