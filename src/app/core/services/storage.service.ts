import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }

  set(key: string, data: any, lifespan?: number): void {
    // If time was not defined, set time to 0. 0 is considered infinite so
    // these items will persist

    const time = lifespan || 0;
    const timestamp = new Date().getTime() + time;

    // Set up the storage item by creating a simple object that will be
    // stringified
    const storageObj = {
      timestamp,
      value: data,
    };

    return localStorage.setItem(key, JSON.stringify(storageObj));
  }

  // Return the value if not expired
  get(key: string) {
    const item = JSON.parse(localStorage.getItem(key));
    let value = null;

    if (item) {
      if (new Date().getTime() <= item.timestamp || item.timestamp === 0) {
        value = item.value;
      } else {
        this.remove(key);
      }
    }
    return value;
  }

  clear(): void {
    localStorage.clear();
  }

  remove(key: string): void {
    localStorage.removeItem(key);
  }

}
