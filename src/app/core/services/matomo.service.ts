import { Injectable } from '@angular/core';
import { IMatomoResponse } from '../models/matomo.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ErrorService } from './error.service';
import { notificationMessages } from '../../../i18n/traductions';
import { APP_CONFIG } from './app-config.service';

@Injectable()
export class MatomoService {

  constructor(
    private http: HttpClient,
    private _errorService: ErrorService,
  ) { }

  getPageMetadataPageMetrics(metadataId: string): Observable<number> {
    const dateNowString = new Date().toISOString().slice(0, 10);
    const dateOneMonthAgo = new Date();
    dateOneMonthAgo.setMonth(new Date().getMonth() - 1);
    const dateOneMonthAgoString = dateOneMonthAgo.toISOString().slice(0, 10);

    const url = `${APP_CONFIG.backendUrls.matomo}?period=range&date=${dateOneMonthAgoString}` +
      `,${dateNowString}` +
      `&segment=pageUrl=@${metadataId}/info`;
    return this.http.get<IMatomoResponse[]>(url).pipe(
      map((results) => {
        return results.length > 0 ? results.reduce((a, b) => a + b.nb_visits, 0) : 0;
      }),
      catchError(
        (err) => {
          throw this._errorService.handleError(err, { message: notificationMessages.matomo.getViews });
        },
      ));
  }
}
