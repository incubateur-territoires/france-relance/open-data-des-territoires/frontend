import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppRoutes } from '../../routes';

export class AppConfig {
  backendUrls: {
    organizations: string;
    
    resources: string;
    credits: string;
    changelog: string;
    auth: string;
    oidc: string;
    middlewareLegacyAuth: string;
    email: string;
    matomo: string;
    elasticsearch: string;
    catalogue: string;
    reuses: string;
    geocoder: string;
    proxyQuery: string;
    proxyWmts: string;
    seo: string;
    datasetUsageStatistics: string;
    backendDomain: string;
  };
  statistics: {
    matomoHost: string;
    matomoId: string;
  };
  externalUrls: {
    credits: string;
    maps:string ;
    maps_default_map:string ;
    legalNotices: string;
    partenaires: string;
  };
  facebook:string;
  rss:string;
  websiteLink:string;
  filterConfig:any;
  urls:any;
  labels:any;
  contact:string;
  twitter:string;
  layer3d: {
    url: string;
  };
  theFunctionalitiesInterruptor: {
    legalNotices: boolean;
    news:boolean;
    feedback:boolean;
    data_access:boolean;
    contribution: boolean;
    documentation: boolean;
    approach: boolean;
    accessibility:boolean;
    cgu:boolean;
    changelog: boolean;
    credits: boolean;
    reuses: boolean;
    partners: boolean;
    nohomepage:boolean; // true pour désactiver la page d'acceuil et rediriger vers la recherche tout acces à la page d'acceuil
    openInMaps:boolean; // true pour activer la fonction permettant d'ouvrir un JDD dans OneGeoMaps
    mapsRestrictedToAdmins:boolean; // true pour n'activer les liens vers maps uniquement aux superusers
    personalData:boolean;
    inseeFilter:boolean;// true pour activer le téléchargement des données à la commune
    processLicences:boolean; // active la gestion des licences GL : 
    // désactivé à priori même sur la future PF GL => supprimer tout ce code mort
    // si ce comportement n'est plus souhaité => TODO après mise en prod GL
  }={legalNotices: false,
    news:false,
    feedback:false,
    data_access:false,
    contribution: false,
    documentation: false,
    approach: false,
    accessibility:false,
    cgu:false,
    changelog: false,
    credits: false,
    reuses: false,
    partners: false,
    nohomepage:false,
    openInMaps:false,
    mapsRestrictedToAdmins:false,
    personalData:false,
    inseeFilter:false,
    processLicences:false
  };
  licenses: License[];
}

class License {
  matchingKeys: string[];
  nameFr: string;
  nameEn: string;
  acronymFr: string;
  acronymEn: string;
  url: string;
}

export let APP_CONFIG: AppConfig = new AppConfig();

@Injectable()
export class AppConfigService {

  constructor( private _httpClient: HttpClient,) { }

  public load() {
    return new Promise<void>((resolve, reject) => {
      const conf = new AppConfig();
      APP_CONFIG = Object.assign(conf, window['portailDataEnvConfig']);
      AppRoutes.root.title.fr=APP_CONFIG.labels.plateformeName;
      resolve();
    });
  }

  public loadHomeJddList() {
    return this._httpClient.get(`./assets/resources/home_jdd.json`);
  }

  public getAppConfig(){
    return APP_CONFIG;
  }
}
