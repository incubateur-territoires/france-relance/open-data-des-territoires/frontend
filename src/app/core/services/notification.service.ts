
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Notification } from '../models';

@Injectable()
export class NotificationService {

  private _notification: BehaviorSubject<Notification> = new BehaviorSubject(null);

  constructor() {}

  notify(notification: Notification) {
    this._notification.next(notification);
  }

  get notification$() {
    return this._notification;
  }

}
