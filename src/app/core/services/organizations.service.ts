import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Organization } from '../models/organization.model';
import { APP_CONFIG } from './app-config.service';

@Injectable()
export class OrganizationsService {

  constructor(
    private _httpClient: HttpClient,
  ) { }

  getOrganizations(partner=false) {
    let url=`${APP_CONFIG.backendUrls.organizations}`
    if(partner){
      url+='?partner=true'
    }
    return this._httpClient.get(url
      ,
      { withCredentials: true },
    ).pipe(
      map((organizationsBack:any) => {
        const organizations = [];
        organizationsBack.results.forEach((organization) => {
          organizations.push(new Organization(organization));
        });
        return organizations;
      }));
  }
}
