import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { DatasetsModule } from '../datasets/datasets.module';
import { SharedModule } from '../shared/shared.module';
import { CoreComponents, MainComponent } from './components';
import { CloseMenuDirective } from './components/main/close-menu-directive';
import { CoreRoutingModule } from './core-routing.module';
import { ErrorsHandler } from './handlers/errors-handler';
import { HttpErrorResponseInterceptor } from './interceptors/http-error-response-interceptor';
import { CoreServices } from './services';


@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    DatasetsModule,
  ],
  declarations: [...CoreComponents, CloseMenuDirective],
  providers: [
    ...CoreServices,
    CookieService,
    {
      provide: ErrorHandler,
      useClass: ErrorsHandler,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorResponseInterceptor,
      multi: true,
    },
  ],
  exports: [
    MainComponent],
})
export class CoreModule { }
