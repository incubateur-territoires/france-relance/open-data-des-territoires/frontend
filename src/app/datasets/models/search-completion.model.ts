export class SearchCompletion {
  'text': string;
  'id': string;
  '_index': string;
  '_score': number;

  constructor(data) {
    this.text = data.text;
    this.text = this.text.replace('-',' ');
    this.id = data.id;
    this._index = data._index;
    this._score = data._score;
  }
}
