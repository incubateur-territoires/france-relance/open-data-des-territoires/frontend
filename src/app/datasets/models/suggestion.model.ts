export class SearchSuggestion {
  'text': string;
  'highlighted': string;
  'score': number;

  constructor(data?) {
    if (data) {
      this.text = data.text;
      this.highlighted = data.highlighted;
      this.score = data.score;
    }
  }
}
