import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '../routes';
import { ResultsComponent } from './components';

export const routes: Routes = [
  {
    path: AppRoutes.research.uri,
    component: ResultsComponent,
    data: {
      title: AppRoutes.research.title,
    },
  },
  {
    path: 'datasets/:id',
    redirectTo: `${AppRoutes.datasets.uri}/:id`,
  },
  {
    path: `${AppRoutes.datasets.uri}/:id`,
    loadChildren: () => import('../dataset-detail/dataset-detail.module').then(m => m.DatasetDetailModule),
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)],
})
export class DatasetsRoutingModule { }
