
import { DatasetResearchService } from '../../services';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SearchBarComponent } from './search-bar.component';
import { MockComponent } from 'ng2-mock-component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ErrorService, NotificationService } from '../../../core/services';
import { StorageService } from '../../../core/services/storage.service';
import { ElasticsearchService } from '../../../elasticsearch/services/elasticsearch.service';

describe('SearchBarComponent', () => {
  let component: SearchBarComponent;
  let fixture: ComponentFixture<SearchBarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule,
      ],
      declarations: [
        SearchBarComponent,
        MockComponent({ selector: 'app-search-input' }),
      ],
      providers: [
        DatasetResearchService,
        ElasticsearchService,
        ErrorService,
        StorageService,
        NotificationService,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
