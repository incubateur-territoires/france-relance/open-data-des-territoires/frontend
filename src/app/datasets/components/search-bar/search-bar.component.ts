import { ViewportScroller } from '@angular/common';
import { Component, ChangeDetectorRef, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { geosource } from '../../../../i18n/traductions';
import { Commune } from '../../../core/models/commune.model';
import { IScope } from '../../../elasticsearch/models';
import { SearchCompletion } from '../../models';
import { DatasetResearchService } from '../../services';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {

  // To be deleted
  // @Input() searchOnChangeScope: boolean = true;
  @Input() set commune(commune: Commune) {
    console.log(commune)
    if(commune){
      this.communeLabel = commune.name;
      this.portail=commune;
      setTimeout(() => { // call the function after the template is rendered/updated
        this.setCommuneLabelWidth();
      }, 0);
    } else {
      this.communeLabel=undefined;
      this.portail=undefined;
    }
    
  };
  @ViewChild('inputSearch', { static: true }) inputSearch: ElementRef;

  searchValue: string;
  geosource = geosource;
  communeLabelWidth: number = 30;
  communeLabel: string;
  portail:Commune=undefined;
  // Autocomplete
  optionsAutocomplete: SearchCompletion[] = [];
  currentAutocompleteFocus = -1;
  displayAutocomplete = false;
  autocompleteIsValid = true;
  
  isLabelWidthSet = false;

  keyUpSubject: Subject<any> = new Subject<any>();

  constructor(
    private _datasetResearchService: DatasetResearchService,
    private _viewportScroller: ViewportScroller,
    private cdref: ChangeDetectorRef,
  ) { }

  ngOnInit() {

    /**
     * We create this subject to have specific behaviour for the keyup event.
     * When keyup event, this subject will emit (see below in the switch of the searchChanged() function)
     * an event and will be catched here.
     * Then we do API call only if users didn't type the last 800ms to avoid
     * multiple api calls.
     */
    this.keyUpSubject.pipe(debounceTime(500)).subscribe(() => {
      this.requestAutocomplete();
    });

    this.searchValue = this._datasetResearchService.searchString;
    // When the search value has changed, we reset the autocomplete list and focus element
    this._datasetResearchService.searchChange$
      .subscribe(
        (value) => {
          this.searchValue = this._datasetResearchService.searchString;
          this.displayAutocomplete = false;
          this.currentAutocompleteFocus = -1;
        },
      );

    }

    ngAfterViewChecked() { // element isn't visible just after communeName is set, thus using AfterContent hook
      if (!this.isLabelWidthSet && this.communeLabel) {
        this.setCommuneLabelWidth();
      }
    }

  // This function is triggered when:
  // - the click press key Enter or select option with the mouse:
  // we make a search query & clean the completion list
  // - any keyup in the search bar :
  // we make the autocomplete query
  // - arrow up and down buttons:
  // we change the focus item in the completion list
  keyUpChanged(key: string) {
    switch (key) {
      // These 2 keys are to go up and down in the autocomplete list items
      case 'ArrowDown':
        if (this.currentAutocompleteFocus < 4) {
          this.currentAutocompleteFocus = this.currentAutocompleteFocus + 1;
        }
        break;
      case 'ArrowUp':
        if (this.currentAutocompleteFocus > 0) {
          this.currentAutocompleteFocus = this.currentAutocompleteFocus - 1;
        }
        break;
      case 'Enter':
        // When Enter is pressed and that we are inside the autocomplete list items
        if (this.currentAutocompleteFocus > -1) {
          this.selectOption(this.optionsAutocomplete[this.currentAutocompleteFocus].text);
        }
        this.displayAutocomplete = false;
        this._datasetResearchService.searchChanged(this.searchValue);
        this.inputSearch.nativeElement.blur();
        this._viewportScroller.scrollToPosition([0, 0]);
        break;
      default:
        this.keyUpSubject.next();
        break;
    }
  }

  // When one autocomplete option is selected
  selectOption(optionText: string) {
    this._datasetResearchService.fromAutocompletion = true;
    const plainText = optionText.replace(/<[^>]*>/g, '');
    this.searchValue = plainText;
    this.currentAutocompleteFocus = -1;
    this.searchChanged();
  }

  searchChanged() {
    this._datasetResearchService.fromAutocompletion = true;
    let filter = undefined;
    if(this.portail){
      filter={"portail":this.portail.elasticSearchName}
    }
    this._datasetResearchService.searchChanged(this.searchValue,filter);
    this.optionsAutocomplete = [];
    this._viewportScroller.scrollToPosition([0, 0]);
  }

  /*
  * Display the autocomplete list: if the list is empty, we request it to ES.
  */
  displayAutocompleteList() {
    this.displayAutocomplete = true;
    if (this.optionsAutocomplete.length < 1) {
      this.requestAutocomplete();
    }
  }

  requestAutocomplete() {
    // tslint:disable-next-line:max-line-length
    let filter={};
    if(this.portail){
      filter= {
          "bool": {
            "should": [
              {
                "nested": {
                  "ignore_unmapped": true,
                  "path": "metadata-fr.responsibleParty",
                  "query": {
                    "term": {
                      "metadata-fr.responsibleParty.organisationName.keyword": this.portail.elasticSearchName
                    }
                  }
                }
              }
            ]
          }
        };
      
      // filter={"metadata-fr.responsibleParty.organisationName":this.portail.elasticSearchName}
    }
    this._datasetResearchService.getAutoComplete(this.searchValue,filter).subscribe(
      (res) => {
        this.optionsAutocomplete = res;
      },
      (err) => {
        this.autocompleteIsValid = false;
      },
    );
  }

  resetResearch() {
    this.searchValue = '';
    this.searchChanged();
  }

  setCommuneLabelWidth = () => {
    const labelContainer = document.querySelector('.portal-commune-label')
    if (this.communeLabel && labelContainer) {
      this.communeLabelWidth = labelContainer.clientWidth + 45;
      this.isLabelWidthSet = true;
      this.cdref.detectChanges();
    }
  }

  get isLoading() {
    return this._datasetResearchService.isLoading;
  }

  get selectedScope(): IScope {
    return this._datasetResearchService.scopeReasearch;
  }

}
