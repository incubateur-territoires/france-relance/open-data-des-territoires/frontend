import { FilterDetailComponent } from './filter-list/filter-detail/filter-detail.component';
import { FilterListComponent } from './filter-list/filter-list.component';
import { InfoCommuneComponent } from './results/info-commune/info-commune.component';
import { ResultDatasetComponent } from './results/result-dataset/result-dataset.component';
import { ResultPostComponent } from './results/result-post/result-post.component';
import { ResultsTabScopeComponent } from './results/results-tab-scope/results-tab-scope.component';
import { ResultsComponent } from './results/results.component';
// tslint:disable-next-line:max-line-length
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SortComponent } from './sort/sort.component';

export { InfoCommuneComponent, ResultsComponent, SearchBarComponent, SortComponent, FilterListComponent, FilterDetailComponent, ResultDatasetComponent, ResultsTabScopeComponent, ResultPostComponent };

// tslint:disable-next-line:variable-name
export const DatasetsComponents = [
  ResultsComponent,
  InfoCommuneComponent,
  SearchBarComponent,
  SortComponent,
  FilterListComponent,
  FilterDetailComponent,
  ResultDatasetComponent,
  ResultsTabScopeComponent,
  ResultPostComponent,
];
