import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoCommuneComponent } from './info-commune.component';

describe('InfoCommuneComponent', () => {
  let component: InfoCommuneComponent;
  let fixture: ComponentFixture<InfoCommuneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoCommuneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoCommuneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
