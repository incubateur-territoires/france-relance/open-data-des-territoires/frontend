import { Component, Input, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import * as ChartAnnotation from 'chartjs-plugin-annotation';

import { Commune } from '../../../../core/models/commune.model';

@Component({
  selector: 'app-info-commune',
  templateUrl: './info-commune.component.html',
  styleUrls: ['./info-commune.component.scss']
})
export class InfoCommuneComponent implements OnInit {

  @Input() commune: Commune;
  chart;

  currentPop="";
  currentPopYear="";
  constructor() { }

  ngOnInit(): void {
    console.log(this.commune);
    const pop = this.commune.stats.population;
    this.currentPop =pop.map(x=>x[1])[pop.length-1];
    this.currentPopYear =pop.map(x=>x[0])[pop.length-1];
    let chartColor: string = this.commune.text_color; // get from specified portal
    let namedChartAnnotation = ChartAnnotation; // even if not setup in options, it is still needed to avoid an error
    namedChartAnnotation["id"] = "annotation";
    Chart.pluginService.register(namedChartAnnotation);
    Chart.defaults.global.defaultFontFamily = 'Titillium Web';
    Chart.defaults.global.defaultFontSize = 14;
    const options = {
      responsive: true,
      maintainAspectRatio: true,

      legend: {
        display: false
      },

      scales: {
        yAxes: [{
          ticks: {
            fontColor: chartColor,
            beginAtZero: true,
            padding: 5,
            precision: 0,
            suggestedMax: 2,
            stepSize: 5000,
            callback: (value: number, index: number, values: number[]):string => {
              if (value > 1000000) {
                value = value / 1000000;
                return value.toLocaleString() + ' M'
              }
              if (value >= 1000) {
                value = value / 1000;
                return value.toLocaleString() + ' k';
              }
              return value.toLocaleString();
            }
          },
          gridLines: {
            color: chartColor,
          },
        }],
        xAxes: [{
          ticks: {
            fontColor: chartColor,
            beginAtZero: true,
            precision: 0,
            suggestedMax: 2,
          },
          gridLines: {
            display: false,
          },
        }],
      },
    }
    new Chart('popEvolChart', {
      type: 'line',
      data: {
        labels: this.commune.stats.population.map(x=>x[0]),
        datasets: [{
          label: 'Évolution de la population',
          data: this.commune.stats.population.map(x=>x[1]),
          color: chartColor,
          backgroundColor: chartColor,
          borderColor: chartColor,
          borderWidth: 1,
          fill: false,
        }]
      },
      options
    });
    new Chart('popAgeChart', {
      type: 'bar',
      data: {
        labels: this.commune.stats.population_par_age.map(x=>x[0]),
        datasets: [{
          label: 'Population par tranche d\'âge',
          data: this.commune.stats.population_par_age.map(x=>x[1]),
          color: chartColor,
          backgroundColor: chartColor,
          borderColor: chartColor,
          borderWidth: 1
        }]
      },
      options
    });
    new Chart('popBirthChart', {
      type: 'line',
      data: {
        labels: this.commune.stats.naissances.map(x=>x[0]),
        datasets: [{
          label: 'Naissances',
          data: this.commune.stats.naissances.map(x=>x[1]),
          color: chartColor,
          backgroundColor: chartColor,
          borderColor: chartColor,
          borderWidth: 1,
          fill: false,
        }]
      },
      options
    });
  }
}
