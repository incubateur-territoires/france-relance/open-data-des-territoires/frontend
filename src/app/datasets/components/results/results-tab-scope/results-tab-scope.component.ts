import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IScope } from '../../../../elasticsearch/models';
import { scopesResearch } from '../../../../shared/variables';

@Component({
  selector: 'app-results-tab-scope',
  templateUrl: './results-tab-scope.component.html',
  styleUrls: ['./results-tab-scope.component.scss'],
})
export class ResultsTabScopeComponent implements OnInit {

  @Input() countResults: [];
  @Input() selectedScope: IScope;
  @Output() scopeChanged = new EventEmitter<IScope>();

  scopesResearch = scopesResearch;

  constructor() { }

  ngOnInit() {
  }

  changeScope(scope: IScope) {
    this.scopeChanged.emit(scope);
  }

}
