import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResultsTabScopeComponent } from './results-tab-scope.component';

describe('ResultsTabScopeComponent', () => {
  let component: ResultsTabScopeComponent;
  let fixture: ComponentFixture<ResultsTabScopeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ResultsTabScopeComponent],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsTabScopeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
