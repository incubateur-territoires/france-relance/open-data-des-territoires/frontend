import { Component, Input, OnInit } from '@angular/core';
import { geosource } from '../../../../../i18n/traductions';
import { AppRoutes } from '../../../../routes';
import { Dataset, typesMetadata } from '../../../../shared/models';

@Component({
  selector: 'app-result-dataset',
  templateUrl: './result-dataset.component.html',
  styleUrls: ['./result-dataset.component.scss'],
})
export class ResultDatasetComponent implements OnInit {

  @Input() dataset: Dataset;
  AppRoutes = AppRoutes;
  typesMetadata = typesMetadata;
  imageUrl: string;

  surTitle = {
    dataset: '',
    series: geosource.datasetTypes.series,
    service: geosource.datasetTypes.serviceSingular,
  };

  constructor() { }

  ngOnInit() {
    if (this.dataset.metadata.image) {
      this.imageUrl = this.dataset.metadata.image.url;
    } else {
      if (this.dataset.metadata.type === typesMetadata.series) {
        this.imageUrl = './assets/img/vignette-collection.png';
      } else {
        this.imageUrl = this.dataset.metadata.type === typesMetadata.dataset ?
          './assets/img/vignette-data-geo.png' : './assets/img/vignette-data.png';
      }
    }

  }

  backupImage() {
    if (this.dataset.metadata.type === typesMetadata.series) {
      this.imageUrl = './assets/img/vignette-collection.png';
    } else {
      this.imageUrl = this.dataset.metadata.type === typesMetadata.dataset ?
        './assets/img/vignette-data-geo.png' : './assets/img/vignette-data.png';
    }
  }

  get datasetDetailsURI() {
    return this.dataset.hasMap || this.dataset.hasTable ? AppRoutes.info.uri : AppRoutes.info.uri;
  }

}
