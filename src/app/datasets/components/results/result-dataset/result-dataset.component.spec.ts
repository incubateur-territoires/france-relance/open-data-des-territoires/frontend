import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ResultDatasetComponent } from '../..';
import { RouterTestingModule } from '@angular/router/testing';

describe('ResultDatasetComponent', () => {
  let component: ResultDatasetComponent;
  let fixture: ComponentFixture<ResultDatasetComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ResultDatasetComponent],
      imports: [
        RouterTestingModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultDatasetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
