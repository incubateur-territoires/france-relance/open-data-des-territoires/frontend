import { Component, OnInit, Input } from '@angular/core';
import { CMSContent } from '../../../../editorialisation/models';
import { AppRoutes } from '../../../../routes';
import { notificationMessages } from '../../../../../i18n/traductions';
import { APP_CONFIG } from '../../../../core/services/app-config.service';

@Component({
  selector: 'app-result-post',
  templateUrl: './result-post.component.html',
  styleUrls: ['./result-post.component.scss'],
})
export class ResultPostComponent implements OnInit {

  AppRoutes = AppRoutes;
  APP_CONFIG = APP_CONFIG;
  notificationMessages = notificationMessages;

  @Input() post: CMSContent;

  constructor() { }

  ngOnInit() {
    
  }

}
