import { DatePipe, ViewportScroller } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { metaDescription, notificationMessages } from '../../../../i18n/traductions';
import { Notification } from '../../../core/models';
import { ICommune } from '../../../core/models/commune.model';
import { NotificationService,CommunesService } from '../../../core/services';
import { APP_CONFIG } from '../../../core/services/app-config.service';

import { CMSContent } from '../../../editorialisation/models';
import { Aggregation, IScope } from '../../../elasticsearch/models';
import { AppRoutes } from '../../../routes';
import { Dataset, IMetadataLink, PaginatorOptions } from '../../../shared/models';
import { SearchSuggestion } from '../../models';
import { DatasetResearchService, ResearchUrlService } from '../../services';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
})
export class ResultsComponent implements OnInit, OnDestroy {

  AppRoutes = AppRoutes;

  results: Dataset[] | CMSContent[];

  countResults: [];
  hasParams: boolean = false;
  searchChangeSub: Subscription;
  // Paginator options
  paginator: PaginatorOptions;
  displayFilters = false;
  displaySuggestion = false;
  suggestion = new SearchSuggestion({});

  selectedScope: IScope;

  isPortailCommune = false;

  commune:ICommune;

  constructor(
    private _datasetResearchService: DatasetResearchService,
    private _notificationService: NotificationService,
    private _communesService:CommunesService,
    private _datePipe: DatePipe,
    private _researchUrlService: ResearchUrlService,
    private _viewportScroller: ViewportScroller,
    private _meta: Meta,
    private _route: ActivatedRoute,
  ) { }

  ngOnInit() {

    // Set the title and description for the research page
    this._meta.updateTag({ name: 'description', content: metaDescription.research.replace('{plateformeName}',APP_CONFIG.labels.plateformeName) });

    // If url with parameters too long, we reset it and display a notif

    if (location.href.length > 2000) {
      this._researchUrlService.reset();
      this._notificationService.notify(new Notification({
        message: notificationMessages.general.urlToolLong,
        type: 'warning',
      }));
    } else {
      this._route.queryParams.subscribe((params) => {
        console.log(params);
        this.manageUrlParameters(params);
      });
    }

    if (!this.hasParams) {
      this.search();
      this.getSuggestion();
    }

    this.countResults = [];
    this.paginator = {
      pageIndex: this._datasetResearchService.pageIndex + 1,
      length: 0,
      pageSize: this._datasetResearchService.pageSize,
      pageSizeOptions: [5, 10, 20],
    };

    this.searchChangeSub = this._datasetResearchService.searchChange$.subscribe(
      (value) => {
        this.search();
        this.getSuggestion();
      },
    );
  }

  // If there are parameters, we extract them to set the ESoptions for a news search.
  private manageUrlParameters(params) {
    this._datasetResearchService.resetActiveAggregations();
    this.commune=undefined;
    this.isPortailCommune = false;
    if (params && Object.keys(params).length > 0) {
      this.hasParams = true;
      if(params.portail){
        this.isPortailCommune = true;
        this._communesService.getCommuneByEsName(params.portail).subscribe((commune)=>{
          this.commune = commune;
        })
        //remove partenaire filter
        this._datasetResearchService.elasticSearchOptions._filters=this._datasetResearchService.elasticSearchOptions._allfilters.filter((filter)=>filter.label!='Partenaires');
      }
      else{
        //remove portail filter
        this._datasetResearchService.elasticSearchOptions._filters=this._datasetResearchService.elasticSearchOptions._allfilters.filter((filter)=>filter.label!='Portail');
        //this._datasetResearchService.elasticSearchOptions._filters=this._datasetResearchService.elasticSearchOptions._allfilters;
      }
      // We need to make an initial request to get the aggregations and subaggregations
      this._datasetResearchService.getResults().subscribe(() => {
        // Create the ESOptions
        const options = this._researchUrlService.getOptionsFromParameters(params);
        this._datasetResearchService.elasticSearchOptions.setElasticsearchOptions(options);
        // Force to recalculate the aggregate counts
        this._datasetResearchService.shouldAggregateResultCount = true;
        // Set the aggregations
        const aggParameters = this._researchUrlService.aggParameters;

        Object.keys(aggParameters).forEach((key) => {
          let aggField = `metadata-fr.${key}`;
          if(key.indexOf('extras')>=0){
            aggField = key;
          }
          const oneAgg = aggParameters[key];

          oneAgg.forEach((troncatedKey: string) => {
            let newAggField = aggField;
            let aggKey = '';

            // Find out if it's an aggregation or subaggregation
            const filters = this._datasetResearchService.elasticSearchOptions.filters;
            let parentAgg = '';
            console.log(key)
            filters.forEach((filter) => {
              if (filter.field === aggField || filter.label.toLowerCase()===key) {
                  newAggField = filter.field;
                
                // No need this rule. In this case we control the parameters, and we want a loose
                // comparison to handle the filter with a 'Date' type (where the key aggregation is a number).
                // tslint:disable-next-line: triple-equals
                const aggregation = filter.aggregations.find(el => el.troncatedKey.trim() == troncatedKey.trim());
                if (aggregation) {
                  aggKey = aggregation.key;
                }
                else if(troncatedKey=='Non Renseigné'){ 
                  // https://redmine.neogeo.fr/issues/13901
                  // On peut passer dans l'url la valeur Non Renseigné 
                  //pour rechercher les éléments pour lesquels la clef n'est présente dans l'index
                  aggKey=troncatedKey;
                }
              }
              if (filter.subField === aggField) {
                // We need to find the parent aggregation
                filter.aggregations.forEach((aggregation) => {
                  if (aggregation.subAggregations.length > 0) {
                    const index = aggregation.subAggregations.findIndex(el => el.troncatedKey === troncatedKey);

                    const subAggregation = aggregation.subAggregations[index];

                    if (subAggregation) {
                      aggKey = subAggregation.key;
                      parentAgg = subAggregation.parentAggregation.key;
                      newAggField = subAggregation.parentAggregation.field;
                    }
                  }
                });
              }
            });
            this._datasetResearchService.updateAggregation(newAggField, aggKey, true, parentAgg, false);
          });
        });
        this._datasetResearchService.triggerSearchChange();
      });
    }
    else {
      //reset filters recorded when there are not get parameters
      this._datasetResearchService.triggerSearchChange();
      this._researchUrlService.reset();
    }
  }

  ngOnDestroy() {
    this.searchChangeSub.unsubscribe();
    // Reset the url parameters
    this._researchUrlService.reset();
  }

  search() {
    this._datasetResearchService.getResults().subscribe(
      (res) => {
        this.selectedScope = this._datasetResearchService.scopeReasearch;

        const position = this._viewportScroller.getScrollPosition();
        this.results = res;

        this.countResults = [];
        this._datasetResearchService.resultsCount.forEach((item) => {
          this.countResults[item.scopeKey] = item.count || 0;
        });

        this.paginator.pageSize = this._datasetResearchService.pageSize;
        this.paginator.pageIndex = this._datasetResearchService.pageIndex + 1;

        // Set pagination depending on the selected scope
        this.paginator.length = this.countResults[this.selectedScope.key];
        this._viewportScroller.scrollToPosition(position);

      },
      (err) => {
        this._notificationService.notify(new Notification({
          message: err.message,
          type: 'error',
        }));
      },
    );

  }

  // When pagination is changed by user, we update results with new pagination options
  changePagination(pageIndex) {
    this._datasetResearchService.shouldAggregateResultCount = false;
    this._datasetResearchService.shouldAggregateFilters = false;
    this._datasetResearchService.paginationChanged(this.paginator.pageSize, pageIndex - 1);
  }

  changePageSize(pageSize) {
    this._datasetResearchService.paginationChanged(pageSize, 0);
  }

  formatFoundItem(e) {
    return `<u>${e.label}:</u>${e.content}`;
  }

  formatProtocols(uriList: IMetadataLink[]): string {
    return uriList.map((uri) => {
      return uri.service;
    }).join(', ');
  }

  toggleFilters() {
    this.displayFilters = !this.displayFilters;
  }

  getSuggestion() {
    this._datasetResearchService.getSuggestion(this._datasetResearchService.searchString).subscribe(
      (suggestion) => {
        this.suggestion = suggestion;
        if (this.suggestion.text !== '' && this.suggestion.text !== null && this.suggestion.text !== undefined) {
          this.displaySuggestion = true;
        } else {
          this.displaySuggestion = false;
        }
      },
      (err) => {
        this.displaySuggestion = false;
      },
    );
  }

  scopeChanged(value: IScope) {
    this.selectedScope = value;
    this._datasetResearchService.scopeChanged(this.selectedScope);
  }

  useSuggestion() {
    this._datasetResearchService.searchChanged(this.suggestion.text);
  }

  formatDate(date: string, format: string) {
    try {
      let res = this._datePipe.transform(date, format);
      if (!res) {
        res = '-';
      }
      return res;
    } catch (error) {
      return '-';
    }
  }

  isDataset(value: Dataset | CMSContent) {
    return value instanceof Dataset;
  }

  get isLoading() {
    return this._datasetResearchService.isLoading;
  }

  get fromResultNumber() {
    return ((this.paginator.pageIndex - 1) * this.paginator.pageSize) + 1;
  }

  get toResultNumber() {
    return Math.min((this.paginator.pageIndex * this.paginator.pageSize), this.paginator.length);
  }
}
