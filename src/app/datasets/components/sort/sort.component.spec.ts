import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SortComponent } from './sort.component';
import { DatasetResearchService, ElasticsearchService } from '../../services';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorService, NotificationService } from '../../../core/services';
import { StorageService } from '../../../core/services/storage.service';
import { FormsModule } from '@angular/forms';

describe('SortComponent', () => {
  let component: SortComponent;
  let fixture: ComponentFixture<SortComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        BrowserAnimationsModule,
        FormsModule,
      ],
      declarations: [SortComponent],
      providers: [
        DatasetResearchService,
        ElasticsearchService,
        ErrorService,
        StorageService,
        NotificationService,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
