import {Component, OnDestroy, OnInit} from '@angular/core';
import { DatasetResearchService } from '../../services';
import { geosource } from '../../../../i18n/traductions';
import { Subscription } from 'rxjs';

interface IDropdownOptions {
  'label': string;
  'value': string;
  'order': 'desc' | 'asc';
  'id': number;
}

@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.scss'],
})
export class SortComponent implements OnInit, OnDestroy {

  // Options list for the sort dropdown
  options: IDropdownOptions[];
  // Selected value of the dropdown
  selectedOption: IDropdownOptions;

  searchChangeSubscription: Subscription;

  dropDownFormatsToggle: boolean = false;

  constructor(
    private _datasetResearchService: DatasetResearchService,
  ) {
    this.options = [
      {
        id: 1,
        label: geosource.sortOptionLabels.relevance,
        value: 'relevance',
        order: 'desc',
      },
      {
        id: 2,
        label: geosource.sortOptionLabels.increasingDate,
        value: 'date',
        order: 'asc',
      },
      {
        id: 3,
        label: geosource.sortOptionLabels.decreasingDate,
        value: 'date',
        order: 'desc',
      },
      {
        id: 4,
        label: geosource.sortOptionLabels.alphabetical,
        value: 'alphabetical',
        order: 'asc',
      },
      {
        id: 5,
        label: geosource.sortOptionLabels.counterAlphabetical,
        value: 'alphabetical',
        order: 'desc',
      },
    ];
  }

  ngOnInit() {
    this.selectedOption = this.getOptionFromValueAndOrder(this._datasetResearchService.sortOptions);
    this.searchChangeSubscription = this._datasetResearchService.searchChange$.subscribe(
      (sortOption: any) => {
        if (sortOption && sortOption.value && sortOption.order) {
          this.selectedOption = this.getOptionFromValueAndOrder(sortOption);
        }
      },
    );
  }

  ngOnDestroy(): void {
    this.searchChangeSubscription.unsubscribe();
  }

  sortValueChanged(option) {
    this.selectedOption = option;
    this._datasetResearchService.sortChanged({
      value: this.selectedOption.value,
      order: this.selectedOption.order,
    });
  }

  get isLoading() {
    return this._datasetResearchService.isLoading;
  }

  private getOptionFromValueAndOrder(sortOption) {
    return this.options.find(
      e => (
        e.value === sortOption.value &&
        e.order === sortOption.order
      ),
    );
  }
}
