import { Location } from '@angular/common';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { geosource } from '../../../../i18n/traductions';
import { Commune } from '../../../core/models/commune.model';
import { NavigationHistoryService } from '../../../core/services';
import { Aggregation, Filter, IActiveFiltersTemplate } from '../../../elasticsearch/models';
import { AppRoutes } from '../../../routes';
import { scopesResearch } from '../../../shared/variables';
import { DatasetResearchService } from '../../services';

@Component({
  selector: 'app-filter-list',
  templateUrl: './filter-list.component.html',
  styleUrls: ['./filter-list.component.scss'],
})
export class FilterListComponent implements OnInit, OnDestroy {





  private _datasetsReloaded: Subscription;
  filters: Filter[];
  // List of active filters
  activeAggregations: IActiveFiltersTemplate[];

  filtersOpen: boolean[] = [];

  @Input() commune: Commune;

  constructor(
    private _datasetResearchService: DatasetResearchService,
    private _location: Location,
    private _navigationHistoryService: NavigationHistoryService,
  ) { }

  ngOnInit() {

    const filters = this._datasetResearchService.filters;
    if (filters) {
      this.initFilters();
    }
    this._datasetsReloaded = this._datasetResearchService.datasetsReloaded$.subscribe(
      (value) => {
        this.initFilters();
      },
    );
  }

  initFilters() {
    const filters = this._datasetResearchService.filters;
    this.activeAggregations = [];
    this.filters = [];

    // If one filter is used as an active filter, we remove it of 'aggregations' property
    // that are used to display the filters categories
    filters.forEach((filter) => {

      filter.aggregations.forEach((aggregation) => {
        if (aggregation.isActive) {
          if (aggregation.label === 'null') {
            aggregation.label = geosource.filter.other;
          }
          this.activeAggregations.push({
            aggregation,
            filter,
          });
        } else if (aggregation.subAggregations.length > 0) {
          aggregation.subAggregations.forEach((subAgg: Aggregation) => {
            if (subAgg.isActive) {
              if (subAgg.label === 'null') {
                subAgg.label = geosource.filter.other;
              }
              this.activeAggregations.push({
                filter,
                aggregation: subAgg,
              });
            }
          });
        }

      });

      // Display/hide filters depending the scope
      const scope = this._datasetResearchService.scopeReasearch;

      if (scope) {
        const isPostResearch = (scope.key === scopesResearch.post.key
          && filter.index === scopesResearch.post.key);
        const isDatasetResearch = (scope.key !== scopesResearch.all.key && scope.key !== scopesResearch.post.key
          && filter.index === scopesResearch.datasets.key);
        const isAllResearch = scope.key === scopesResearch.all.key;

        if (isPostResearch || isDatasetResearch || isAllResearch) {
          this.filters.push(filter);
        }
      }
    });
  }

  removeFilter(aggregationToRemove: IActiveFiltersTemplate) {
    const parentKey = (aggregationToRemove.aggregation.parentAggregation) ?
      aggregationToRemove.aggregation.parentAggregation.key : '';
    this._datasetResearchService.updateAggregation(aggregationToRemove.filter.field,
      aggregationToRemove.aggregation.key, false,
      parentKey);
    this._datasetResearchService.triggerSearchChange();

  }

  resetActiveAggregations() {
    this._datasetResearchService.resetActiveAggregations();
    this._datasetResearchService.triggerSearchChange();
    let query=''
    if(this.commune){
      query='portail='+this.commune.elasticSearchName;
    }
    this._location.go(AppRoutes.research.uri, query);
    this._navigationHistoryService.add(this._location.path());
  }

  ngOnDestroy() {
    if (this._datasetsReloaded) {
      this._datasetsReloaded.unsubscribe();
    }
  }
}
