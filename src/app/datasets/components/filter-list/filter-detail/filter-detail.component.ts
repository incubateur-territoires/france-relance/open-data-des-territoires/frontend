import { Component, Input, OnInit } from '@angular/core';
import { Aggregation, Filter } from '../../../../elasticsearch/models';
import { DatasetResearchService } from '../../../services';

@Component({
  selector: 'app-filter-detail',
  templateUrl: './filter-detail.component.html',
  styleUrls: ['./filter-detail.component.scss'],
})
export class FilterDetailComponent implements OnInit {

  @Input() filter: Filter;
  @Input() isOpen?: boolean;
  subAreOpen: boolean[] = [];

  showList = false;

  displayFilterItems = true;

  dropDownItemToggle = false;

  selectedItems=[];

  dropDownFilter=undefined;

  constructor(
    private _datasetResearchService: DatasetResearchService,
  ) { }

  ngOnInit() {
    this.displayFilterItems = this.isOpen;
    if (this.filter.subField) {
      this.filter.aggregations.forEach((agg) => {
        this.subAreOpen[agg.key] = false;
      });
    }

    if (this.filter.widget=='combo'){
      this.selectedItems=this.ItemList.filter(x=>x.isActive);
    }
  }

  getItemLabel(x){
    return x.label +' ('+x.count_per_metadata+')';
  }

  get ItemList(){
    if(this.dropDownFilter){
      return this.filter.aggregations.filter(x=>x.label.toLowerCase().indexOf(this.dropDownFilter.toLowerCase())>=0).filter(x=>x.count_per_metadata>0).sort((a,b)=>a.label.localeCompare(b.label));
    }
    return this.filter.aggregations.filter(x=>x.count_per_metadata>0).sort((a,b)=>a.label.localeCompare(b.label));
  }

  setItem(x){
    if(this.selectedItems.indexOf(x)<0)
      this.selectedItems.push(x);
    this.changeFilter(x);
  }

  removeItem(x){
    this.selectedItems = this.selectedItems.filter(function(item) {
      return item.label !== x.label
      });
    this.changeFilter(x);
  }

  changeRange(filter){
    this._datasetResearchService.dateRangeChange();
  }
  
  changeFilter(aggregation: Aggregation) {
    const newValueActive = !aggregation.isActive;
    const parentAggregationKey = (aggregation.parentAggregation) ? aggregation.parentAggregation.key : '';
    this._datasetResearchService.updateAggregation(
      this.filter.field, aggregation.key,
      newValueActive, parentAggregationKey, true);
    this._datasetResearchService.triggerSearchChange();
  }

  toggleFilterItems() {
    this.displayFilterItems = !this.displayFilterItems;
  }

  /*
  * Method used to set the HTML undetermined property for the parent aggregation
  * If not all subaggregations are active, then undeterminated is set to true.
  */
  isUndeterminated(aggregation: Aggregation) {
    const isActive = aggregation.isActive;
    let allSubAggActive = true;
    let countSubAggregationsActive = 0;
    if (aggregation.subAggregations.length > 0) {
      aggregation.subAggregations.forEach((subAgg) => {
        if (subAgg.isActive) {
          countSubAggregationsActive += 1;
        } else {
          allSubAggActive = false;
        }
      });
    }

    const isUndeterminated = !isActive && (countSubAggregationsActive > 0) && !allSubAggActive;
    return isUndeterminated;
  }

  isChecked(filter: Filter, aggregation: Aggregation) {
    const isActive = aggregation.isActive;
    let subAggregationsActive = true;
    if (filter.subField) {
      aggregation.subAggregations.forEach((subAgg) => {
        if (!subAgg.isActive) {
          subAggregationsActive = false;
        }
      });
    }

    return isActive || subAggregationsActive;
  }

  get isLoading() {
    return this._datasetResearchService.isLoading;
  }

}
