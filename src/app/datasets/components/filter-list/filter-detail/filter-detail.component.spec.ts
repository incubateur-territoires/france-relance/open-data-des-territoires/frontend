import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FilterDetailComponent } from './filter-detail.component';
import { DatasetResearchService, ElasticsearchService } from '../../../services';
import { HttpClientModule } from '@angular/common/http';
import { ErrorService, NotificationService } from '../../../../core/services';
import { InlineSVGService } from 'ng-inline-svg/lib/inline-svg.service';
import { StorageService } from '../../../../core/services/storage.service';
import { FormsModule } from '@angular/forms';

describe('FilterDetailComponent', () => {
  let component: FilterDetailComponent;
  let fixture: ComponentFixture<FilterDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        FilterDetailComponent,
      ],
      imports: [
        HttpClientModule,
        FormsModule,
      ],
      providers: [
        DatasetResearchService,
        ElasticsearchService,
        ErrorService,
        StorageService,
        NotificationService,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterDetailComponent);
    component = fixture.componentInstance;
    component.filter = {
      field: '',
      label: '',
      aggregations: [{
        key: '',
        doc_count: 10,
        count_per_metadata: 3,
        isActive: true,
        label: '',
        troncatedLabel: '',
        field: '',
        subAggregations: [],
        type: '',
        findInSubAggregations: (key: string) => { return null; },
        isAllSubAggregationsActive: () => { return false; },
      }],
      type: '',
      index: '',
      nestedPath: '',
      findInAggregationsAndSubAggregations: (key: string) => { return null; },
      findActiveAggregations: () => { return null; },
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
