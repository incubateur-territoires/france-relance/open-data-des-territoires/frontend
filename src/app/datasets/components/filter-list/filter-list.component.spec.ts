import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FilterListComponent } from './filter-list.component';
import { FilterDetailComponent } from './filter-detail/filter-detail.component';
import { DatasetResearchService, ElasticsearchService } from '../../services';
import { HttpClientModule } from '@angular/common/http';
import { ErrorService, NotificationService } from '../../../core/services';
import { StorageService } from '../../../core/services/storage.service';
import { MockComponent } from 'ng2-mock-component';
import { FormsModule } from '@angular/forms';

describe('FilterListComponent', () => {
  let component: FilterListComponent;
  let fixture: ComponentFixture<FilterListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        FilterListComponent,
        FilterDetailComponent,
        MockComponent({ selector: 'app-sort' }),

      ],
      imports: [
        HttpClientModule,
        FormsModule,
      ],
      providers: [
        DatasetResearchService,
        ElasticsearchService,
        ErrorService,
        StorageService,
        NotificationService,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
