import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ElasticsearchModule } from '../elasticsearch/elasticsearch.module';
import { SharedModule } from '../shared/shared.module';
import { DatasetsComponents, SearchBarComponent } from './components';
import { DatasetsRoutingModule } from './datasets-routing.module';
import { DatasetsServices } from './services';

@NgModule({
  declarations: [...DatasetsComponents],
  exports: [
    SearchBarComponent,
  ],
  imports: [
    CommonModule,
    DatasetsRoutingModule,
    ElasticsearchModule,
    SharedModule,
    FormsModule,
  ],
  providers: [...DatasetsServices, DatePipe],
})
export class DatasetsModule { }
