import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { NavigationHistoryService } from '../../core/services';
import { Aggregation, IElasticsearchOptions, ISortOption } from '../../elasticsearch/models';
import { AppRoutes } from '../../routes';
import { scopesResearch } from '../../shared/variables';

@Injectable()
export class ResearchUrlService {

  private _parameters = {};
  private _aggParameters = {};

  constructor(
    private _location: Location,
    private _navigationHistoryService: NavigationHistoryService
  ) { }

  // Get the ES options from parameters
  getOptionsFromParameters(params: Object) {
    const options = {} as IElasticsearchOptions;

    Object.keys(params).forEach((key) => {
      // For the search query
      if (key === 'q') {
        options.searchString = params[key];
        this._parameters[key] = params[key];
      } else if (key === 'scope') {
        this.extractScopeOptions(options, params, key);
      } else if (key === 'sort') {
        this.extractSortOptions(params, key, options);
      } else if (key === 'page') {
        this.extractPaginationOptions(key, params, options);
      } else {
        // First we check if it's an aggregation (it should but if someone enter a random filter)
        const aggs = params[key].split(',');
        this._aggParameters[key] = [];
        aggs.forEach((agg: string) => {
          this._aggParameters[key].push(agg);
        });
      }
    });

    return options;
  }

  private extractPaginationOptions(key: string, params: Object, options: IElasticsearchOptions) {
    // Be sure we don't have a negative page index
    const pageIndex = Math.max(0, Number(params[key]) - 1);
    this._parameters[key] = pageIndex.toString();
    options.pageIndex = pageIndex;
  }

  private extractSortOptions(params: Object, key: string, options: IElasticsearchOptions) {
    // Check the sign of the value (desc or asc)
    const startWithNeg = params[key].startsWith('-');
    const value = params[key].split('-').join('');
    const optionsSort = {
      value,
      order: startWithNeg ? 'desc' : 'asc',
    };
    const acceptedSortValues = ['date', 'relevance', 'alphabetical'];
    // Check if sort value exists
    if (acceptedSortValues.includes(optionsSort.value)) {
      options.sortOptions = optionsSort;
      this._parameters[key] = params[key];
    }
  }

  private extractScopeOptions(options: IElasticsearchOptions, params: Object, key: string) {
    const scope = scopesResearch[params[key]];
    if (scope) {
      options.scope = scope;
      this._parameters[key] = scopesResearch[params[key]];
    }
  }

  // Set a parameter in the _parameters variable, and change the url
  setParameter(key: string, searchValue: string) {
    this._parameters[key] = searchValue;
    const params = this.generateUrlParams();
    this._location.go(AppRoutes.research.uri, params);
    this._navigationHistoryService.add(this._location.path());
  }
    // Set parameters in the _parameters variable, and change the url
    setParameters(list:any) {
      list.forEach(element => {
        this._parameters[element[0]] = element[1];
      });
      
      const params = this.generateUrlParams();
      this._location.go(AppRoutes.research.uri, params);
      this._navigationHistoryService.add(this._location.path());
    }

  // Set an aggregation parameter in the _aggParameters variable, and change the url
  setAggParameter(aggregation: Aggregation, isActiveAgg: boolean) {
    const fieldKey = aggregation.field.replace(/metadata-fr./, '');

    if (isActiveAgg) {
      this.addAggregation(fieldKey, aggregation.troncatedKey);
    } else {
      this.removeAggregation(fieldKey, aggregation.troncatedKey);
    }

    // We check if it's a parent aggregation,
    // if yes, remove all the children anyway because we don't need it.
    if (!aggregation.parentAggregation) {
      // If it's a parent, remove all the children from the parameters
      if (aggregation.subAggregations && aggregation.subAggregations.length > 0) {

        if (aggregation.subAggregations[0].parentAggregation.key !== aggregation.key) {
          const aggField = aggregation.subAggregations[0].field.replace(/metadata-fr./, '');
          delete this._aggParameters[aggField];
        }

      }
    }
    // It's a subaggregation that is updated:
    // - the parent aggregation becomes active if all the sub aggregations are active
    // - the parent aggregation becomes inactive if NOT all the sub aggregations are active
    // tslint:disable-next-line: brace-style
    else {
      const parentAggregation = aggregation.parentAggregation;
      const parentFieldKey = parentAggregation.field.replace(/metadata-fr./, '');

      if (parentAggregation.isAllSubAggregationsActive()) {
        // Remove the current field for subaggregations
        const aggField = aggregation.field.replace(/metadata-fr./, '');
        delete this._aggParameters[aggField];
        this.addAggregation(parentFieldKey, parentAggregation.troncatedKey);

      } else {
        // Remove the parentAgg as filter, and add all the subAgg active
        this.removeAggregation(parentFieldKey, parentAggregation.troncatedKey);
        aggregation.parentAggregation.subAggregations.forEach((subAgg) => {
          const subAggFieldKey = subAgg.field.replace(/metadata-fr./, '');
          if (subAgg.isActive) {
            this.addAggregation(subAggFieldKey, subAgg.troncatedKey);
          } else {
            this.removeAggregation(subAggFieldKey, subAgg.troncatedKey);
          }
        });
      }
    }
    const params = this.generateUrlParams();
    this._location.go(AppRoutes.research.uri, params);
    this._navigationHistoryService.add(this._location.path());
  }

  private addAggregation(fieldKey: string, aggregationKey: string) {
    // If new, init the array
    if (!this._aggParameters[fieldKey]) {
      this._aggParameters[fieldKey] = [];
    }
    // If value not exists already
    if (!this._aggParameters[fieldKey].includes(aggregationKey)) {
      this._aggParameters[fieldKey].push(aggregationKey);
    }
  }

  private removeAggregation(fieldKey: string, aggregationKey: string) {
    if (this._aggParameters[fieldKey]) {
      const index = this._aggParameters[fieldKey].indexOf(aggregationKey);
      if (index !== -1) this._aggParameters[fieldKey].splice(index, 1);
      // If array is empty, then we remove the parent field.
      if (this._aggParameters[fieldKey].length === 0) {
        delete this._aggParameters[fieldKey];
      }
    }
  }

  setPaginationParameter(pageIndex: number) {
    this._parameters['page'] = pageIndex + 1;
    const params = this.generateUrlParams();
    this._location.go(AppRoutes.research.uri, params);
    this._navigationHistoryService.add(this._location.path());
  }

  setSortParameter(option: ISortOption) {
    // If desc we prefix with '-'
    const value = option.order === 'desc' ? `-${option.value}` : option.value;
    this._parameters['sort'] = value;
    const params = this.generateUrlParams();
    this._location.go(AppRoutes.research.uri, params);
    this._navigationHistoryService.add(this._location.path());
  }

  // Generate the parameters to set into the url from the _parameters and _aggParametersa arrays
  generateUrlParams() {
    let params = '';

    // Manage all the parameters except aggregations
    Object.keys(this._parameters).forEach((key) => {
      if (this._parameters[key]) {
        if (params !== '') {
          params += '&';
        }
        params += `${key}=${this._parameters[key]}`;
      }
    });

    // Aggregations. If one agg key has multiple value, separate it with comma
    Object.keys(this._aggParameters).forEach((key) => {
      if (this._aggParameters[key]) {
        if (params !== '') {
          params += '&';
        }
        const values = this._aggParameters[key].join(',');
        params += `${key}=${values}`;
      }
    });

    return params;
  }

  reset() {
    this._parameters = {};
    this._aggParameters = {};
  }

  get aggParameters() {
    return this._aggParameters;
  }

  get parameters() {
    return this._parameters;
  }
}
