import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { geosource, notificationMessages } from '../../../i18n/traductions';
import { ErrorService } from '../../core/services';
import { APP_CONFIG } from '../../core/services/app-config.service';
import { CMSContent } from '../../editorialisation/models';
import { IESCMSResponse } from '../../editorialisation/models/cms-content.model';
// tslint:disable-next-line: max-line-length
import { Aggregation, ElasticsearchOptions, Filter, ICountScope, IElasticsearchHit, IScope, ISortOption } from '../../elasticsearch/models';
import { ElasticsearchService } from '../../elasticsearch/services/elasticsearch.service';
import { AppRoutes } from '../../routes';
import { Dataset, Highlights } from '../../shared/models';
import { scopesResearch } from '../../shared/variables';
import { SearchCompletion, SearchSuggestion } from '../models';
import { ResearchUrlService } from './research-url.service';

declare var _paq: any;

@Injectable()
export class DatasetResearchService {
  APP_CONFIG = APP_CONFIG;
  private _searchChangeSubject: Subject<any>;
  private _datasetsReloadedSubject: Subject<any>;
  private _scopeChangedSubject: Subject<any>;
  private _elasticsearchOptions: ElasticsearchOptions;
  private _resultsCount: ICountScope[];
  private _pendingRequests: number = 0;
  private _lastFilterClicked: string = null;

  constructor(
    private _errorService: ErrorService,
    private _elasticsearchService: ElasticsearchService,
    private _router: Router,
    private _researchUrlService: ResearchUrlService,
  ) {
    this._searchChangeSubject = new Subject<any>();
    this._datasetsReloadedSubject = new Subject<any>();
    this._scopeChangedSubject = new Subject<any>();
    this._elasticsearchOptions = new ElasticsearchOptions({ pageSize: 5 });
    this._resultsCount = [];
  }


  getLastPublishedDatasets(): Observable<Dataset[]> {
    this._pendingRequests += 1;
    let elasticsearchOptions = new ElasticsearchOptions({ pageSize: 3 });
    elasticsearchOptions._sortOptions = {
      value: 'date',
      order: 'desc',
    };
    return this._elasticsearchService.getAllEntities(elasticsearchOptions).pipe(
      map((e) => {     
        this._pendingRequests -= 1;  
        return e.hits.hits.map(x=>x._source);
      }),
      catchError(
        (err) => {
          this._pendingRequests -= 1;
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getResults });
        },
      ));
  }
  /**
   *  Get results from elasticsearch (can be Datasets or Articles).
   *  Options: filter by query string and pagination
   */
  getResults(): Observable<Dataset[] | CMSContent[]> {
    this._pendingRequests += 1;
    return this._elasticsearchService.getAllEntities(this._elasticsearchOptions).pipe(
      map((e) => {
        if (this._elasticsearchOptions.shouldAggregateFilters) {
          // Get the aggregations that will be used to display the filters
          let aggregations = (e.hits.hits) ? e.aggregations : [];
          // gestion licence desactivée sur PIGMA
          if(APP_CONFIG.theFunctionalitiesInterruptor.processLicences ){
             aggregations = this.manageLicenceAggregations(aggregations);
          }
          

          this._elasticsearchOptions.filters.forEach((filter) => {
            console.log(filter); 
            console.log(aggregations); 
            // We check which one filter was lasted clicked.
            // We don't update the aggregation counts for the clicked filter
            // (except if it has not anymore active aggregations)
            if (
              this._lastFilterClicked === null ||
              filter.field !== this._lastFilterClicked
            ) {

              // Reset all the aggregation count to 0
              filter.aggregations.forEach((agg) => {
                agg.count_per_metadata = 0;
                agg.subAggregations.forEach((subAgg) => {
                  subAgg.count_per_metadata = 0;
                });
              });

              // Update the aggregation counts if ES provides results
              if (aggregations[filter.field]) {
                const fieldBucket = (filter.subField) ? aggregations[filter.field][filter.field]
                  : aggregations[filter.field];
                if (fieldBucket) {
                  fieldBucket.buckets.forEach((agg) => {
                    const countESField = (filter.subField) ? agg.count_per_metadata['uuid']?agg.count_per_metadata['uuid']:agg.count_per_metadata
                      : agg.count_per_metadata;

                    const index = filter.aggregations.findIndex(e => e.key === agg.key);
                    agg.count_per_metadata = (countESField) ? countESField.value ?countESField.value:countESField : 0;
                    let currentAggregation: Aggregation = null;
                    if (index !== -1) {
                      currentAggregation = filter.aggregations[index];
                      filter.aggregations[index].count_per_metadata = agg.count_per_metadata;
                    } else {
                      currentAggregation = new Aggregation(agg);
                      currentAggregation.field = filter.field;
                      filter.aggregations.push(currentAggregation);
                    }

                    // If it's a multi level filter, we update the subaggregation counts as well
                    if (filter.subField) {
                      agg[filter.subField].buckets.forEach((subAgg) => {
                        const index = currentAggregation.subAggregations.findIndex(e => e.key === subAgg.key);
                        if(subAgg.count_per_metadata['uuid']){
                          subAgg.count_per_metadata = (subAgg.count_per_metadata['uuid'].value || 0);
                        }
                        

                        if (index !== -1) {
                          currentAggregation.subAggregations[index].count_per_metadata =
                            subAgg.count_per_metadata;
                        } else {
                          const subAggregation = new Aggregation(subAgg);
                          subAggregation.parentAggregation = currentAggregation;
                          currentAggregation.subAggregations.push(subAggregation);
                          subAggregation.field = filter.subField;
                        }
                      });
                    }
                  });
                }

                this.sortAggregations(filter.aggregations, filter.type);
                if (filter.subField) {
                  filter.aggregations.forEach((agg) => {
                    this.sortAggregations(agg.subAggregations, agg.type);
                  });
                }
              }
            }
          });
        }

        const results: any = [];

        e.hits.hits.forEach((result) => {
          if (result._source['metadata-fr']) {
            results.push(this.initilizeDataset(result));
          } else {
            const source = result._source as IESCMSResponse;
            if (source.type === scopesResearch.post.key) {
              const cmsPost = new CMSContent(source);
              result.inner_hits.data.hits.hits.forEach((document) => {
                const highlights = document.highlight;
                for (const field in highlights) {
                  if (highlights.hasOwnProperty(field)) {

                    // Split the field to separate origine of the field (metadata-fr/data-fr)
                    // and the field itself (abstract/title...)
                    const arr = field.split('.');
                    const key = arr.pop(); // ex url, title, abstract....
                    // Depending on the way fields have been indexed in ES, some will have two fields
                    // in highlight metadata-fr.abstract.keyword et metadata-fr.abstract.keyword
                    // The highlighting on the keyword is not interesting because the whole field is surrouded by
                    // the <em></em> and not just the searched word
                    // So only add the highlight to the array if the key is different of keyword
                    if (key !== 'suggest') {
                      // If the source of the key is in the metadata of the document

                      // To simplify the display we put the highlited abstract in a particular variable
                      if (key === 'title') {
                        cmsPost.content.titleHighlight = highlights[field].join();
                      }

                      if (key !== 'title') {
                        if (!cmsPost.content.highlight.includes(highlights[field].join())) {
                          cmsPost.content.highlight.push(highlights[field].join());
                        }
                      }

                    }
                  }
                }
              });
              results.push(cmsPost);
            }

          }

        });


        // Set results count
          this._resultsCount = [];
          const countScopes = e.aggregations['count_by_scope'].buckets;
          let countDatasets = 0;
          countScopes.forEach((bucket) => {
            if (scopesResearch.datasets.elasticType.includes(bucket.key)) {
              countDatasets += bucket.count.value;
            } else {
              this._resultsCount.push({
                scopeKey: bucket.key,
                count: bucket.count.value,
              });
            }
          });

          // Datasets scope (datasets + non geographic datasets + series)
          this._resultsCount.push({
            scopeKey: scopesResearch.datasets.key,
            count: countDatasets,
          });

          // All scope
          const countAll = this._resultsCount.reduce((acc, obj) => {
            const count = scopesResearch.all.elasticType.includes(obj.scopeKey) ? acc + obj.count : acc;
            return count;
          }, 0);
          this._resultsCount.push({
            scopeKey: scopesResearch.all.key,
            count: countAll,
          });

        // Matomo tracking
        if(window['_paq']){
          _paq.push(['trackSiteSearch', this.searchString]);
        }
          

        this._elasticsearchOptions.shouldAggregateFilters = true;
        this._elasticsearchOptions.shouldAggregateResultCount = false;
        this._elasticsearchOptions.fromAutocompletion = false;
        // Notify that data have been reloaded
        this._datasetsReloadedSubject.next();

        this._pendingRequests -= 1;

        return results;
      }),
      catchError(
        (err) => {
          this._pendingRequests -= 1;
          throw this._errorService.handleError(err, { message: notificationMessages.geosource.getResults });
        },
      ),
    );
  }

  private manageLicenceAggregations(aggregations) {
    const hasAlreadyOtherLicenses = this._elasticsearchOptions.otherLicenceAggregations.length > 0;
    for (const property in aggregations) {
      if (aggregations.hasOwnProperty(property)) {

        if (property === 'metadata-fr.license') {

          let countOtherBucket = 0;

          for (let i = aggregations[property]['buckets'].length - 1; i >= 0; i = i - 1) {
            const bucket = aggregations[property]['buckets'][i];
            if (!this._elasticsearchOptions.knownLicences.includes(bucket.key)) {
              countOtherBucket += bucket.count_per_metadata.value;
              aggregations[property]['buckets'].splice(i, 1);
              if (!hasAlreadyOtherLicenses) {
                this._elasticsearchOptions.otherLicenceAggregations.push(bucket.key);
              }
            }
          }

          aggregations[property]['buckets'].push({
            key: geosource.licence.other,
            count_per_metadata: {
              value: countOtherBucket,
            },
          });
        }
      }
    }
    if (!hasAlreadyOtherLicenses) {
      //this._searchChangeSubject.next();
    }
    return aggregations;
  }

  private sortAggregations(aggregations: Aggregation[], type: string) {
    if (type === 'date') {
      // Sort by date from the newest to the lastest
      aggregations.sort((a, b) => {
        let res: number;
        const x = a.key;
        const y = b.key;
        if (x < y) { res = 1; }
        if (x > y) { res = -1; }
        return res;
      });
    } else {
      aggregations.sort((a, b) => {
        // Order first by the number of result
        let res = b.count_per_metadata - a.count_per_metadata;
        // If same number of dataset
        if (res === 0) {
          let x = a.key;
          let y = b.key;
          // Order by alphabetical order in string
          if (typeof a.key === 'string' && typeof b.key === 'string') {
            x = a.key.toLowerCase();
            y = b.key.toLowerCase();
            if (x < y) { res = -1; }
            if (x > y) { res = 1; }
          } else {
            // else bigger key first (newest date first for exemple)
            if (x < y) { res = 1; }
            if (x > y) { res = -1; }
          }
        }
        return res;
      });
    }

  }

  private initilizeDataset(hit: IElasticsearchHit): Dataset {
    const dataset = new Dataset({ metadata: hit._source['metadata-fr'], fields: hit._source['fields'] });

    dataset.slug = hit._source['slug'];
    dataset.highlights = new Highlights();

    if ((<any>hit.inner_hits.data.hits.total).value) {
      dataset.metadata.total_documents = (<any>hit.inner_hits.data.hits.total).value;
    }
    else {
      dataset.metadata.total_documents = hit.inner_hits.data.hits.total;
    }

    hit.inner_hits.data.hits.hits.forEach((document) => {

      const highlights = document.highlight;

      for (const field in highlights) {

        if (highlights.hasOwnProperty(field)) {

          // Split the field to separate origine of the field (metadata-fr/data-fr)
          // and the field itself (abstract/title...)
          const arr = field.split('.');
          const key = arr.pop(); // ex url, title, abstract....
          const source = arr.shift(); // metadata-fr or data-fr

          // Depending on the way fields have been indexed in ES, some will have two fields
          // in highlight metadata-fr.abstract.keyword et metadata-fr.abstract.keyword
          // The highlighting on the keyword is not interesting because the whole field is surrouded by
          // the <em></em> and not just the searched word
          // So only add the highlight to the array if the key is different of keyword
          if (key !== 'suggest') {
            // If the source of the key is in the metadata of the document
            if (source === 'metadata-fr') {
              // To simplify the display we put the highlited abstract in a particular variable
              if (key === 'title') {
                dataset.setHighlightedTitle(highlights[field].join());
              }
            }
            if (key !== 'title') {
              if (!dataset.highlights.highlightedDescription.includes(highlights[field].join())) {
                dataset.highlights.highlightedDescription.push(highlights[field].join());
              }
            }

          }
        }
      }
    });

    return dataset;
  }

  getAutoComplete(text: string,filter:any=undefined): Observable<SearchCompletion[]> {
    const options: SearchCompletion[] = [];
    const titleOptions : SearchCompletion[] = [];
    return this._elasticsearchService.getAutoComplete(text,filter).pipe(
      map((e) => {
        // We will loop over all the hits, look if an highlight have been found,
        // and if yes, we add the first one to our autocompletion options list.
        const searchOption = {};
        if (e['hits']['hits']) {
          e['hits']['hits'].forEach((hit) => {
            searchOption['id'] = hit._id;
            searchOption['_index'] = hit._index;
            searchOption['_score'] = hit._score;
            const highlight = hit['highlight'];
            if (highlight) {
              Object.keys(highlight).forEach((key, index) => {
                // We check if this text highlight already exists in the options array
                // We want a unique text in the list, no duplicata.
                const found = options.some((el) => {
                  return el.text.toLowerCase() === highlight[key][0].toLowerCase();
                });
                const same = text.toLowerCase() === highlight[key][0].toLowerCase().replace(/<[^>]*>/g, '');;

                if (!same && !found ) {
                  if (key === 'metadata-fr.title' || key === 'content-fr.title') {
                    searchOption['text'] = highlight[key][0];
                    titleOptions.push(new SearchCompletion(searchOption));
                  }
                  else {
                    searchOption['text'] = highlight[key][0];
                    options.push(new SearchCompletion(searchOption));
                  }

                }
              });
            }

          });
        }
        const results = titleOptions.concat(options);
        if (results.length > 5) {
          results.length = 5;
        }

        return results;
      }),
    );
  }

  getSuggestion(query: string): Observable<SearchSuggestion> {
    this._pendingRequests += 1;
    return this._elasticsearchService.getSuggestion(query).pipe(
      map((res) => {
        const suggestions = res.suggest['suggestion'][0].options;
        let suggestion: SearchSuggestion;
        if (suggestions && suggestions.length > 0) {
          suggestion = new SearchSuggestion(suggestions[0]);
        } else {
          suggestion = new SearchSuggestion();
        }
        return suggestion;
      }),
      tap(() => {
        this._pendingRequests -= 1;
      }),
    );
  }

  /**
   * Update an aggregation for a specific filter.
   * Some logic is necessary depending if it's a 'parent' aggregation or a subaggregation
   * When the aggregation is updated, the searchChangeSubject is triggered.
  */
  updateAggregation(
    filterField: string, aggregationKey: string,
    isActive: boolean, aggregationParentKey = '', updateUrlParams = true) {
    const filtersToUpdate = this._elasticsearchOptions.filters.filter((el => el.field === filterField));
    console.log(filtersToUpdate);
    filtersToUpdate.forEach(filterToUpdate => {
      // If we find the filter to update
      const aggregation = filterToUpdate.findInAggregationsAndSubAggregations(aggregationKey, aggregationParentKey);

      if (aggregation) {
        filterToUpdate.setAggregationAndSubaggregation(aggregation, isActive);
        if (updateUrlParams) {
          this._researchUrlService.setAggParameter(aggregation, isActive);
        }
      }
      else if(aggregationKey=='Non Renseigné'){
        // https://redmine.neogeo.fr/issues/13901
        // On peut passer dans l'url la valeur Non Renseigné 
        //pour rechercher les éléments pour lesquels la clef n'est présente dans l'index        
        let newAggreg = new Aggregation({key:aggregationKey,isActive:true,doc_count:0,count_per_metadata:0});
        newAggreg.isActive=true;
        filterToUpdate.aggregations.push(newAggreg);
      }

      // By default, we set the lastclicked filter.
      // But if there is no more active aggregation for this filter, we
      // want to set it to null to force display new ES aggregations count.
      this._lastFilterClicked = filterField;
      const aggrActive = filterToUpdate.findActiveAggregations();
      if (!isActive && aggrActive.length === 0) {
        this._lastFilterClicked = null;
      }

      this._elasticsearchOptions.pageIndex = 0;
      this._elasticsearchOptions.shouldAggregateFilters = true;
      this._elasticsearchOptions.shouldAggregateResultCount = true;
    });

  }

  resetActiveAggregations(): void {
    this._elasticsearchOptions.filters.forEach((element) => {
      if(element.widget!='hidden'){
        element.aggregations.forEach((aggregation) => {
          aggregation.isActive = false;
  
          if (aggregation.subAggregations.length > 0) {
            aggregation.subAggregations.forEach((subAgg) => {
              subAgg.isActive = false;
            });
          }
        });
      }

    });
    this._elasticsearchOptions.pageIndex = 0;
    // We don't want to recalculate the number of dataset per filter when on is removed
    // because it only depends on the search string and not on which filter is activated or not
    this._lastFilterClicked = null;
    this._elasticsearchOptions.shouldAggregateFilters = true;
    this._elasticsearchOptions.shouldAggregateResultCount = true;
    this._researchUrlService.reset();
  }

  searchChanged(value: string,filter=undefined) {
    this._lastFilterClicked = null;
    this._elasticsearchOptions.searchString = value;
    this._elasticsearchOptions.pageIndex = 0;
    this._elasticsearchOptions.shouldAggregateResultCount = true;
    this._elasticsearchOptions.sortOptions.value = (value && true) ? 'relevance' : 'date';
    this._elasticsearchOptions.sortOptions.order = 'desc';
    const racine = this._router.url.split('/').pop();
    if (racine !== AppRoutes.research.uri) {
      let params:any={};
      if(filter ){
        params=filter;
      }
      if (value === '') {
        this._router.navigate(['/', `${AppRoutes.research.uri}`], { queryParams: params });
        
      } else {
        params.q=value;
        this._router.navigate(['/', `${AppRoutes.research.uri}`], { queryParams: params });
      }
      return;
    }
    //TODO verifier l'utilité de cette partie
    if(filter){
      this._researchUrlService.setParameters([['portail', filter.portail],['q', value]]);
    }
    else{
      this._researchUrlService.setParameter('q', value);
    }

   
    this._searchChangeSubject.next(this._elasticsearchOptions.sortOptions);
  }

  scopeChanged(scope: IScope) {
    this._elasticsearchOptions.pageIndex = 0;
    this._elasticsearchOptions.scope = scope;
    this._elasticsearchOptions.shouldAggregateResultCount = false;
    this._researchUrlService.setParameter('scope', scope.key);
    if(scope.key == scopesResearch.post.key && this._researchUrlService.parameters['sort']==undefined){
      this._elasticsearchOptions._sortOptions = {
        value:  'date',
        order: 'desc',
      };
      this._searchChangeSubject.next(this._elasticsearchOptions.sortOptions);
    }
    else if(this._researchUrlService.parameters['sort']==undefined && this._researchUrlService.parameters['q']!=undefined && this._researchUrlService.parameters['q'].length>0){
      this._elasticsearchOptions._sortOptions = {
        value:  'relevance',
        order: 'desc',
      };
      this._searchChangeSubject.next(this._elasticsearchOptions.sortOptions);
    }
    else{
      this._searchChangeSubject.next();
    }
    
  }

  resetResearch(triggerNext = true) {
    this._lastFilterClicked = null;
    this._elasticsearchOptions.searchString = '';
    this._elasticsearchOptions.pageIndex = 0;
    this._elasticsearchOptions.scope = scopesResearch.all;
    this.resetActiveAggregations();
    if (triggerNext) {
      this.triggerSearchChange();
    }
  }

  dateRangeChange() {
    this._elasticsearchOptions.shouldAggregateFilters = true;
    this._elasticsearchOptions.shouldAggregateResultCount = true;
    this._searchChangeSubject.next();
  }

  triggerSearchChange() {
    this._searchChangeSubject.next();
  }

  get searchChange$(): Observable<string> {
    return this._searchChangeSubject.asObservable();
  }

  get isLoading() {
    return this._pendingRequests > 0 ? true : false;
  }

  get datasetsReloaded$(): Observable<string> {
    return this._datasetsReloadedSubject.asObservable();
  }

  get scopeChanged$(): Observable<string> {
    return this._scopeChangedSubject.asObservable();
  }

  get searchString(): string {
    return this._elasticsearchOptions.searchString;
  }

  /* SORT */
  get sortOptions(): ISortOption {
    return this._elasticsearchOptions.sortOptions;
  }

  sortChanged(option: ISortOption) {
    this._elasticsearchOptions.sortOptions = option;

    this._researchUrlService.setSortParameter(option);
    this._searchChangeSubject.next();
  }

  /* PAGINATION */
  paginationChanged(pageSize: number, pageIndex: number) {
    this._elasticsearchOptions.pageSize = pageSize;
    this._elasticsearchOptions.pageIndex = pageIndex;

    this._researchUrlService.setPaginationParameter(pageIndex);
    this._searchChangeSubject.next();
  }

  get pageSize() {
    return this._elasticsearchOptions.pageSize;
  }

  set pageSize(value: number) {
    this._elasticsearchOptions.pageSize = value;
  }

  get pageIndex() {
    return this._elasticsearchOptions.pageIndex;
  }

  get filters(): Filter[] {
    return this._elasticsearchOptions.filters;
  }

  /**
   * When set to true add aggregation to get the result count for the actual request
  */
  set shouldAggregateResultCount(value: boolean) {
    this._elasticsearchOptions.shouldAggregateResultCount = value;
  }

  /**
   * When set to true add aggregations to get the list of values for the different
   * filters and the number of dataset that match each value
   */
  set shouldAggregateFilters(value: boolean) {
    this._elasticsearchOptions.shouldAggregateFilters = value;
  }

  set scopeReasearch(value: IScope) {
    this._elasticsearchOptions.scope = value;
    this._scopeChangedSubject.next();
  }

  get scopeReasearch() {
    return this._elasticsearchOptions.scope;
  }

  /* Result number */

  get resultsCount(): ICountScope[] {
    return this._resultsCount;
  }

  set fromAutocompletion(value: boolean) {
    this._elasticsearchOptions.fromAutocompletion = value;
  }

  set elasticSearchOptions(esOptions: ElasticsearchOptions) {
    this._elasticsearchOptions = esOptions;
  }

  get elasticSearchOptions() {
    return this._elasticsearchOptions;
  }
}
