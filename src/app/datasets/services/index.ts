import { DatasetResearchService } from './dataset-research.service';
import { ResearchUrlService } from './research-url.service';

export { DatasetResearchService, ResearchUrlService };

// tslint:disable-next-line:variable-name
export const DatasetsServices = [
  DatasetResearchService,
  ResearchUrlService,
];
