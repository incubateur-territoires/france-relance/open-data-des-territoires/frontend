export const userServicesStatuses = {
  pending: 'Demande en cours',
  opened: 'Ok',
  expired: 'Expiré',
  other: '-',
};

export const notificationMessages = {
  edito: {
    getPage: 'Impossible de charger la page',
    getCategories: 'Impossible de charger les catégories',
    getPosts: 'Impossible de charger la liste des posts',
    getPost: 'Impossible de charger le post',
    getCategoriesAndPosts: 'Impossible de charger la listes des posts et des catégories',
    getCategoriesAndPost: 'Impossible de chager le post et les catégories',
    getHomeVideoPost: 'Impossible de charger le contenu du post',
    getMediaUrl: 'Impossible de récupérer l\'url du média',
    getDataProducers: 'Impossible de charger la liste des partenaires.',
    getCredits: 'Impossible de charger la liste des crédits.',
    getReuses: 'Impossible de charger la liste des réutilisations.',
    news: 'actualité',
    getDatasetRelatedPosts: 'Impossible de charger la liste des actualités associées à ce jeux de données.',
  },
  geosource: {
    getResults: 'Impossible de charger la liste de datasets',
    getDatasetById: 'Impossible de charger le dataset',
    getAutocomplete: 'Impossible de récupérer le resultat de l\'autocompletion',
    getSuggestion: 'Impossible de récupérer le resultat de la suggestion',
    mapError: 'Une erreur s\'est produite avec la carte',
    getDatasetChildren: 'Impossible de récupérer les enfants du jeu de données',
    getDatasetParentInfo: 'Impossible de récupérer les informations du jeu de données parent',
    getDatasetData: 'Impossible de récupérer la donnée du jeu de données',
    getAggregationsList: 'Impossible de récupérer les aggregations',
    getDataFromCoordinates: 'Aucune donnée à ces coordonnées',
  },
  matomo: {
    getViews: 'Impossible de récupérer le nombre de vues',
  },
  signup: {
    // tslint:disable-next-line:max-line-length
    accountCreated: 'La procédure de création de compte s\'est déroulée correctement. Vous allez recevoir un email vous permettant de l\'activer.',
    accountCreationFailed: 'Désolé, nous n\'avons pas pu créer votre',
    existingAccount: 'Un compte existe déjà pour cet email.',
    uncompleteForm: 'Le formulaire est incomplet.',
  },
  userServices: {
    addSuccess: 'Vous avez demandé l\'accès à',
    addFailed: 'Une erreur est survenue lors de la demande d\'accès aux services.',
    addFailedDetailed: 'Une erreur est survenue lors de la demande d\'accès à ',
    failedToLoadUserResources: 'Une erreur est survenue lors du chargement de vos données.',
    initializationError: 'Une erreur est survenue lors de l\'initialization de la page.',
    removeFailed: 'Une erreur est survenue lors de la suppression de vos accès.',
    removeFailedDetailed: 'Une erreur est survenue lors de la suppression de votre accès à ',
    removeSuccess: 'Nous avons supprimé votre accès à ',
    renewSuccess: 'Nous avons bien reçu votre demande de renouvellement d\'accès pour ',
    renewFailed: 'Une erreur est survenue lors de la demande de renouvellement d\'accès.',
    renewFailedDetailed: {
      partOne: 'Nous n\'avons pas pu renouveler votre accès à ',
      partTwo: '. Votre accès doit avoir été validé avant de pouvoir être renouvellé.',
    },
  },
  userInfo: {
    failedToLoadUserInfo: 'Une erreur est survenue lors du chargement de vos informations.',
    failedToUpdateUserInfo: 'Une erreur est survenue lors de la mise à jour de vos informations.',
    userInfoUpdated: 'Vos informations ont bien été enregistrées.',
    failedToUpdateUserPassword: 'Une erreur est survenue lors de la mise à jour de votre mot de passe.',
    userPasswordUpdated: 'Votre mot de passe a été mis à jour.',
    wrongOldPassword: 'Il semblerait que l\'ancien mot de passe saisi soit incorrect.',
    errorDeletingAccount: 'Une erreur est survenue lors de la suppression de votre compte.',
    userInit: 'Une erreur est survenue lors de la récupération des informations de votre compte.',
  },
  general: {
    failedDownloadFile: 'Désolé, il n\'a pas été possible de télécharger le fichier. Essayez plus tard !',
    failedDownloadFileUnauthorized: 'Vous n\'avez pas les droits nécéssaires pour télécharger ce fichier.',
    failedDownloadFileUnauthenticated: 'Vous devez être connecté pour accéder à ce fichier.',
    urlToolLong: 'L\'url est trop longue, elle ne peut pas être utilisée.',
  },
  userPasswordForgotten: {
    failed: 'Une erreur s\'est produite lors du lancement de la procédure de réinitialisation du mot de passe.',
    success: 'Un email vous a été envoyé.',
  },
  userPasswordReset: {
    failed: 'Une erreur s\'est produite lors de la réinitialisation de votre mot de passe.',
    success: 'Votre mot de passe a bien été réinitialisé.',
  },
  resources: {
    initializationError: 'Une erreur s\'est produite lors de la récupération des ressources.',
  },
  alreadyAuthenticated: 'Vous ne pouvez pas accéder à cette page lorsque vous êtes authentifié.',
  contact: {
    error: 'Désolé, nous n\'avons pas pu envoyer votre demande, veuillez réessayer ultérieurement',
    success: 'Votre message a bien été envoyé',
  },
  feedback: {
    success: 'Merci de nous avoir fait part de vos retours!',
    error: 'Oups, une erreur en survenu pendant l\'envoie de vos retours.',
  },
};

export const geosource = {
  accessRestriction: {
    open: 'Accès ouvert',
    restricted: 'Accès sous condition',
  },
  updateFrequencies: {
    weekly: 'Mises à jour hebdomadaires',
    biannually: 'Mises à jour bi-annuelles',
    quarterly: 'Mises à jour trimestrielles',
    irregular: 'Mises à jour irrégulières',
    monthly: 'Mises à jour mensuelles',
    daily: 'Mises à jour journalières',
    annually: 'Mises à jour annuelles',
    asneeded: 'Mises à jour si besoin',
    continual: 'Mises à jour continues',
    notplanned: 'Mises à jour non plannifiées',
    unknown: 'Fréquence de mise à jour non connue',
    fortnightly: 'Mises à jour bi-mensuelles',
    other: 'Fréquence de mise à jour non connue',
  },
  metadataCategories: {
    accessibilite: 'Accessibilité',
    localisation: 'Localisation',
    occupationdusol: 'Occupation du sol',
    environnement: 'Environnement',
    equipements: 'Équipements',
    urbanisme: 'Urbanisme',
    culture: 'Culture',
    services: 'Services',
    imagerie: 'Imagerie',
    economie: 'Économie',
    transport: 'Transport',
    citoyennete: 'Citoyenneté',
    limitesadministratives: 'Limites administratives',
  },
  datasetTypes: {
    dataset: 'Données géographiques',
    nonGeographicDataset: 'Données non géographiques',
    serviceSingular: 'Service',
    service: 'Services',
    series: 'Collection de données géographiques',
    nonGeographicSeries: 'Collection de données non géographiques',
  },
  datasetData: {
    maxDataDisplayedMessage: 'The maximum number of data displayed is set to ',
  },
  datasetServices: {
    wmsTitle: 'WMS',
    wmsSubtitle: 'WAREHOUSE MANAGEMENT SYSTEM',
    wmsDescription: 'Le service de visuation géographique par excellence',
    wfsTitle: 'WFS',
    wfsSubtitle: 'WORLDWIDE FLIGHT SERVICES',
    wfsDescription: 'Interrogez des serveurs cartographiques afin de manipuler des objets géographiques',
    pdfTitle: 'PDF',
    pdfSubtitle: 'PORTABLE DOCUMENT FORMAT',
    jsonTitle: 'JSON',
    jsonSubtitle: 'JAVASCRIPT OBJECT NOTATION',
  },

  filterCategories: {
    categories: 'Thématiques',
    keywords: 'Mots clés',
    dataProducers: 'Organismes',
    licences: 'Licences',
    formats: 'Formats de données',
    services: 'Services web',
    publicationYear: 'Année de publication',
    publicationDate: 'Date de publication',
    type: 'Types de données',
    granularity:'Granularité territoriale',
    updateFrequency: 'Fréquence de mise à jour',
    territory: 'Territoire', 
  },
  button: {
    more: '+ Voir plus',
    less: '- Voir moins',
  },
  sortOptionLabels: {
    relevance: 'Pertinence',
    increasingDate: 'Date (croissante)',
    decreasingDate: 'Date (décroissante)',
    alphabetical: 'Titre (A &rarr; Z)',
    counterAlphabetical: 'Titre (Z &rarr; A)',
  },
  researchScope: {
    all: 'Tous',
    datasets: 'Données',
    series: 'Séries de données',
    services: '',
    articles: 'Actualités',
    pages: 'Pages',
  },
  placeholders: {
    all: 'ex: cadastre, randonnée, bibliothèque',
    datasets: 'ex: piscine',
    series: 'ex: ortho',
    services: 'ex: calculateur',
    posts: 'ex: traboules',
    address: 'Entrer une adresse',
  },
  errorItem: {
    all: 'document',
    datasets: 'donnée',
    series: 'série',
    services: 'service',
    posts: 'article',
    pages: 'page',
  },
  mapMessages: {
    copied: 'Copié !',
    share: 'Copier le lien',
    shareFor: 'Copier le lien pour',
  },
  downloads: {
    abort: 'Annuler le téléchargement en cours',
    download: 'Télécharger le fichier',
  },
  licence: {
    other: 'Autre licence',
  },
  filter: {
    other: 'Autres',
  },
};


export const metaDescription = {
  home: "La plateforme des données des acteurs du territoire de {plateformeName}. Elle permet la recherche et l'utilisation des données ouvertes (open data).",
  research: 'La plateforme {plateformeName} offre un accès à plusieurs centaines de jeux de données. Ils peuvent être recherchés par mots-clés, par filtres, et triés suivant différents critères.',
  dataset: "Des métadonnées décrivent la donnée : contexte et origine, producteur, thématique, statistiques, conditions d'accès (API).",
  news: '{plateformeName} vous informe en continu sur les données ouvertes (open data) sur le territoire et les services offerts par la plateforme',
};
export const introText = {
  research: "La plateforme {plateformeName} offre un accès à plusieurs centaines de jeux de données. Recherchez-les grâce à la recherche par mot-clé et par filtres : partenaire producteur de la donnée, thématique (environnement, transport…), format… Et explorez-les en accédant à leur page détaillée : cartographie, tableau textuel des données, description, lien de téléchargement et accès par API.",
  news: "{plateformeName} vous informe des dernières actualités de sa plateforme de données : ouverture de nouveaux jeux de données disponibles pour le territoire, exemples d'utilisation, dossiers thématiques en lien avec les jeux de données publiés, focus sur les partenaires, événements, nouvelles fonctionnalités de la plateforme..."
};

export const subjects = [
  {
    key: 'licenseQuestion',
    value: 'question sur les licences',
  },
  {
    key: 'technicalQuestion',
    value: 'question technique',
  },
  {
    key: 'anomalyReport',
    value: 'signalement d\'anomalie',
  },
  {
    key: 'contactRequest',
    value: 'demande de contact',
  },
  {
    key: 'infoRequest',
    value: 'demande d\'information',
  },
  {
    key: 'newDataRequest',
    value: 'demande de nouvelle donnée',
  },
  {
    key: 'other',
    value: 'autre',
  },
];

export const pageTitles = {
  home: 'Accueil',
  siteMap: 'Plan du site',
  datasets: 'Jeux de données',
  credits: 'Crédits',
  partners: 'Partenaires',
  changelog: 'Dernières évolutions',
  actors: 'Les acteurs',
  signup: 'Inscription',
  login: 'Connexion',
  passwordReset: 'Réinitialisation du mot de passe',
  passwordForgotten: 'Mot de passe oublié',
  contact: 'Contactez nous',
  accessManagement: 'Mes accès aux données',
  userServices: 'Mes demandes',
  availableData: 'Autres données disponibles',
  userProfil: 'Mon profil',
  userInfo: 'Mes informations',
  userPasswordUpdate: 'Modifier mon mot de passe',
  contribution: 'Publier un jeu de données',
  reuses: 'Réutilisations',
  news:'Actualités',
};

export const datatsetDataRepresentationType = {
  long: {
    areal: 'Représentation surfacique',
    linear: 'Représentation linéaire',
    punctual: 'Représentation ponctuelle',
  },
  short: {
    areal: 'Surfacique',
    linear: 'Linéaire',
    punctual: 'Ponctuel',
  },
};

export const feedback = {
  placeholder: 'Partagez vos remarques',
};

export const contactTrad = {
  subjectPlaceholder: 'Selectionnez un sujet',
  reuseSubject: 'Soumettre une réutilisation',
  contributionSubject: 'Contribution',
};

export const cmsPageLink = {
  providers: {
    title: 'Voir les partenaires',
    imgAlt: 'Picto partenaires',
  },
  contact: {
    title: 'Nous contacter',
    imgAlt: 'Picto nous contacter',
  },
  documentation: {
    title: 'Voir la documentation',
    imgAlt: 'Picto documentation',
  },
  approach: {
    title: 'Voir la démarche',
    imgAlt: 'Picto la démarche',
  },
};

export const reusesTypes = {
  app: 'Application',
  web: 'Site web',
  article: 'Article',
  news: 'Actualité',
};

export const buttonCopyLinkToCliboardMessages = {
  hover: 'Copier le lien',
  copied: 'Copié !',
};

export const datasetStatistics = {
  request: 'requête',
  requests: 'requêtes',
  otherServices: 'Autres',
};
