
export const userServicesStatuses = {
  pending: 'Pending',
  opened: 'Ok',
  expired: 'Expired',
  other: '-',
};

export const notificationMessages = {
  edito: {
    getPage: 'Failed to get the page',
    getCategories: 'Failed to get categories',
    getPosts: 'Failed to get the list of posts',
    getPost: 'Failed to get the post',
    getCategoriesAndPosts: 'Failed to get posts list with categories',
    getCategoriesAndPost: 'Failed to get the post and the categories',
    getHomeVideoPost: 'Failed to load home video post',
    getMediaUrl: 'Failed to get media url',
    getDataProducers: 'Failed to get the list of patners.',
    getCredits: 'Failed to get the list of credits.',
    getReuses: 'Failed to get the list of reuses.',
    news: 'news',
    getDatasetRelatedPosts: 'Failed to get the news related to this dataset.',
  },
  geosource: {
    getResults: 'Failed to get the list of datasets',
    getDatasetById: 'Failed to get the dataset',
    getAutocomplete: 'Failed to get autocomplete results',
    getSuggestion: 'Failed to get suggestion result',
    mapError: 'Something went wrong with the map',
    getDatasetChildren: 'Failed to get the dataset children',
    getDatasetParentInfo: 'Failed to get the datatset parent info',
    getDatasetData: 'Failed to get the dataset data',
    getAggregationsList: 'Failed to get aggregations',
    getDataFromCoordinates: 'No data at these coordinates',
  },
  matomo: {
    getViews: 'Cannot get the views count',
  },
  signup: {
    // tslint:disable-next-line:max-line-length
    accountCreated: 'The account creation process has been been completed. You will receive an email in order to activate it.',
    accountCreationFailed: 'Sorry, we couldn\'t create your account',
    existingAccount: 'An account already exists for that email.',
    uncompleteForm: 'Incomplete form.',
  },
  userServices: {
    addSuccess: 'You requested access to ',
    addFailed: 'An error occured while requesting access to the services.',
    addFailedDetailed: 'An error occured while requesting access to ',
    failedToLoadUserResources: 'An error occured while loading your data.',
    initializationError: 'An error occured while initializing the page.',
    removeFailed: 'An error occured while deleting your access.',
    removeFailedDetailed: 'An error occured while deleting your access to ',
    removeSuccess: 'We successfully removed your access to ',
    renewSuccess: 'We successfully received your request for renewal of access for ',
    renewFailed: 'An error occured during the request for renewal of access.',
    renewFailedDetailed: {
      partOne: 'We could not renew your access to ',
      partTwo: '. Your access must have been validated before you can renew it.',
    },
  },
  userInfo: {
    failedToLoadUserInfo: 'An error occured while loading your information.',
    failedToUpdateUserInfo: 'An error occured while updating your information.',
    userInfoUpdated: 'Your information have been saved.',
    userPasswordUpdated: 'Your password has been updated.',
    failedToUpdateUserPassword: 'An error occured while updating your password.',
    wrongOldPassword: 'Looks like the old password entered is incorrect.',
    errorDeletingAccount: 'An error occured while deleting your account.',
    userInit: 'An error occured while retrieving your account information.',
  },
  general: {
    failedDownloadFile: 'Sorry, it was not possible to download the file. Try later !',
    failedDownloadFileUnauthorized: 'You don\'t have the necessary rights to download this file.',
    failedDownloadFileUnauthenticated: 'You need to be authenticated to download this file.',
    urlToolLong: 'The url is too long, it can not be used.',
  },
  userPasswordForgotten: {
    failed: 'An error occured while initiating the password reset procedure',
    success: 'An email has been sent to you.',
  },
  userPasswordReset: {
    failed: 'An error occured while resetting your password.',
    success: 'Your password has been successfully changed.',
  },
  resources: {
    initializationError: 'Could not get the resources.',
  },
  alreadyAuthenticated: 'You can\'t access this page while you are authenticated.',
  contact: {
    error: 'Sorry, we couldn\'t send your message, please try again later',
    success: 'Your message has been sent successfully.',
  },
  feedback: {
    success: 'Thank you for your feedback!',
    error: 'Oops, an error occured while sending your feedback.',
  },
};

export const geosource = {
  accessRestriction: {
    open: 'Open access',
    restricted: 'Conditional access',
  },
  updateFrequencies: {
    weekly: 'Weekly updates',
    biannually: 'Bi-annual updates',
    quarterly: 'Quarterly updates',
    irregular: 'Irregular updates',
    monthly: 'Monthly updates',
    daily: 'Daily updates',
    annually: 'Annual updates',
    asneeded: 'Updated if needed',
    continual: 'Continuous updates',
    notplanned: 'Unplanned updates',
    fortnightly: 'Bi-Monthly updates',
    unknown: 'Unknown frequency update',
    other: 'Unknown frequency update',
  },
  metadataCategories: {
    localisation: 'Localisation',
    accessibilite: 'Accessibility',
    occupationdusol: 'Land use',
    environnement: 'Environment',
    equipements: 'Equipments',
    urbanisme: 'Urbanism',
    culture: 'Culture',
    services: 'Services',
    imagerie: 'Imagery',
    economie: 'Economy',
    transport: 'Transport',
    citoyennete: 'Citizenship',
    limitesadministratives: 'Administrative limits',
  },
  datasetTypes: {
    dataset: 'Geographical data',
    nonGeographicDataset: 'Non geographical data',
    serviceSingular: 'Service',
    service: 'Services',
    series: 'Geographical data series',
    nonGeographicSeries: 'Non geographical data series',

  },
  datasetData: {
    maxDataDisplayedMessage: 'The maximum number of data displayed is set to ',
  },
  datasetServices: {
    wmsTitle: 'WMS',
    wmsSubtitle: 'WEB MAP SERVICE',
    wmsDescription: 'Le service de visuation géographique par excellence',
    wfsTitle: 'WFS',
    wfsSubtitle: 'WEB FEATURE SERVICE',
    wfsDescription: 'Interrogez des serveurs cartographiques afin de manipuler des objets géographiques',
    pdfTitle: 'PDF',
    pdfSubtitle: 'PORTABLE DOCUMENT FORMAT',
    jsonTitle: 'JSON',
    jsonSubtitle: 'JAVASCRIPT OBJECT NOTATION',
  },
  filterCategories: {
    categories: 'Themes',
    keywords: 'Keywords',
    dataProducers: 'Partners',
    licences: 'Licences',
    formats: 'Formats',
    services: 'Services',
    publicationYear: 'Publication year',
    publicationDate: 'Publication date',
    type: 'Types',
    granularity:'Coverage granularity',
    updateFrequency: 'Update frequency',
    territory: 'Territory', 
  },
  button: {
    more: '+ See more',
    less: '- See less',
  },
  sortOptionLabels: {
    relevance: 'Relevance',
    increasingDate: 'Date (ascending)',
    decreasingDate: 'Date (descending)',
    alphabetical: 'Title (A &rarr; Z)',
    counterAlphabetical: 'Title (Z &rarr; A)',
  },
  researchScope: {
    all: 'All',
    datasets: 'Datasets',
    series: 'Data series',
    services: '',
    articles: 'News',
    pages: 'Pages',
  },
  placeholders: {
    all: 'e.g. cadastre, randonnée, bibliothèque',
    datasets: 'e.g. piscine',
    series: 'e.g. ortho',
    services: 'e.g. calculateur',
    posts: 'e.g. traboules',
    address: 'Enter an address',
  },
  errorItem: {
    all: 'document',
    datasets: 'data',
    series: 'serie',
    services: 'service',
    articles: 'Articles',
    posts: 'post',
    pages: 'page',
  },
  mapMessages: {
    copied: 'Copied !',
    share: 'Copy the link',
    shareFor: 'Copy the link for',
  },
  downloads: {
    abort: 'Cancel the download in progress',
    download: 'Download the file',
  },
  licence: {
    other: 'Other license',
  },
  filter: {
    other: 'Other',
  },
};


export const metaDescription = {
  home: 'The data platform of the partners of {plateformeName}. It allows the search and use of open data.',
  research: 'The {plateformeName} platform offers access to several hundred datasets. They can be searched by keywords, by filters, and sorted according to different criteria.',
  dataset: 'Metadata describe the data: context and origin, producer, subject, statistics, access conditions (API).',
  news: '{plateformeName} informs you continuously about the open data on the territory and the services offered by the platform',
};
export const introText = {
  research: "The {plateformeName} platform offers access to several hundred data sets. Search for them using keywords and filters: data producing partner, theme (environment, transport, etc.), format, etc. And explore them by accessing their detailed page: cartography, textual table of data, description, download link and API access.",
  news: "{plateformeName} informs you about the latest news of its data platform: opening of new datasets available for the territory, examples of use, thematic files related to the published datasets, focus on the data partners, events, new features of the platform..."
};

export const subjects = [
  {
    key: 'licenseQuestion',
    value: 'question about licences',
  },
  {
    key: 'technicalQuestion',
    value: 'technical question',
  },
  {
    key: 'anomalyReport',
    value: 'anomaly report',
  },
  {
    key: 'contactRequest',
    value: 'request contact',
  },
  {
    key: 'infoRequest',
    value: 'information request',
  },
  {
    key: 'newDataRequest',
    value: 'request for new data',
  },
  {
    key: 'other',
    value: 'other',
  },
];

export const pageTitles = {
  home: 'home',
  siteMap: 'Site map',
  datasets: 'Data sets',
  credits: 'Credits',
  partners: 'Partners',
  news: 'News',
  changelog: 'Last changes',
  actors: 'Actors',
  signup: 'Sign up',
  login: 'Log in',
  passwordReset: 'Password reset',
  passwordForgotten: 'Password forgotten',
  contact: 'Contact us',
  accessManagement: 'Data accesses',
  userServices: 'My requests',
  availableData: 'Other available data',
  userProfil: 'My profile',
  userInfo: 'My information',
  userPasswordUpdate: 'Change my password',
  contribution: 'Contribution',
  reuses: 'Reuses',
};

export const datatsetDataRepresentationType = {
  long: {
    areal: 'Areal representation',
    linear: 'Linear representation',
    punctual: 'Punctual representation',
  },
  short: {
    areal: 'Areal',
    linear: 'Linear',
    punctual: 'Punctual',
  },
};

export const feedback = {
  placeholder: 'Share your opinion',
};

export const contactTrad = {
  subjectPlaceholder: 'Select a subject',
  reuseSubject: 'Submit a reuse',
  contributionSubject: 'Contribution',
};

export const cmsPageLink = {
  providers: {
    title: 'See the partners',
    imgAlt: 'Picto partners',
  },
  contact: {
    title: 'Contact us',
    imgAlt: 'Picto contact us',
  },
  documentation: {
    title: 'See the documentation',
    imgAlt: 'Picto documentation',
  },
  approach: {
    title: 'See the approach',
    imgAlt: 'Picto approach',
  },
};

export const reusesTypes = {
  app: 'App',
  web: 'Website',
  article: 'Article',
  news: 'News',
};

export const buttonCopyLinkToCliboardMessages = {
  hover: 'Copy the link',
  copied: 'Copied!',
};

export const datasetStatistics = {
  request: 'request',
  requests: 'requests',
  otherServices: 'Others',
};
