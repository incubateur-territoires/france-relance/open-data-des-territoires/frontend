import * as packageJson from '../../package.json';
import { AppRoutes } from '../app/routes';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
  version: (<any>packageJson).version,

  production: true,
  passwordMinLength: 6,
  enableEsFrontCache: false,

  // Path to the built app in a particular language
  angularAppHost: {
    fr: '/portail/fr',
    en: '/portail/en',
  },

  // CMS
  cmsStaticPages: {},
  cmsContent: {
    categoryUniqueContent: 'static',
    slugPostVideo: 'video-dgl',
  },

  // Cookies
  cguAcceptedCookieName: 'CGU_ACCEPTED',

};

// Use variable route names (AppRoutes.credits...) for each static page to attribute the corresponding
// cms page in order to make sure that if the name of the route change in the config file (routes.ts),
// the cms page will still be served.
environment.cmsStaticPages[AppRoutes.accessibility.uri] = 'accessibilite';
environment.cmsStaticPages[AppRoutes.approach.uri] = 'la-demarche';
environment.cmsStaticPages[AppRoutes.documentation.uri] = 'utiliser-la-plateforme-data-du-grand-lyon';
environment.cmsStaticPages[AppRoutes.legalNotices.uri] = 'mentions-legales';
environment.cmsStaticPages[AppRoutes.personalData.uri] = 'donnees-personnelles';
environment.cmsStaticPages[AppRoutes.credits.uri] = 'credits';
environment.cmsStaticPages[AppRoutes.cgu.uri] = 'conditions-generales-dutilisation';
environment.cmsStaticPages[AppRoutes.beginners.uri] = 'le-coin-des-debutants';
