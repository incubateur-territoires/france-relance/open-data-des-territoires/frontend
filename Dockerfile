# Stage 0, based on Node.js, to build and compile Angular
FROM node:14 as builder

RUN npm install -g npm@latest
WORKDIR /app

# Copy the package.json file first in order to cache the modules
COPY ./package.json .
COPY ./package-lock.json .
COPY ./patch.js .

# Install npm dependencies
RUN npm install

# Launch postinstall script (to include crypto module)
RUN npm run postinstall

# Copy the project
COPY ./angular.json .
COPY ./tsconfig.json .
COPY ./src ./src

# Building the Angular app /dist i18n
RUN npm run build-i18n:prod


# Stage 1, based on Nginx, to have only the compiled app
FROM nginx

## Install dependency to get lua module in .conf
RUN apt-get update

RUN apt-get --assume-yes install nginx-extras

ARG DEST_DIR=/var/www/html
RUN rm /etc/nginx/conf.d/*
COPY default.conf /etc/nginx/sites-available/default

COPY --from=builder /app/dist/fr $DEST_DIR/fr
COPY src/assets/config/config-preprod.json $DEST_DIR/fr/assets/config/config.json
COPY --from=builder /app/dist/en $DEST_DIR/en
COPY src/assets/config/config-preprod.json $DEST_DIR/en/assets/config/config.json

